'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:SeatsAssignCtrl
 * @description
 * # SeatsAssignCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')

.controller('SeatCreateCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // ---- ---- ---- ----
    $scope.seatingClassList = [];
    $scope.seats = {};

    $scope.seatRow = [];
    $scope.seatColumn = [];

    // Get List
    HelperService.getListActive('seating_class').then(function(response)    {
        $scope.seatingClassList = response;
    });

    // Process Methods
    // ---- ---- ---- ----
    $scope.getSeatList = function() {
        RESTFactory.get('seat').then(function(response) {
            var seatAll = response.data;

            $scope.seats = {};
            for (var index in seatAll)  {
                if (seatAll[index].seating_class == $scope.seatingClass && seatAll[index].active == 1)    {
                    if (_.isEmpty($scope.seats[seatAll[index].x]))
                        $scope.seats[seatAll[index].x] = {};

                    $scope.seats[seatAll[index].x][seatAll[index].y] = {
                        id: seatAll[index].id,
                        name: seatAll[index].name,
                        seating_class: seatAll[index].seating_class,
                        x: seatAll[index].x,
                        y: seatAll[index].y,
                        active: seatAll[index].active,
                    }
                }
            }

        });
    };

    $scope.getIndex = function(x,y) {
        $scope.rowIndex = x;
        $scope.colIndex = y;
    };

    $scope.getSeatingTable = function()    {
        // Reset Value
        $scope.seatRow = [];
        $scope.seatColumn = [];

        // Row
        for (var i=0; i < $scope.rows; i++)
            
            $scope.seatRow.push(i);

        // Column
        for (var i=0; i < $scope.cols; i++)
            $scope.seatColumn.push(i);
    };

    $scope.getTableDimensions = function(id)   {
        for (var index in $scope.seatingClassList) {
            if ($scope.seatingClassList[index].id == id)   {
                $scope.rows = $scope.seatingClassList[index].rows;
                $scope.cols = $scope.seatingClassList[index].cols;
            }
        }
    };

    $scope.clear = function()   {
        $scope.seats = {};
    };

    $scope.check = function()   {
        console.log($scope.seats)
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.saveChanges = function() {

        // Map params by $scope.seats key reference
        var params = {};

        try {
            for (var x in $scope.seats) {

                for (var y in $scope.seats[x])    {

                    // Determine if POST or PUT Request
                    if ('id' in $scope.seats[x][y]) { // Id exist, update
                        if (/\S/.test($scope.seats[x][y].name))   { // Detect if only whitespace
                            params = $.param($scope.seats[x][y]);
                            RESTFactory.put('seat/' + $scope.seats[x][y].id, params).then(function(response)  {
                                // Response if success

                            }, function()   {
                                // Response if fail
                            });
                        } else  {
                            RESTFactory.delete('seat/' + $scope.seats[x][y].id); // Deactivate old seat
                        }
                    } else { // No id found. Create new seat
                        if (/\S/.test($scope.seats[x][y].name))   { // Create if not blank
                            params = $.param({
                                'seating_class': $scope.seatingClass,
                                'name': $scope.seats[x][y].name,
                                'x': x,
                                'y': y,
                            });
                            RESTFactory.post('seat', params);
                        }
                    }
                }
            }
            toaster.pop("success", "Success", "Seats successfully updated");

        } catch (err)   {
            toaster.pop("error", "Error", "Seats addition failed. Error: " + err);
        }
        
    };

});
