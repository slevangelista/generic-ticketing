<?php

function JSONResponse($status, $response = array()){
    $app = \Slim\Slim::getInstance();
    $app->status($status);
    $app->contentType('application/json');
    echo json_encode($response);
}

function generateBarcode($status, $drawing){
    $app = \Slim\Slim::getInstance();
    $app->status($status);
    $app->contentType('image/png');
    $drawing->finish(BCGDrawing::IMG_FORMAT_PNG, 'lala.png');
}

function getNumberPadding($name, $digits = 6) {
    global $slim;

    // Get Counter
    $counter = $slim->db->read('counter', array(
        'where' => 'name = :name',
        'params' => array(
            ':name' => $name,
        )
    ));

    // Increment value by 1 in database
    $slim->db->updateByPk('counter', $counter['id'], array(
        'values' => array(
            'value' => ':val'
        ),
        'params' => array(
            ':val' => $counter['value']+1
        )
    ));

    return $counter['code'] . str_pad($counter['value']+1, $digits, 0, STR_PAD_LEFT);
}

function getNumberPaddingPerDigit($name, $number, $digits = 6) {
    global $slim;

    // Get Counter
    $counter = $slim->db->read('counter', array(
        'where' => 'name = :name',
        'params' => array(
            ':name' => $name,
        )
    ));

    return $counter['code'] . str_pad($number+1, $digits, 0, STR_PAD_LEFT);
}

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields,$request_params) {
    $error = false;
    $error_fields = "";
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["status"] = "error";
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        JSONResponse(200, $response);
        $app->stop();
    }
}

function joinList($id, $arrayList)  {
    foreach ($arrayList as $key=>$value)    {
        if ($id == $value['id'])    {
            $output = $value;
            break;
        }
    }

    return $output;
}

/* cURL Get Function */
function getCURL($url, $route)  {
    //  cURL Initializations
    $header = array(
        'Accept: application/json',
        'Authorization: Basic '
    );

    // Start cURL 
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url . $route);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPGET, true);

    $output = curl_exec($curl);
    curl_close($curl);

    return $output;
}

function postCURL($url, $route, $params)    {
    // Start cURL 
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url . $route);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, count($params));
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));

    $output = curl_exec($curl);

    curl_close($curl);
    return $output;
}

function getDateRange($start_date, $end_date) 
{
    $date_interval = (strtotime ( $end_date ) - strtotime ( $start_date )) / 86400; // 1 day = 86400 seconds
    $date = array ();
    
    for($i = 0; $i <= $date_interval; $i ++) {
        $date [$i] ['day'] = date ( 'D', strtotime ( $start_date . ' + ' . $i . ' days' ) );
        $date [$i] ['month'] = date ( 'M', strtotime ( $start_date . ' + ' . $i . ' days' ) );
        $date [$i] ['date'] = date ( 'j', strtotime ( $start_date . ' + ' . $i . ' days' ) );
        $date [$i] ['year'] = date ( 'Y', strtotime ( $start_date . ' + ' . $i . ' days' ) );
        $date [$i] ['date_map'] = date ( 'Y', strtotime ( $start_date . ' + ' . $i . ' days' ) ) . '-' . date ( 'm', strtotime ( $start_date . ' + ' . $i . ' days' ) ) . '-' . date ( 'd', strtotime ( $start_date . ' + ' . $i . ' days' ) );
    }
    
    return $date;
}

