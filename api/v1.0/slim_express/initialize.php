<?php

// initialize.php
// Written By: Syd Evangelista
// Initialize SlimExpress runtime here

// Require Files (Must be on this order)

// Composer Vendor file
require_once "../vendor/autoload.php";

// DOMPDF Settings
define('DOMPDF_ENABLE_AUTOLOAD', false);
require_once '../vendor/dompdf/dompdf/dompdf_config.inc.php';

// Libraries
require_once "./slim_express/libs/SlimModel.php";
require_once "./slim_express/libs/SlimController.php";

// Config
require_once "./config/settings.php";

// Instantiate $slim instance of factory class `SlimController`
$slim = new SlimController();

$slim->db = new SlimModel($slimConfig['db']);
$slim->route = new \Slim\Slim($slimConfig['app']);
