<?php

$slim->route->get("/report/tellers_and_pursers/:voyage/:id", function($voyage, $user_id)	{

	// Init
	global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    /* Initialize */
    $table = array();
    $total_fare = 0;
    $total_differential = 0;
    $collectibles = 0;

    /* Get Waybill */
    $blacklist = "6,7";
    $sql = "
    	SELECT
    		waybill.id as waybill_id,
    		cargo.id as cargo_id,
    		cargo.shipper,
    		cargo.plate_num,
    		cargo.address,
		    waybill.series_no,
            cargo.on_account,
            cargo_class.name as cargo_name,
    		waybill.price_paid,
            waybill.original_price,
    		waybill.lading_no
    	FROM waybill
    	LEFT JOIN cargo ON waybill.cargo = cargo.id
        LEFT JOIN cargo_class ON waybill.cargo_class = cargo_class.id
    	WHERE voyage = :voyage AND waybill.created_by = :user_id AND status NOT IN ({$blacklist})
    ";

    // Execute SQL
    $waybill = $slim->db->SQL($sql, array(
    	':voyage' => $voyage,
	    ':user_id' => $user_id,
    ));

    /* Get Baggage */
    $baggage = $slim->db->readAll('baggage', array(
        'where' => 'voyage = :voyage AND created_by = :user_id AND status NOT IN (6,7)',
        'params' => array(
            ':voyage' => $voyage,
            ':user_id' => $user_id
        )
    ));

    /* Loop Waybill */
    $i = 0;
    foreach($waybill as $key => $value) {
        $total_fare = $total_fare + (int) $value['original_price'];
        $total_differential += (int) $value['original_price'] - $value['price_paid'];

        /* Determine if collectibles */
        if ($value['on_account'])
            $collectibles += (int) $value['original_price'];

        $table[$i]['waybill_id'] = $value['waybill_id'];
        $table[$i]['series_no'] = $value['series_no'];
        $table[$i]['lading_no'] = $value['lading_no'];
        $table[$i]['shipper'] = $value['shipper'];
        $table[$i]['plate_num'] = $value['plate_num'];
        $table[$i]['cargo_name'] = $value['cargo_name'];
        $table[$i]['price_paid'] = $value['original_price'];
        $table[$i]['discounted_price'] = $value['price_paid'];
        $table[$i]['differential'] = $value['original_price'] - $value['price_paid'];
        $table[$i]['on_account'] = $value['on_account'];
        $i++;
    }

    /* Loop Baggage */
    foreach ($baggage as $key => $value)	{
    	$total_fare = $total_fare + (int)$value['price_paid'];

        $table[$i]['waybill_id'] = NULL;
        $table[$i]['series_no'] = $value['series_no'];
        $table[$i]['price_paid'] = $value['price_paid'];
        $table[$i]['discounted_price'] = $value['price_paid'];

        /* Waybill Details (Displayed as null) */
        $table[$i]['lading_no'] = null;
        $table[$i]['shipper'] = 0;
        $table[$i]['plate_num'] = null;
        $table[$i]['cargo_name'] = null;
        $table[$i]['differential'] = null;
        $table[$i]['on_account'] = null;
        $i++;
    }

    /* Summarize Revenues */
    $total_fare_less_collectibles = ($total_fare - $total_differential) - $collectibles;

    // Pass Data
    $resp['data']['table'] = $table;
    $resp['data']['total_fare'] = $total_fare;
    $resp['data']['total_fare_less_differential'] = $total_fare - $total_differential;
    $resp['data']['total_fare_less_collectibles'] = $total_fare_less_collectibles;

    // echo "<pre>" . print_r($table, 1) . "</pre>";
    JSONResponse($status, $resp);

});
