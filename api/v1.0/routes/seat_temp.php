<?php

$slim->route->get('/seat_temp/request/voyage/:voyage_id', function($voyage_id)   {
    global $slim;
    
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = $slim->db->readAll("seat_temp", array(
        'where' => 'voyage = :voyage AND active = 1',
        'order' => 'seat_name ASC',
        'params' => array(
            ':voyage' => $voyage_id
        )
    ));

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);
});

$slim->route->get('/seat_temp/request/voyage/:voyage_id/seating_class/:seating_class_id', function($voyage_id, $seating_class_id)   {
    global $slim;
    
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = $slim->db->readAll("seat_temp", array(
        'where' => 'voyage = :voyage AND class = :seating_class AND active = 1 AND status = 14',
        'order' => 'seat_name ASC',
        'params' => array(
            ':voyage' => $voyage_id,
            ':seating_class' => $seating_class_id
        )
    ));

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);
});

$slim->route->get('/seat_temp/request/voyage/:voyage_id/seat/:seat_id', function($voyage_id, $seat_id)   {
    global $slim;    
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        $seat_temp = $slim->db->read("seat_temp", array(
            'where' => 'voyage = :voyage AND seat = :seat AND active = 1',
            'order' => 'seat ASC',
            'params' => array(
                ':voyage' => $voyage_id,
                ':seat' => $seat_id,
            )
        ));

        if ($seat_temp)
            $resp['data'] = $seat_temp;
        else
            throw new Exception("No seat temp found for voyage {$voyage_id} and seat {$seat_id}");


    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Passenger fare request failed. Error: " . $e->getMessage();
    }

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);
});

$slim->route->delete('/seat_temp/request/voyage/:voyage_id', function($voyage_id)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        $slim->db->delete('seat_temp', array(
            'where' => 'voyage = :voyage',
            'params' => array(
                ':voyage' => $voyage_id
            )
        ));
    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Passenger fare request failed. Error: " . $e->getMessage();
    }

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);

});