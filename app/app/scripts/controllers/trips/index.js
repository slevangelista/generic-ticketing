'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:tripIndexCtrl
 * @description
 * # tripIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('TripIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.tripItems = {};
    $scope.newTrip = {
        'departure_time': new Date(),
        'arrival_time': new Date(),
    };

    // Generate List
    HelperService.getListActive('route').then(function(response)    {
        $scope.routeList = response;
    });

    // Pagination
    $scope.tripCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getTripByPage();
        $scope.getTripByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.tripItems[data.id] = {
            'id': data.id,
            'name': data.name,
            'route_id': data.route_id,
            // Dates are static, they are not important and do not represent any data transaction whatsoever
            // They are just added to comply to javascript date format
            'departure_time': new Date('January 01, 1990 ' + data.departure_time),
            'arrival_time': new Date('January 01, 1990 ' + data.arrival_time), 
            'active': data.active
        };
    };

    $scope.joinRoute = function(id) {
        return HelperService.join(id, $scope.routeList).name;
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    $scope.check = function()   {
        console.log($rootScope.trip);
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getTripByPage = function()    {
        RESTFactory.get('trip/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.tripList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getTripByCount = function()   {
        RESTFactory.get('trip/' + 'request/count').then(function(response) {
            $scope.tripCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addTrip = function() {
        // Set timepickers to time string
        $scope.newTrip.departure_time = HelperService.getTime($scope.newTrip.departure_time);
        $scope.newTrip.arrival_time = HelperService.getTime($scope.newTrip.arrival_time);

        var params = $.param($scope.newTrip);
        RESTFactory.post('trip', params).then(function(response)  {
            // Response if success
            $scope.newTrip = {
                'departure_time': new Date(),
                'arrival_time': new Date(),
            };
            $scope.update();
            toaster.pop("success", "Success", "Trip successfully added");
            
            $('#addModal').modal('hide');
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Trip failed to add");
        });
    };  

    $scope.editTrip = function()   {
        // Set timepickers to time string
        $scope.tripItems[$scope.currentIndex].departure_time = HelperService.getTime($scope.tripItems[$scope.currentIndex].departure_time);
        $scope.tripItems[$scope.currentIndex].arrival_time = HelperService.getTime($scope.tripItems[$scope.currentIndex].arrival_time);

        var params = $.param($scope.tripItems[$scope.currentIndex]);
        RESTFactory.put('trip/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Trip successfully edited");

            $('#editModal').modal('hide');
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Trip failed to edit");
        });
    };

});
