'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:cargoIndexCtrl
 * @description
 * # cargoIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('CargoOnAccountCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.cargoItems = {};

    // Pagination
    $scope.cargoCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Get List
    HelperService.getListActive('shipper').then(function(response)  {
        $scope.shipperList = response;
    });

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getCargoByPage();
        $scope.getCargoByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.cargoItems[data.id] = {
            'id': data.id,
            'plate_num': data.plate_num,
            'shipper': data.shipper,
            'address': data.address,
            'article_no': data.article_no,
            'article_desc': data.article_desc,
            'on_account': 1,
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // Join
    $scope.joinShipper = function(id) {
        return HelperService.join(id, $scope.shipperList).name;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getCargoByPage = function()    {
        RESTFactory.get('cargo/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.cargoList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getCargoByCount = function()   {
        RESTFactory.get('cargo/' + 'request/count').then(function(response) {
            $scope.cargoCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addCargo = function() {
        // Add on-account
        $scope.newCargo.on_account = 1;
        
        var params = $.param($scope.newCargo);
        RESTFactory.post('cargo', params).then(function(response)  {
            // Response if success
            $scope.newCargo = {};
            $scope.update();
            toaster.pop("success", "Success", "Cargo successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Cargo failed to add");
        });
    };  

    $scope.editCargo = function()   {
        var params = $.param($scope.cargoItems[$scope.currentIndex]);
        RESTFactory.put('cargo/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Cargo successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Cargo failed to edit");
        });
    };

});

