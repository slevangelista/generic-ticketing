<?php

// Get
$slim->route->get("/vessel_template/request/seating_class/:id", function($id)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = $slim->db->readAll('vessel_template', array(
        'where' => "seating_class_id = :id AND active = 1",
        'params' => array(
            ':id' => $id
        )
    ));

    JSONResponse($status, $resp);
});

$slim->route->get('/vessel_template/request/voyage/:id', function($id)    {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // First: Get vessel for this voyage
    $voyage = $slim->db->readByPk('voyage', $id);
    $vessel_id = $voyage['vessel'];

    // Second: Get vessel template
    $vessel_template = $slim->db->readAll('vessel_template', array(
        'where' => 'vessel_id = :vessel_id AND active = 1',
        'params' => array(
            ':vessel_id' => $vessel_id
        )
    ));

    /* Pass vessel_template value to response */
    $resp['data'] = $vessel_template;

    JSONResponse($status, $resp);
});

$slim->route->get("/vessel_template/request/vessel/:id", function($id)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = $slim->db->readAll('vessel_template', array(
        'where' => "vessel_id = :id AND active = 1",
        'params' => array(
            ':id' => $id
        )
    ));

    JSONResponse($status, $resp);
});

// Delete
$slim->route->delete("/vessel_template/request/vessel/:id", function($id)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    if ($id)   {
        $slim->db->update('vessel_template', array(
            'where' => 'vessel_id = :id',
            'values' => array(
                'active' => 0
            ),
            'params' => array(
                ':id' => $id
            )
        ));
    } 

    JSONResponse($status, $resp);
});