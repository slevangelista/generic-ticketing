'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:ReportsTrafficReportCtrl
 * @description
 * # ReportsTrafficReportCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('ReportsTrafficReportCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

    // Initializations
    // ---- ---- ---- ----
    $scope.input = {};

    // Get Lists

    $scope.open = function($event, option) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.open[option] = true;
    };

    // Vessel
    HelperService.getListActive('vessel').then(function(response)    {
        $scope.vesselAll = response;
        $scope.vesselAll.splice(0, 0, {id: 0, name: " -- Show All -- "});
    });
    // Route
    HelperService.getListActive('route').then(function(response)    {
        $scope.routeAll = response;
        $scope.routeAll.splice(0, 0, {id: 0, name: " -- Show All -- "});
    });

    // Process Methods
    // ---- ---- ---- ----
    $scope.getToday = function(option) {
        $scope.input[option] = new Date();
    };

    $scope.onDateChange = function(option)    {
        $scope.input[option] = HelperService.getDate($scope.input[option]);
    };

    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });

        saveAs(blob, "traffic_report.xls");
    };

    /* REST Method */
    $scope.getDailyMonitoring = function()	{
    	RESTFactory.get('report/traffic_report/' + $scope.input.start_date + '/' + $scope.input.end_date + '/' + $scope.input.vessel + '/' + $scope.input.route).then(function(response)	{
    		$scope.dailyMonitoring = response.data;

            /* Create an array list for voyages (child of date) */
            for (var index in $scope.dailyMonitoring.date)  {
                $scope.dailyMonitoring.date[index].voyage_array = [];
                for (var i in $scope.dailyMonitoring.date[index].voyage)    {
                    $scope.dailyMonitoring.date[index].voyage_array.push($scope.dailyMonitoring.date[index].voyage[i]);
                    // console.log($scope.dailyMonitoring.date[index].voyage[i]);
                }
            }

            // console.log($scope.dailyMonitoring);
    	}, function(response)	{
            $scope.dailyMonitoring = undefined;
    		toaster.pop("error", "Error", "No Voyage Found");
    	});
    };

});