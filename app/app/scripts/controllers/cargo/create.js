'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:CargoCreateCtrl
 * @description
 * # CargoCreateCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('CargoCreateCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

	/* Initializations */
	$scope.waybill = {};
	$scope.cargo = {};
	$scope.disable = {};
	$scope.waybillItems = {};

	$scope.advanced_options = false;
	$scope.options_display = {
		true: "Hide",
		false: "Show"
	};
	$scope.enable_print = true;
	$scope.new_shipper_plate = false;
	$scope.is_accountable = false;
	$scope.bool = {
		true: "Yes",
		false: "No"
	};
	$scope.boolArray = ['No', 'Yes'];
	$scope.shipper_list = [];
	$scope.existing_shipper_list = [];

	// Pagination
    $scope.waybillCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

	/* Watchers */
	$scope.$watch(function(scope)	{
		return $scope.shipper;
	}, function(newVal, oldVal)	{
		if (typeof newVal === 'object')	{
			$scope.getShipperPlates();
			$scope.cargo.shipper = $scope.shipper.id;
		} else	{
			$scope.shipper_plates = [];
			$scope.cargo = {};
			$scope.new_shipper_plate = false;
			$scope.disable = {};
		}
	});

	/* Output Methods */
	$scope.displayShipper = function()	{
		if (typeof $scope.shipper === 'object' && $scope.shipper)	{
			return $scope.shipper.name;
		}
		else
			return "No shipper selected";
	};

	$scope.displayExistingShipper = function()	{
		if (! $scope.existing_shipper_list.length)	{
			return "";
		} else {
			return "Shipper Name Already Exist!";
		}
	};

	/* Process Methods */
	$scope.toggleIsNewPlateNum = function()	{
		if (! $scope.new_shipper_plate)	{
			/* Set Cargo to blank */
			$scope.cargo = {
				shipper: $scope.shipper.id
			};
			$scope.disable.plate_list = true;
		} else	{
			$scope.disable.plate_list = false;
		}
	}

	$scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.init = function()	{
    	// Populate lists
    	getTableList(['cargo_fare_rates', 'cargo_class', 'route', 'trip']);
    	$scope.updateSeries();
    };

    $scope.update = function()	{
    	// Reset
    	$scope.waybill = {
    		voyage: $scope.waybill.voyage,
    		series_no: $scope.waybill.series_no,
    	};
		$scope.cargo = {};
		$scope.disable = {};
		$scope.waybillItems = {};

    	$scope.getRoute();
    	$scope.getCargoRateList();
    	$scope.getWaybillList();
    	$scope.getWaybillCount();
    };

    // Get Waybill Indexes
    $scope.getItems = function(data)    {
        $scope.waybillItems[data.id] = {
            'id': data.id,
            'lading_no': data.lading_no,
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.trip_list).departure_time;
    };

    $scope.joinCargoClass = function(id)   {
        return HelperService.join(id, $scope.cargo_class_list).name;
    };

    $scope.joinTripByRoute = function(id)   {
        return HelperService.join(id, $scope.trip_list).route_id;
    };

    // Get Route
    $scope.getRoute = function()    {
        // Loop through voyage
        for (var index in $scope.voyageList)    {
            if ($scope.voyageList[index].id == $scope.waybill.voyage)   {
                $scope.route = $scope.joinTripByRoute($scope.voyageList[index].trip);
            }
        }
    };

    $scope.getCargoRateList = function()    {
        var cargo_rate_all = $scope.cargo_fare_rates_list;
        $scope.cargo_fare_rates_list = [];

        for (var index in cargo_rate_all)    {
            if ($scope.route == cargo_rate_all[index].route) {
                $scope.cargo_fare_rates_list.push(cargo_rate_all[index]);
            }
        }
    };

    $scope.setAccountable = function()	{
 		if ($scope.cargo.on_account == 1)	{
 			$scope.cargo.on_account = true;
 		} else if ($scope.cargo.on_account == 0)	{
 			$scope.cargo.on_account = false;
 		}
    };

    $scope.setRate = function(classID, rate, original_rate, discount) {
        $scope.waybill.cargo_class = classID;
        $scope.waybill.price_paid = rate;
        $scope.waybill.original_price = original_rate;
        $scope.waybill.discount = discount;
    };

    // Voyage Display
    $scope.onVoyageSelect = function()  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.waybill.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.waybill.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.waybill.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.waybill.voyage = null;
    };
    // End voyage select

    // Get list
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            var voyageAll = response.data;
            $scope.voyageList = [];

            for (var index in voyageAll)    {
                if (voyageAll[index].status != 10)
                    $scope.voyageList.push(voyageAll[index]);
            };

        });
    };

	/* REST Methods */
	$scope.typeShipper = function(name)	{
		return RESTFactory.get('shipper/search/' + name).then(function(response)	{
			$scope.shipper_list = response.data;
			return response.data;
		});
	};

	$scope.nameShipper = function()	{
		$scope.existing_shipper_list = [];
		RESTFactory.get('shipper/name/' + $scope.create_shipper.name).then(function(response)	{
			$scope.existing_shipper_list = response.data;
		}, function()	{
			// Response if fail
		});
	};

	$scope.getShipperPlates = function()	{
		if ($scope.shipper.id)	{
			RESTFactory.get('cargo/request/shipper/' + $scope.shipper.id).then(function(response)	{
				$scope.shipper_plates = response.data;
			}, function()	{
				toaster.pop("warning", "Warning", "No cargo found for this shipper");
				// Response if fail
			});
		}
	};

	$scope.createNewShipper = function()	{
		RESTFactory.post('shipper', $scope.create_shipper).then(function(response)	{
			toaster.pop("success", "Success", "Shipper " + response.data.name + " successfully created");
		}, function(response)	{
			// Response if fail
		});
	};

	/* Create Cargo */
	$scope.createCargo = function()	{
		/* Enable isWaybillPrinting */
		$scope.isWaybillPrinting = true;

		/* Check if cargo needs to be created */
		if ($scope.cargo.id)	{
			$scope.waybill.cargo = $scope.cargo.id;
			$scope.waybill.on_account = $scope.cargo.on_account;

			// Create Waybill
			$scope.createWaybill();
		} else {
			RESTFactory.post('cargo', $scope.cargo).then(function(response)	{
				$scope.waybill.cargo = response.data.id;
				$scope.waybill.on_account = response.data.on_account;
				
				// Create Waybill
				$scope.createWaybill();
			});
		}
	};

	/* Create Waybill */
	$scope.createWaybill = function()	{
		// Populate waybill
        $scope.waybill.created_by = $rootScope.id;
        $scope.waybill.print = $scope.enable_print;

        // Put nulls to these values (REST have values for this)
        $scope.waybill.lading_no = null;
        $scope.waybill.booking_no = null;

		RESTFactory.post('waybill', $scope.waybill).then(function(response)	{

			/* Reset Fields to null */
			$scope.cargo = {};
			$scope.shipper = undefined;
			$scope.waybill = {
				voyage: $scope.waybill.voyage
			};
			$scope.shipper_list = [];
			$scope.existing_shipper_list = [];

			/* Update Series */
			$scope.updateSeries();

			/* Update */
			$scope.update();

			/* Disable isWaybillPrinting */
			$scope.isWaybillPrinting = false;

			toaster.pop("success", "Success", "Waybill successfully created");
		}, function(response)	{
			/* Disable isWaybillPrinting */
			$scope.isWaybillPrinting = false;

			toaster.pop("error", "Error", "Failed to create waybill");
		});
	};

	$scope.getWaybillList = function()	{
		RESTFactory.get('waybill/voyage/' + $scope.waybill.voyage + '/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response)	{
			$scope.waybillList = response.data;
		});
	};

	$scope.getWaybillCount = function()	{
		RESTFactory.get('waybill/count/voyage/' + $scope.waybill.voyage).then(function(response)	{
			$scope.waybillCount = response.data;
		});
	};

	$scope.updateSeries = function()    {
        if (typeof $rootScope.id === 'undefined')   {
            /* Load session values again, then recall method again */
            RESTFactory.get('session').then(function(response)  {
                $rootScope.id = response.id;
                $rootScope.username = response.username;
                $rootScope.role = response.role;
                $rootScope.fullname = response.fullname;
                $rootScope.current_series = response.current_series;
                $scope.updateSeries();
            });
        } else {
            RESTFactory.get('users/' + $rootScope.id).then(function(response) {
                $scope.waybill.series_no = response.data.current_series;
            });
        }
    };

	/* Internal Methods */
	var getTableList = function(tables)	{
		var processCounter = 0; // Counter
		for (var index in tables)	{
		(function(index){ 
			
			RESTFactory.get(tables[index]).then(function(response)	{
				$scope[tables[index]+'_list'] = response.data;

				// Table Counter
				processCounter++;

				// If processCounter equals table length
				if (processCounter == tables.length)	
					$scope.isDataLoading = false;

			}, function(response)	{
				// Response on fail
			});

		})(index); 
		};
	};

	/* Print, Refund, Cancel */
	$scope.printWaybill = function() {
        RESTFactory.get('renderWaybill/' + $scope.waybillItems[$scope.currentIndex].lading_no).then(function(response) {
            // Response if success
        }, function(response)   {
            // Response if fail
        });
    };

    $scope.refundWaybill = function()   {
        var params = $.param({
            'status': 6
        });
        RESTFactory.put('waybill/' + $scope.currentIndex, params).then(function() {
            $scope.update();

            // Get Waybill Details (For Revenue Update)
            RESTFactory.get('waybill/' + $scope.currentIndex).then(function(response) {
                // Update Ticket Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.price_paid,
                });
                RESTFactory.post('revenue/waybill/refund', revenueParams);
            });
        });
    };

    $scope.cancelWaybill = function()   {
        var params = $.param({
            'status': 7
        });
        RESTFactory.put('waybill/' + $scope.currentIndex, params).then(function() {
            $scope.update();

            // Get Waybill Details (For Revenue Update)
            RESTFactory.get('waybill/' + $scope.currentIndex).then(function(response) {
                // Update Ticket Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.price_paid,
                });
                RESTFactory.post('revenue/waybill/cancel', revenueParams);
            });
        });
    };

});
