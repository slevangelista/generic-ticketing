<?php

$slim->route->get("/cargo/request/shipper/:shipper_id", function($shipper_id)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        $cargo = $slim->db->readAll("cargo", array(
            'where' => 'shipper = :shipper',
            'params' => array(
                ':shipper' => $shipper_id
            )
        ));

        if ($cargo)
            $resp['data'] = $cargo;
        else
            throw new Exception ("No cargo found for selected shipper");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Cargo request failed. Error: " . $e->getMessage();  
    }

    JSONResponse($status, $resp);

});

// Get all record with pagination OR custom request record
$slim->route->get("/cargo/:page/:items", function($page, $items)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Check for custom request
    if ($page == 'request') {
        switch ($items) {
            case 'last_record':
                $resp['data'] = $slim->db->read('cargo', array(
                    'order' => 'id DESC',
                    'limit' => 1
                ));
                break;
            case 'count':  
                $resp['data'] = count($slim->db->readAll('cargo', array(
                    'where' => 'on_account = 1'
                )));
                break;
            case 'count_active':
                $resp['data'] = count($slim->db->readAll('cargo', array(
                    'where' => 'active = 1'
                )));
                break;
            default: 
                $status = "404";
                $resp['status'] = 'error';
                $resp['message'] = 'Not Found';
                break;
        }
    } else {
        // Pagination Logic
        $index = 0;
        for($i = 1; $i < $page; $i++) {
            $index = $index + $items;
        }

        $resp['data'] = $slim->db->readAll('cargo', array(
            'order' => 'id ASC',
            'where' => 'on_account = 1',
            'limit' => "{$index}, {$items}",
        ));
    }

    JSONResponse($status, $resp);
});

$slim->route->post("/cargo", function() {

    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    if ($data_url = $slim->route->request->getBody())   {
        $data = json_decode($data_url, true);
 
        /* If data is null, parse string */
        if (!$data)
            parse_str($data_url, $data);

        try {
            
            if (! $data)
                throw new Exception("Data is null");
            
            /* Check if plate number already exists in database */
            $cargo = $slim->db->read('cargo', array(
                'where' => 'plate_num = :plate_num',
                'params' => array(
                    ':plate_num' => $data['plate_num']
                )
            ));

            if ($cargo) { // Plate Number already exists, use this for response
                $resp['data'] = $cargo;
            } else { // Does not exists, create new one then use inserted cargo as response
                
                foreach ($data as $key => $value) {
                    $values[$key] = ':' . $key;
                    $params[':' . $key] = $value;
                }

                $slim->db->create('cargo', array(
                    'values' => $values,
                    'params' => $params
                ));

                /* Get last inserted id */
                $cargo_id = $slim->db->lastInsertId();

                /* Generate Response */
                $resp['data'] = $slim->db->readByPk('cargo', $cargo_id);
            }

        } catch (Exception $e) {
            /* Throw bad request response */
            $status = "400";
            $resp['status'] = 'error';
            $resp['message'] = $e->getMessage();
        }

    } else {
        /* Throw bad request response */
        $status = "400";
        $resp['status'] = 'error';
        $resp['message'] = 'Invalid Request';
    }

    JSONResponse($status, $resp);
});