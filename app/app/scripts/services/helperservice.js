'use strict';

/**
 * @ngdoc service
 * @name archipelagoWebAppApp.HelperService
 * @description
 * # HelperService
 * Service in the archipelagoWebAppApp.
 */
angular.module('archipelagoWebAppApp')
    .service('HelperService', function HelperService($rootScope, RESTFactory) {
    
    // Internal Methods
    var getScheme = function(table)  {
        // Search for the table in rootscope
        for (var i in $rootScope.schemes)   {
            if ($rootScope.schemes[i] == table)
                return $rootScope[$rootScope.schemes[i]].data;
        }
    };

    // Join Helper Services
    this.join = function(id, items) {
        // Link items to your id
        if (typeof items !== 'undefined')   {
            if (!_.isEmpty(items))  {
                for (var i in items)    {
                    if (items[i].id == id)
                        return items[i];
                }
            } else
                return "";
        } else
            return "";
    };

    // Get List
    this.getListActive = function(table)  {
        return RESTFactory.get(table).then(function(response)  {
            var items = response.data;
            var arrayList = [];

            for (var i in items)    {
                if (items[i].active == 1)
                    arrayList.push(items[i]);
            }
            return arrayList;
        });
    }

    // Get Date
    this.getDate = function(date)   {
        // Change departure_date to proper format
        // var date_string = date.toLocaleDateString();
        // var date_array = date_string.split("/");
        // console.log(date_string);
        var month = (date.getMonth()*1 + 1);
        var day = (date.getDate()*1);

        return date.getUTCFullYear() + '-' + ("0" + (month)).slice(-2) + '-' + ("0" + day).slice(-2);  
    }

    // Get time from timepicker
    this.getTime = function(time)   {
        var timeToString = time.toTimeString();
        var time_array = timeToString.split(" ");

        var time_split = time_array[0].split(":");
        var time = time_split[0] + ":" + time_split[1] + ":00"; 

        return time;
    }

    // Capitalize first letter
    this.capitalizeFirstChar = function(word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    }

    // Split string based on lenght (int)
    this.splitByLength = function(string, length)   {
        var string_array = string.split("");
        var new_string = "";

        for (var i=0; i < length; i++) {
            new_string = new_string + string_array[i];
        }

        return new_string;
    }

});
