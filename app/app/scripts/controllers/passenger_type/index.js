'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:passengerTypeIndexCtrl
 * @description
 * # passengerTypeIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('PassengerTypeIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.passengerTypeItems = {};

    // Pagination
    $scope.passengerTypeCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getPassengerTypeByPage();
        $scope.getPassengerTypeByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.passengerTypeItems[data.id] = {
            'id': data.id,
            'name': data.name,
            'code': data.code,
            'description': data.description,
            'is_promo': data.is_promo,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getPassengerTypeByPage = function()    {
        RESTFactory.get('passenger_type/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.passengerTypeList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getPassengerTypeByCount = function()   {
        RESTFactory.get('passenger_type/' + 'request/count').then(function(response) {
            $scope.passengerTypeCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addPassengerType = function() {
        var params = $.param($scope.newPassenger);
        RESTFactory.post('passenger_type', params).then(function(response)  {
            // Response if success
            $scope.newPassenger = {};
            $scope.update();
            toaster.pop("success", "Success", "Passenger type successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Passenger type failed to add");
        });
    };  

    $scope.editPassengerType = function()   {
        var params = $.param($scope.passengerTypeItems[$scope.currentIndex]);
        RESTFactory.put('passenger_type/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Passenger type successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Passenger type failed to edit");
        });
    };

});
