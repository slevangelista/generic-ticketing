<?php

	$slim->route->get("/report/passenger_manifest/:voyage_id",function($voyage){
	global $slim;
	$status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

	$sql = "SELECT CONCAT_WS( ',', passenger.last_name, passenger.first_name ) as passenger_name , passenger.gender, passenger.gender,"
         . " passenger.age, passenger.nationality, passenger.civil_status, passenger.address, passenger.contact,"
         . " voyage.departure_date,vessel.name AS vessel_name, vessel.code AS vessel_code,"
         . " route.name AS route_name, route.dest_port, route.source_port"
         . " FROM ticket"
         . " INNER JOIN passenger ON ticket.passenger = passenger.id"
         . " INNER JOIN voyage ON voyage.id = ticket.voyage"
         . " INNER JOIN vessel ON vessel.id = voyage.vessel"
         . " INNER JOIN trip ON trip.id = voyage.trip"
         . " INNER JOIN route on route.id = trip.route_id"
         . " AND voyage.id = :voyage_id";

	$params = array(':voyage_id' => $voyage);


    $resp["data"] = $slim->db->SQL($sql,$params);
    //echo '<pre>',print_r($resp["data"],1),'</pre>';
    JSONResponse($status, $resp);
    
	});