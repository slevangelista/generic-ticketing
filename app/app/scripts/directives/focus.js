'use strict';

/**
 * @ngdoc directive
 * @name archipelagoWebAppApp.directive:focus
 * @description
 * # focus
 */
angular.module('archipelagoWebAppApp')
  .directive('focus', function () {
    return function(scope, element) {
        element[0].focus();
    } 
  });
