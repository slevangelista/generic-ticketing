'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:CargoCtrl
 * @description
 * # CargoCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('CargoCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

    // Initializations
    // ---- ---- ---- ----
    $scope.waybill = {};
    $scope.cargo = {};

    /* Shipper Options */
    $scope.shipperOptionList = ['Select Shipper', 'Create New Shipper'];
    //$scope.shipperOptionList = ['Select Shipper'];
    $scope.shipperOption = 0;

    $scope.disableCargoInput = false;
    $scope.bool = ['No', 'Yes'];

    // Get List
    HelperService.getListActive('cargo_fare_rates').then(function(response) {
        $scope.cargoRateAll = response;
    });

    HelperService.getListActive('cargo_class').then(function(response) {
        $scope.cargoClassList = response;
    });

    HelperService.getListActive('route', function(response) {
        $scope.routeList = response;
    });

    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Process Methods
    // ---- ---- ---- ----
    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.openCargoList = function()   {
        $('#cargoRateModal').modal('show');
    };

    $scope.init = function()    {
        $scope.updateSeries();
    };

    $scope.update = function()  {
        $scope.getRoute();
        $scope.getCargoRateList();
        $scope.getShipperList();
    };

    // Voyage Display
    $scope.onVoyageSelect = function()  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.waybill.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.waybill.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.waybill.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.waybill.voyage = null;
    };
    // End voyage select

    // Get list
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            var voyageAll = response.data;
            $scope.voyageList = [];

            for (var index in voyageAll)    {
                if (voyageAll[index].status != 10)
                    $scope.voyageList.push(voyageAll[index]);
            };

        });
    };

    $scope.getCargoRateList = function()    {
        var cargoRateAll = $scope.cargoRateAll;
        $scope.cargoRateList = [];

        for (var index in cargoRateAll)    {
            if ($scope.cargoRoute == cargoRateAll[index].route) {
                $scope.cargoRateList.push(cargoRateAll[index]);
            }
        }
    };

    $scope.getRoute = function()    {
        // Loop through voyage
        for (var index in $scope.voyageList)    {
            if ($scope.voyageList[index].id == $scope.waybill.voyage)   {
                $scope.cargoRoute = $scope.joinTripByRoute($scope.voyageList[index].trip);
            }
        }
    };

    // Joins
    $scope.joinCargoClass = function(id)   {
        return HelperService.join(id, $scope.cargoClassList).name;
    };

    $scope.joinTripByRoute = function(id)   {
        return HelperService.join(id, $scope.tripList).route_id;
    };

    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    $scope.setRate = function(classID, rate, original_rate, discount) {
        $scope.waybill.cargo_class = classID;
        $scope.waybill.price_paid = rate;
        $scope.waybill.original_price = original_rate;
        $scope.waybill.discount = discount;
    };

    $scope.resetFields = function()    {
        $scope.new_shipper = null;
        $scope.waybill = {};
        $scope.cargo = {};
    };

    $scope.resetPlateNum = function()   {
        $scope.new_shipper = null;
        $scope.waybill.price_paid = null;
        $scope.cargo.plate_num = null;
    };

    $scope.plateOptionChange = function()    {
        /* Enable Cargo Input */
        $scope.disableCargoInput = false;

        /* Clear Cargo Values */
        $scope.cargo.plate_num = null;
        $scope.cargo.article_no = null;
        $scope.cargo.address = null;
        $scope.cargo.article_desc = null;
        $scope.cargo.on_account = null;

    };

    $scope.getPlateNum = function() {
        
        /* Fill Cargo Values */
        $scope.cargo.plate_num = HelperService.join($scope.selectedPlateNum, $scope.cargoList).plate_num;
        $scope.cargo.article_no = HelperService.join($scope.selectedPlateNum, $scope.cargoList).article_no;
        $scope.cargo.address = HelperService.join($scope.selectedPlateNum, $scope.cargoList).address;
        $scope.cargo.article_desc = HelperService.join($scope.selectedPlateNum, $scope.cargoList).article_desc;
        $scope.cargo.on_account = HelperService.join($scope.selectedPlateNum, $scope.cargoList).on_account;
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.getShipperList = function()  {
        HelperService.getListActive('shipper').then(function(response)  {
            $scope.shipperList = response;
        });
    };

    $scope.updateSeries = function()    {
        if (typeof $rootScope.id === 'undefined')   {
            $scope.updateSeries();
        } else {
            RESTFactory.get('users/' + $rootScope.id).then(function(response) {
                $scope.waybill.series_no = response.data.current_series;
            });
        }
    };

    $scope.getCargoList = function()    {
        RESTFactory.get("cargo/request/shipper/" + $scope.cargo.shipper).then(function(response)  {
            $scope.resetPlateNum();

            $scope.cargoList = response.data;

            /* Plate Options */
            $scope.plateOptionList = ['Create New Cargo', 'Select Plate Number'];
            $scope.plateOption = 0;

        }, function(response)   {
            toaster.pop("warning", "Warning", "No cargo found for this shipper");

            /* Plate Options */
            $scope.plateOptionList = ['Create New Cargo'];
            $scope.plateOption = 0;

        });
    };

    $scope.addCargo = function() {

        if ($scope.shipperOption == 1)   { // Create new shipper, then proceed on waybill creation
            /* Create New Shipper */
            var shipperParams = $.param({
                name: $scope.new_shipper,
            });
            RESTFactory.post('shipper', shipperParams).then(function(response) {
                $scope.cargo.shipper = response.data.id
                
                // Create Waybil;
                createWayBill();
            });
        } else { // Select Shipper, then proceed on waybill creation
            // Create Waybill
            createWayBill();
        }
    };

    var createWayBill = function()  {
        // Add Cargo
        var cargoParams = $.param($scope.cargo);

        RESTFactory.post('cargo', cargoParams).then(function(response)  {
            // console.log(response.data);
            
            // Populate waybill
            $scope.waybill.cargo = response.data.id;
            $scope.waybill.created_by = $rootScope.id;

            // Put nulls to these values (REST have values for this)
            $scope.waybill.lading_no = null;
            $scope.waybill.booking_no = null;
            $scope.waybill.on_account = response.data.on_account;
            // $scope.waybill.waybill_type = 2;

            var waybillParams = $.param($scope.waybill);

            // Add Waybill
            RESTFactory.post("waybill", waybillParams).then(function(respn)    {
                $scope.resetFields();
                $scope.updateSeries();
                toaster.pop("success", "Success", "Waybill successfully added");
            }, function()   {
                // Response if fail
                toaster.pop("error", "Error", "Waybill failed to add");
            });
                
        });
    };

});
