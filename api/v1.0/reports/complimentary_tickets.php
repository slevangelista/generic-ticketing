<?php

$slim->route->get("/report/complimentary_tickets/:voyage", function($voyage)	{

	// Init
	global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $sql = "
    	SELECT
    		waybill.id as waybill_id,
    		cargo.id as cargo_id,
    		cargo.shipper,
    		cargo.plate_num,
    		cargo.address,
    		waybill.lading_no
    	FROM waybill
    	LEFT JOIN cargo ON waybill.cargo = cargo.id
    	WHERE voyage = :voyage AND price_paid = 0
    ";

    // Execute SQL
    $query = $slim->db->SQL($sql, array(
    	':voyage' => $voyage
    ));

    // Pass Data
    $resp['data']['table'] = $query;

    // echo "<pre>" . print_r($query, 1) . "</pre>";
    JSONResponse($status, $resp);

});