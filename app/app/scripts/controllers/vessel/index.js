'use strict';

/**
* @ngdoc function
* @name archipelagoWebAppApp.controller:VesselCtrl
* @description
* # VesselCtrl
* Controller of the archipelagoWebAppApp
*/
angular.module('archipelagoWebAppApp')
.controller('VesselCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // ---- ---- ---- ----
    $scope.vesselItems = {};
    $scope.vesselCapacity = 0;
    $scope.vesselTemplateList = {};

    $scope.seatingClassList = [];
    $scope.seatingClassPerVessel = {};

    // Pagination
    $scope.vesselCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Generate Lists
    HelperService.getListActive('seating_class').then(function(response)    {
        $scope.seatingClassList = response;
    });

    // Process Methods
    // ---- ---- ---- ----
    $scope.getVesselItems = function(value)  {
        $scope.vesselItems[value.id] = {
            'id': value.id,
            'name': value.name,
            'code': value.code,
            'capacity': value.capacity,
            'active': value.active,
        };
    };

    $scope.update = function()  {
        $scope.getVessel();
        $scope.getVesselCount();
    };

    $scope.joinSeatingClass = function(id)    {
        return HelperService.join(id, $scope.seatingClassList).name;
    };

    $scope.setIndex = function(id)  {
        $scope.index = id;
    };

    $scope.resetFields = function() {
        $scope.seatClass = [];
        $scope.vesselName = null;
        $scope.vesselCode = null;
        $scope.vesselCapacity = 0;
    };

    $scope.getVesselTemplatePerIndex = function()   {
        $scope.seatingClassPerVessel[$scope.index] = [];

        for (var index in $scope.vesselTemplateList[$scope.index])  {
            $scope.seatingClassPerVessel[$scope.index].push($scope.vesselTemplateList[$scope.index][index].seating_class_id);
        }
    };

    $scope.check = function(index) {
        console.log($scope.seatingClassPerVessel[$scope.index]);
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.getVessel = function()  {
        RESTFactory.get('vessel/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.vesselList = response.data;
        });
    };

    $scope.getVesselCount = function()  {
        RESTFactory.get('vessel/request/count').then(function(response){
            $scope.vesselCount = response.data;
        });
    };

    $scope.getVesselTemplate = function(id)  {
        RESTFactory.get('vessel_template/request/vessel/' + id).then(function(response) {
            $scope.vesselTemplateList[id] = response.data;
        });
    };  

    $scope.addVessel = function() {
        var vessel = $.param({
            'name': $scope.vesselName,
            'code': $scope.vesselCode,
            'capacity': $scope.vesselCapacity,
        });

        // Insert Vessel
        RESTFactory.post('vessel', vessel).then(function(response)  {

            RESTFactory.get('vessel/request/last_record').then(function(response)  {
            
                // Get Vessel Template by Seating Class
                for (var index in $scope.seatingClass)  {
                    var vesselTemplate = $.param({
                        'vessel_id': response.data.id,
                        'seating_class_id': $scope.seatingClass[index],
                    });

                    RESTFactory.post('vessel_template', vesselTemplate).then(function(response) {
                        // Response if success
                        $scope.update();
                        $scope.resetFields();
                    });
                }
            });

            toaster.pop("success", "Success", "Vessel successfully added");

        }, function()  {
            // Show error notice
            toaster.pop("error", "Error", "Vessel failed to add");
        });
    };

    $scope.editVessel = function()  {
        var vessel = $.param($scope.vesselItems[$scope.index]);
        RESTFactory.put('vessel/' + $scope.index, vessel).then(function(response)   {
            
            RESTFactory.delete('vessel_template/request/vessel/' + $scope.vesselItems[$scope.index].id).then(function(response)  {
                if (typeof $scope.seatingClassPerVessel[$scope.index] !== 'undefined')   {
                    for (var index in $scope.seatingClassPerVessel[$scope.index])   {
                        var vesselTemplate = $.param({
                            'vessel_id': $scope.vesselItems[$scope.index].id,
                            'seating_class_id': $scope.seatingClassPerVessel[$scope.index][index],
                        });

                        RESTFactory.post('vessel_template', vesselTemplate).then(function(response) {
                            // Response if success
                            $scope.update();
                            $scope.resetFields();

                        });
                    }
                }
            });

            toaster.pop("success", "Success", "Vessel successfully edited");
        }, function()   {
            // Resp if fail
            toaster.pop("error", "Error", "Vessel failed to edit");
        });
    };

    // For Vessel Add
    $scope.getVesselCapacityAdd = function(ids) {
        $scope.vesselCapacity = 0;

        // Loop number of select
        for (var id in $scope.seatingClass)  {
            // Loop SeatClassList
            for (var index in $scope.seatingClassList)  {
                if ($scope.seatingClassList[index].id == $scope.seatingClass[id])  {
                    RESTFactory.get('seat/request/count_by_seat_class/' + $scope.seatingClass[id]).then(function(response) {
                        $scope.vesselCapacity += parseInt(response.data);
                    }, function() {
                        // Resp if fail
                    });
                }
            }
        }
    };

    // For Vessel Edit
    $scope.getVesselCapacityEdit = function(ids) {
        $scope.vesselItems[$scope.index].capacity = 0;

        // Loop number of select
        for (var id in $scope.seatingClassPerVessel[$scope.index])  {
            // Loop SeatClassList
            for (var index in $scope.seatingClassList)  {
                if ($scope.seatingClassList[index].id == $scope.seatingClassPerVessel[$scope.index][id])  {
                    RESTFactory.get('seat/request/count_by_seat_class/' + $scope.seatingClassPerVessel[$scope.index][id]).then(function(response) {
                        $scope.vesselItems[$scope.index].capacity = $scope.vesselItems[$scope.index].capacity*1 + parseInt(response.data*1);
                    }, function() {
                        // Resp if fail
                    });
                }
            }
        }
    };

});
