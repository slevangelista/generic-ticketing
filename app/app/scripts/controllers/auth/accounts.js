'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:AuthAccountsCtrl
 * @description
 * # AuthAccountsCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('AuthAccountsCtrl', function ($scope, RESTFactory, HelperService, toaster) {

	// Initialize
    // --- ---- ---- ----
    $scope.userItems = {};

    // Get list
    HelperService.getListActive('roles').then(function(response)	{
    	$scope.roleList = response;
    });

    RESTFactory.get('printer').then(function(response)    {
        $scope.printerList = response.data;
    });

    // Pagination
    $scope.userCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getUserByPage();
        $scope.getUserByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.userItems[data.id] = {
            'id': data.id,
            'role': data.role,
            'employee_no': data.employee_no,
            'first_name': data.first_name,
            'last_name': data.last_name,
            'email': data.email,
            'username': data.username,
            'assigned_printer': data.assigned_printer,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // Joins
    $scope.joinRole = function(id)	{
		return HelperService.join(id, $scope.roleList).name;
    };

    $scope.joinPrinter = function(id)  {
        return HelperService.join(id, $scope.printerList).host_name + '@' + HelperService.join(id, $scope.printerList).host_address;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getUserByPage = function()    {
        RESTFactory.get('users/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.userList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getUserByCount = function()   {
        RESTFactory.get('users/' + 'request/count').then(function(response) {
            $scope.userCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addUser = function() {
        var params = $.param($scope.newUser);
        RESTFactory.post('users', params).then(function(response)  {
            $scope.newUser = {};
            $scope.update();

            toaster.pop('success', "Success", "New user successfully added");
        }, function(response)  {
            toaster.pop('error', "Error", response.data.message);
        });
    };  

    $scope.editUser = function()   {
        var params = $.param($scope.userItems[$scope.currentIndex]);
        RESTFactory.put('users/' + $scope.currentIndex, params).then(function(response)  {
            $scope.update();

            toaster.pop('success', "Success", "User successfully updated");
        }, function(response)  {
            toaster.pop('error', "Error", response.data.message);

        });
    };

    $scope.changePassword = function()	{
    	var params = $.param($scope.newPassword);
        RESTFactory.put('users/request/passwordAdmin/' + $scope.currentIndex, params).then(function(response)  {
            $scope.newPassword = {};
            $scope.update();

            toaster.pop('success', "Success", "Password has successfully been changed");
        }, function(response)  {
            toaster.pop('error', "Error", response.data.message);
        });
    };

});
