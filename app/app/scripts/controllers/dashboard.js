'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('DashboardCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

	// Initializations
	// ---- ---- ---- ----;
    $scope.displayVoyageAlert = false;

	// Revenue Summary
	$scope.revenue = {
		ticket: 0,
		waybill: 0,
		baggage: 0,
		upgrades: 0,
	};

	// Chart Configurations
	$scope.chartType = "bar";
	$scope.chartList = ["bar", "pie", "line", "point", "area"];
	$scope.config = {
		waitForHeightAndWidth: true,
	    title: 'Voyages',
	    tooltips: true,
	    labels: false,
	    mouseover: function() {},
	    mouseout: function() {},
	    click: function() {},
	    legend: {
	      display: true,
	      //could be 'left, right'
	      position: 'right'
	    }
	};

	// Process Methods
	// ---- ---- ---- ----
	$scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

	$scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
    	$scope.selectedDate = HelperService.getDate($scope.selectedDate);
    	$scope.config.title = "Voyages as of " + $scope.selectedDate;
        $scope.getVoyageData();
    };

    var getTotalRevenue = function(voyageData)	{
    	$scope.revenue = {
    		ticket: 0,
			waybill: 0,
			baggage: 0,
			upgrades: 0,
    	};
    	for (var index in voyageData)	{
    		for (var i in voyageData[index].y)	{
    			switch (i)	{
    				case '0': // Ticket
    					$scope.revenue.ticket += voyageData[index].y[i] * 1;
    				break;
    				case '1': // Waybill
	    				$scope.revenue.waybill += voyageData[index].y[i] * 1;
    				break;
    				case '2': // Baggage
    					$scope.revenue.baggage += voyageData[index].y[i] * 1;
    				break;
    				case '3': // Upgrades
    					$scope.revenue.upgrades += voyageData[index].y[i] * 1;
    				break;
    			}
    		}
    	}
    	$scope.revenue.total = ($scope.revenue.ticket + $scope.revenue.waybill + $scope.revenue.baggage + $scope.revenue.upgrades);
    };

	// REST Methods
	// ---- ---- ---- ----
	$scope.getVoyageData = function()	{

		RESTFactory.get('voyage/request/data/' + $scope.selectedDate).then(function(response)	{
			var voyageData = _.toArray(response.data);
			getTotalRevenue(voyageData); // Get revenue summary

			$scope.data = {
				series: ['Tickets', 'Waybills', 'Baggage', 'Upgrades', 'Deductions'],
				data: voyageData
			};

			$scope.displayVoyageAlert = true;
		}, function(response)	{
			$scope.revenue = {
	    		ticket: 0,
				waybill: 0,
				baggage: 0,
				upgrades: 0,
	    	};
			
            // Do not trigger this on first load
            if ($scope.displayVoyageAlert)  {
    			toaster.pop('error', "Error", "No voyage found for selected date");
            }

			$scope.data = {
				series: [],
				data: null
			};
            $scope.displayVoyageAlert = true;
		});
	};

});
