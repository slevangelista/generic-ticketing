'use strict';

angular.module('archipelagoWebAppApp')
  .directive('popOver', function () {
    return function(scope, element, attrs) {
        element.find("a[rel=popover]").popover({ placement: 'top', html: 'true'});
    };
  });
