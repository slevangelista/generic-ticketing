<?php

$slim->route->get("/baggage/request/last_record", function()   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = $slim->db->read('baggage', array(
        'order' => 'id DESC',
        'limit' => 1
    ));

    JSONResponse($status, $resp);
});

/* Get count */
$slim->route->get("/baggage/request/count", function() use ($slim)   {
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        $resp['data'] = count($slim->db->readAll('baggage'));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Baggage request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

// Baggage Pagination
$slim->route->get("/baggage/:page/:items", function($page, $items)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Pagination Logic
    $index = 0;
    for($i = 1; $i < $page; $i++) {
        $index = $index + $items;
    }

    $sql = "
        SELECT 
            baggage.id AS id, 
            voyage.number AS voyage, 
            CONCAT( passenger.first_name, ' ', passenger.last_name ) AS passenger, 
            ticket.ticket_no AS ticket,
            baggage.price_paid AS price_paid,
            baggage_type.weight AS baggage_type,
            status.name AS status,
            status.id AS status_id
        FROM baggage
        LEFT JOIN passenger ON baggage.passenger = passenger.id
        LEFT JOIN voyage ON baggage.voyage = voyage.id
        LEFT JOIN ticket ON baggage.ticket = ticket.id
        LEFT JOIN baggage_type ON baggage.baggage_type = baggage_type.id
        LEFT JOIN status ON baggage.status = status.id
        ORDER BY id ASC
        LIMIT {$index}, {$items}
    ";

    $resp['data'] = $slim->db->SQL($sql);

    JSONResponse($status, $resp);
});

$slim->route->get("/baggage/request/ticket/:ticket", function($ticket)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {

        $baggage = $slim->db->read("baggage", array(
            'where' => 'ticket = :ticket',
            'order' => 'id DESC',
            'params' => array(
                ':ticket' => $ticket
            ),
        ));

        if ($baggage)
            $resp['data'] = $baggage;
        else
            throw new Exception("No baggage found for this ticket");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Baggage request failed. " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});