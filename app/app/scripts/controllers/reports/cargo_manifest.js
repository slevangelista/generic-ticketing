'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:ReportsCargoManifestCtrl
 * @description
 * # ReportsCargoManifestCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
  .controller('ReportsCargoManifestCtrl', function ($scope,RESTFactory, HelperService) {

  	// Initializations
    // ---- ---- ---- ----
    $scope.cargos = {};
    $scope.cargo_report = {};

    // Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Methods
    // ---- ---- ---- ----
    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.update = function()  {
        $scope.getCargos();
    };

    // Voyage Display
    $scope.onVoyageSelect = function()  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.cargos.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.cargos.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.cargos.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.cargos.voyage = null;
    };
    // End voyage select

    // Get List
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            $scope.voyageList = response.data;
        });
        $scope.voyageList = [];
    };

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    // REST
    $scope.getCargos = function()    {
        RESTFactory.get('report/cargo_manifest/' + $scope.cargos.voyage).then(function(response){
            var manifest = response.data;
            $scope.vessel = manifest["vessel_details"];

            var route = manifest["vessel_details"]["route_name"];
            var split_route = route.split("-");
            $scope.port_origin = split_route[0];
            $scope.port_destination = split_route[1];

            $scope.cargos_report = manifest["cargos"];

            var weight = 0;

            for(var i = 0; i < manifest["cargos"].length; i++){
                weight = weight + parseInt(manifest["cargos"][i]["weight"]);
            }

            $scope.total_weight = weight;
           
        }, function()   {
            // Response if Failed
        });
    };

    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });

        var filename = "Cargo_manifest";
        saveAs(blob, filename+".xls");
    };
  
  });
