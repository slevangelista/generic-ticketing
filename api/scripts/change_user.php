<?php

require_once "../v1.0/slim_express/libs/SlimModel.php";

/* Change ownership of ff. users */

// rgemuel: 11 (Original) -> 19
// markjohn: 12 (Original) -> 20

function changeOwnership($old_key, $new_key)	{

	/* Set Slim Configuration */
	$slim = new SlimModel(array(
		'host' => '10.21.10.1',
	    'database' => 'archipelago',
	    'user' => 'root',
	    'pass' => 'mysqladmin'
	));

	/* Set tables array to change */
	$tables = array('ticket', 'waybill', 'baggage', 'upgrades', 'deductions', 'net_cash');

	foreach ($tables as $key => $value)	{

		$slim->update($value, array(
			'where' => "created_by = :old_created_by",
			'values' => array(
				'created_by' => ":new_created_by"
			),
			'params' => array(
				':old_created_by' => $old_key,
				':new_created_by' => $new_key
			)
		));
	}
} 

// Main
try {

    // Change Ownership of rgemuel	
	changeOwnership(11, 19);

    // Change Ownership of markjohn
    changeOwnership(12, 20);

	// If all change script succeeded	
	echo "Ownership change success!\n";

} catch (Exception $e) {
	echo $e->getMessage() . "\n";
}
