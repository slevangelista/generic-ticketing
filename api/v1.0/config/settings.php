<?php

// settings.php
// Written By: Syd Evangelista
// Set your slim express settings here

// Database Settings
$slimConfig['db'] = array(
    'host' => 'localhost',
    'database' => 'ticketingDB',
    'user' => 'root',
    'pass' => 'mysqladmin'
);

// Application Settings
// Equivalent to $app = new Slim/Slim(array())
$slimConfig['app'] = array(
    'debug' => true,
);

// API Root
// Set url of archipelago project
$slimConfig['url'] = "localhost/archipelago";
