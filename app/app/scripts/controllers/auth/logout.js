'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('LogoutCtrl', function ($location, $window, RESTFactory, toaster, $rootScope)    {

    RESTFactory.get('logout').then(function (results) {
    	/* Reset rootscope values */
		$rootScope.authenticated = false;
		$rootScope.id = null;
		$rootScope.username = null;
		$rootScope.role = null;
		
        toaster.pop('success', "Success", "You have successfully logged off");
        $location.path('/login');
    });

});