<?php

/* Get permission based on role */
$slim->route->get("/permissions/request/role/:role", function($role)	{
	global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
    	$permissions = $slim->db->readAll('permissions', array(
    		'where' => 'role = :role AND active = 1',
    		'params' => array(
    			':role' => $role
    		)
    	));

    	if ($permissions)
    		$resp['data'] = $permissions;

    } catch (Exception $e) {
    	$status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Permission request failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});