<?php

// GET

// Net Cash
$slim->route->get("/revenue/:revenue/:voyage", function($revenue, $voyage)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        $resp['data'] = $slim->db->read($revenue, array(
            'where' => "voyage = :voyage",
            'params' => array(
                ':voyage' => $voyage
            )
        ));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

// POST

/* Tickets */
$slim->route->post("/revenue/ticket/checkin", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateTicket(array(
                'voyage' => $data['voyage'],
                'seating_class' => $data['seating_class'],
                'passenger_type' => $data['passenger_type']
            ), 'checkin');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/ticket/board", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateTicket(array(
                'voyage' => $data['voyage'],
                'seating_class' => $data['seating_class'],
                'passenger_type' => $data['passenger_type']
            ), 'board');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/ticket/refund", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateTicket(array(
                'voyage' => $data['voyage'],
                'seating_class' => $data['seating_class'],
                'passenger_type' => $data['passenger_type'],
                'revenue' => $data['price_paid']
            ), 'refund');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/ticket/cancel", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateTicket(array(
                'voyage' => $data['voyage'],
                'seating_class' => $data['seating_class'],
                'passenger_type' => $data['passenger_type'],
                'revenue' => $data['price_paid']
            ), 'cancel');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/ticket/transfer", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Transfer Seat Increment */
            Revenue::updateTicket(array(
                'voyage' => $data['old_voyage'],
                'seating_class' => $data['old_seating_class'],
                'passenger_type' => $data['passenger_type']
            ), 'transferSeat');

            // Check if seating_class and/or voyage from old and new are different
            if ($data['old_seating_class'] != $data['new_seating_class'] || $data['old_voyage'] != $data['new_voyage']) {

                /* Determine if change is either in seating_class or voyage (or both) */
                
                // Seating Class
                if ($data['old_seating_class'] != $data['new_seating_class'])
                    $seating_class_inc = 1;
                else
                    $seating_class_inc = 0;

                // Voyage
                if ($data['old_voyage'] != $data['new_voyage']) 
                    $voyage_inc = 1;
                else
                    $voyage_inc = 0;

                // Deduct revenue from old
                Revenue::updateTicket(array(
                    'voyage' => $data['old_voyage'],
                    'seating_class' => $data['old_seating_class'],
                    'passenger_type' => $data['passenger_type'],
                    'revenue' => $data['revenue'],
                    'seating_class_inc' => $seating_class_inc,
                    'voyage_inc' => $voyage_inc,
                ), 'transferMinusRevenue');

                // Add revenue to new
                Revenue::updateTicket(array(
                    'voyage' => $data['new_voyage'],
                    'seating_class' => $data['new_seating_class'],
                    'passenger_type' => $data['passenger_type'],
                    'revenue' => $data['revenue']
                ), 'transferAddRevenue');
            }
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

/* Waybills */
$slim->route->post("/revenue/waybill/refund", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateWaybill(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'refund');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/waybill/cancel", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateWaybill(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'cancel');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

/* Baggage */
$slim->route->post("/revenue/baggage/create", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateBaggage(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'create');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/baggage/refund", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateBaggage(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'refund');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/baggage/cancel", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateBaggage(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'cancel');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

/* Upgrades */

$slim->route->post("/revenue/upgrades/create", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateUpgrades(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'create');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/upgrades/refund", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateUpgrades(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'refund');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/upgrades/cancel", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateUpgrades(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'cancel');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

/* Deductions */

$slim->route->post("/revenue/deductions/create", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateDeductions(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'create');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/deductions/activate", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateDeductions(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'activate');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/deductions/deactivate", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateDeductions(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'deactivate');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/deductions/update", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateDeductions(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'update');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

/* Net Cash */

$slim->route->post("/revenue/net_cash/create", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateNetCash(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'create');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/net_cash/activate", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateNetCash(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'activate');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/net_cash/deactivate", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateNetCash(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'deactivate');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post("/revenue/net_cash/update", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Update Revenue */
            Revenue::updateNetCash(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'update');
        }
        else
            throw new Exception("Empty data array thrown");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

