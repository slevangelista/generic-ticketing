'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:SeatclassIndexCtrl
 * @description
 * # SeatclassIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('PrinterIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.printerItems = {};

    // Pagination
    $scope.printerCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getPrinterByPage();
        $scope.getPrinterByCount();
        $scope.getUserList();
    };
    
    $scope.getItems = function(data)    {
        $scope.printerItems[data.id] = {
            'id': data.id,
            'host_name': data.host_name,
            'host_address': data.host_address,
            'printer': data.printer,
            'port': data.port,
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getUserList = function() {
        RESTFactory.get('users').then(function(response)   {
            $scope.userList = response.data;
        });
    }

    $scope.getPrinterByPage = function()    {
        RESTFactory.get('printer/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.printerList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getPrinterByCount = function()   {
        RESTFactory.get('printer/' + 'request/count').then(function(response) {
            $scope.printerCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addPrinter = function() {
        var params = $.param($scope.newPrinter);
        RESTFactory.post('printer', params).then(function(response)  {
            // Response if success
            $scope.newPrinter = {};
            $scope.update();

            toaster.pop("success", "Success", "Printer successfully added");

        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Printer failed to add");
        });
    };  

    $scope.editPrinter = function()   {
        var params = $.param($scope.printerItems[$scope.currentIndex]);
        RESTFactory.put('printer/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();

            toaster.pop("success", "Success", "Printer successfully edited");

        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Printer failed to edit");
        });
    };

});
