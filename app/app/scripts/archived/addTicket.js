$scope.addTickets = function()  {

    // Initialize and/or reset values
    $scope.seatArray = {};
    var seatRowArray = {};

    // Determine available seats based on seating class
    RESTFactory.get('seat_temp/request/voyage/' + $scope.tickets.voyage).then(function(seat_temp_response)   {
        $scope.seatTempAll = seat_temp_response.data;

        // Loop SeriesList
        for (var index in $scope.seriesList)    {
            (function(index){

                $scope.seatTempList = [];

                // Determine Seat List Available (Based on voyage and seating class)
                for (var i in $scope.seatTempAll)  {
                    if ($scope.seatTempAll[i].class == $scope.seriesList[index].seating_class)  
                    {
                        $scope.seatTempList.push($scope.seatTempAll[i]);
                    }
                }

                if ($scope.seatTempList.length != 0)    {

                    // Get Seat
                    $scope.seriesList[index].seat = $scope.seatTempList[0].seat;
                    $scope.seatArray[index] = $scope.seatTempList[0].id; // Save instance of id

                    // Remove from index
                    var arrayIndex = $scope.seatTempAll.indexOf($scope.seatTempList[0]);
                    $scope.seatTempAll.splice(arrayIndex, 1);

                    // Reserve Seating Status
                    var seatParams = $.param({
                        'status': 15 // This is seat temp status for reserved
                    });
                    RESTFactory.put('seat_temp/' + $scope.seatArray[index], seatParams);

                    // Initialize Static Values
                    $scope.seriesList[index].created_by = $rootScope.id;
                    $scope.seriesList[index].ticket_type = 2;
                    $scope.seriesList[index].series_no = (parseInt($rootScope.current_series)*1 + index*1);

                    // Get Booking Counter Value
                    RESTFactory.get('counter/1').then(function(counter_resp)  { // 1 stands for booking counter
                        
                        $scope.seriesList[index].booking_no = counter_resp.data.value;
                        
                        var ticketParams = $.param($scope.seriesList[index]);
                        RESTFactory.post('ticket', ticketParams).then(function(ticket_resp)  {

                            toaster.pop("success", "Success", "Ticket " + ticket_resp.data + " successfully created");
                            $scope.update();

                            $scope.seriesCounter++;
                            if ($scope.seriesList.length == $scope.seriesCounter)   {

                                // Update Booking No
                                var counterParam = $.param({
                                    'value': parseInt($scope.seriesList[index].booking_no) + 1
                                });
                                RESTFactory.put('counter/1', counterParam);

                                $scope.updateSeries();
                                $scope.resetSeriesList();
                                $scope.resetInputFields();
                                $scope.seriesCounter = 0;
                            }
                        }, function(ticket_resp)   {
                            toaster.pop("error", "Error", ticket_resp.data.message);
                        });

                    });

                } else  {
                    // Theoretically user should not reach this alert
                    toaster.pop("error", "Error", "No more seat available for this seating class");
                }

            })(index);
        }
    });
}