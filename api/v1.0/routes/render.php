<?php

$slim->route->get('/renderTicket/:ticket_no', function($ticket_no) {
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Render Ticket
    try {
        Render::renderTicket($ticket_no);
    } catch (Exception $e)  {
        $status = 400;
        $resp['message'] = "Error in printing request. Message: " . $e->getMessage();
    }

    JSONResponse($status, $resp);
    
});

$slim->route->get('/renderWaybill/:lading_no', function($lading_no)  {
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Render Lading No
    try {
        Render::renderWaybill($lading_no);
    } catch (Exception $e)  {
        $status = 400;
        $resp['message'] = "Error in printing request. Message: " . $e->getMessage();
    }

    JSONResponse($status, $resp);
});