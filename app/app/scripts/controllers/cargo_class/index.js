'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:cargoClassIndexCtrl
 * @description
 * # cargoClassIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('CargoClassIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.cargoClassItems = {};

    // Pagination
    $scope.cargoClassCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getCargoClassByPage();
        $scope.getCargoClassByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.cargoClassItems[data.id] = {
            'id': data.id,
            'name': data.name,
            'lane_meter': data.lane_meter,
            'description': data.description,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getCargoClassByPage = function()    {
        RESTFactory.get('cargo_class/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.cargoClassList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getCargoClassByCount = function()   {
        RESTFactory.get('cargo_class/' + 'request/count').then(function(response) {
            $scope.cargoClassCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addCargoClass = function() {
        var params = $.param($scope.newCargo);
        RESTFactory.post('cargo_class', params).then(function(response)  {
            // Response if success
            $scope.newCargo = {};
            $scope.update();
            toaster.pop("success", "Success", "Cargo class successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Cargo class failed to add");
        });
    };  

    $scope.editCargoClass = function()   {
        var params = $.param($scope.cargoClassItems[$scope.currentIndex]);
        RESTFactory.put('cargo_class/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Cargo class successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Cargo class failed to edit");
        });
    };

});
