<?php

$slim->route->get("/report/daily_revenue/:date/:vessel/:route", function($date, $vessel, $route)    {
    global $slim;
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());
    $status = 200;

    $complete = array();
    $voyage = array();
    $ticket = array();
    $baggage = array();
    $cargo = array();
    $upgrade = array();
    $deductions = array();
    $userRevenue = array();

    $sql = "
        SELECT
            voyage.id as id 
        FROM voyage
        LEFT JOIN trip ON voyage.trip = trip.id
        WHERE voyage.departure_date = :departure_date
    ";

    $params_array[':departure_date'] = $date;

    // If vessel is 0 (Indicates Show All)
    if ($vessel)   {
        $sql .= " AND voyage.vessel = :vessel";
        $params_array[':vessel'] = $vessel;
    }

    // If route is 0 (Indicates Show All)
    if ($route)    {
        $sql .= " AND trip.route_id = :route";
        $params_array[':route'] = $route;
    }

    $voyageList = $slim->db->SQL($sql, $params_array);

    // Get Passenger Type List
    $passengerTypeList = $slim->db->readAll('passenger_type', array(
        'where' => 'active = 1'
    ));

    // Get User List
    $userList = $slim->db->readAll('users', array(
        'where' => 'active = 1'
    ));

    // Initialize user keys
    foreach ($userList as $user)    {
        
        // Initialize
        if (!isset($userRevenue[$user['id']]['price_paid']))
            $userRevenue[$user['id']]['price_paid'] = 0;

        // Set user id
        $userRevenue[$user['id']]['user'] = $user['id'];               
    };

    // Loop VoyageList to get number of transactions per voyage on tickets
    foreach($voyageList as $key => $value)  {
        
        // Ticket Collection
        $blacklist = "6,7";
        $sql = "
            SELECT * FROM ticket
            WHERE voyage = :voyage AND status NOT IN ({$blacklist})
        ";

        $voyage[$value['id']] = $slim->db->SQL($sql, array(
            ':voyage' => $value['id']
        ));

        // Initialize a data if empty since it will not go proc the loop if empty
        if (empty($voyage[$value['id']]))
            $complete[$value['id']] = array();

        // Loop by ticket, combine price by seating classes
        foreach ($voyage[$value['id']] as $val) {

            // Generate Revenue Generated Per User
            foreach ($userList as $user)    {
                if ($user['id'] == $val['created_by'])  {
                    $userRevenue[$user['id']]['price_paid'] += $val['price_paid'];
                    $userRevenue[$user['id']]['user'] = $user['id'];
                }                
            };

            if (!isset($ticket[$val['seating_class']][$val['passenger_type']]))
                 $ticket[$val['seating_class']][$val['passenger_type']] = 0; // Initialize

            $ticket[$val['seating_class']][$val['passenger_type']] += $val['price_paid'];

            foreach ($passengerTypeList as $passenger)  {

                // Exclude the ff. results (since these types don't have revenue at all)
                $exclude = array(4);

                if (!in_array($passenger['id'], $exclude))    {
                    if (!isset($complete[$value['id']][$val['seating_class']][$passenger['id']]))  
                        $complete[$value['id']][$val['seating_class']][$passenger['id']] = 0; // Initialize

                    if ($passenger['id'] == $val['passenger_type'])  {
                        $complete[$value['id']][$val['seating_class']][$passenger['id']] += $val['price_paid'];
                    }
                }
            }   
        }

        // Baggage Collection
        $sql = "
            SELECT 
                baggage.price_paid as price_paid,
                baggage.created_by
            FROM baggage
            LEFT JOIN ticket ON baggage.ticket = ticket.id
            WHERE ticket.voyage = :voyage
        ";

        $baggage_list[$value['id']] = $slim->db->SQL($sql, array(
            ':voyage' => $value['id']
        ));

        if (!isset($baggage[$value['id']]))  
            $baggage[$value['id']] = 0; // Initialize

        foreach ($baggage_list[$value['id']] as $baggage_key => $baggage_val)    {

            // Generate Revenue Generated Per User
            foreach ($userList as $user)    {
                if ($user['id'] == $baggage_val['created_by'])  {
                    $userRevenue[$user['id']]['price_paid'] += $baggage_val['price_paid'];
                    break;
                }                
            };

            $baggage[$value['id']] += $baggage_val['price_paid'];
        }

        // Vehicle Rates
        $sql = "
            SELECT 
                *
            FROM waybill
            WHERE voyage = :voyage
        ";

        $cargo_list[$value['id']] = $slim->db->SQL($sql, array(
            ':voyage' => $value['id']
        ));

        if (!isset($cargo[$value['id']]))  
            $cargo[$value['id']] = 0; // Initialize

        foreach ($cargo_list[$value['id']] as $cargo_key => $cargo_val)    {
            // Generate Revenue Generated Per User
            foreach ($userList as $user)    {
                if ($user['id'] == $cargo_val['created_by'])  {
                    $userRevenue[$user['id']]['price_paid'] += $cargo_val['price_paid'];
                    break;
                }                
            };

            $cargo[$value['id']] += $cargo_val['price_paid'];
        }

        // Upgrade Rates
        $sql = "
            SELECT 
                *
            FROM upgrades
            WHERE voyage = :voyage
        ";

        $upgrade_list[$value['id']] = $slim->db->SQL($sql, array(
            ':voyage' => $value['id']
        ));

        if (!isset($upgrade[$value['id']]))  
            $upgrade[$value['id']] = 0; // Initialize

        foreach ($upgrade_list[$value['id']] as $upgrade_key => $upgrade_val)    {
            // Generate Revenue Generated Per User
            foreach ($userList as $user)    {
                if ($user['id'] == $upgrade_val['created_by'])  {
                    $userRevenue[$user['id']]['price_paid'] += $upgrade_val['amt'];
                    break;
                }                
            };

            $upgrade[$value['id']] += $upgrade_val['amt'];
        }

        // Deduction Rates
        $sql = "
            SELECT
                *
            FROM deductions
            WHERE voyage = :voyage
        ";

        $deduction_list[$value['id']] = $slim->db->SQL($sql, array(
            ':voyage' => $value['id']
        ));

        if (!isset($deductions[$value['id']]))
            $deductions[$value['id']] = 0; // Initialize

        foreach ($deduction_list[$value['id']] as $deduction_key => $deduction_val)   {
            // Generate Revenue Generated Per User
            foreach ($userList as $user)    {
                if ($user['id'] == $deduction_val['created_by'])  {
                    $userRevenue[$user['id']]['price_paid'] -= $deduction_val['amt'];
                    break;
                }   
            }

            $deductions[$value['id']] += $deduction_val['amt'];
        }
    }

    $resp['data']['complete'] = $complete;
    $resp['data']['voyage'] = $voyage;
    $resp['data']['ticket'] = $ticket;
    $resp['data']['baggage'] = $baggage;
    $resp['data']['cargo'] = $cargo;
    $resp['data']['upgrade'] = $upgrade;
    $resp['data']['deductions'] = $deductions;
    $resp['data']['userRevenue'] = $userRevenue;

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);

});
