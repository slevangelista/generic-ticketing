<?php

$slim->route->get('/net_cash/request/voyage/:voyage_id/:page/:items', function($voyage_id, $page, $items)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        // Pagination Logic
        $index = 0;
        for($i = 1; $i < $page; $i++) {
            $index = $index + $items;
        }

        $net_cash = $slim->db->readAll('net_cash', array(
            'where' => 'voyage = :voyage_id',
            'order' => 'id DESC',
            'limit' => "{$index}, {$items}",
            'params' => array(
                ':voyage_id' => $voyage_id
            )
        ));

        if ($net_cash)
            $resp['data'] = $net_cash;

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Net Cash request failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);

});

$slim->route->get('/net_cash/request/voyage/:voyage_id/count', function($voyage_id)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        $count = count($slim->db->readAll('net_cash', array(
            'where' => 'voyage = :voyage_id',
            'params' => array(
                ':voyage_id' => $voyage_id
            )
        )));

        $resp['data'] = $count;

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Net Cash request failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);

});