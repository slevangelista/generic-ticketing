<?php

$slim->route->get("/passenger/request/voyage/:voyage", function($voyage)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $sql = "
        SELECT 
            passenger.id,
            passenger.first_name,
            passenger.last_name
        FROM ticket
        LEFT JOIN passenger on ticket.passenger = passenger.id
        WHERE ticket.voyage = :voyage
    ";

    $resp['data'] = $slim->db->SQL($sql, array(
        ':voyage' => $voyage
    ));

    JSONResponse($status, $resp);
});