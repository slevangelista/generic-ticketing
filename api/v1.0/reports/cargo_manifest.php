<?php

	$slim->route->get("/report/cargo_manifest/:voyage_id", function($voyage){

		// Init
		global $slim;
    	$status = "200";
    	$resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    	$vessel_sql = "SELECT voy.id as voyage_id, voy.number as voyage_number,ves.name as vessel_name, ves.code as vessel_code,"
    	            . " ves.gross_register_tonnage, ves.net_register_tonnage, route.name as route_name"
                    . " FROM voyage voy"
					. " INNER JOIN vessel ves ON ves.id = voy.vessel"
					. " INNER JOIN trip ON trip.id = voy.trip"
					. " INNER JOIN route ON route.id = trip.route_id"
					. " AND voy.id = :voyage_id";

		$params = array(':voyage_id' => $voyage);
		$vessel = $slim->db->SQL($vessel_sql,$params);
		$manifest["vessel_details"] = $vessel[0];

		$cargos_sql = "SELECT * FROM cargo"
					. " INNER JOIN waybill ON waybill.cargo = cargo.id"
					. " AND waybill.voyage =  :voyage_id";
		$cargos = $slim->db->SQL($cargos_sql,$params);
		$manifest["cargos"] = $cargos;
    	//echo '<pre>',print_r($manifest,1),'</pre>';
    	 $resp["data"] = $manifest;

    	JSONResponse($status, $resp);


	});
?>