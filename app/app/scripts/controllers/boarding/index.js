'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:BoardingIndexCtrl
 * @description
 * # BoardingIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('BoardingIndexCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

	// Init
	// ---- ---- ---- ----
	$scope.tickets = {};

	// Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Seating Class
    HelperService.getListActive('seating_class').then(function(response)    {
        $scope.seatingClassList = response;
    });

	// Process Methods
	// ---- ---- ---- ----
	$scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

	$scope.init = function()	{

	};

	$scope.update = function()	{
		$scope.getTicketList();
	};

	// Voyage Display
    $scope.onVoyageSelect = function()  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id, vesselId)   {
        $scope.tickets.voyage = id;
        $scope.vesselId = vesselId;
    };

    $scope.deactivateVoyage = function()    {
        $scope.tickets.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.tickets.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.tickets.voyage = null;
    };
    // End voyage select

    $scope.resetInputFields = function()    {
        $scope.tickets.first_name = null;
        $scope.tickets.last_name = null;
    };

    $scope.resetSeriesList = function()    {
        $scope.seriesList = [];
        $scope.selectedSeats = {};
    };

    // Get List (More complex algorithm)
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            var voyageAll = response.data;
            $scope.voyageList = [];

            for (var index in voyageAll)    {
                if (voyageAll[index].status != 10)
                    $scope.voyageList.push(voyageAll[index]);
            };

        });
    };

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    $scope.joinSeatingClassCode = function(id)  {
        return HelperService.join(id, $scope.seatingClassList).code;
    };

    // REST
    $scope.getTicketList = function()	{
    	RESTFactory.get("ticket/request/voyage/" + $scope.tickets.voyage).then(function(response)	{
    		$scope.ticketList = response.data;

    		// Segregate Tickets
    		$scope.segregateTickets();
    	}, function()	{
    		// Do on error
    	});
    };

    /* Segregate Tickets by class */
    $scope.segregateTickets = function()	{
    	$scope.segregatedTicketList = {};
    	for (var index in $scope.ticketList)	{
			
	    	var seatCode = $scope.joinSeatingClassCode($scope.ticketList[index].seating_class);
	    	
	    	/* Init if not yet defined */
	    	if (typeof $scope.segregatedTicketList[seatCode] === 'undefined')
	    		$scope.segregatedTicketList[seatCode] = {};

	    	// All
	    	// Reserved-Paid
			if (typeof $scope.segregatedTicketList[seatCode]['all'] === 'undefined')
	    		$scope.segregatedTicketList[seatCode]['all'] = [];


	    	// Reserved-Paid
			if (typeof $scope.segregatedTicketList[seatCode][2] === 'undefined')
	    		$scope.segregatedTicketList[seatCode][2] = [];

	    	// Checked-In
			if (typeof $scope.segregatedTicketList[seatCode][3] === 'undefined')
	    		$scope.segregatedTicketList[seatCode][3] = [];

			// Boarded	    	
			if (typeof $scope.segregatedTicketList[seatCode][4] === 'undefined')
	    		$scope.segregatedTicketList[seatCode][4] = [];

	    	$scope.segregatedTicketList[seatCode]['all'].push($scope.ticketList[index]);
			$scope.segregatedTicketList[seatCode][$scope.ticketList[index].status].push($scope.ticketList[index]);	    	

    	}
    	// console.log($scope.segregatedTicketList);
    };

});
