<?php

class Stats {

    public function insertStatsData($data){
        global $slim;

        $insert_sql = "
            INSERT INTO `stats_hourly_passenger` (
                `voyage_number`, 
                `trip_code`, 
                `year`, 
                `month`, 
                `day`, 
                `day_name`, 
                `hour`, 
                `passenger_type`, 
                `seating_class`, 
                `ct`, 
                `boarded`, 
                `checked_in`, 
                `refunded`, 
                `cancelled`, 
                `transferred`, 
                `revenue`) 
            VALUES (
                :voyage_number, 
                :trip_code, 
                YEAR(NOW()), 
                MONTH(NOW()), 
                DAY(NOW()), 
                DAYNAME(NOW()), 
                HOUR(NOW()),
                :passenger_type, 
                :seating_class, 
                '1',
                '0', 
                '0', 
                '0', 
                '0', 
                '0', 
                :revenue)
            ON DUPLICATE KEY UPDATE
            ct = ct + 1,
            revenue = revenue + :revenue
        ";

        $slim->db->SQL($insert_sql, array(
            ':voyage_number' => $data["voyage_number"],
            ':trip_code' => $data["trip_code"],
            ':passenger_type' => $data["passenger_type"],
            ':seating_class' => $data["seating_class"],
            ':revenue' => $data["ticket_price"]
        ));
    }

    public static function updateStats($data,$action){
        global $slim;

        if($action == 'boarded'){

            $set_sql = "SET boarded = boarded + 1";
            self::updatePassengerStats($data, $set_sql);
        }

        elseif($action == 'checked_in'){
            $set_sql = "SET checked_in = checked_in + 1";
            self::updatePassengerStats($data, $set_sql);
        }

        elseif($action == 'refunded'){
            $set_sql = "SET refunded = refunded + 1,ct = ct - 1, revenue = revenue - {$data['ticket_price']}";
            self::updatePassengerStats($data, $set_sql);
        }

        elseif($action == 'cancelled'){
            $set_sql = "SET cancelled = cancelled + 1, ct = ct -1, revenue = revenue - {$data['ticket_price']}";
            self::updatePassengerStats($data, $set_sql);
        }

        elseif($action == 'transferred'){
            $set_sql = "SET transferred = transferred + 1, ct = ct - 1, revenue = revenue - {$data['ticket_price']}";
            self::updatePassengerStats($data['old'], $set_sql);
            self::insertStatsData($data['new']);
        }

    }

    function updatePassengerStats($data,$set_sql){
        $update_sql = "UPDATE stats_hourly_passenger "
                    . " $set_sql"
                    . " WHERE voyage_number = :voyage_number"
                    . " AND trip_code = :trip_code"
                    . " AND passenger_type = :passenger_type"
                    . " AND seating_class = :seating_class";

        $slim->db->SQL($update_sql, array(
            ':voyage_number' => $data["voyage_number"],
            ':trip_code' => $data["trip_code"],
            ':passenger_type' => $data["passenger_type"],
            ':seating_class' => $data["seating_class"],
            ':revenue' => $data["ticket_price"]
        ));
    }
}
