'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:ReportsDailyRevenueCtrl
 * @description
 * # ReportsDailyRevenueCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('ReportsDailyRevenueCtrl', function ($scope, RESTFactory, HelperService, toaster, $http) {

    // Initializations
    // ---- ---- ---- ----
    $scope.input = {};

    // Get Lists

    $scope.open = function($event, option) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.open[option] = true;
    };

    // Vessel
    HelperService.getListActive('vessel').then(function(response)    {
        $scope.vesselAll = response;
        $scope.vesselAll.splice(0, 0, {id: 0, name: " -- Show All -- "});
    });
    // Route
    HelperService.getListActive('route').then(function(response)    {
        $scope.routeAll = response;
        $scope.routeAll.splice(0, 0, {id: 0, name: " -- Show All -- "});
    });

    // Process Methods
    // ---- ---- ---- ----
    $scope.getToday = function(option) {
        $scope.input[option] = new Date();
    };

    $scope.onDateChange = function(option)    {
        $scope.input[option] = HelperService.getDate($scope.input[option]);
    };

    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });

        saveAs(blob, "daily_revenue.xls");
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.getDailyRevenue = function()  {
        RESTFactory.get('report/daily_revenue/' + $scope.input.start_date + '/' + $scope.input.end_date + '/' + $scope.input.vessel + '/' + $scope.input.route).then(function(response) {
            $scope.dailyRevenue = response.data;

            /* Turn into array */
            $scope.dailyRevenue.users = _.toArray($scope.dailyRevenue.users);

        }, function(response)   {
            toaster.pop("error", "Error", "No Voyage Found");
        });
    };

});