<?php

$slim->route->get("/report/inclusive_ticket/:voyage/:id", function($voyage, $user_id)	{

	// Init
	global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array(
    	'total_fare' => 0,
        'total_fare_w_diff' => 0,
    ));

    // Init Data
    $seating_class = null;
    $passenger_type = null;
    $is_first_instance = TRUE;

    // Init Counter
    $i = 0;

    // Init $sql
    $sql = null;

    // Get Tickets
    $sql .= "
    	SELECT  
    		ticket.ticket_no,
    		ticket.booking_no,
    		ticket.series_no,
    		passenger_type.name as passenger_type,
            passenger_type.id as passenger_type_id,
            passenger_type.is_promo,
    		seating_class.code as seating_class,
            seating_class.id as seating_class_id,
            route.id as route_id,
    		price_paid
    	FROM ticket
    	LEFT JOIN passenger_type ON ticket.passenger_type = passenger_type.id
    	LEFT JOIN seating_class ON ticket.seating_class = seating_class.id
    	LEFT JOIN voyage ON ticket.voyage = voyage.id
        LEFT JOIN trip ON voyage.trip = trip.id
        LEFT JOIN route ON trip.route_id = route.id
        WHERE voyage = :voyage AND ticket.created_by = :user_id AND ticket.status NOT IN (6,7)
    ";

    // Execute SQL
    $tickets = $slim->db->SQL($sql, array(
    	':voyage' => $voyage,
    	':user_id' => $user_id
    ));

    // Get Upgrades
    $upgrades = $slim->db->readAll('upgrades', array(
        'where' => 'voyage = :voyage AND created_by = :user_id',
        'params' => array(
            ':voyage' => $voyage,
            ':user_id' => $user_id
        )
    ));

    /* Get Ticket Series */
    foreach ($tickets as $key => $value)	{

        /* Check first if rate is set as promo */
        if ($value['is_promo']) {

            /* Get fare differential */

            // Original Fare
            $original_fare_q = $slim->db->read('passenger_fare', array(
                'where' => 'route = :route AND class = :class AND type = :type',
                'params' => array(
                    ':route' => $value['route_id'],
                    ':class' => $value['seating_class_id'],
                    ':type' => 1, // Set static to full fare
                )
            ));
            $original_fare = $original_fare_q['price'];

            // Discounted Fare
            $discounted_fare_q = $slim->db->read('passenger_fare', array(
                'where' => 'route = :route AND class = :class AND type = :type',
                'params' => array(
                    ':route' => $value['route_id'],
                    ':class' => $value['seating_class_id'],
                    ':type' => $value['passenger_type_id']
                )
            ));
            $discounted_fare = $discounted_fare_q['price'];

            // Get differential
            $differential = $original_fare - $discounted_fare;

            // Change Price Paid Value to original
            $value['price_paid'] = $original_fare;

        } else {
            $differential = 0;
        }

        /* Get total fare */
    	$resp['data']['total_fare'] += $value['price_paid'];
        $resp['data']['total_fare_w_diff'] += $value['price_paid'] - $differential;

    	// Load data on init
    	if ($is_first_instance)	{
    		$resp['data']['table'][$i]['ticket_start'] = $value['series_no'];
    		$resp['data']['table'][$i]['ticket_end'] = $value['series_no'];
	   		$resp['data']['table'][$i]['seating_class'] = $value['seating_class'];
			$resp['data']['table'][$i]['passenger_type'] = $value['passenger_type'];

            /* Set Fare */
			$resp['data']['table'][$i]['total_fare'] = $value['price_paid'] + 0; // Removes useless decimal
			$resp['data']['table'][$i]['fare'] = $value['price_paid'] + 0; // Removes useless decimal
			$resp['data']['table'][$i]['no_of_items'] = 1;
		
            /* Set Differential */
            $resp['data']['table'][$i]['differential'] = $differential;
            $resp['data']['table'][$i]['total_differential'] = $differential;
        }
    	// Check if states are the same
    	else if ($seating_class == $value['seating_class'] && $passenger_type == $value['passenger_type'] && $series_end == $value['series_no'])	{
    		$resp['data']['table'][$i]['ticket_end'] = $value['series_no'];

            /* Set Fare */
    		$resp['data']['table'][$i]['total_fare'] = $resp['data']['table'][$i]['total_fare'] + $value['price_paid'];
			$resp['data']['table'][$i]['no_of_items'] += 1;
    	
            /* Set Differential */
            $resp['data']['table'][$i]['total_differential'] += $differential;

        } else {

    		$i++;
    		$resp['data']['table'][$i]['ticket_start'] = $value['series_no'];
    		$resp['data']['table'][$i]['ticket_end'] = $value['series_no'];
	   		$resp['data']['table'][$i]['seating_class'] = $value['seating_class'];
			$resp['data']['table'][$i]['passenger_type'] = $value['passenger_type'];

            /* Set Fare */
			$resp['data']['table'][$i]['total_fare'] = $value['price_paid'] + 0; // Removes useless decimal
			$resp['data']['table'][$i]['fare'] = $value['price_paid'] + 0; // Removes useless decimal
			$resp['data']['table'][$i]['no_of_items'] = 1;
    	
            /* Set Differential */
            $resp['data']['table'][$i]['differential'] = $differential;
            $resp['data']['table'][$i]['total_differential'] = $differential;
        }

    	// Save Row State
    	$is_first_instance = FALSE;
        $series_end = $value['series_no'] + 1;
    	$seating_class = $value['seating_class'];
    	$passenger_type = $value['passenger_type'];

        // Remove ticket end if ticket start = ticket end
        if ($resp['data']['table'][$i]['ticket_start'] == $resp['data']['table'][$i]['ticket_end']) {
            $resp['data']['table'][$i]['ticket_end'] = null;
        }

    }

    /* Get Upgrades */
    foreach ($upgrades as $key => $value)   {
        $i++; // Increment $i
        $resp['data']['table'][$i]['passenger_type'] = 'Upgrades';
        $resp['data']['table'][$i]['ticket_start'] = $value['series_no'];
        $resp['data']['table'][$i]['total_fare'] = $value['amt'] + 0; // Removes useless decimal
        $resp['data']['table'][$i]['fare'] = $value['amt'] + 0; // Removes useless decimal
        $resp['data']['table'][$i]['no_of_items'] = 1;

        // Init
        $resp['data']['table'][$i]['seating_class'] = NULL;
        $resp['data']['table'][$i]['ticket_end'] = NULL;
        $resp['data']['table'][$i]['differential'] = 0;
        $resp['data']['table'][$i]['total_differential'] = 0;
    }

    // echo "<pre>" . print_r($resp['data']['table'], 1) . "</pre>";
    JSONResponse($status, $resp);

});
