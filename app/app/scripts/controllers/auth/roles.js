'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:roleIndexCtrl
 * @description
 * # roleIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('AuthRolesCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.roleItems = {};

    // Pagination
    $scope.roleCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getRolesByPage();
        $scope.getRolesByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.roleItems[data.id] = {
            'id': data.id,
            'name': data.name,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getRolesByPage = function()    {
        RESTFactory.get('roles/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.roleList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getRolesByCount = function()   {
        RESTFactory.get('roles/' + 'request/count').then(function(response) {
            $scope.roleCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addRoles = function() {
        var params = $.param($scope.newRole);
        RESTFactory.post('roles', params).then(function(response)  {
            // Response if success
            $scope.newRole = {};
            $scope.update();
            toaster.pop("success", "Success", "Role successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Role failed to add");
        });
    };  

    $scope.editRoles = function()   {
        var params = $.param($scope.roleItems[$scope.currentIndex]);
        RESTFactory.put('roles/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Role successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Role failed to edit");
        });
    };

});
