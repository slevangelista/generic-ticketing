<?php

$slim->route->get("/passenger_fare/request/voyage/:voyage_id/seating_class/:seating_class_id/passenger_type/:passenger_type_id"
	, function($voyage_id, $seating_class_id, $passenger_type_id)	{
	global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
    		
        // Get voyage
        $voyage = $slim->db->readByPk('voyage', $voyage_id);

        // Get trip from voyage
        $trip = $slim->db->readByPk('trip', $voyage['trip']);

        // Get Price Range
        $passenger_fare = $slim->db->read('passenger_fare', array(
            'where' => 'route = :route AND class = :class AND type = :type',
            'params' => array(
                ':route' => $trip['route_id'],
                ':class' => $seating_class_id,
                ':type' => $passenger_type_id,
            )
        ));

        // Pass passenger fare to response (if there is result)
        if ($passenger_fare)
	        $resp['data'] = $passenger_fare;
	    else
	    	throw new Exception("No passenger fare found");

    } catch (Exception $e) {
    	$status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Passenger fare request failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});