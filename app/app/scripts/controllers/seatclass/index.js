'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:SeatclassIndexCtrl
 * @description
 * # SeatclassIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('SeatclassIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.seatClassItems = {};

    // Pagination
    $scope.seatClassCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getSeatingClassByPage();
        $scope.getSeatingClassByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.seatClassItems[data.id] = {
            'id': data.id,
            'name': data.name,
            'code': data.code,
            'rows': data.rows,
            'cols': data.cols,
            'description': data.description,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getSeatingClassByPage = function()    {
        RESTFactory.get('seating_class/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.seatClassList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getSeatingClassByCount = function()   {
        RESTFactory.get('seating_class/' + 'request/count').then(function(response) {
            $scope.seatClassCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addSeatingClass = function() {
        var params = $.param($scope.newSeat);
        RESTFactory.post('seating_class', params).then(function(response)  {
            // Response if success
            $scope.newSeat = {};
            $scope.update();

            toaster.pop("success", "Success", "Seating Class successfully added");

        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Seating Class failed to add");
        });
    };  

    $scope.editSeatingClass = function()   {
        var params = $.param($scope.seatClassItems[$scope.currentIndex]);
        RESTFactory.put('seating_class/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();

            toaster.pop("success", "Success", "Seating Class successfully edited");

        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Seating Class failed to edit");
        });
    };

});
