<?php

// Autoload Slim Express
require_once "./slim_express/initialize.php";

// Include models
include "./models/session_model.php";

// Include helper classes and functions
include "./helpers/default.php";
include "./helpers/render.php";
include "./helpers/password_hash.php";
include "./helpers/revenue.php";

// Include Authentication
include "./routes/authentication.php";

// Include Reports
include "./reports/inclusive_tickets.php";
include "./reports/tellers_and_pursers.php";
include "./reports/complimentary_tickets.php";
include "./reports/passenger_manifest.php";
include "./reports/inspection_report.php";
include "./reports/daily_revenue.php";
include "./reports/cargo_manifest.php";
include "./reports/traffic_report.php";

// Include your routes here (Load default last)
include "./routes/users.php";
include "./routes/render.php";
include "./routes/sync.php";
include "./routes/net_cash.php";
include "./routes/barcode.php";
include "./routes/shipper.php";
include "./routes/report_html.php";
include "./routes/cargo.php";
include "./routes/seat.php";
include "./routes/permissions.php";
include "./routes/deductions.php";
include "./routes/vessel.php";
include "./routes/revenue.php";
include "./routes/upgrades.php";
include "./routes/seating_class.php";
include "./routes/seat_temp.php";
include "./routes/seat_layout.php";
include "./routes/passenger_fare.php";
include "./routes/baggage.php";
include "./routes/waybill.php";
include "./routes/vessel_template.php";
include "./routes/passenger.php";
include "./routes/voyage.php";
include "./routes/ticket.php";
include "./routes/default.php";

$slim->route->run();
