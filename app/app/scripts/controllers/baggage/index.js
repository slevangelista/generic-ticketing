'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:BaggageListCtrl
 * @description
 * # BaggageListCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('BaggageIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialization
    // ---- ---- ---- ----

    // Pagination
    $scope.baggageCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  


    // Process Methods
    // ---- ---- ---- ----
    $scope.update = function()  {
        $scope.getBaggageList();
        $scope.getBaggageCount();
    };

    $scope.getBaggageIndex = function(id) {
        $scope.currentIndex = id;
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.getBaggageList = function()  {
        RESTFactory.get('baggage/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response) {
            $scope.baggageList = response.data;
        });
    };

    $scope.getBaggageCount = function()  {
        RESTFactory.get('baggage/request/count').then(function(response){
            $scope.baggageCount = response.data;
        });
    };

    // REST Methods
    // ---- ---- ---- ----

    /* Refund */
    $scope.refundBaggage = function()    {
        var params = $.param({
            'status': 6
        });
        RESTFactory.put('baggage/' + $scope.currentIndex, params).then(function() {
            $scope.update();

            // Get Baggage Details (For Revenue Update)
            RESTFactory.get('baggage/' + $scope.currentIndex).then(function(response) {
                // Update Baggage Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.price_paid
                });
                RESTFactory.post('revenue/baggage/refund', revenueParams);
            });
        });
    };

    /* Cancelled */
    $scope.cancelBaggage = function()    {
        var params = $.param({
            'status': 7
        });
        RESTFactory.put('baggage/' + $scope.currentIndex, params).then(function() {
            $scope.update();

            // Get Baggage Details (For Revenue Update)
            RESTFactory.get('baggage/' + $scope.currentIndex).then(function(response) {
                // Update Baggage Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.price_paid
                });
                RESTFactory.post('revenue/baggage/cancel', revenueParams);
            });
        });
    };

});
