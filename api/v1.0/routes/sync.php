<?php

/* Sync Route for Vessel (Since VesselTemplate also needs to be updated) */
$slim->route->put('/sync/vessels/vessel', function()	{

	global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array(
    	'inserted' => 0,
    	'updated' => 0,
    	'unchanged' => 0
    ));

    try {

    	// Define url link
    	$url = 'http://128.199.232.112/api/v1/';
		
		// Get cloud response
    	$response = getCURL($url, 'vessels');
    	
    	// Decode JSON Response
    	$cloud_data = json_decode($response, true);
 
    	// Get local database table
    	$local_data = $slim->db->readAll('vessel');

    	// Loop local data (Set key to primary key)
    	foreach ($local_data as $value)	{
    		foreach ($value as $k => $v)	{
    			$local_data_pk[$value['id']][$k] = $v;
    		}
    	}

    	// Loop cloud data (since this is the `true` data)
    	foreach ($cloud_data['data'] as $value)	{

    		// Loop value to get keys
			foreach ($value as $key => $val)	{
				$cloud_values[$key] = ':' . $key;
				$cloud_params[':' . $key] = $val;
			}

    		// Check if local data key is set (Based on primary key)
    		if ( isset ($local_data_pk[$value['id']]) )	{

    			// Data is set. Check if data is updated
    			if ($value['updated_at'] != $local_data_pk[$value['id']]['updated_at'])	{

    				/* Update dates are not the same. Update local data */
    				
    				// Update data
    				$slim->db->updateByPk('vessel', $value['id'], array(
    					'values' => $cloud_values,
    					'params' => $cloud_params
    				));
    			
    				// Increment updated count
	    			$resp['data']['updated']++;
    			} else {

    				/* No changes found. Increment unchanged response */
    				$resp['data']['unchanged']++;
    			}
    			
    		} else {

    			// Data is not set. Insert data into local database
    			$slim->db->create('vessel', array(
    				'values' => $cloud_values,
    				'params' => $cloud_params
    			), true);

    			// Increment inserted count
    			$resp['data']['inserted']++;

    		}
    	}

    	/* Additional Step: Truncate vessel template and replace it with values in vessel template in cloud */
    	$sql = "
    		TRUNCATE TABLE vessel_template
    	";
    	$slim->db->SQL($sql, array());

    	// Get vessel template from cloud
    	$response_vessel_temp = getCURL($url, 'vesselTemplates');

    	// JSON Decode
    	$vessel_template = json_decode($response_vessel_temp, true);

    	// Loop vessel template
    	foreach ($vessel_template['data'] as $value)	{
			// Loop value to get keys
			foreach ($value as $key => $val)	{
				$cloud_vessel_temp_values[$key] = ':' . $key;
				$cloud_vessel_temp_params[':' . $key] = $val;
			}

			// Insert into vessel template cloud values
			$slim->db->create('vessel_template', array(
				'values' => $cloud_vessel_temp_values,
				'params' => $cloud_vessel_temp_params
			));
    	}

    	// If update is successful, echo response
		$resp['message'] = "vessel successfully updated.";


    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Sync failed on vessel. Error: " . $e->getMessage();
    }

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);


});

/* Sync Route */
$slim->route->put('/sync/:route_cloud/:route_local', function($route_cloud, $route_local)	{

	global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array(
    	'inserted' => 0,
    	'updated' => 0,
    	'unchanged' => 0
    ));

    try {

    	// Define url link
    	$url = 'http://128.199.232.112/api/v1/';
		
		// Get cloud response
    	$response = getCURL($url, $route_cloud);
    	
    	// Decode JSON Response
    	$cloud_data = json_decode($response, true);
 
    	// Get local database table
    	$local_data = $slim->db->readAll($route_local);
 
    	// Loop local data (Set key to primary key)
    	foreach ($local_data as $value)	{
    		foreach ($value as $k => $v)	{
    			$local_data_pk[$value['id']][$k] = $v;
    		}
    	}

    	// Loop cloud data (since this is the `true` data)
    	foreach ($cloud_data['data'] as $value)	{

    		// Loop value to get keys
			foreach ($value as $key => $val)	{
				$cloud_values[$key] = ':' . $key;
				$cloud_params[':' . $key] = $val;
			}

    		// Check if local data key is set (Based on primary key)
    		if ( isset ($local_data_pk[$value['id']]) )	{

    			// Data is set. Check if data is updated
    			if ($value['updated_at'] != $local_data_pk[$value['id']]['updated_at'])	{

    				/* Update dates are not the same. Update local data */
    				
    				// Update data
    				$slim->db->updateByPk($route_local, $value['id'], array(
    					'values' => $cloud_values,
    					'params' => $cloud_params
    				));
    			
    				// Increment updated count
	    			$resp['data']['updated']++;
    			} else {

    				/* No changes found. Increment unchanged response */
    				$resp['data']['unchanged']++;
    			}
    			
    		} else {

    			// Data is not set. Insert data into local database
    			$slim->db->create($route_local, array(
    				'values' => $cloud_values,
    				'params' => $cloud_params
    			));

    			// Increment inserted count
    			$resp['data']['inserted']++;

    		}
    	}

    	// If update is successful, echo response
		$resp['message'] = $route_local . " successfully updated.";


    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Sync failed on {$route_local}. Error: " . $e->getMessage();
    }

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);

});