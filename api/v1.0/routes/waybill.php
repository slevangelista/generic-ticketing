<?php

// Waybill Pagination w/ Join
$slim->route->get("/waybill/:page/:items", function($page, $items)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Pagination Logic
    $index = 0;
    for($i = 1; $i < $page; $i++) {
        $index = $index + $items;
    }

    // Check for custom request
    if ($page == 'request') {
        switch ($items) {
            case 'last_record':
                $resp['data'] = $slim->db->read('waybill', array(
                    'order' => 'id DESC',
                    'limit' => 1
                ));
                break;
            case 'count':  
                $resp['data'] = count($slim->db->readAll('waybill'));
                break;
            default: 
                $status = "404";
                $resp['status'] = 'error';
                $resp['message'] = 'Not Found';
                break;
        }
    } else {
        $sql = "
            SELECT
                waybill.id as id,
                voyage.number as voyage,
                waybill.series_no,
                waybill.cargo_class,
                waybill.price_paid,
                waybill.status,
                waybill.lading_no
            FROM waybill
            LEFT JOIN voyage ON waybill.voyage = voyage.id
            ORDER BY id ASC
            LIMIT {$index}, {$items}
        ";

        $resp['data'] = $slim->db->SQL($sql, array());
    }

    JSONResponse($status, $resp);
});

$slim->route->get('/waybill/voyage/:voyage/:page/:items', function($voyage, $page, $items) use($slim)    {
    $status = 200;
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        // Pagination Logic
        $index = 0;
        for($i = 1; $i < $page; $i++) {
            $index = $index + $items;
        }

        $sql = "
            SELECT
                waybill.id as id,
                waybill.series_no,
                waybill.cargo_class,
                waybill.price_paid,
                waybill.status,
                waybill.lading_no,
                status.name as status_name,
                cargo_class.name as cargo_class_name
            FROM waybill
            LEFT JOIN cargo_class ON waybill.cargo_class = cargo_class.id
            LEFT JOIN status ON waybill.status = status.id
            WHERE waybill.status NOT IN (6,7) AND waybill.voyage = :voyage
            ORDER BY id DESC
            LIMIT {$index}, {$items}
        ";

        $resp['data'] = $slim->db->SQL($sql, array(
            ':voyage' => $voyage
        ));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = 'error';
        $resp['message'] = $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->get('/waybill/count/voyage/:voyage', function($voyage) use ($slim)    {

    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        $resp['data'] = count($slim->db->readAll('waybill', array(
            'where' => 'status NOT IN (6,7) AND voyage = :voyage',
            'params' => array(
                ':voyage' => $voyage
            )
        )));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = 'error';
        $resp['message'] = $e->getMessage();
    }

    JSONResponse($status, $resp);

});

// POST
// Waybill has a special REST POST for appending padding
$slim->route->post("/waybill", function()    {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    if ($data_url = $slim->route->request->getBody())   {
        $data = json_decode($data_url, true);
 
        /* If data is null, parse string */
        if (!$data)
            parse_str($data_url, $data);

        try {

            if (! $data)
                throw new Exception("Data is null");

            /* Reassign print variable */
            $print = $data['print'];
            unset($data['print']);

            foreach ($data as $key => $value) {
                $values[$key] = ':' . $key;
            }

            // Do manual input of params
            $lading_no = getNumberPadding('lading');
            $booking_no = getNumberPadding('booking');

            $slim->db->create('waybill', array(
                'values' => $values,
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':cargo_class' => $data['cargo_class'],
                    ':price_paid' => $data['price_paid'],
                    ':original_price' => $data['original_price'],
                    ':discount' => $data['discount'],
                    ':series_no' => $data['series_no'],
                    ':cargo' => $data['cargo'],
                    ':lading_no' => $lading_no,
                    ':booking_no' => $booking_no,
                    ':created_by' => $data['created_by'],
                    ':on_account' => $data['on_account'],
                )
            ));

            // Update series
            $slim->db->updateByPk('users', $data['created_by'], array(
                'values' => array(
                    'current_series' => ':current_series',
                ),
                'params' => array(
                    ':current_series' => $data['series_no']+1
                )
            ));

            // Print Waybill
            if ($print)
                Render::renderWaybill($lading_no);

            // Update Revenue
            Revenue::updateWaybill(array(
                'voyage' => $data['voyage'],
                'revenue' => $data['price_paid']
            ), 'create');

        } catch (Exception $e)  {
            $status = "400";
            $resp['status'] = 'error';
            $resp['message'] = $e->getMessage();
        }

    } else {
        $status = "400";
        $resp['status'] = 'error';
        $resp['message'] = 'Invalid Request';
    }

    JSONResponse($status, $resp);
});
