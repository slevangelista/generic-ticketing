'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:VoyageIndexCtrl
 * @description
 * # VoyageIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('VoyageIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.VoyageItems = {};
    $scope.VoyageClosed = {};

    // Pagination
    $scope.VoyageCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Get List

    // Vessel List
    HelperService.getListActive('vessel').then(function(response)   {
        $scope.vesselList = response;
    });

    // Status List
    HelperService.getListActive('status').then(function(response)   {
        $scope.statusAll = response;
        $scope.statusList = [];

        for (var index in $scope.statusAll)    {
            if ($scope.statusAll[index].entity_id == 2 && $scope.statusAll[index].id != 8)    {
                $scope.statusList.push($scope.statusAll[index]);
            }
        }
    });

    // Trip List
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Process Methods
    // --- ---- ---- ----
    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.update = function()  {
        $scope.getVoyageByPage();
        $scope.getVoyageByCount();
    };

    $scope.getItems = function(data)    {
        $scope.VoyageItems[data.id] = {
            'id': data.id,
            'number': data.number,
            'vessel': data.vessel,
            'trip': data.trip,
            'departure_date': new Date(data.departure_date),
            'port_officer': data.port_officer,
            'status': data.status,
        };
    };

    $scope.joinVessel = function(id)  {
        return HelperService.join(id, $scope.vesselList).name;
    };

    $scope.joinTrip = function(id) {
        return HelperService.join(id, $scope.tripList).name;
    };

    $scope.joinStatus = function(id) {
        return HelperService.join(id, $scope.statusAll).name;
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    $scope.check = function()   {
        console.log($scope.VoyageItems[$scope.currentIndex]);
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getVoyageByPage = function()    {
        RESTFactory.get('voyage/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.VoyageList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getVoyageByCount = function()   {
        RESTFactory.get('voyage/' + 'request/count').then(function(response) {
            $scope.VoyageCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addVoyage = function() {
        // Change departure_date to proper format
        $scope.newVoyage.departure_date = HelperService.getDate($scope.newVoyage.departure_date);

        // Add capacity and available seats (Determined by vessel)
        for (var index in $scope.vesselList)   {
            if ($scope.vesselList[index].id == $scope.newVoyage.vessel)    {
                $scope.newVoyage.capacity = $scope.vesselList[index].capacity;
                $scope.newVoyage.available_seats = $scope.vesselList[index].capacity;
            }
        }
        // Proceed with POST
        var params = $.param($scope.newVoyage);
        RESTFactory.post('voyage', params).then(function(response)  {
            // Response if success
            $scope.newVoyage = {};
            $scope.update();

            toaster.pop("success", "Success", "Voyage successfully added");

        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Voyage failed to add");
        });
    };  

    $scope.editVoyage = function()   {
        // Change departure_date to proper format
        $scope.VoyageItems[$scope.currentIndex].departure_date = HelperService.getDate($scope.VoyageItems[$scope.currentIndex].departure_date);

        // Proceed with POST
        var params = $.param($scope.VoyageItems[$scope.currentIndex]);
        RESTFactory.put('voyage/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success

            $scope.update();

            toaster.pop("success", "Success", "Voyage successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Voyage failed to edit");
        });
    };

    $scope.cancelVoyage = function()    {
        if ($scope.VoyageItems[$scope.currentIndex].status == 10)   {
            toaster.pop("error", "Error", "Voyage is already closed");
        } else if ($scope.VoyageItems[$scope.currentIndex].status == 9)    {
            toaster.pop("error", "Error", "Voyage has already undergone advanced booking");
        } else if ($scope.VoyageItems[$scope.currentIndex].status == 7)    {
            toaster.pop("error", "Error", "Voyage is already cancelled");
        } else {
            var params = $.param({}); // Just an empty param
            RESTFactory.put("voyage/request/cancel/" + $scope.currentIndex, params).then(function(response) {
                toaster.pop('success', "Success", "Voyage successfully cancelled");

                // Update
                $scope.update();
                
            }, function()   {
                toaster.pop("error", "Error", "Voyage cancellation failed");
            });
        }
    };

    $scope.boardPassengers = function() {
        if ($scope.VoyageItems[$scope.currentIndex].status != 10 && $scope.VoyageItems[$scope.currentIndex].status != 7)   {
            RESTFactory.put('ticket/boardAll/voyage/' + $scope.currentIndex).then(function(response)  {
                toaster.pop("success", "Success", "All passengers have successfully been boarded");
            }, function(response)   {
                toaster.pop("error", "Error", "Passenger boarding fail! Please try again (Contact system administrator should this message persists)");
            });
        } else {
            toaster.pop("error", "Error", "Voyage is already closed");
        }
    };

    $scope.closeVoyage = function() {
        try {
            var status = $scope.VoyageClosed[$scope.currentIndex].status;

            /* Determine voyage close type */
            if (status == 9) { // Adv. Booking Closed

                if ($scope.VoyageItems[$scope.currentIndex].status == 10)   {
                    toaster.pop("error", "Error", "Voyage is already closed");
                } else if ($scope.VoyageItems[$scope.currentIndex].status == 9) {
                    toaster.pop("error", "Error", "Voyage advance booking is already closed");
                } else if ($scope.VoyageItems[$scope.currentIndex].status == 7) {
                    toaster.pop("error", "Error", "Voyage is already cancelled");
                } else  {
                    var params = $.param({}); // Just an empty param
                    RESTFactory.put('ticket/voyage/' + $scope.currentIndex, params).then(function(response) {
                        var voyageParams = $.param({
                            'status': 9
                        });
                        RESTFactory.put('voyage/' + $scope.currentIndex, voyageParams).then(function(resp)  {
                            $scope.update();
                            toaster.pop("success", "Success", "Advanced booking closed for voyage " + $scope.VoyageItems[$scope.currentIndex].number);
                        });
                    }, function(response)   {
                        toaster.pop("error", "Error", "Advanced booking closed failed for voyage " + $scope.VoyageItems[$scope.currentIndex].number + "! Contact system administrator if error persists");
                    });
                }
                
            } else if (status == 10)    {

                if ($scope.VoyageItems[$scope.currentIndex].status == 10)   {
                    toaster.pop("error", "Error", "Voyage is already closed");
                }
                else if ($scope.VoyageItems[$scope.currentIndex].status == 7) {
                    toaster.pop("error", "Error", "Voyage is already cancelled");
                } else {
                    var status_keys = [1,2,3];
                    /* This will be reversed. If a result is received from the API then cancel the close request*/
                    RESTFactory.get('ticket/request/voyage/' + $scope.currentIndex + '/status/' + status_keys).then(function(response)  { // Cancel voyage close request
                        toaster.pop("error", "Error", "Voyage cannot be closed! There are still pending reserved and checked-in tickets for this voyage");
                    }, function(response)   { // If success do this
                        // Proceed with voyage close
                        var params = $.param({
                            'status': 10
                        });
                        RESTFactory.put('voyage/' + $scope.currentIndex, params).then(function(response)    {

                            $scope.update();
                            toaster.pop("success", "Success", "Voyage " + $scope.VoyageItems[$scope.currentIndex].number + " successfully closed!");

                            // Send post request to transfer report to cloud
                            $scope.sendReportToCloud();
                        });

                    });
                }
            }

        } catch (err)   {
            toaster.pop("error", "Error", "Voyage closing failed! Please select a closing option");
        }
    };

    $scope.sendReportToCloud = function()  {
        var reports = [
            //'tellers_and_pursers',
            //'inclusive_tickets',
            //'daily_revenue',
            //'daily_monitoring'
        ];

        for (var index in reports)  {
            RESTFactory.post('html/' + reports[index], {'voyage': $scope.currentIndex}).then(function(response)    {
                // Do nothing
            });
        }
    }

});
