    var addSeatTemp = function(vesselId, latestId)    {
        RESTFactory.get('vessel_template').then(function(vessel_template_resp)  {
            // Phase 1: Get seating classes for this vessel
            var vesselTemplateAll = vessel_template_resp.data;
            var seatingClassPerVessel = [];

            for (var index in vesselTemplateAll)    {
                if (vesselTemplateAll[index].vessel_id == vesselId) {
                    seatingClassPerVessel.push(vesselTemplateAll[index].seating_class_id);
                }
            }

            // Phase 2: Get seat_temp list for this voyage (if any). Disable all found (if any)
            RESTFactory.get('seat_temp').then(function(response)    {
                var seatTempAll = response.data;

                for (var index in seatTempAll)  {
                    if (seatTempAll[index].voyage == latestId) {
                        RESTFactory.delete('seat_temp/' + seatTempAll[index].id);
                    }
                }

                // Phase 3: Get seats w/ respect to seating class
                RESTFactory.get('seat').then(function(seat_resp)    {
                    var seatAll = seat_resp.data;
                    var seatList = [];

                    var params = {}; // Params initialization
                    for (var index in seatingClassPerVessel)    {
                        for (var i in seatAll)  {
                            if (seatAll[i].active == 1 && seatingClassPerVessel[index] == seatAll[i].seating_class)   {
                                params = $.param({
                                    'voyage': latestId,
                                    'seat': seatAll[i].id,
                                    'class': seatAll[i].seating_class,
                                    'seat_name': seatAll[i].name,
                                });

                                RESTFactory.post('seat_temp', params);
                            }
                        }
                    }   
                });        
            });
        });
    };