'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:TicketScanCtrl
 * @description
 * # TicketScanCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('TicketScanCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

    // Initialize Tickets
    // ---- ---- ---- ----
    $scope.ticket = {};
    $scope.seats = {};

    $scope.seatList = {};
    $scope.seatingClassList = {};
    $scope.voyageList = {};

    // Get Lists
    HelperService.getListActive('seating_class').then(function(response)    {
        $scope.seatingClassAll = response;
    });

    HelperService.getListActive('baggage_type').then(function(response) {
        $scope.baggageTypeList = response;
    });

    // Passenger Type
    HelperService.getListActive('passenger_type').then(function(response)   {
        $scope.passengerTypeList = response;
    });

    // Used in baggage
    // ---- ---- ---- ----
    $scope.getBaggagePrice = function(id)   {
        for (var index in $scope.baggageTypeList)   {
            if ($scope.baggageTypeList[index].id == id) {
                $scope.newBaggage.price = $scope.baggageTypeList[index].price;
                break;
            }
        }
    };

    $scope.addBaggage = function()  {
        // Add other details
        var params = $.param({
            'ticket': $scope.baggageIndex,
            'passenger': $scope.passengerId,
            'voyage': $scope.currentTicket.voyage_id,
            'price_paid': $scope.newBaggage.price,
            'series_no': $scope.newBaggage.series_no,
            'baggage_type': $scope.newBaggage.weight,
            'created_by': $rootScope.id,
        });
        RESTFactory.post('baggage', params).then(function() {
            toaster.pop("success", "Success", "Baggage added successfully for passenger " + $scope.ticket.result.passenger);

            // Get Baggage Details (For Revenue Update)
            RESTFactory.get('baggage/request/ticket/' + $scope.baggageIndex).then(function(response) {
                // Update Baggage Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.price_paid
                });
                RESTFactory.post('revenue/baggage/create', revenueParams);
            });
        }, function()   {
            toaster.pop("error", "Error", "Baggage adding failed for passenger " + $scope.ticket.result.passenger);
        });
    };

    $scope.getBaggageIndex = function(id, passengerId)    {
        $scope.baggageIndex = $scope.currentTicket.id;
        $scope.passengerId = $scope.currentTicket.passenger_id;
        $scope.passengerName = $scope.currentTicket.passenger;
    };

    // Process Methods
    // ---- ---- ---- ----
    $scope.focus = function()   {
        $("#ticketInput").focus();
    };

    $scope.scanTicket = function()  {
        $scope.ticket.final = $scope.ticket.init;
        $scope.ticket.init = null;

        $scope.getTicket();
        $scope.focus();
    };

    // For Seat Transfer

    // REST Methods
    // ---- ---- ---- ----
    $scope.updateSeries = function()    {
        if (typeof $rootScope.id === 'undefined')   {
            /* Load session values again, then recall method again */
            RESTFactory.get('session').then(function(response)  {
                $rootScope.id = response.id;
                $rootScope.username = response.username;
                $rootScope.role = response.role;
                $rootScope.fullname = response.fullname;
                $rootScope.current_series = response.current_series;
                $scope.updateSeries();
            });
        } else {
            RESTFactory.get('users/' + $rootScope.id).then(function(response) {
                $scope.series_no = response.data.current_series;
            });
        }
    };
    
    $scope.getTicket = function()   {
        RESTFactory.get('ticket/request/ticket_no/' + $scope.ticket.final).then(function(response)  {
            $scope.ticket.result = response.data;
            $scope.currentTicket = $scope.ticket.result;
            $scope.upgradeCost = 0; // Set upgrade cost (to reset as well)

            // Get total price paid
            $scope.getTotalPricePaid();

            // Save selected seat
            $scope.selectedSeat = $scope.ticket.result.seat_id;

            $scope.getVoyageList();
        }, function()   {
            $scope.ticket.result = null;
        });
    };

    $scope.getTotalPricePaid = function()   {
        $scope.totalPricePaid = ($scope.currentTicket.price_paid*1);
        RESTFactory.get('upgrades/request/ticket/' + $scope.currentTicket.id).then(function(response)   {
            var upgradeList = response.data;

            for (var index in upgradeList)  {
                $scope.totalPricePaid = ($scope.totalPricePaid*1) + (upgradeList[index].amt*1);
            };
        });
    };

    $scope.getVoyageList = function()   {
        // Get date only from create_time
        var date_array = $scope.ticket.result.create_time.split(" ");
        $scope.date = date_array[0];

        RESTFactory.get('voyage/request/date_range/' + $scope.date).then(function(response)  {
            var voyageAll = response.data;
            $scope.voyageList = [];

            for (var index in voyageAll)    {
                // if (voyageAll[index].status != 10)
                    $scope.voyageList.push(voyageAll[index]);

                if (voyageAll[index].id == $scope.ticket.result.voyage_id)  {
                    $scope.currentVoyageStatus = voyageAll[index].status;
                }
            };

            $scope.currentVoyage = $scope.ticket.result.voyage_id;

            // Save selected voyage
            $scope.selectedVoyage = $scope.currentVoyage;

            /* Get ticket and passenger list for current voyage */
            $scope.getTicketTemplate();

            /* Get Seating Class List */
            $scope.getSeatingClassList();
        });
    };

    $scope.getSeatingClassList = function() {
        /* Get Seating Class based from current voyage */
        $scope.seatingClassList = [];

        // First: Get Vessel Template for this voyage
        RESTFactory.get('vessel_template/request/voyage/' + $scope.currentVoyage).then(function(response)   {
            $scope.vesselTemplateList = response.data;

            for(var vesselTemplateIndex in $scope.vesselTemplateList) {
                for (var seatingClassIndex in $scope.seatingClassAll)  {
                    if ($scope.vesselTemplateList[vesselTemplateIndex].seating_class_id == $scope.seatingClassAll[seatingClassIndex].id)  {
                        $scope.seatingClassList.push($scope.seatingClassAll[seatingClassIndex]);
                    }
                }
            }
            $scope.currentSeatingClass = $scope.ticket.result.seating_class_id;

            // Save selected seating class
            if (typeof $scope.selectedSeatingClass === 'undefined')
                $scope.selectedSeatingClass = $scope.currentSeatingClass;

            /* Get Seat List */
            $scope.getSeatTemplate();
        });
    };

    $scope.checkInTicket = function()   {
        var params = $.param({
            'status': 3
        });
        RESTFactory.put('ticket/' + $scope.ticket.result.id, params).then(function() {
            $scope.getTicket();
            $scope.focus();

            toaster.pop("success", "Success", "Ticket " + $scope.ticket.result.ticket_no + " successfully checked-in");

            // Update ticket revenue
            var revenueParams = $.param({
                'voyage': $scope.ticket.result.voyage_id,
                'seating_class': $scope.ticket.result.seating_class_id,
                'passenger_type': $scope.ticket.result.passenger_type_id
            });
            RESTFactory.post('revenue/ticket/checkin', revenueParams);

        }, function()   {
            toaster.pop("error", "Error", "Ticket " + $scope.ticket.result.ticket_no + " check-in failed");
        });
    };

    $scope.boardTicket = function() {
        var params = $.param({
            'status': 4
        });
        RESTFactory.put('ticket/' + $scope.ticket.result.id, params).then(function() {
            $scope.getTicket();
            $scope.focus();

            // Update ticket revenue
            var revenueParams = $.param({
                'voyage': $scope.ticket.result.voyage_id,
                'seating_class': $scope.ticket.result.seating_class_id,
                'passenger_type': $scope.ticket.result.passenger_type_id
            });
            RESTFactory.post('revenue/ticket/board', revenueParams);

            toaster.pop("success", "Success", "Ticket " + $scope.ticket.result.ticket_no + " successfully boarded");
        }, function()   {
            toaster.pop("error", "Error", "Ticket " + $scope.ticket.result.ticket_no + " boarding failed");
        });
    };

    $scope.boardAndPrintTicket = function() {
        var params = $.param({
            'status': 4
        });
        RESTFactory.put('ticket/' + $scope.ticket.result.id, params).then(function() {
            $scope.getTicket();
            $scope.focus();

            // Update ticket revenue
            var revenueParams = $.param({
                'voyage': $scope.ticket.result.voyage_id,
                'seating_class': $scope.ticket.result.seating_class_id,
                'passenger_type': $scope.ticket.result.passenger_type_id
            });
            RESTFactory.post('revenue/ticket/board', revenueParams);

            toaster.pop("success", "Success", "Ticket " + $scope.ticket.result.ticket_no + " successfully boarded");

            RESTFactory.get('renderBoardingPass/' + $scope.ticket.result.ticket_no).then(function() {
                toaster.pop("success", "Success", "Boarding Pass for " + $scope.ticket.result.ticket_no + " successfully printed");
            }, function()   {
                toaster.pop("error", "Error", "Boarding Pass for " + $scope.ticket.result.ticket_no + " failed to print");
            });
        }, function()   {
            toaster.pop("error", "Error", "Ticket " + $scope.ticket.result.ticket_no + " boarding failed");
        });
    };

    /* Used for seat transfer modal */
    $scope.getSeatTemplate = function() {
        getSeatList();
        getTableDimensions($scope.currentSeatingClass); 
        getSeatingTable();
    };

    var getSeatList = function() {
        HelperService.getListActive('seat').then(function(response) {
            $scope.seatList = response;
            var seatAll = response;
            $scope.seats = {};

            for (var index in seatAll)  {
                if (seatAll[index].seating_class == $scope.currentSeatingClass && seatAll[index].active == 1)    {
                    if (_.isEmpty($scope.seats[seatAll[index].x]))
                        $scope.seats[seatAll[index].x] = {};

                    $scope.seats[seatAll[index].x][seatAll[index].y] = {
                        id: seatAll[index].id,
                        name: seatAll[index].name,
                        seating_class: seatAll[index].seating_class,
                        x: seatAll[index].x,
                        y: seatAll[index].y,
                        active: seatAll[index].active,
                    }
                }
            }
        });
    };

    var getTableDimensions = function(id)   {
        var seatingClassList = $scope.seatingClassList;

        for (var index in seatingClassList) {
            if (seatingClassList[index].id == id)   {
                $scope.rows = seatingClassList[index].rows;
                $scope.cols = seatingClassList[index].cols;
            }
        }
    };

    var getSeatingTable = function()    {
        // Reset Value
        $scope.seatRow = [];
        $scope.seatColumn = [];

        // Row
        for (var i=0; i < $scope.rows; i++)
            $scope.seatRow.push(i);

        // Column
        for (var i=0; i < $scope.cols; i++)
            $scope.seatColumn.push(i);
    };

    /* Joins */
    $scope.joinPassengerType = function(id) {
        return HelperService.join(id, $scope.passengerTypeList).name;
    };

    $scope.joinPassenger = function(id) {
        var name = HelperService.join(id, $scope.passengerList).first_name + ' ' + HelperService.join(id, $scope.passengerList).last_name;
        return name;
    };

    $scope.joinVoyage = function(id)    {
        return HelperService.join(id, $scope.voyageList).number;
    };

    $scope.joinSeatingClass = function(id)  {
        return HelperService.join(id, $scope.seatingClassList).code;
    }

    $scope.joinSeats = function(id)  {
        return HelperService.join(id, $scope.seatList).name;
    }

    /* Get Seat Color */
    $scope.getSeatColor = function(x,y)    {
        // Check for x
        if (x in $scope.seats)  {
            // Check for y
            if (y in $scope.seats[x])   {
                    
                var id = $scope.seats[x][y].id;
                var ticket = $scope.joinTicket(id);
                
                if (id == $scope.selectedSeat && $scope.currentVoyage == $scope.selectedVoyage 
                    && $scope.currentSeatingClass == $scope.selectedSeatingClass)  {
                    return "background-color: #5CB85C; color: white;"; // Returns green indicator for selected seat
                }
                else if (ticket != 1)   { // Seat Taken
                    if (ticket.id == $scope.currentTicket.id) 
                        return "background-color: #F0AD4E; color: white;"; // Returns orange indicator for original seat (if changed)
                    else
                        return "background-color: #D9534F; color: white;"; // Red for taken seat
                } else { // Seat not Taken
                    return "background-color: white;";
                }
            }
        }
        return "background-color: white;"; // If nothing is found
    };

    /* Get Seat Title */
    $scope.getSeatTitle = function(x,y) {
        // Check for x
        if (x in $scope.seats)  {
            // Check for y
            if (y in $scope.seats[x])   {
                    
                var id = $scope.seats[x][y].id;
                var ticket = $scope.joinTicket(id);

                if (ticket != 1)   { // Seat Taken
                    return "Ticket No: " + ticket.ticket_no;
                    // + ", Passenger: " + $scope.joinPassenger(ticket.passenger)
                    // 
                } else { // Seat not Taken
                    return "Seat Available";
                }
            }
        }
        return ""; // If nothing is found
    };

    /* REST used for seat indicator */
    $scope.joinTicket = function(seat)  {
        if ($scope.currentVoyage != null) {
            for (var index in $scope.ticketList)  {
                if ($scope.ticketList[index].seat == seat && $scope.ticketList[index].voyage == $scope.currentVoyage)    {
                    return $scope.ticketList[index];
                }
            }
            return 1; // Return something other than null
        } else {
            return 1; // Return something other than null
        }
    };

    $scope.getTicketTemplate = function()   {
        getTicketList();
        getPassengerList();
    };

    var getTicketList = function() {
        RESTFactory.get('ticket/request/voyage/' + $scope.currentVoyage).then(function(response)    {
            $scope.ticketList = response.data;
        });
    };  

    var getPassengerList = function() {
        RESTFactory.get('passenger/request/voyage/' + $scope.currentVoyage).then(function(response)    {
            $scope.passengerList = response.data;
        });
    };  

    /* Display Current Seat Status */
    $scope.displaySelectedSeat = function() {
        return $scope.joinSeats($scope.selectedSeat) + " - " + $scope.joinSeatingClass($scope.selectedSeatingClass) + "@" + $scope.joinVoyage($scope.selectedVoyage);
    };

    $scope.displayCurrentSeat = function()  {
        if (typeof $scope.ticket.result !== 'undefined' && $scope.ticket.result != null)    {
            return $scope.joinSeats($scope.ticket.result.seat_id) + " - " + $scope.joinSeatingClass($scope.ticket.result.seating_class_id) + "@" 
                    + $scope.joinVoyage($scope.ticket.result.voyage_id);
        }
        else    {
            return "";
        }
    };

    /* Seat Transfer Controllers */
    $scope.getSelectedSeat = function(x,y) {
        // Check for x
        if (x in $scope.seats)  {
            // Check for y
            if (y in $scope.seats[x])   {
                  
                var id = $scope.seats[x][y].id;
                var ticket = $scope.joinTicket(id);

                // Check if there is existing ticket for selected seat
                if (ticket != 1 && ticket.id != $scope.currentTicket.id)    { // Seat is taken
                    toaster.pop("error", "Error", "Seat is already taken! Select another seat");
                } else { // Seat is Available
                    $scope.selectedSeat = id;
                    $scope.selectedVoyage = $scope.currentVoyage;
                    $scope.selectedSeatingClass = $scope.currentSeatingClass;

                    /* Get price rate of new seat*/
                    $scope.getPricePaid();
                }
            }
        }
    };

    $scope.getPricePaid = function()    {
        RESTFactory.get('passenger_fare/request/voyage/'+ $scope.selectedVoyage +'/seating_class/' + $scope.selectedSeatingClass 
            + '/passenger_type/' + $scope.currentTicket.passenger_type_id).then(function(response)  {
            $scope.selectedPassengerFare = response.data;
            
            /* Get upgrade cost */
            $scope.getUpgradeCost();
        }, function(response)   {
            toaster.pop("error", "Error", "No passenger fare found for this seat! Please select another seat (Selected seat will revert back to original)");

            /* Revert selected seat to original */
            $scope.selectedSeat = $scope.currentTicket.seat_id;
            $scope.selectedVoyage = $scope.currentTicket.voyage_id;
            $scope.selectedSeatingClass = $scope.currentTicket.seating_class_id;

        });
    };

    $scope.getUpgradeCost = function()  {
        var currentFare = $scope.totalPricePaid;
        var selectedFare = $scope.selectedPassengerFare.price;

        var upgradeCost = (selectedFare*1) - (currentFare*1);

        if (upgradeCost <= 0) // Reduced cost, meaning a downgrade
            $scope.upgradeCost = 0;
        else 
            $scope.upgradeCost = upgradeCost; 
    };

    $scope.transferSeat = function()    {

        /* Check if voyage was transferred */
        if ($scope.ticket.result.voyage_id != $scope.selectedVoyage)    {

            // Update old ticket to cancel
            var oldTicketParam = $.param({
                'status': 7
            })
            RESTFactory.put('ticket/' + $scope.ticket.result.id, oldTicketParam).then(function(response)  {
                // Voyage transfer detected, cancel original ticket and create a new one
                var newTicketParam = $.param({
                    'passenger': $scope.ticket.result.passenger_id,
                    'passenger_type': $scope.ticket.result.passenger_type_id,
                    'voyage': $scope.selectedVoyage,
                    'seat': $scope.selectedSeat,
                    'seating_class': $scope.selectedSeatingClass,
                    'ticket_type': 2,
                    'series_no': $scope.newSeriesNo,
                    'created_by': $rootScope.id
                });
                RESTFactory.post('ticket/request/voyageTransfer', newTicketParam).then(function()   {
                    transferUpdate();
                    $scope.focus();
                    toaster.pop("success", "Success", "Seat transfer success");
                }, function()   {
                    toaster.pop("error", "Error", "Ticket transfer failed! Please try again");
                });

                // Get Ticket Details (For Revenue Update)
                RESTFactory.get('ticket/' + $scope.ticket.result.id).then(function(response) {
                    // Update Ticket Revenue
                    var revenueParams = $.param({
                        voyage: response.data.voyage,
                        seating_class: response.data.seating_class,
                        passenger_type: response.data.passenger_type,
                    });
                    RESTFactory.post('revenue/ticket/cancel', revenueParams);
                });
            });

        } else {
            // Update ticket to new seat values
            var ticketParams = $.param({
                'seat': $scope.selectedSeat,
                'voyage': $scope.selectedVoyage,
                'seating_class': $scope.selectedSeatingClass,
            });

            // No voyage transfer. Update only
            RESTFactory.put('ticket/' + $scope.ticket.result.id, ticketParams).then(function()  {
                transferUpdate();
                $scope.focus();
                toaster.pop("success", "Success", "Seat transfer success");

            }, function()   {
                toaster.pop("error", "Error", "Ticket transfer failed! Please try again");
            });

        }

    };

    $scope.transferSeatAndPrint = function()    {

        /* Check if voyage was transferred */
        if ($scope.ticket.result.voyage_id != $scope.selectedVoyage)    {

            // Update old ticket to cancel
            var oldTicketParam = $.param({
                'status': 7
            })
            RESTFactory.put('ticket/' + $scope.ticket.result.id, oldTicketParam).then(function(response)  {
                // Voyage transfer detected, cancel original ticket and create a new one
                var newTicketParam = $.param({
                    'passenger': $scope.ticket.result.passenger_id,
                    'passenger_type': $scope.ticket.result.passenger_type_id,
                    'voyage': $scope.selectedVoyage,
                    'seat': $scope.selectedSeat,
                    'seating_class': $scope.selectedSeatingClass,
                    'ticket_type': 2,
                    'series_no': $scope.newSeriesNo,
                    'created_by': $rootScope.id
                });
                RESTFactory.post('ticket/request/voyageTransfer', newTicketParam).then(function(response)   {
                    transferUpdate();
                    $scope.focus();
                    toaster.pop("success", "Success", "Seat transfer success");

                    /* Print new ticket */
                    RESTFactory.get('renderTicket/' + response.data);

                }, function()   {
                    toaster.pop("error", "Error", "Ticket transfer failed! Please try again");
                });

                // Get Ticket Details (For Revenue Update)
                RESTFactory.get('ticket/' + $scope.ticket.result.id).then(function(response) {
                    // Update Ticket Revenue
                    var revenueParams = $.param({
                        voyage: response.data.voyage,
                        seating_class: response.data.seating_class,
                        passenger_type: response.data.passenger_type,
                    });
                    RESTFactory.post('revenue/ticket/cancel', revenueParams);
                });
            });

        } else {
            // Update ticket to new seat values
            var ticketParams = $.param({
                'seat': $scope.selectedSeat,
                'voyage': $scope.selectedVoyage,
                'seating_class': $scope.selectedSeatingClass,
            });

            // No voyage transfer. Update only
            RESTFactory.put('ticket/' + $scope.ticket.result.id, ticketParams).then(function()  {
                transferUpdate();
                $scope.focus();
                toaster.pop("success", "Success", "Seat transfer success");

                /* Print new ticket */
                RESTFactory.get('renderTicket/' + $scope.currentTicket.ticket_no);

            }, function()   {
                toaster.pop("error", "Error", "Ticket transfer failed! Please try again");
            });

        }

    };

    var transferUpdate = function() {
        /* Note: Transactions here are intentionally done asynchronously, since synchronization does not matter */

        // Update seat_temp of current seat
        RESTFactory.get('seat_temp/request/voyage/' + $scope.currentTicket.voyage_id + '/seat/' + $scope.currentTicket.seat_id)
        .then(function(response)    {
            var seat_temp = response.data;

            // Update
            var params = $.param({
                'status': 14
            });
            RESTFactory.put('seat_temp/' + seat_temp.id, params);
        });

        // Update seat_temp of new seat
        RESTFactory.get('seat_temp/request/voyage/' + $scope.selectedVoyage + '/seat/' + $scope.selectedSeat)
        .then(function(response)    {
            var seat_temp = response.data;

            // Update
            var params = $.param({
                'status': 15
            });
            RESTFactory.put('seat_temp/' + seat_temp.id, params);
        });

        // Insert Ticket Upgrade
        // Do this if upgrade cost is relevant
        if ($scope.upgradeCost > 0) {
            var upgradeParams = $.param({
                'ticket_id': $scope.currentTicket.id,
                'voyage': $scope.selectedVoyage,
                'from_fare': $scope.currentTicket.price_paid,
                'to_fare': $scope.selectedPassengerFare.price,
                'created_by': $rootScope.id,
                'amt': $scope.upgradeCost
            });
            RESTFactory.post('upgrades', upgradeParams).then(function() {
                // Get Upgrade Details (For Revenue Update)
                RESTFactory.get('upgrades/request/ticket/' + $scope.currentTicket.id).then(function(response) {
                    // Update Upgrade Revenue
                    var revenueParams = $.param({
                        voyage: response.data[response.data.length-1].voyage,
                        price_paid: response.data[response.data.length-1].amt
                    });
                    RESTFactory.post('revenue/upgrades/create', revenueParams);
                });
            });
        }

        // Update ticket revenue
        if ($scope.ticket.result.seat_id != $scope.selectedSeat || $scope.ticket.result.seating_class_id != $scope.selectedSeatingClass || 
            $scope.ticket.result.voyage_id != $scope.selectedVoyage)    {
            var revenueParams = $.param({
                old_seat: $scope.ticket.result.seat_id,
                old_voyage: $scope.ticket.result.voyage_id,
                old_seating_class: $scope.ticket.result.seating_class_id,
                new_seat: $scope.selectedSeat,
                new_voyage: $scope.selectedVoyage,
                new_seating_class: $scope.selectedSeatingClass,
                passenger_type: $scope.ticket.result.passenger_type_id,
                revenue: $scope.ticket.result.price_paid
            });
            RESTFactory.post("revenue/ticket/transfer", revenueParams).then(function() {
                // Promise callback if success       
            });
        }

        // Re-get ticket
        $scope.getTicket();
    }

});
