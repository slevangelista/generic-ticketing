var io = require('socket.io')(3000,function(){
  console.log('Archipelago Websocket Started');
});

io.sockets.on("connection", function(socket) {
    
    // On Connection
    console.log('user connected');

    // On Disconnect
    socket.on("disconnect", function(o) {
        console.log('user left');
    });

    /* Node Server Listener */
    socket.on("server", function(data)  {

        // Check if data.disableAdd is set
        if (!typeof data.disableAdd !== 'undefined')  {

            if (data.disableAdd)    {
                io.sockets.emit('broadcast', data)
            } else {
                io.sockets.emit('broadcast', data)
            }
        }

    });
});