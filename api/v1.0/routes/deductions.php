<?php

$slim->route->get('/deductions/request/voyage/:voyage_id/:page/:items', function($voyage_id, $page, $items)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        // Pagination Logic
        $index = 0;
        for($i = 1; $i < $page; $i++) {
            $index = $index + $items;
        }

        $deductions = $slim->db->readAll('deductions', array(
            'where' => 'voyage = :voyage_id',
            'order' => 'id DESC',
            'limit' => "{$index}, {$items}",
            'params' => array(
                ':voyage_id' => $voyage_id
            )
        ));

        if ($deductions)
            $resp['data'] = $deductions;

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Disburstment request failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);

});

$slim->route->get('/deductions/request/voyage/:voyage_id/count', function($voyage_id)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        $count = count($slim->db->readAll('deductions', array(
            'where' => 'voyage = :voyage_id',
            'params' => array(
                ':voyage_id' => $voyage_id
            )
        )));

        $resp['data'] = $count;

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Disburstment request failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);

});