<?php

/* Routes */
$slim->route->get('/report/daily_revenue/:voyage', function($voyage_id) use ($slim)  {
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());
    $status = 200;

    try {
        
        $sql = "
            SELECT
                voyage.id AS id,
                voyage.trip,
                voyage.number,
                voyage.departure_date,
                trip.departure_time as departure_time,
                route.name as route_name
            FROM voyage
            LEFT JOIN trip ON voyage.trip = trip.id
            LEFT JOIN route ON trip.route_id = route.id
            WHERE voyage.id = :id
        ";

        $voyage = $slim->db->SQL($sql, array(
            ':id' => $voyage_id
        ));

        $resp['data'] = getDailyRevenue($voyage);

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = $e->getMessage();
    }

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);
});

$slim->route->get('/report/daily_revenue/:date_start/:date_end/:vessel/:route', function($date_start, $date_end, $vessel, $route)   {
    global $slim;
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());
    $status = 200;

    try {
        
        /* Check if correct start/end */
        if ( strtotime($date_start) > strtotime($date_end) )
            throw new Exception("Start date must not exceed end date");

        /* Step 1: SQL of voyage based on date range */
        $sql = "
            SELECT
                voyage.id AS id,
                voyage.trip,
                voyage.number,
                voyage.departure_date,
                trip.departure_time as departure_time,
                route.name as route_name
            FROM voyage
            LEFT JOIN trip ON voyage.trip = trip.id
            LEFT JOIN route ON trip.route_id = route.id
            WHERE departure_date >= :date_start AND departure_date <= :date_end
        ";
        $params[':date_start'] = $date_start;
        $params[':date_end'] = $date_end;

        // Step 1.A: Add vessel param (if selected)
        if ($vessel)   {
            $sql .= " AND vessel = :vessel";
            $params[':vessel'] = $vessel;
        }

        // Step 1.B: Add route param (if selected)
        if ($route)    {
            $sql .= " AND trip.route_id = :route";
            $params[':route'] = $route;
        }

        $voyage = $slim->db->SQL($sql, $params);
        $resp['data'] = getDailyRevenue($voyage);

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = $e->getMessage();
    }

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);
});

// Internal Function
function getDailyRevenue($voyage) {
    global $slim;

    /* Step 2: Get Seating Class */
    $seating_class = $slim->db->readAll('seating_class', array(
        'where' => 'active = 1'
    ));

    /* Step 3: Get Passenger Fare */
    $passenger_type = $slim->db->readAll('passenger_type', array(
        'where' => 'id NOT IN (4,7) AND active = 1'
    ));

    // Set blacklist
    $blacklist = "6,7";

    // Init total revenue and discount
    $total_revenue = 0;
    $total_discount_passenger = 0;
    $total_discount_waybill = 0;
    $total_discount = 0;
    $total_discount_waybill = 0;

    // Check if voyage is empty
    if (! $voyage)
        throw new Exception("No voyage found");

    /* Step 4: Loop voyage, use code as key */
    foreach ($voyage as $voyage_key => $voyage_value)  {

        /* Get trip of voyage */
        $trip = $slim->db->readByPk('trip', $voyage_value['trip']);

        // Set voyageList (with voyage id as key)
        $voyageList[$voyage_value['id']] = $voyage_value['id'];

        // Set total revenue per voyage
        if ( !isset ($revenue_voyage[$voyage_value['id']]) )
            $revenue_voyage[$voyage_value['id']] = 0;

        // Get route id for this trip code
        $route = $slim->db->readByPk('trip', $voyage_value['trip']);

        /* Ticket */

        // Loop seating class, use code as key
        foreach ($seating_class as $seating_class_key => $seating_class_value)   {

            // Loop passenger type
            foreach ($passenger_type as $passenger_type_key => $passenger_type_value)   {

                // Get Ticket
                $ticket = $slim->db->read('ticket', array(
                    'select' => 'SUM(price_paid) AS price_paid',
                    'where' => "voyage = :voyage AND seating_class = :seating_class AND passenger_type = :passenger_type AND status NOT IN ({$blacklist})",
                    'params' => array(
                        ':voyage' => $voyage_value['id'],
                        ':seating_class' => $seating_class_value['id'],
                        ':passenger_type' => $passenger_type_value['id']
                    )
                ));
                // Pass revenue to full fare if promo
                if ($passenger_type_value['is_promo'])  {

                    /* Change Passenger Type Value to `Full Fare` */
                    // $passenger_type_value['name'] = "Full Fare";

                    /* Get fare differential */

                    // Original Fare
                    $original_fare_q = $slim->db->read('passenger_fare', array(
                        'where' => 'route = :route AND class = :class AND type = :type',
                        'params' => array(
                            ':route' => $trip['route_id'],
                            ':class' => $seating_class_value['id'],
                            ':type' => 1, // Set static to full fare
                        )
                    ));
                    $original_fare = $original_fare_q['price'];

                    // Discounted Fare
                    $discounted_fare_q = $slim->db->read('passenger_fare', array(
                        'where' => 'route = :route AND class = :class AND type = :type',
                        'params' => array(
                            ':route' => $trip['route_id'],
                            ':class' => $seating_class_value['id'],
                            ':type' => $passenger_type_value['id']
                        )
                    ));

                    $discounted_fare = $discounted_fare_q['price'];

                    // Get ticket promo count
                    $ticket_promo_count = count($slim->db->readAll('ticket', array(
                        'where' => "voyage = :voyage AND seating_class = :seating_class AND passenger_type = :passenger_type AND status NOT IN ({$blacklist})",
                        'params' => array(
                            ':voyage' => $voyage_value['id'],
                            ':seating_class' => $seating_class_value['id'],
                            ':passenger_type' => $passenger_type_value['id']
                        )
                    )));
                    
                    // Get differential
                    $differential = $original_fare - $discounted_fare;

                    // Get discount sum
                    $discount_sum = $ticket_promo_count * $differential;

                    // Get new ticket revenue
                    $ticket['price_paid'] = $ticket['price_paid'] + $discount_sum;

                    // Summarize Discounts
                    if ( !isset ($discount_passenger[$seating_class_value['code']][$voyage_value['id']]) )
                        $discount_passenger[$seating_class_value['code']][$voyage_value['id']] = 0;

                    $discount_passenger[$seating_class_value['code']][$voyage_value['id']] += $discount_sum;

                    // Total Discount Per Class
                    if ( !isset ($discount_passenger[$seating_class_value['code']]['total']) )
                        $discount_passenger[$seating_class_value['code']]['total'] = 0;

                    $discount_passenger[$seating_class_value['code']]['total'] += $discount_sum;

                    // Total Discount Per Voyage
                    if ( !isset ($discount_passenger_per_voyage[$voyage_value['id']]) )
                        $discount_passenger_per_voyage[$voyage_value['id']] = 0;

                    $discount_passenger_per_voyage[$voyage_value['id']] += $discount_sum;

                    // Total Summation
                    $total_discount_passenger += $discount_sum;
                    $total_discount += $discount_sum;

                }

                /* Revenue per voyage, seating class and fare */
                if ( !isset ($revenue_ticket[$seating_class_value['code']][$voyage_value['id']][$passenger_type_value['name']]) )
                    $revenue_ticket[$seating_class_value['code']][$voyage_value['id']][$passenger_type_value['name']] = 0;

                $revenue_ticket[$seating_class_value['code']][$voyage_value['id']][$passenger_type_value['name']] += $ticket['price_paid'];

                /* Revenue per seating class and fare */
                if ( !isset ($revenue_ticket[$seating_class_value['code']]['total'][$passenger_type_value['name']]) )
                    $revenue_ticket[$seating_class_value['code']]['total'][$passenger_type_value['name']] = 0;

                $revenue_ticket[$seating_class_value['code']]['total'][$passenger_type_value['name']] += $ticket['price_paid'];

                /* Total Revenue */
                $revenue_voyage[$voyage_value['id']] += $ticket['price_paid'];
                $total_revenue += $ticket['price_paid'];
            }
        }
    
        /* Waybills */
        $waybill = $slim->db->read('waybill', array(
            'select' => 'SUM(original_price) AS original_price, SUM(price_paid) AS discounted_price',
            'where' => "voyage = :voyage AND status NOT IN ({$blacklist})",
            'params' => array(
                ':voyage' => $voyage_value['id']
            )
        ));
        if ( !isset ($revenue_waybill[$voyage_value['id']]) ) // Per Voyage
            $revenue_waybill[$voyage_value['id']] = 0;
        if ( !isset ($revenue_waybill['total']) ) // Total
            $revenue_waybill['total'] = 0;

        if ( !isset ($discount_waybill[$voyage_value['id']]) ) // Per Voyage
            $discount_waybill[$voyage_value['id']] = 0;
        if ( !isset ($discount_waybill['total']) ) // Total
            $discount_waybill['total'] = 0;

        // Increment   
        $revenue_waybill[$voyage_value['id']] += $waybill['original_price'];
        $revenue_waybill['total'] += $waybill['original_price'];
        $revenue_voyage[$voyage_value['id']] += $waybill['original_price'];
        $total_revenue += $waybill['original_price'];

        // Discount waybill
        $discount_waybill[$voyage_value['id']] += $waybill['original_price'] - $waybill['discounted_price'];
        $discount_waybill['total'] += $waybill['original_price'] - $waybill['discounted_price'];
        $total_discount_waybill += $waybill['original_price'] - $waybill['discounted_price'];
        $revenue_voyage[$voyage_value['id']] -= $waybill['original_price'] - $waybill['discounted_price'];
        $revenue_voyage_no_pax_discount[$voyage_value['id']] = $revenue_voyage[$voyage_value['id']];
        $total_revenue -= $waybill['original_price'] - $waybill['discounted_price'];

        /* Baggage */
        $baggage = $slim->db->read('baggage', array(
            'select' => 'SUM(price_paid) AS price_paid',
            'where' => "voyage = :voyage AND status NOT IN ({$blacklist})",
            'params' => array(
                ':voyage' => $voyage_value['id']
            )
        ));
        if ( !isset ($revenue_baggage[$voyage_value['id']]) ) // Per Voyage
            $revenue_baggage[$voyage_value['id']] = 0;
        if ( !isset ($revenue_baggage['total']) ) // Total
            $revenue_baggage['total'] = 0;

        // Increment   
        $revenue_baggage[$voyage_value['id']] += $baggage['price_paid'];
        $revenue_baggage['total'] += $baggage['price_paid'];
        $revenue_voyage[$voyage_value['id']] += $baggage['price_paid'];
        $total_revenue += $baggage['price_paid'];

        /* Upgrades */
        $upgrades = $slim->db->read('upgrades', array(
            'select' => 'SUM(amt) AS price_paid',
            'where' => "voyage = :voyage AND status NOT IN ({$blacklist})",
            'params' => array(
                ':voyage' => $voyage_value['id']
            )
        ));
        if ( !isset ($revenue_upgrade[$voyage_value['id']]) ) // Per Voyage
            $revenue_upgrade[$voyage_value['id']] = 0;
        if ( !isset ($revenue_upgrade['total']) ) // Total
            $revenue_upgrade['total'] = 0;

        // Increment   
        $revenue_upgrade[$voyage_value['id']] += $upgrades['price_paid'];
        $revenue_upgrade['total'] += $upgrades['price_paid'];
        $revenue_voyage[$voyage_value['id']] += $upgrades['price_paid'];
        $total_revenue += $upgrades['price_paid'];

        /* Deductions */
        $deductions = $slim->db->read('deductions', array(
            'select' => 'SUM(amt) AS price_paid',
            'where' => "voyage = :voyage AND active = 1",
            'params' => array(
                ':voyage' => $voyage_value['id']
            )
        ));

        /* Net Cash */
        $net_cash = $slim->db->read('net_cash', array(
            'select' => 'SUM(amt) AS price_paid',
            'where' => "voyage = :voyage AND active = 1",
            'params' => array(
                ':voyage' => $voyage_value['id']
            )
        ));

        /* Deduct Total Revenue By Discounts */
        if (!isset ($discount_passenger_per_voyage[$voyage_value['id']]) )
            $discount_passenger_per_voyage[$voyage_value['id']] = 0;

        $revenue_voyage[$voyage_value['id']] -= $discount_passenger_per_voyage[$voyage_value['id']];
        $total_revenue -= $discount_passenger_per_voyage[$voyage_value['id']];

    }

    /* Total Revenue (without discounts) */
    $total_revenue_no_deductions = $total_revenue + array_sum($discount_passenger_per_voyage);

    /* Get On-Account Waybill Lists */
    $voyage_implode = implode(", ", $voyageList);
    $sql = "
        SELECT * FROM waybill
        LEFT JOIN cargo on waybill.cargo = cargo.id
        WHERE waybill.voyage IN ({$voyage_implode}) AND waybill.on_account = 1
    ";
    $on_account_waybills = $slim->db->SQL($sql);

    /* Total Revenue Minus On-Account Waybills */
    $sql = "
        SELECT SUM(original_price) AS original_price, SUM(price_paid) AS discounted_price FROM waybill
        LEFT JOIN cargo on waybill.cargo = cargo.id
        WHERE waybill.voyage IN ({$voyage_implode}) AND waybill.on_account = 1 AND status NOT IN ({$blacklist})
    ";
    $on_account_sum = $slim->db->SQL($sql);
    $total_rev_minus_waybills = $total_revenue - $on_account_sum[0]['discounted_price'];

    /* Get revenue per account */
    $sql = "
        SELECT
            users.id as id,
            users.employee_no,
            users.username,
            users.first_name,
            users.last_name,
            users.email,
            roles.name as role,
            users.active
        FROM users
        LEFT JOIN roles ON users.role = roles.id
        WHERE users.active = 1
    ";
    $users_list = $slim->db->SQL($sql);

    foreach ($users_list as $key => $value)  {
        if (!isset ($user_revenue[$value['id']]) ) 
            $user_revenue[$value['id']] = 0;

        // Ticket
        $user_ticket = $slim->db->read('ticket', array(
            'select' => 'SUM(price_paid) as price_paid',
            'where' => "voyage IN ({$voyage_implode}) AND created_by = :user AND status NOT IN ({$blacklist})",
            'params' => array(
                ':user' => $value['id']
            )
        ));

        // Waybill
        $user_waybill = $slim->db->read('waybill', array(
            'select' => "SUM(price_paid) as price_paid",
            'where' => "voyage IN ({$voyage_implode}) AND created_by = :user AND on_account = 0 AND status NOT IN ({$blacklist})",
            'params' => array(
                ':user' => $value['id']
            )
        ));

        // Baggage
        $user_baggage = $slim->db->read('baggage', array(
            'select' => 'SUM(price_paid) as price_paid',
            'where' => "voyage IN ({$voyage_implode}) AND created_by = :user AND status NOT IN ({$blacklist})",
            'params' => array(
                ':user' => $value['id']
            )
        ));

        // Upgrade
        $user_upgrade = $slim->db->read('upgrades', array(
            'select' => "SUM(amt) as price_paid",
            'where' => "voyage IN ({$voyage_implode}) AND created_by = :user AND status NOT IN ({$blacklist})",
            'params' => array(
                ':user' => $value['id']
            )
        ));

        $users[$value['id']]['employee_no'] = $value['employee_no'];
        $users[$value['id']]['role'] = $value['role'];
        $users[$value['id']]['first_name'] = $value['first_name'];
        $users[$value['id']]['last_name'] = $value['last_name'];
        $users[$value['id']]['revenue'] = $user_ticket['price_paid'] + $user_waybill['price_paid'] + $user_baggage['price_paid'] + $user_upgrade['price_paid'];

        // Unset if revenue = 0
        if ($users[$value['id']]['revenue'] <= 0)
            unset($users[$value['id']]);
    }

    /* Get Total Revenue of users */
    $total_users_revenue = 0;
    foreach ($users as $key => $value)  {
        $total_users_revenue = $total_users_revenue + $value['revenue'];
    }

    /* Get Cash Beginning (Net Cash) */
    $cash_on_hand = $slim->db->readAll('net_cash', array(
        'where' => "voyage IN ({$voyage_implode}) AND active = 1"
    ));

    $cash_on_hand_total = 0;
    foreach($cash_on_hand as $key => $value)    {
        $cash_on_hand_total += $value['amt'];
    }
    $cash_on_hand_total += $total_revenue;

    /* Get Disbursment (Deductions) */
    $deductions = $slim->db->readAll('deductions', array(
        'where' => "voyage IN ({$voyage_implode}) AND active = 1"
    ));

    $deductions_total = 0;
    foreach($deductions as $key => $value)    {
        $deductions_total += $value['amt'];
    }
    $deductions_total = $cash_on_hand_total - $deductions_total;

    return array(
        'voyage' => $voyage,
        'passenger_type' => $passenger_type,
        'revenue_ticket' => $revenue_ticket,
        'revenue_upgrade' => $revenue_upgrade,
        'revenue_waybill' => $revenue_waybill,
        'revenue_baggage' => $revenue_baggage,
        'revenue_voyage' => $revenue_voyage,
        'revenue_voyage_no_pax_discount' => $revenue_voyage_no_pax_discount,
        'discount_passenger' => $discount_passenger,
        'discount_waybill' => $discount_waybill,
        'total_discount_passenger' => $total_discount_passenger,
        'total_discount_waybill' => $total_discount_waybill,
        'total_discount' => $total_discount,
        'total_revenue' => $total_revenue,
        'total_revenue_no_deductions' => $total_revenue_no_deductions,
        'on_account_waybills' => $on_account_waybills,
        'total_rev_minus_waybills' => $total_rev_minus_waybills,
        'users' => $users,
        'total_users_revenue' => $total_users_revenue,
        'cash_on_hand' => $cash_on_hand,
        'cash_on_hand_total' => $cash_on_hand_total,
        'deductions' => $deductions,
        'deductions_total' => $deductions_total,

        'discount_passenger_per_voyage' => $discount_passenger_per_voyage
    );

    // var_dump($deductions);    
}