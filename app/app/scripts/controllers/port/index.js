'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:portIndexCtrl
 * @description
 * # portIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('PortCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.portItems = {};

    // Pagination
    $scope.portCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getPortByPage();
        $scope.getPortByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.portItems[data.id] = {
            'id': data.id,
            'name': data.name,
            'code': data.code,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getPortByPage = function()    {
        RESTFactory.get('port/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.portList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getPortByCount = function()   {
        RESTFactory.get('port/' + 'request/count').then(function(response) {
            $scope.portCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addPort = function() {
        var params = $.param($scope.newPort);
        RESTFactory.post('port', params).then(function(response)  {
            // Response if success
            $scope.newPort = {};
            $scope.update();
            toaster.pop("success", "Success", "Port successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Port failed to add");
        });
    };  

    $scope.editPort = function()   {
        var params = $.param($scope.portItems[$scope.currentIndex]);
        RESTFactory.put('port/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Port successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Port failed to edit");
        });
    };

});
