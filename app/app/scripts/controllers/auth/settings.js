'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:AuthAccountsCtrl
 * @description
 * # AuthAccountsCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('AuthSettingsCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

	// Initialize
    // --- ---- ---- ----
    $scope.userItems = {};

    // Get list
    HelperService.getListActive('roles').then(function(response)	{
    	$scope.roleList = response;
    });

    RESTFactory.get('printer').then(function(response)    {
        $scope.printerList = response.data;
    });

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getUser();
    };
    
    $scope.getItems = function()    {
        $scope.userItems = {
            'id': $scope.userAccount.id,
            'role': $scope.userAccount.role,
            'employee_no': $scope.userAccount.employee_no,
            'first_name': $scope.userAccount.first_name,
            'last_name': $scope.userAccount.last_name,
            'email': $scope.userAccount.email,
            'username': $scope.userAccount.username,
            'assigned_printer': $scope.userAccount.assigned_printer,
            'active': $scope.userAccount.active
        };
    };

    // Joins
    $scope.joinRole = function(id)	{
		return HelperService.join(id, $scope.roleList).name;
    };

    $scope.joinPrinter = function(id)  {
        return HelperService.join(id, $scope.printerList).host_name + '@' + HelperService.join(id, $scope.printerList).host_address;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getUser = function()    {
        RESTFactory.get('users/' + $rootScope.id).then(function(response){
            $scope.userAccount = response.data;
            $scope.getItems();
        }, function(response)   {
            toaster.pop('error', "Error", response.data.message);
        });
    };

    $scope.editAccount = function()   {
        var params = $.param($scope.userItems);
        RESTFactory.put('users/' + $rootScope.id, params).then(function(response)  {
            $scope.update();
            toaster.pop('success', "Success", "User successfully updated");
        }, function(response)  {
            toaster.pop('error', "Error", response.data.message);
        });
    };

    $scope.changePassword = function()	{
    	var params = $.param($scope.newPassword);
        RESTFactory.put('users/request/passwordUser/' + $rootScope.id, params).then(function(response)  {
            $scope.newPassword = {};
            $scope.update();

            toaster.pop('success', "Success", "Password has successfully been changed");
        }, function(response)  {
            toaster.pop('error', "Error", response.data.message);
        });
    };

});
