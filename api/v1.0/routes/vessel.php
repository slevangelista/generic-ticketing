<?php

$slim->route->get('/vessel/request/voyage/:voyage_id', function($voyage_id)	{
	global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
    	$sql = "
    		SELECT
    			vessel.id as id,
    			vessel.name as name
    		FROM voyage
    		LEFT JOIN vessel ON voyage.vessel = vessel.id
    		WHERE voyage.id = :voyage_id
    	";

    	$resp['data'] = $slim->db->SQL($sql, array(
    		':voyage_id' => $voyage_id
    	));

    } catch (Exception $e) {
    	$status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Vessel request failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});