<?php

$slim->route->get('/session', function() {
    global $slim;
    $status = "200";

    $db = new SessionModel();
    $session = $db->getSession();
    JSONResponse($status, $session);
});

$slim->route->get('/logout', function() {
    $db = new SessionModel();
    $status = "200";

    $session = $db->destroySession();
    $response["status"] = "info";
    $response["message"] = "Logged out successfully";
    JSONResponse($status, $response);
});

$slim->route->post('/login', function() {

    global $slim;

    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    if ($data_url = $slim->route->request->getBody())   {
        $data = json_decode($data_url, true);
 
        /* If data is null, parse string */
        if (! $data)
            parse_str($data_url, $data);

        /* If data is still null, throw exception */
        if (! $data)
            throw new Exception("Data is null");

        // Check if username/password exists
        verifyRequiredParams(array('username', 'password'), $data);
        
        // Get record based on username
        $user = $slim->db->read('users', array(
            'where' => 'username = :username',
            'params' => array(
                ':username' => $data['username']
            )
        ));

        if ($user)  {
            if (passwordHash::check_password($user['password'], $data['password']))  {

                $resp['message'] = 'Logged in successfully.';
                $resp['data']['username'] = $user['username'];
                $resp['data']['id'] = $user['id'];
                $resp['data']['createdAt'] = $user['created_at'];

                if (!isset($_SESSION)) {
                    session_start();
                }

                /* Pass the ff. data to Session */
                $_SESSION['id'] = $user['id'];
                $_SESSION['username'] = $user['username'];
                $_SESSION['fullname'] = $user['first_name'] . ' ' . $user['last_name'];
                $_SESSION['role'] = $user['role'];
                $_SESSION['current_series'] = $user['current_series'];

            } else {
                $resp['status'] = 'error'; // Status will remain 200. This is not a bad request, just an invalid one
                $resp['message'] = 'Login failed. Incorrect credentials';
            }
        } else {
            $resp['status'] = "error";
            $resp['message'] = 'No such user is registered';
        }

    } else {
        $resp['status'] = 'error';
        $resp['message'] = 'Invalid Request';
    }

    JSONResponse($status, $resp);

});
