<?php

// Get last record
$slim->route->get("/upgrades/request/last_record", function() use ($slim)   {
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        $resp['data'] = $slim->db->read('upgrades', array(
            'order' => 'id DESC',
            'limit' => 1
        ));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Upgrade request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

/* Get count */
$slim->route->get("/upgrades/request/count", function() use ($slim)   {
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        $resp['data'] = count($slim->db->readAll('upgrades'));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Upgrade request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

// Upgrades Pagination
$slim->route->get("/upgrades/:page/:items", function($page, $items)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Pagination Logic
    $index = 0;
    for($i = 1; $i < $page; $i++) {
        $index = $index + $items;
    }

    $sql = "
        SELECT 
            upgrades.id AS id, 
            upgrades.series_no AS series_no,
            voyage.number AS voyage, 
            upgrades.amt AS amt,
            status.name AS status,
            status.id AS status_id
        FROM upgrades
        LEFT JOIN voyage ON upgrades.voyage = voyage.id
        LEFT JOIN status ON upgrades.status = status.id
        ORDER BY id ASC
        LIMIT {$index}, {$items}
    ";

    $resp['data'] = $slim->db->SQL($sql);

    JSONResponse($status, $resp);
});

$slim->route->get('/upgrades/request/ticket/:ticket_id', function($ticket_id)	{
    global $slim;    
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        $upgrades = $slim->db->readAll("upgrades", array(
            'where' => 'ticket_id = :ticket_id',
            'params' => array(
                ':ticket_id' => $ticket_id,
            )
        ));

        if ($upgrades)
        	$resp['data'] = $upgrades;

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Upgrade request failed. Error: " . $e->getMessage();
    }

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);
});

