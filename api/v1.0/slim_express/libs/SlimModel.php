<?php

// SlimModel.php
// Written By: Syd Evangelista
// SlimModel Class for General SQL Data-Handling. Uses PDO-SQL

class SlimModel    {

    private $db;

    // SlimModel: Constructor
    // Connect to database on class instantiate
    // @params data array
    // array params: host(String), database(String), user(String), pass(String)
    public function __construct($data)  {
        try {
            $this->db = new PDO("mysql:host={$data['host']};dbname={$data['database']}", $data['user'], $data['pass']);
        } catch (Exception $e) {
            echo "SlimModel Exception: Unable to connect to database.\n";
            echo "Exception Message: " . $e->getMessage() . "\n";
        }
    }

    // SlimModel: SQL
    // SlimModel Default SQL method
    // @params $sql String, $params array
    public function SQL($sql, $params = array())   {

        // PDO: Prepare SQL Query
        $query = $this->db->prepare($sql);

        // Determine PDO Params
        if (!empty($params))    {
            foreach ($params as $key => $value) {
                $query->bindValue($key, $value);
            }
        }

        // PDO: Execute
        $query->execute();

        // PDO: FetchAll result as ASSOC array
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    // SlimModel: Create
    // SlimModel INSERT DBhandler methods

    // SlimModel: create (public)
    // Creates new record
    // @params $table String, $data array, $debug Boolean
    // array params: select String Default NULL, where String Default NULL, order String Default NULL, params String Default NULL
    public function create($table, $data = array('values' => array(), 'params' => array()), $debug = FALSE)    {
        
        try {

            // Get key => value pair of values
            foreach ($data['values'] as $key => $value)   {
                $fields_array[] = '`' . $key . '`';
                $values_array[] = $value;
            }

            $fields = implode(', ', $fields_array);
            $values = implode(', ', $values_array);

            $sql = "INSERT INTO `{$table}` ({$fields}) VALUES ({$values})";

            // PDO: Prepare SQL Query
            $query = $this->db->prepare($sql);

            // Determine PDO Params
            foreach ($data['params'] as $key => $value) {
                $query->bindValue($key, $value);
            }

            // When $debug is enabled, show SQL Query Syntax
            if ($debug)
                echo "SQL: " . $sql . "<br />\n";

            // PDO: Execute
            $query->execute();
        } catch (Exception $e) {
            echo "SlimModel Exception: Error on create.\n";
            echo "Exception Message: " . $e->getMessage() . "\n";   
        }
    }

    // SlimModel: Read
    // SlimModel SELECT DBhandler methods

    // SlimModel: readAPI (protected)
    // API Function for all public read functions to call
    // @params $table String, $data array, $debug Boolean
    // array params: select String Default NULL, where String Default NULL, order String Default NULL, params String Default NULL
    protected function readAPI($table, $data = array('select' => NULL, 'where' => NULL, 'order' => NULL, 'limit' => NULL, 'params' => array()), $debug = FALSE)  {
        // Set SQL Variable 
        $sql = NULL;

        // Determine SELECT field parameter
        if (empty($data['select']))
            $select = "*";
        else
            $select = $data['select'];

        $sql .= "SELECT {$select} FROM {$table}";

        // Determine other SELECT parameters

            // WHERE clause
            if (!empty($data['where']))
                $sql .= " WHERE " . $data['where'];
            // ORDER clause
            if (!empty($data['order']))
                $sql .= " ORDER BY " . $data['order'];
            // LIMIT clause
            if (!empty($data['limit']))
                $sql .= " LIMIT " . $data['limit'];

        // PDO: Prepare SQL Query
        $query = $this->db->prepare($sql);

        // Determine PDO Params
        if (!empty($data['params']))    {
            foreach ($data['params'] as $key => $value) {
                $query->bindValue($key, $value);
            }
        }

        // PDO: Execute
        $query->execute();

        // When $debug is enabled, show SQL Query Syntax
        if ($debug)
            echo "SQL: " . $sql . "<br />\n";

        return $query;
    }

    // SlimModel: read (public)
    // Read Single Record. Uses PDO::Fetch()
    // @params $table String, $data array, $debug Boolean
    // array params: select String Default NULL, where String Default NULL, order String Default NULL, params String Default NULL
    public function read($table, $data = array('select' => NULL, 'where' => NULL, 'order' => NULL, 'limit' => NULL, 'params' => array()), $debug = FALSE)  {
        try {
            // Call readAPI method
            $query = $this->readAPI($table, $data, $debug);

            // PDO: Fetch result as ASSOC array
            $result = $query->fetch(PDO::FETCH_ASSOC);

            return $result;
        } catch (Exception $e) {
            echo "SlimModel Exception: Error on read.\n";
            echo "Exception Message: " . $e->getMessage() . "\n";
        }
    }

    // SlimModel: readAll (public)
    // Read Single Record. Uses PDO::FetchAll()
    // @params $table String, $data array, $debug Boolean
    // array params: select String Default NULL, where String Default NULL, order String Default NULL, params String Default NULL
    public function readAll($table, $data = array('select' => NULL, 'where' => NULL, 'order' => NULL, 'limit' => NULL, 'params' => array()), $debug = FALSE)  {
        try {
            // Call readAPI method
            $query = $this->readAPI($table, $data, $debug);

            // PDO: FetchAll result as ASSOC array
            $result = $query->fetchAll(PDO::FETCH_ASSOC);

            return $result;
        }
        catch(Exception $e) {
            echo "SlimModel Exception: Error on readAll.\n";
            echo "Exception Message: " . $e->getMessage() . "\n";
        }
    }

    // SlimModel: readyByPk (public)
    // Read Single Record with Primary Key. Uses PDO::Fetch(). Important: Table Primary Key must be `id` in order for this to work
    // @params $table String, $id Int, $debug Boolean
    public function readByPk($table, $id, $debug = FALSE)   {
        try {
            // Call readAPI method
            $query = $this->readAPI($table, array(
                'where' => 'id = :id',
                'params' => array(
                    ':id' => $id,
                )),
                $debug
            );

            // PDO: Fetch result as ASSOC array
            $result = $query->fetch(PDO::FETCH_ASSOC);

            return $result;
        }
        catch(Exception $e) {
            echo "SlimModel Exception: Error on readyByPk.\n";
            echo "Exception Message: " . $e->getMessage() . "\n";
        }
    }

    // SlimModel: Update
    // SlimModel UPDATE DBhandler methods

    // SlimModel: updateAPI (protected)
    // API Function for all public update functions to call
    // @params $table String, $data array, $debug Boolean
    // array params: where String Default NULL, params Array Key->Value Pair, values Array Key->Value Pair
    protected function updateAPI($table, $data = array('where' => NULL, 'values' => array(), 'params' => array()), $debug)  {
        // Checks where and value index values. Throw an exception if they are null

        // Check where
        if (empty($data['where']))
            throw new Exception("Update where clause is null.");
        // Check value
        else if (empty($data['values']))
            throw new Exception("Update values is null.");
        // No exceptions thrown. Proceed
        else {
            
            $where = 'WHERE ' . $data['where'];
            
            // Key refers to field name
            // Value refers to field value
            foreach ($data['values'] as $key => $value) {
                $set_array[] = "`" . $key . "` = " . $value;
            }

            $set = implode(', ', $set_array);

            $sql = "UPDATE `{$table}` SET {$set} {$where}";

            // PDO: Prepare SQL Query
            $query = $this->db->prepare($sql);

            // Determine PDO Params
            if (!empty($data['params']))    {
                foreach ($data['params'] as $key => $value) {
                    $query->bindValue($key, $value);
                }
            }

            // PDO: Execute
            $query->execute();

            // When $debug is enabled, show SQL Query Syntax
            if ($debug)
                echo "SQL: " . $sql . "<br />\n";

            return $query;
        }
    }

    // SlimModel: update (public)
    // Update record based on condition.
    // @params $table String, $data array, $debug Boolean
    // array params: where String Default NULL, params Array Key->Value Pair, values Array Key->Value Pair
    public function update($table, $data = array('where' => NULL, 'values' => array(), 'params' => array()), $debug = FALSE)    {
        try {
            $this->updateAPI($table, $data, $debug);
        } catch (Exception $e) {
            echo "SlimModel Exception: Error on update.\n";
            echo "Exception Message: " . $e->getMessage() . "\n";
        }
    }

    // SlimModel: updateByPk (public)
    // Update model based on primary key. Primary key field MUST be named as 'id' otherwise this won't work
    // @params $table String, $id Int, $data array, $debug Boolean
    // array params: where String Default NULL, params Array Key->Value Pair, values Array Key->Value Pair
    public function updateByPk($table, $id, $data = array('values' => array(), 'params' => array()), $debug = FALSE)    {
        try {
            $data['params'][':id'] = $id;
            $this->updateAPI($table, array('where' => "id = :id", 'values' => $data['values'], 'params' => $data['params']), $debug);
        } catch (Exception $e) {
            echo "SlimModel Exception: Error on updateByPk.\n";
            echo "Exception Message: " . $e->getMessage() . "\n";
        }
    }

    // SlimModel: Delete
    // SlimModel DELETE DBhandler methods

    protected function deleteAPI($table, $data = array('where' => NULL, 'params' => array()), $debug = FALSE)    {
        // Checks where index value. Throw an exception if they are null

        // Check where
        if (empty($data['where']))
            throw new Exception("Delete where clause is null.");
        // No exceptions thrown. Proceed
        else {
            $where = "WHERE " . $data['where'];
            $sql = "DELETE FROM `{$table}` {$where}";

            // PDO: Prepare SQL Query
            $query = $this->db->prepare($sql);

            // Determine PDO Params
            if (!empty($data['params']))    {
                foreach ($data['params'] as $key => $value) {
                    $query->bindValue($key, $value);
                }
            }

            // PDO: Execute
            $query->execute();

            // When $debug is enabled, show SQL Query Syntax
            if ($debug)
                echo "SQL: " . $sql . "<br />\n";
        }
    }

    public function delete($table, $data = array('where' => NULL, 'params' => array()), $debug = FALSE)    {
        try {
            $this->deleteAPI($table, $data, $debug);
        } catch (Exception $e) {
            echo "SlimModel Exception: Error on delete.\n";
            echo "Exception Message: " . $e->getMessage() . "\n";
        }
    }

    public function deleteByPk($table, $id, $debug = FALSE)    {
        try {
            $data['params'][':id'] = $id;
            $this->deleteAPI($table, array('where' => "id = :id", 'params' => $data['params']), $debug);
        } catch (Exception $e) {
            echo "SlimModel Exception: Error on deleteByPk.\n";
            echo "Exception Message: " . $e->getMessage() . "\n";
        }
    }

    public function lastInsertId()  {
        try {
            return $this->db->lastInsertId();
        } catch (Exception $e) {
            echo "SlimModel Exception: Error on lastInsertId.\n";
            echo "Exception Message: " . $e->getMessage() . "\n";
        }
    }

}