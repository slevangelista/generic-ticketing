<?php

$slim->route->get("/seat_layout/request/voyage/:voyage", function($voyage_id) {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        $voyage = $slim->db->readByPk('voyage', $voyage_id);

        $sql = "
            SELECT s.id as seat,s.name as seat_name, s.seating_class as seating_class FROM seat as s,vessel_template as vt WHERE vt.seating_class_id = s.seating_class AND vt.vessel_id = :vessel AND s.id NOT IN(
            SELECT seat FROM ticket WHERE voyage = :voyage AND status NOT IN (5,6,7) AND is_seated = 1
            )
        ";

        $resp['data'] = $slim->db->SQL($sql, array(
            ':voyage' => $voyage_id,
            ':vessel' => $voyage['vessel']
        ));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Seat layout request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});