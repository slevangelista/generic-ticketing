<?php

class SessionModel {

    function __construct()  {

    }

    public function getSession(){
        if (!isset($_SESSION)) {
            session_start();
        }
        $sess = array();
        if(isset($_SESSION['id']))
        {
            $sess["id"] = $_SESSION['id'];
            $sess["username"] = $_SESSION['username'];
            $sess["current_series"] = $_SESSION['current_series'];
            $sess['fullname'] = $_SESSION['fullname'];
            $sess["role"] = $_SESSION['role'];
        }
        else
        {
            $sess["id"] = '';
            $sess["username"] = 'Guest';
            $sess["role"] = null;
            $sess["fullname"] = '';
            $sess["current_series"] = '';
        }
        return $sess;
    }

    public function destroySession(){
        if (!isset($_SESSION)) {
            session_start();
        }

        if(isset($_SESSION['id']))  {
            unset($_SESSION['id']);
            unset($_SESSION['username']);
            unset($_SESSION['role']);
            unset($_SESSION['fullname']);
            unset($_SESSION['current_series']);
            $info='info';
            if(isset($_COOKIE[$info]))
            {
                setcookie ($info, '', time() - $cookie_time);
            }
            $msg="Logged Out Successfully...";
        }
        else{
            $msg = "Not logged in...";
        }
        return $msg;
    }
 
}
