'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:ReportsInspectionReportCtrl
 * @description
 * # ReportsInspectionReportCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
  .controller('ReportsInspectionReportCtrl', function ($scope, RESTFactory, HelperService) {
    
    // Initializations
    // ---- ---- ---- ----
    $scope.waybill = {};

    // Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Shipper
    HelperService.getListActive('shipper').then(function(response) {
        $scope.shipperList = response;
    });

    // Methods
    // ---- ---- ---- ----
    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.update = function()  {
        $scope.getWaybill();
    };

    // Voyage Display
    $scope.onVoyageSelect = function()  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.waybill.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.waybill.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.waybill.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.waybill.voyage = null;
    };
    // End voyage select

    // Get List
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            $scope.voyageList = response.data;
        });
        $scope.voyageList = [];
    };

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    $scope.joinShipper = function(id)   {
        return HelperService.join(id, $scope.shipperList).name;
    };

    // REST
    $scope.getWaybill = function()    {
        RESTFactory.get('report/inspection_report/' + $scope.waybill.voyage).then(function(response){
            $scope.waybill_report = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });

        var filename = "Cargo";
        saveAs(blob, filename+".xls");
    };

  });


