-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 11, 2015 at 07:17 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ticketingDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `baggage`
--

CREATE TABLE IF NOT EXISTS `baggage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voyage` int(11) DEFAULT NULL,
  `series_no` int(11) DEFAULT NULL,
  `passenger` int(11) DEFAULT NULL,
  `ticket` int(11) DEFAULT NULL,
  `waybill` int(11) DEFAULT NULL,
  `price_paid` decimal(20,2) NOT NULL DEFAULT '0.00',
  `baggage_type` tinyint(4) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `passenger_id` (`passenger`),
  KEY `voyage_id` (`voyage`),
  KEY `baggage_type_id` (`baggage_type`),
  KEY `status_id` (`status`),
  KEY `created_by` (`created_by`),
  KEY `ticket_id` (`ticket`),
  KEY `waybill` (`waybill`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `baggage_type`
--

CREATE TABLE IF NOT EXISTS `baggage_type` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `weight` varchar(100) NOT NULL,
  `description` tinytext NOT NULL,
  `price` decimal(20,2) NOT NULL DEFAULT '0.00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `baggage_type`
--

INSERT INTO `baggage_type` (`id`, `weight`, `description`, `price`, `active`, `created_at`, `updated_at`) VALUES
(1, '1kg - 19kg', '', 20.00, 1, '2015-01-28 21:05:50', '2015-01-28 21:05:50'),
(2, '20kg - 40kg', '', 50.00, 1, '2015-01-28 21:05:50', '2015-01-28 21:05:50'),
(3, '40kg and above', '', 100.00, 1, '2015-01-28 21:05:50', '2015-01-28 21:05:50');

-- --------------------------------------------------------

--
-- Table structure for table `cargo`
--

CREATE TABLE IF NOT EXISTS `cargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plate_num` varchar(10) DEFAULT NULL,
  `shipper` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `article_no` varchar(100) DEFAULT NULL,
  `article_desc` tinytext,
  `on_account` tinyint(4) DEFAULT '0',
  `weight` int(11) NOT NULL DEFAULT '0',
  `length` int(11) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `container_no` varchar(10) DEFAULT NULL,
  `container_seal_no` varchar(20) DEFAULT NULL,
  `package_no` varchar(20) DEFAULT NULL,
  `package_kind` varchar(20) DEFAULT NULL,
  `contents` varchar(100) DEFAULT NULL,
  `handling_code` varchar(10) DEFAULT NULL,
  `commodity_code` varchar(20) DEFAULT NULL,
  `volume` float DEFAULT NULL,
  `amount_value` float DEFAULT NULL,
  `consignee` varchar(100) DEFAULT NULL,
  `prepaid_origin` float DEFAULT NULL,
  `payable_destination` float DEFAULT NULL,
  `account_receivable_origin` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cargo_class`
--

CREATE TABLE IF NOT EXISTS `cargo_class` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` tinytext,
  `lane_meter` varchar(50) DEFAULT NULL,
  `bundled_passenger` int(11) NOT NULL DEFAULT '2',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `cargo_class`
--

INSERT INTO `cargo_class` (`id`, `name`, `description`, `lane_meter`, `bundled_passenger`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Tricycle/Motorcycle', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(2, 'Motorcycle 400cc', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(3, 'SUV/Cars', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(4, '4-Wheel Passenger Jeep', '', '', 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(5, '4-Wheeler ELF', '', '', 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(6, '6-Wheel Passenger Jeep', '', '', 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(7, '6-Wheeler ELF', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(8, '6-Wheeler Forward Regular', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(9, '6-Wheeler Forward Long Chassis', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(10, 'Mini Bus', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(11, 'Bus', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(12, '8-Wheeler Truck', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(13, '8-Wheeler Dump Truck', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(14, '10-Wheeler Dump Truck', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(15, '10-Wheeler Truck Drop Side', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(16, '10-Wheeler Truck Wing Van', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(17, '12-Wheeler Truck Drop Side', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(18, '12-Wheeler Truck Wing Van', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(19, '16-Wheeler Truck Wing Van', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(20, 'Extended Trucks', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(21, 'Trailer 40 ft. without load', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(22, 'Trailer 40 ft. with load', '', '', 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(23, '4-Wheeler Multicab', NULL, NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(24, 'Mini Bus (with 20% discount)', '20% discount for 10-25 passengers', NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(25, 'Mini Bus (with 30% discount)', '30% discount for 26 up passengers', NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(26, 'Bus (with 20% discount)', '20% discount for 10-25 passengers', NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(27, 'Bus (with 30% discount)', '20% discount for 26 up passengers', '', 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(28, 'Complimentary Ticket', '100% Discounted Fare', NULL, 2, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29');

-- --------------------------------------------------------

--
-- Table structure for table `cargo_fare_rates`
--

CREATE TABLE IF NOT EXISTS `cargo_fare_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route` int(11) NOT NULL,
  `class` tinyint(4) NOT NULL,
  `lane_meter_rate` int(11) NOT NULL,
  `regular_rate` decimal(20,2) NOT NULL DEFAULT '0.00',
  `discounted_rate` decimal(20,2) NOT NULL DEFAULT '0.00',
  `discount` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `route` (`route`),
  KEY `class` (`class`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `cargo_fare_rates`
--

INSERT INTO `cargo_fare_rates` (`id`, `route`, `class`, `lane_meter_rate`, `regular_rate`, `discounted_rate`, `discount`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, 720.00, 612.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(2, 1, 2, 0, 1000.00, 850.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(3, 1, 3, 0, 1536.00, 1306.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(4, 1, 4, 0, 1700.00, 1445.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(5, 1, 5, 0, 1700.00, 1445.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(6, 1, 6, 0, 1900.00, 1615.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(7, 1, 7, 0, 1900.00, 1615.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(8, 1, 8, 0, 2300.00, 1955.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(9, 1, 9, 0, 2800.00, 2380.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(10, 1, 10, 0, 2300.00, 1955.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(11, 1, 11, 0, 2580.00, 2193.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(12, 1, 12, 0, 3100.00, 2635.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(13, 1, 13, 0, 2800.00, 2380.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(14, 1, 14, 0, 3100.00, 2635.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(15, 1, 15, 0, 3100.00, 2635.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(16, 1, 16, 0, 3400.00, 2890.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(17, 1, 17, 0, 3400.00, 2890.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(18, 1, 18, 0, 3686.00, 3134.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(19, 1, 19, 0, 4368.00, 3713.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(20, 1, 20, 0, 4704.00, 3999.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(21, 1, 21, 0, 10500.00, 8925.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(22, 1, 22, 0, 26500.00, 22525.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(23, 1, 23, 0, 1250.00, 1063.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(24, 2, 1, 0, 720.00, 612.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(25, 2, 2, 0, 1000.00, 850.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(26, 2, 3, 0, 1536.00, 1306.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(27, 2, 4, 0, 1700.00, 1445.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(28, 2, 5, 0, 1700.00, 1445.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(29, 2, 6, 0, 1900.00, 1615.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(30, 2, 7, 0, 1900.00, 1615.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(31, 2, 8, 0, 2300.00, 1955.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(32, 2, 9, 0, 2800.00, 2380.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(33, 2, 10, 0, 2300.00, 1955.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(34, 2, 11, 0, 2580.00, 2193.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(35, 2, 12, 0, 3100.00, 2635.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(36, 2, 13, 0, 2800.00, 2380.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(37, 2, 14, 0, 3100.00, 2635.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(38, 2, 15, 0, 3100.00, 2635.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(39, 2, 16, 0, 3400.00, 2890.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(40, 2, 17, 0, 3400.00, 2890.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(41, 2, 18, 0, 3686.00, 3134.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(42, 2, 19, 0, 4368.00, 3713.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(43, 2, 20, 0, 4704.00, 3999.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(44, 2, 21, 0, 10500.00, 8925.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(45, 2, 22, 0, 26500.00, 22525.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(46, 2, 23, 0, 1250.00, 1063.00, 15, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(49, 1, 24, 0, 2300.00, 1840.00, 20, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(50, 2, 24, 0, 2300.00, 1840.00, 20, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(52, 1, 25, 0, 2300.00, 1610.00, 30, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(53, 2, 25, 0, 2300.00, 1610.00, 30, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(54, 1, 26, 0, 2580.00, 2064.00, 20, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(55, 2, 26, 0, 2580.00, 2064.00, 20, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(56, 1, 27, 0, 2580.00, 1806.00, 30, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(57, 2, 27, 0, 2580.00, 1806.00, 30, 1, '2015-01-28 21:12:50', '2015-01-28 21:12:50'),
(58, 1, 28, 0, 0.00, 0.00, 100, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(59, 2, 28, 0, 0.00, 0.00, 100, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29');

-- --------------------------------------------------------

--
-- Table structure for table `counter`
--

CREATE TABLE IF NOT EXISTS `counter` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `counter`
--

INSERT INTO `counter` (`id`, `name`, `code`, `value`) VALUES
(1, 'booking', 'B', 57524),
(2, 'ticket', 'B', 123323),
(3, 'lading', 'B', 17320),
(4, 'series', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `deductions`
--

CREATE TABLE IF NOT EXISTS `deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voyage` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `amt` decimal(20,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT '1',
  `active` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `net_cash`
--

CREATE TABLE IF NOT EXISTS `net_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voyage` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `amt` varchar(45) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `passenger`
--

CREATE TABLE IF NOT EXISTS `passenger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `age` tinyint(3) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `prefix` varchar(5) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `civil_status` char(1) DEFAULT NULL,
  `nationality` varchar(100) DEFAULT 'Filipino',
  `address` varchar(255) DEFAULT NULL,
  `birth_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `passenger_fare`
--

CREATE TABLE IF NOT EXISTS `passenger_fare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL,
  `route` int(11) NOT NULL,
  `class` tinyint(4) NOT NULL,
  `price` decimal(20,2) NOT NULL DEFAULT '0.00',
  `active` tinyint(4) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class` (`class`),
  KEY `route` (`route`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `passenger_fare`
--

INSERT INTO `passenger_fare` (`id`, `type`, `route`, `class`, `price`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 300.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(2, 1, 1, 2, 250.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(3, 1, 1, 3, 190.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(4, 2, 1, 1, 240.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(5, 2, 1, 2, 200.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(6, 2, 1, 3, 152.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(7, 3, 1, 1, 150.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(8, 3, 1, 2, 125.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(9, 3, 1, 3, 95.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(10, 5, 1, 1, 210.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(11, 5, 1, 2, 190.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(12, 5, 1, 3, 170.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(13, 6, 1, 1, 168.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(14, 6, 1, 2, 152.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(15, 6, 1, 3, 136.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(16, 4, 1, 1, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(17, 4, 1, 2, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(18, 4, 1, 3, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(19, 1, 2, 1, 300.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(20, 1, 2, 2, 250.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(21, 1, 2, 3, 190.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(22, 2, 2, 1, 240.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(23, 2, 2, 2, 200.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(24, 2, 2, 3, 152.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(25, 3, 2, 1, 150.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(26, 3, 2, 2, 125.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(27, 3, 2, 3, 95.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(28, 4, 2, 1, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(29, 4, 2, 2, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(30, 4, 2, 3, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(31, 5, 2, 1, 210.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(32, 5, 2, 2, 190.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(33, 5, 2, 3, 170.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(34, 6, 2, 1, 168.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(35, 6, 2, 2, 152.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(36, 6, 2, 3, 136.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(37, 1, 1, 4, 300.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(38, 1, 1, 5, 250.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(39, 1, 1, 6, 190.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(40, 2, 1, 4, 240.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(41, 2, 1, 5, 200.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(42, 2, 1, 6, 152.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(43, 3, 1, 4, 150.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(44, 3, 1, 5, 125.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(45, 3, 1, 6, 95.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(46, 5, 1, 4, 210.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(47, 5, 1, 5, 190.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(48, 5, 1, 6, 170.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(49, 6, 1, 4, 168.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(50, 6, 1, 5, 152.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(51, 6, 1, 6, 136.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(52, 4, 1, 4, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(53, 4, 1, 5, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(54, 4, 1, 6, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(55, 7, 1, 1, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(56, 7, 1, 2, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(57, 7, 1, 3, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(58, 7, 1, 4, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(59, 7, 1, 5, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(60, 7, 1, 6, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(61, 1, 2, 4, 300.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(62, 1, 2, 5, 250.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(63, 1, 2, 6, 190.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(64, 2, 2, 4, 240.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(65, 2, 2, 5, 200.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(66, 2, 2, 6, 152.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(67, 3, 2, 4, 150.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(68, 3, 2, 5, 125.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(69, 3, 2, 6, 95.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(70, 4, 2, 4, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(71, 4, 2, 5, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(72, 4, 2, 6, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(73, 5, 2, 4, 210.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(74, 5, 2, 5, 190.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(75, 5, 2, 6, 170.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(76, 7, 2, 1, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(77, 7, 2, 2, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(78, 7, 2, 3, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(79, 7, 2, 4, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(80, 7, 2, 5, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29'),
(81, 7, 2, 6, 0.00, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29');

-- --------------------------------------------------------

--
-- Table structure for table `passenger_type`
--

CREATE TABLE IF NOT EXISTS `passenger_type` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `description` tinytext,
  `is_promo` int(11) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `passenger_type`
--

INSERT INTO `passenger_type` (`id`, `name`, `code`, `description`, `is_promo`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Full Fare', 'FF', 'No discount', 0, 1, '2015-01-28 21:03:34', '2015-01-28 21:03:34'),
(2, 'Student/Senior/PWD (20% off)', 'SSPWD', '20% Discounted Rate', 0, 1, '2015-01-28 21:03:34', '2015-01-28 21:03:34'),
(3, 'Childen (50% off)', 'C', '50% Discounted Rate', 0, 1, '2015-01-28 21:03:34', '2015-01-28 21:03:34'),
(4, 'With PASS/Driver/Assistant (100% off)', 'IWPDA', '100% Discounted Rate', 0, 1, '2015-01-28 21:03:34', '2015-01-28 21:03:34'),
(5, 'Special Promo  - Full Fare', 'SP-FF', 'Special Discount Promo for all FastCat Passengers (December 15, 2014 - March 15, 2015', 1, 1, '2015-01-28 21:03:34', '2015-01-28 21:03:34'),
(6, 'Special Promo - Student/Senior/PWD', 'SP-SSPWD', 'Special Discount Promo for all FastCat Passengers (December 15, 2014 - March 15, 2015)', 0, 0, '2015-01-28 21:03:34', '2015-01-28 21:03:34'),
(7, 'Infant', 'I', '', 0, 1, '2015-01-28 21:09:29', '2015-01-28 21:09:29');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) DEFAULT NULL,
  `url_route` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `role`, `url_route`, `created_at`, `updated_at`, `active`) VALUES
(1, 1, '/ticket', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(2, 1, '/auth/accounts', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(3, 1, '/auth/roles', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(4, 1, '/auth/permissions', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(5, 1, '/ticket/scan', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(6, 1, '/ticket/list', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(7, 1, '/cargo', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(8, 1, '/cargo/waybill', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(9, 1, '/seat', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(10, 1, '/deductions', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(11, 1, '/baggage', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(12, 1, '/voyage', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(13, 1, '/trips', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(14, 1, '/port', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(15, 1, '/route', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(16, 1, '/vessel', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(17, 1, '/seat/create', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(18, 1, '/seating_class', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(19, 1, '/passenger_type', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(20, 1, '/passenger_fare', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(21, 1, '/cargo_class', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(22, 1, '/cargo_rate', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(23, 1, '/baggage_type', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(24, 1, '/reports/daily_revenue', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(25, 1, '/reports/inclusive_tickets', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(26, 1, '/reports/tellers_and_pursers', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(27, 1, '/reports/complimentary_tickets', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(28, 1, '/reports/inspection_report', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(29, 1, '/reports/manifest', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(30, 1, '/reports/cargo_manifest', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(48, 1, '/shipper/index', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(49, 1, '/cargo/on_account', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(50, 1, '/printer/index', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(51, 1, '/net_cash/index', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(52, 1, '/baggage/new_baggage', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(53, 1, '/upgrades/index', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(54, 1, '/upgrades/list', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(55, 3, '/ticket', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(56, 3, '/ticket/scan', '2015-02-08 20:22:13', '2015-04-22 05:34:52', 0),
(57, 3, '/ticket/list', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(58, 3, '/upgrades/index', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(59, 3, '/upgrades/list', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(60, 3, '/seat', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(61, 3, '/reports/inclusive_tickets', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(62, 3, '/reports/manifest', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(63, 3, '/reports/cargo_manifest', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(64, 3, '/voyage', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(65, 6, '/ticket/scan', '2015-02-08 20:22:13', '2015-04-22 05:34:52', 1),
(66, 4, '/cargo', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(67, 4, '/cargo/waybill', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(68, 4, '/baggage/new_baggage', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(69, 4, '/baggage', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(70, 4, '/seat', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(71, 4, '/reports/tellers_and_pursers', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(72, 4, '/reports/complimentary_tickets', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(73, 4, '/reports/inspection_report', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(74, 4, '/reports/manifest', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(75, 4, '/reports/cargo_manifest', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(76, 5, '/ticket/list', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(77, 5, '/upgrades/list', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(78, 5, '/cargo/waybill', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(79, 5, '/baggage', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(80, 5, '/net_cash/index', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(81, 5, '/deductions', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(82, 5, '/reports/daily_revenue', '2015-02-09 09:22:13', '2015-02-10 06:25:41', 1),
(83, 1, '/sync', '2015-02-10 11:25:07', '2015-02-10 06:25:41', 1),
(84, 1, '/reports/traffic_report', '2015-03-12 16:03:35', '2015-03-13 00:03:35', 1),
(86, 5, '/seat', '2015-03-12 18:30:15', '2015-03-13 02:30:15', 1),
(87, 5, '/reports/traffic_report', '2015-03-16 14:06:15', '2015-03-16 22:06:15', 1),
(88, 1, '/cargo/create', '2015-03-18 20:28:45', '2015-03-19 04:28:45', 1),
(89, 4, '/cargo/create', '2015-03-25 15:40:42', '2015-03-25 23:40:42', 1),
(91, 6, '/seat', '2015-04-21 21:52:28', '2015-04-22 05:52:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `port`
--

CREATE TABLE IF NOT EXISTS `port` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `active` tinyint(4) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `port`
--

INSERT INTO `port` (`id`, `code`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'BAT', 'BATANGAS', 1, '2015-02-08 20:18:32', '2015-02-10 06:14:20'),
(2, 'CAL', 'CALAPAN', 1, '2015-02-08 20:18:32', '2015-02-10 06:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `printer`
--

CREATE TABLE IF NOT EXISTS `printer` (
  `id` int(11) NOT NULL,
  `host_address` varchar(100) DEFAULT NULL,
  `host_name` varchar(100) DEFAULT NULL,
  `port` varchar(100) DEFAULT NULL,
  `printer` varchar(100) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `printer`
--

INSERT INTO `printer` (`id`, `host_address`, `host_name`, `port`, `printer`, `active`, `created_at`, `updated_at`) VALUES
(0, '127.0.0.1', 'localhost', '631', 'TSC-TDP-225', 1, '2015-02-08 19:54:56', '2015-02-10 06:14:49'),
(1, '172.31.1.117', 'joy', '631', 'TSC-TDP-225', 1, '2015-02-08 19:54:56', '2015-02-10 06:14:49'),
(2, '172.31.1.30', 'neil', '631', 'TSC-TDP-225', 1, '2015-02-08 19:54:56', '2015-02-10 06:14:49'),
(3, '10.20.10.1', 'batangas-server', '631', 'TSC-TDP-225', 1, '2015-02-08 19:54:56', '2015-02-10 06:14:49'),
(4, '10.21.10.1', 'calapan-server', '631', 'TSC-TDP-225', 1, '2015-02-08 19:54:56', '2015-02-13 06:14:49');

-- --------------------------------------------------------

--
-- Table structure for table `revenue_baggage`
--

CREATE TABLE IF NOT EXISTS `revenue_baggage` (
  `voyage` int(11) NOT NULL,
  `year` varchar(45) DEFAULT NULL,
  `month` varchar(45) DEFAULT NULL,
  `day` varchar(45) DEFAULT NULL,
  `day_name` varchar(45) DEFAULT NULL,
  `hour` varchar(45) DEFAULT NULL,
  `ct` int(11) DEFAULT '1',
  `refunded` int(11) DEFAULT '0',
  `cancelled` int(11) DEFAULT '0',
  `revenue` decimal(20,2) DEFAULT NULL,
  PRIMARY KEY (`voyage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `revenue_deductions`
--

CREATE TABLE IF NOT EXISTS `revenue_deductions` (
  `voyage` int(11) NOT NULL,
  `year` varchar(45) DEFAULT NULL,
  `month` varchar(45) DEFAULT NULL,
  `day` varchar(45) DEFAULT NULL,
  `day_name` varchar(45) DEFAULT NULL,
  `hour` varchar(45) DEFAULT NULL,
  `ct` int(11) DEFAULT '1',
  `revenue` decimal(20,2) DEFAULT NULL,
  PRIMARY KEY (`voyage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `revenue_net_cash`
--

CREATE TABLE IF NOT EXISTS `revenue_net_cash` (
  `voyage` int(11) NOT NULL,
  `year` varchar(45) DEFAULT NULL,
  `month` varchar(45) DEFAULT NULL,
  `day` varchar(45) DEFAULT NULL,
  `day_name` varchar(45) DEFAULT NULL,
  `hour` varchar(45) DEFAULT NULL,
  `ct` int(11) DEFAULT '1',
  `revenue` decimal(20,2) DEFAULT NULL,
  PRIMARY KEY (`voyage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `revenue_ticket`
--

CREATE TABLE IF NOT EXISTS `revenue_ticket` (
  `voyage` int(11) NOT NULL,
  `seating_class` int(11) NOT NULL,
  `passenger_type` int(11) NOT NULL,
  `year` int(5) DEFAULT NULL,
  `month` int(3) DEFAULT NULL,
  `day` int(3) DEFAULT NULL,
  `day_name` varchar(20) DEFAULT NULL,
  `hour` int(3) DEFAULT NULL,
  `ct` int(6) DEFAULT '0',
  `boarded` int(6) DEFAULT '0',
  `checked_in` int(6) DEFAULT '0',
  `no_show` varchar(45) DEFAULT NULL,
  `refunded` int(6) DEFAULT '0',
  `cancelled` int(6) DEFAULT '0',
  `transferred_seat` int(6) DEFAULT '0',
  `transferred_seating_class` int(11) DEFAULT NULL,
  `transferred_voyage` int(11) DEFAULT NULL,
  `revenue` decimal(20,2) DEFAULT '0.00',
  PRIMARY KEY (`voyage`,`seating_class`,`passenger_type`),
  UNIQUE KEY `year` (`year`,`month`,`day`,`day_name`,`hour`,`voyage`,`passenger_type`,`seating_class`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `revenue_upgrades`
--

CREATE TABLE IF NOT EXISTS `revenue_upgrades` (
  `voyage` int(11) NOT NULL,
  `year` varchar(45) DEFAULT NULL,
  `month` varchar(45) DEFAULT NULL,
  `day` varchar(45) DEFAULT NULL,
  `day_name` varchar(45) DEFAULT NULL,
  `hour` varchar(45) DEFAULT NULL,
  `ct` int(11) DEFAULT '1',
  `refunded` int(11) NOT NULL DEFAULT '0',
  `cancelled` int(11) NOT NULL DEFAULT '0',
  `revenue` decimal(20,2) DEFAULT NULL,
  PRIMARY KEY (`voyage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `revenue_waybill`
--

CREATE TABLE IF NOT EXISTS `revenue_waybill` (
  `voyage` int(11) NOT NULL,
  `year` varchar(45) DEFAULT NULL,
  `month` varchar(45) DEFAULT NULL,
  `day` varchar(45) DEFAULT NULL,
  `day_name` varchar(45) DEFAULT NULL,
  `hour` varchar(45) DEFAULT NULL,
  `ct` int(11) DEFAULT '1',
  `refunded` int(11) DEFAULT '0',
  `cancelled` int(11) DEFAULT '0',
  `revenue` decimal(20,2) DEFAULT NULL,
  PRIMARY KEY (`voyage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `active` varchar(45) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`, `active`) VALUES
(1, 'Administrator', '2015-02-09 22:31:06', '2015-02-10 19:31:06', '1'),
(2, 'Guest', '2015-02-09 22:31:06', '2015-02-10 19:31:06', '1'),
(3, 'Teller', '2015-02-09 22:31:06', '2015-02-10 19:31:06', '1'),
(4, 'Purser', '2015-02-09 22:31:06', '2015-02-10 19:31:06', '1'),
(5, 'Cashier', '2015-02-09 22:31:06', '2015-02-10 19:31:06', '1'),
(6, 'Inspector', '2015-04-21 21:34:06', '2015-04-22 05:34:06', '1');

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE IF NOT EXISTS `route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_port` int(11) NOT NULL,
  `dest_port` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `active` tinyint(4) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `source_port` (`source_port`),
  KEY `dest_port` (`dest_port`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`id`, `source_port`, `dest_port`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'BATANGAS-CALAPAN', 1, '2015-01-28 20:59:56', '2015-01-28 20:59:56'),
(2, 2, 1, 'CALAPAN-BATANGAS', 1, '2015-01-28 20:59:56', '2015-01-28 20:59:56');

-- --------------------------------------------------------

--
-- Table structure for table `seat`
--

CREATE TABLE IF NOT EXISTS `seat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seating_class` tinyint(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seating_class` (`seating_class`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `seat`
--

INSERT INTO `seat` (`id`, `seating_class`, `name`, `x`, `y`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'BC5', 1, 6, 1, '2015-05-11 11:12:13', NULL),
(2, 1, 'BC6', 1, 7, 1, '2015-05-11 11:12:13', NULL),
(3, 1, 'BC1', 1, 1, 1, '2015-05-11 11:12:13', NULL),
(4, 1, 'BC3', 1, 3, 1, '2015-05-11 11:12:13', NULL),
(5, 1, 'BC2', 1, 2, 1, '2015-05-11 11:12:13', NULL),
(6, 1, 'BC4', 1, 5, 1, '2015-05-11 11:12:13', NULL),
(7, 1, 'BC10', 2, 5, 1, '2015-05-11 11:12:13', NULL),
(8, 1, 'BC7', 2, 1, 1, '2015-05-11 11:12:13', NULL),
(9, 1, 'BC9', 2, 3, 1, '2015-05-11 11:12:13', NULL),
(10, 1, 'BC11', 2, 6, 1, '2015-05-11 11:12:13', NULL),
(11, 1, 'BC8', 2, 2, 1, '2015-05-11 11:12:13', NULL),
(12, 1, 'BC12', 2, 7, 1, '2015-05-11 11:12:13', NULL),
(13, 2, 'PE2', 2, 4, 1, '2015-05-11 11:12:36', NULL),
(14, 2, 'PE3', 3, 4, 1, '2015-05-11 11:12:36', NULL),
(15, 2, 'PE1', 1, 4, 1, '2015-05-11 11:12:36', NULL),
(16, 3, 'E3', 3, 3, 1, '2015-05-11 11:13:05', NULL),
(17, 3, 'E6', 1, 10, 1, '2015-05-11 11:13:05', NULL),
(18, 3, 'E2', 2, 2, 1, '2015-05-11 11:13:05', NULL),
(19, 3, 'E5', 2, 9, 1, '2015-05-11 11:13:05', NULL),
(20, 3, 'E1', 1, 1, 1, '2015-05-11 11:13:05', NULL),
(21, 3, 'E4', 3, 8, 1, '2015-05-11 11:13:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `seating_class`
--

CREATE TABLE IF NOT EXISTS `seating_class` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(10) NOT NULL,
  `description` text,
  `rows` int(11) NOT NULL,
  `cols` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `seating_class`
--

INSERT INTO `seating_class` (`id`, `name`, `code`, `description`, `rows`, `cols`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Business Class', 'BC', 'M1 & M2', 11, 9, 1, '2015-01-28 21:02:30', '2015-01-28 21:02:30'),
(2, 'Premium Economy', 'PE', 'M1 & M2', 22, 9, 1, '2015-01-28 21:02:30', '2015-01-28 21:02:30'),
(3, 'Economy', 'E', 'M1 & M2', 29, 12, 1, '2015-01-28 21:02:30', '2015-01-28 21:02:30');

-- --------------------------------------------------------

--
-- Table structure for table `shipper`
--

CREATE TABLE IF NOT EXISTS `shipper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` tinytext,
  `color` varchar(20) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `entity_id`, `name`, `description`, `color`, `active`) VALUES
(1, 1, 'Reserved', 'The booking is complete and has locked out further bookings for the same seat. No Payment is associated with this booking.', '#FFCC33', 1),
(2, 1, 'Reserved (Paid)', 'The booking has been completed, reserved, and a full payment has been received.', '#3366FF', 1),
(3, 1, 'Checked-In', 'Already Checked-In . ', '#66CC00', 1),
(4, 1, 'Boarded', 'Passenger already boarded', '#FF0066', 1),
(5, 1, 'No Show', 'The booking has been canceled. Locked seat assignment has been removed.', '#FF6666', 1),
(6, 1, 'Refunded', NULL, '#FF6666', 1),
(7, 1, 'Canceled', NULL, '#FF6666', 1),
(8, 2, 'Open', NULL, '', 1),
(9, 2, 'Advance Booking Closed', NULL, '', 1),
(10, 2, 'Voyage Closed', NULL, '', 1),
(11, 3, 'Paid', 'Excess baggage has been paid.', '#3366FF', 1),
(12, 3, 'Refunded', 'Excess baggage has been refunded.', '#FF6666', 1),
(13, 3, 'Canceled', 'Excess baggage has been canceled.', '#FF6666', 1),
(14, 4, 'Open', 'Open Seat', '', 1),
(15, 4, 'Reserved', 'Seat is reserved', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `passenger` int(11) DEFAULT NULL,
  `voyage` int(11) NOT NULL,
  `seat` int(11) DEFAULT NULL,
  `seating_class` tinyint(4) NOT NULL,
  `ticket_no` varchar(100) NOT NULL,
  `series_no` varchar(100) NOT NULL,
  `booking_no` varchar(100) NOT NULL,
  `ticket_type` tinyint(4) NOT NULL,
  `passenger_type` tinyint(4) NOT NULL,
  `price_paid` decimal(20,2) NOT NULL DEFAULT '0.00',
  `status` int(11) NOT NULL DEFAULT '2',
  `is_seated` tinyint(4) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `passenger` (`passenger`),
  KEY `voyage` (`voyage`),
  KEY `seat` (`seat`),
  KEY `seating_class` (`seating_class`),
  KEY `ticket_type` (`ticket_type`),
  KEY `status` (`status`),
  KEY `ticket_no` (`ticket_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trip`
--

CREATE TABLE IF NOT EXISTS `trip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `departure_time` time NOT NULL,
  `arrival_time` time NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trip`
--

INSERT INTO `trip` (`id`, `route_id`, `name`, `departure_time`, `arrival_time`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sample Trip', '19:00:00', '21:00:00', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `upgrades`
--

CREATE TABLE IF NOT EXISTS `upgrades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) DEFAULT NULL,
  `series_no` int(11) DEFAULT NULL,
  `voyage` int(11) DEFAULT NULL,
  `from_fare` varchar(100) DEFAULT NULL,
  `to_fare` varchar(100) DEFAULT NULL,
  `amt` decimal(20,2) DEFAULT '0.00',
  `status` int(11) DEFAULT '2',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `voyage` (`voyage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_no` varchar(100) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(128) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `current_series` int(11) DEFAULT '1',
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role` int(11) DEFAULT '2',
  `assigned_printer` int(11) DEFAULT '3',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employee_no`, `username`, `password`, `first_name`, `last_name`, `email`, `current_series`, `lastvisit_at`, `role`, `assigned_printer`, `active`, `created_at`, `updated_at`) VALUES
(1, '1001', 'admin', '$2a$10$98ef9e2080c794f9d4e90e/3bm.5rofJHYs1lNn.OZ9qb.thdXIkC', 'Administrator', 'Pogi', 'admin@imperium.ph', 24, '2015-01-01 11:15:14', 1, 4, 1, '2013-05-22 05:15:20', '2015-02-10 06:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `vessel`
--

CREATE TABLE IF NOT EXISTS `vessel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `capacity` int(11) NOT NULL DEFAULT '0',
  `gross_register_tonnage` float DEFAULT NULL,
  `net_register_tonnage` float DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `vessel`
--

INSERT INTO `vessel` (`id`, `name`, `code`, `capacity`, `gross_register_tonnage`, `net_register_tonnage`, `created_at`, `updated_at`, `active`) VALUES
(1, 'Sample Vessel', 'SV', 48, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vessel_template`
--

CREATE TABLE IF NOT EXISTS `vessel_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vessel_id` int(11) NOT NULL,
  `seating_class_id` int(11) NOT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `vessel_template`
--

INSERT INTO `vessel_template` (`id`, `vessel_id`, `seating_class_id`, `active`) VALUES
(1, 1, 3, 1),
(2, 1, 2, 1),
(3, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `voyage`
--

CREATE TABLE IF NOT EXISTS `voyage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(100) NOT NULL,
  `vessel` int(11) NOT NULL,
  `trip` int(11) DEFAULT NULL,
  `departure_date` date NOT NULL,
  `capacity` int(11) NOT NULL,
  `available_seats` int(11) NOT NULL,
  `port_officer` text,
  `status` int(11) NOT NULL DEFAULT '8',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `number` (`number`),
  KEY `status` (`status`),
  KEY `vessel` (`vessel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `waybill`
--

CREATE TABLE IF NOT EXISTS `waybill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cargo` int(11) NOT NULL,
  `voyage` int(11) NOT NULL,
  `cargo_class` tinyint(4) NOT NULL,
  `lading_no` varchar(100) NOT NULL,
  `series_no` varchar(100) NOT NULL,
  `booking_no` varchar(100) NOT NULL,
  `waybill_type` tinyint(4) NOT NULL DEFAULT '2',
  `price_paid` decimal(20,2) NOT NULL,
  `original_price` decimal(20,2) NOT NULL,
  `discount` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `on_account` tinyint(4) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `client` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lading_no` (`lading_no`),
  KEY `created_by` (`created_by`),
  KEY `status_id` (`status`),
  KEY `cargo_id` (`cargo`),
  KEY `voyage_id` (`voyage`),
  KEY `cargo_class_id` (`cargo_class`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `seat`
--
ALTER TABLE `seat`
  ADD CONSTRAINT `seat_ibfk_1` FOREIGN KEY (`seating_class`) REFERENCES `seating_class` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
