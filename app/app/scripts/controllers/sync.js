'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:SyncCtrl
 * @description
 * # SyncCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('SyncCtrl', function ($scope, RESTFactory, HelperService, toaster) {

	// Initializations
	// ---- ---- ---- ----
	$scope.syncTable = [
		{'cloud': 'baggageTypes', 'local': 'baggage_type'},
		{'cloud': 'cargoClasses', 'local': 'cargo_class'},
		{'cloud': 'cargoFareRates', 'local': 'cargo_fare_rates'},
		{'cloud': 'passengerFares', 'local': 'passenger_fare'},
		{'cloud': 'passengerTypes', 'local': 'passenger_type'},
		{'cloud': 'permissions', 'local': 'permissions'},
		{'cloud': 'ports', 'local': 'port'},
		{'cloud': 'printers', 'local': 'printer'},
		{'cloud': 'routes', 'local': 'route'},
		{'cloud': 'roles', 'local': 'roles'},
		{'cloud': 'seatingClasses', 'local': 'seating_class'},
		{'cloud': 'seats', 'local': 'seat'},
		{'cloud': 'trips', 'local': 'trip'},  
		{'cloud': 'accounts', 'local': 'users'},
		{'cloud': 'vessels', 'local': 'vessel'}
	];
	$scope.isSync = false;

	// REST Methods
	// ---- ---- ---- ----
	$scope.startSync = function()	{

		// Start sync status
		$scope.isSync = true;
		$scope.syncCounter = 0;

		for (var index in $scope.syncTable)	{
		(function(index){ 

			var params = $.param({}); // Init empty param
			RESTFactory.put('sync/' + $scope.syncTable[index].cloud + '/' + $scope.syncTable[index].local, params).then(function(response)	{
				// Toast response
				console.log($scope.syncTable[index].local + ":");
				console.log(response.data);
				toaster.pop("success", "Success", $scope.syncTable[index].local + " successfully synced");
				
				// Call method endSync (will end sync on proper time)
				endSync(response);
			}, function(response)	{
				// Toast response
				toaster.pop("error", "Error", $scope.syncTable[index].local + " sync failed");
				
				// Call method endSync (will end sync on proper time)
				endSync();
			});

		})(index); 
		}
	}

	var endSync = function(response)	{
		$scope.syncCounter++;
		if ($scope.syncCounter == $scope.syncTable.length)	{
			$scope.isSync = false;
			toaster.pop("info", "Info", "Synchronization compete");
		}
	}

});
