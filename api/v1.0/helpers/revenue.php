<?php

class Revenue {

    /* Private Variables */

    /* Public Methods */
    public function __construct()   {

    }

    public static function updateTicket($data, $action)  {
        try {
            switch ($action)    {
                case 'create':
                    self::onTicketCreate($data);
                    break;
                case 'checkin':
                    self::onTicketCheckIn($data);
                    break;
                case 'board':
                    self::onTicketBoard($data);
                    break;
                case 'no_show':
                    self::onTicketNoShow($data);
                    break;
                case 'refund':
                    self::onTicketRefund($data);
                    break;
                case 'cancel':
                    self::onTicketCancel($data);
                    break;
                case 'transferSeat':
                    self::onTicketTransfer($data);
                    break;
                case 'transferAddRevenue':
                    self::onTicketTransferAdd($data);
                    break;
                case 'transferMinusRevenue':
                    self::onTicketTransferMinus($data);
                    break;
                default:
                    throw new Exception ("Invalid action inputted in updateTicket");
                    break;
            }    
        } catch (Exception $e) {
            // Return error message
            return $e->getMessage();
        }
    }

    public static function updateWaybill($data, $action) {
        try {
            switch ($action)    {
                case 'create':
                    self::onWaybillCreate($data);
                    break;
                case 'refund':
                    self::onWaybillRefund($data);
                    break;
                case 'cancel':
                    self::onWaybillCancel($data);
                    break;
                default:
                    throw new Exception ("Invalid action inputted in updateWaybill");
                    break;
            }    
        } catch (Exception $e) {
            // Return error message
            return $e->getMessage();
        }
    }

    public static function updateBaggage($data, $action)  {
        try {
            switch ($action)    {
                case 'create':
                    self::onBaggageCreate($data);
                    break;
                case 'refund':
                    self::onBaggageRefund($data);
                    break;
                case 'cancel':
                    self::onBaggageCancel($data);
                    break;
                default:
                    throw new Exception ("Invalid action inputted in updateBaggage");
                    break;
            }    
        } catch (Exception $e) {
            // Return error message
            return $e->getMessage();
        }
    } 

    public static function updateUpgrades($data, $action) {
        try {
            switch ($action)    {
                case 'create':
                    self::onUpgradeCreate($data);
                    break;
                case 'refund':
                    self::onUpgradeRefund($data);
                    break;
                case 'cancel':
                    self::onUpgradeCancel($data);
                    break;
                default:
                    throw new Exception ("Invalid action inputted in updateUpgrades");
                    break;
            }    
        } catch (Exception $e) {
            // Return error message
            return $e->getMessage();
        }
    }

    public static function updateDeductions($data, $action)   {
        try {
            switch ($action)    {
                case 'create':
                    self::onDeductionCreate($data);
                    break;
                case 'activate':
                    self::onDeductionActivate($data);
                    break;
                case 'deactivate':
                    self::onDeductionDeactivate($data);
                    break;
                case 'update':
                    self::onDeductionUpdate($data);
                    break;
                default:
                    throw new Exception ("Invalid action inputted in updateDeductions");
                    break;
            }    
        } catch (Exception $e) {
            // Return error message
            return $e->getMessage();
        }
    }

    public static function updateNetCash($data, $action)   {
        try {
            switch ($action)    {
                case 'create':
                    self::onNetCashCreate($data);
                    break;
                case 'activate':
                    self::onNetCashActivate($data);
                    break;
                case 'deactivate':
                    self::onNetCashDeactivate($data);
                    break;
                case 'update':
                    self::onNetCashUpdate($data);
                    break;
                default:
                    throw new Exception ("Invalid action inputted in updateDeductions");
                    break;
            }    
        } catch (Exception $e) {
            // Return error message
            return $e->getMessage();
        }
    }

    /* Internal Methods */

    // Ticket
        function onTicketCreate($data)   {
            global $slim;

            $sql = "INSERT INTO `revenue_ticket` (`voyage`, `seating_class`, `passenger_type`, `year`, `month`, `day`, `day_name`, `hour`, `ct`, `boarded`, `checked_in`, `no_show`, `refunded`, `cancelled`, `transferred_seat`, `transferred_seating_class`, `transferred_voyage`, `revenue`) VALUES (:voyage, :seating_class, :passenger_type, YEAR(NOW()), MONTH(NOW()), DAY(NOW()), DAYNAME(NOW()), HOUR(NOW()), '1', '0', '0', '0', '0', '0', '0', '0', '0', :revenue) ON DUPLICATE KEY UPDATE ct = ct + 1, revenue = revenue + :revenue";

            $slim->db->SQL($sql, array(
                ':voyage' => $data['voyage'],
                ':seating_class' => $data['seating_class'],
                ':passenger_type' => $data['passenger_type'],
                ':revenue' => $data['revenue']
            ));
        }   

        function onTicketCheckIn($data) {
            global $slim;

            $slim->db->update("revenue_ticket", array(
                'where' => 'voyage = :voyage AND seating_class = :seating_class AND passenger_type = :passenger_type',
                'values' => array(
                    'checked_in' => '`checked_in` + 1'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':seating_class' => $data['seating_class'],
                    ':passenger_type' => $data['passenger_type']
                )
            ));
        }

        function onTicketBoard($data)   {
            global $slim;

            $slim->db->update("revenue_ticket", array(
                'where' => 'voyage = :voyage AND seating_class = :seating_class AND passenger_type = :passenger_type',
                'values' => array(
                    'boarded' => '`boarded` + 1'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':seating_class' => $data['seating_class'],
                    ':passenger_type' => $data['passenger_type']
                )
            ));
        }

        function onTicketNoShow($data)   {
            global $slim;

            $slim->db->update("revenue_ticket", array(
                'where' => 'voyage = :voyage AND seating_class = :seating_class AND passenger_type = :passenger_type',
                'values' => array(
                    'no_show' => '`no_show` + 1'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':seating_class' => $data['seating_class'],
                    ':passenger_type' => $data['passenger_type']
                )
            ));
        }

        function onTicketRefund($data)  {
            global $slim;

            $slim->db->update("revenue_ticket", array(
                'where' => 'voyage = :voyage AND seating_class = :seating_class AND passenger_type = :passenger_type',
                'values' => array(
                    'refunded' => '`refunded` + 1',
                    'revenue' => '`revenue` - :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':seating_class' => $data['seating_class'],
                    ':passenger_type' => $data['passenger_type'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

        function onTicketCancel($data)  {
            global $slim;

            $slim->db->update("revenue_ticket", array(
                'where' => 'voyage = :voyage AND seating_class = :seating_class AND passenger_type = :passenger_type',
                'values' => array(
                    'cancelled' => '`cancelled` + 1',
                    'revenue' => '`revenue` - :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':seating_class' => $data['seating_class'],
                    ':passenger_type' => $data['passenger_type'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

        function onTicketTransfer($data)    {
            global $slim;

            $slim->db->update("revenue_ticket", array(
                'where' => 'voyage = :voyage AND seating_class = :seating_class AND passenger_type = :passenger_type',
                'values' => array(
                    'transferred_seat' => '`transferred_seat` + 1',
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':seating_class' => $data['seating_class'],
                    ':passenger_type' => $data['passenger_type'],
                )
            ));
        }

        function onTicketTransferAdd($data) {
            global $slim;

            $sql = "INSERT INTO `revenue_ticket` (`voyage`, `seating_class`, `passenger_type`, `year`, `month`, `day`, `day_name`, `hour`, `ct`, `boarded`, `checked_in`, `no_show`, `refunded`, `cancelled`, `transferred_seat`, `transferred_seating_class`, `transferred_voyage`, `revenue`) VALUES (:voyage, :seating_class, :passenger_type, YEAR(NOW()), MONTH(NOW()), DAY(NOW()), DAYNAME(NOW()), HOUR(NOW()), '1', '0', '0', '0', '0', '0', '0', '0', '0', :revenue) ON DUPLICATE KEY UPDATE ct = ct + 1, revenue = revenue + :revenue";

            $slim->db->SQL($sql, array(
                ':voyage' => $data['voyage'],
                ':seating_class' => $data['seating_class'],
                ':passenger_type' => $data['passenger_type'],
                ':revenue' => $data['revenue']
            ));
        }

        function onTicketTransferMinus($data)   {
            global $slim;

            $slim->db->update("revenue_ticket", array(
                'where' => 'voyage = :voyage AND seating_class = :seating_class AND passenger_type = :passenger_type',
                'values' => array(
                    'transferred_seating_class' => '`transferred_seating_class` + :seating_class_inc',
                    'transferred_voyage' => '`transferred_voyage` + :voyage_inc',
                    'revenue' => '`revenue` - :revenue',
                    'ct' => '`ct` - 1'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':seating_class' => $data['seating_class'],
                    ':passenger_type' => $data['passenger_type'],
                    ':seating_class_inc' => $data['seating_class_inc'],
                    ':voyage_inc' => $data['voyage_inc'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

    // Waybill
        function onWaybillCreate($data) {
            global $slim;
            $sql = "INSERT INTO `revenue_waybill` (`voyage`, `year`, `month`, `day`, `day_name`, `hour`, `revenue`) VALUES (:voyage, YEAR(NOW()), MONTH(NOW()), DAY(NOW()), DAYNAME(NOW()), HOUR(NOW()), :revenue) ON DUPLICATE KEY UPDATE ct = ct + 1, revenue = revenue + :revenue";

            $slim->db->SQL($sql, array(
                ':voyage' => $data['voyage'],
                ':revenue' => $data['revenue']
            ));
        }

        function onWaybillRefund($data) {
            global $slim;

            $slim->db->update("revenue_waybill", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'refunded' => '`refunded` + 1',
                    'revenue' => '`revenue` - :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

        function onWaybillCancel($data) {
            global $slim;

            $slim->db->update("revenue_waybill", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'cancelled' => '`cancelled` + 1',
		    'revenue' => '`revenue` - :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
		    ':revenue' => $data['revenue']
                )
            ));
        }

    // Baggage
        function onBaggageCreate($data) {
            global $slim;
            $sql = "INSERT INTO `revenue_baggage` (`voyage`, `year`, `month`, `day`, `day_name`, `hour`, `revenue`) VALUES (:voyage, YEAR(NOW()), MONTH(NOW()), DAY(NOW()), DAYNAME(NOW()), HOUR(NOW()), :revenue) ON DUPLICATE KEY UPDATE ct = ct + 1, revenue = revenue + :revenue";

            $slim->db->SQL($sql, array(
                ':voyage' => $data['voyage'],
                ':revenue' => $data['revenue']
            ));
        }

        function onBaggageRefund($data) {
            global $slim;

            $slim->db->update("revenue_baggage", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'refunded' => '`refunded` + 1',
                    'revenue' => '`revenue` - :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

        function onBaggageCancel($data) {
            global $slim;

            $slim->db->update("revenue_baggage", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'revenue' => '`revenue` - :revenue',
                    'cancelled' => '`cancelled` + 1'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

    // Upgrades
        function onUpgradeCreate($data)  {
            global $slim;
            $sql = "INSERT INTO `revenue_upgrades` (`voyage`, `year`, `month`, `day`, `day_name`, `hour`, `revenue`) VALUES (:voyage, YEAR(NOW()), MONTH(NOW()), DAY(NOW()), DAYNAME(NOW()), HOUR(NOW()), :revenue) ON DUPLICATE KEY UPDATE ct = ct + 1, revenue = revenue + :revenue";

            $slim->db->SQL($sql, array(
                ':voyage' => $data['voyage'],
                ':revenue' => $data['revenue']
            ));
        }

        function onUpgradeRefund($data) {
            global $slim;

            $slim->db->update("revenue_upgrades", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'refunded' => '`refunded` + 1',
                    'revenue' => '`revenue` - :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

        function onUpgradeCancel($data) {
            global $slim;

            $slim->db->update("revenue_upgrades", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'revenue' => '`revenue` - :revenue',
                    'cancelled' => '`cancelled` + 1'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

    // Deductions
        function onDeductionCreate($data)   {
            global $slim;
            $sql = "INSERT INTO `revenue_deductions` (`voyage`, `year`, `month`, `day`, `day_name`, `hour`, `revenue`) VALUES (:voyage, YEAR(NOW()), MONTH(NOW()), DAY(NOW()), DAYNAME(NOW()), HOUR(NOW()), :revenue) ON DUPLICATE KEY UPDATE ct = ct + 1, revenue = revenue + :revenue";

            $slim->db->SQL($sql, array(
                ':voyage' => $data['voyage'],
                ':revenue' => $data['revenue']
            ));
        }

        function onDeductionActivate($data) {
            global $slim;

            $slim->db->update("revenue_deductions", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'revenue' => '`revenue` + :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

        function onDeductionDeactivate($data)   {
            global $slim;

            $slim->db->update("revenue_deductions", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'revenue' => '`revenue` - :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

        function onDeductionUpdate($data)   {
            global $slim;

            $slim->db->update("revenue_deductions", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'revenue' => '`revenue` - :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

    // Net Cash
        function onNetCashCreate($data)   {
            global $slim;
            $sql = "INSERT INTO `revenue_net_cash` (`voyage`, `year`, `month`, `day`, `day_name`, `hour`, `revenue`) VALUES (:voyage, YEAR(NOW()), MONTH(NOW()), DAY(NOW()), DAYNAME(NOW()), HOUR(NOW()), :revenue) ON DUPLICATE KEY UPDATE ct = ct + 1, revenue = revenue + :revenue";

            $slim->db->SQL($sql, array(
                ':voyage' => $data['voyage'],
                ':revenue' => $data['revenue']
            ));
        }

        function onNetCashActivate($data) {
            global $slim;

            $slim->db->update("revenue_net_cash", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'revenue' => '`revenue` + :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

        function onNetCashDeactivate($data)   {
            global $slim;

            $slim->db->update("revenue_net_cash", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'revenue' => '`revenue` - :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }

        function onNetCashUpdate($data)   {
            global $slim;

            $slim->db->update("revenue_net_cash", array(
                'where' => 'voyage = :voyage',
                'values' => array(
                    'revenue' => '`revenue` - :revenue'
                ),
                'params' => array(
                    ':voyage' => $data['voyage'],
                    ':revenue' => $data['revenue']
                )
            ));
        }
}
