<?php

// Get voyage by date
$slim->route->get("/voyage/request/date/:date", function($date)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = $slim->db->readAll('voyage', array(
        'where' => 'departure_date = :date',
        'params' => array(
            ':date' => $date
        )
    ));

    JSONResponse($status, $resp);
});

$slim->route->get("/voyage/request/data/:date", function($date) {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {

        // Init data
        $data = array();
        
        // Get voyage list
        $voyage = $slim->db->readAll('voyage', array(
            'where' => 'departure_date = :date',
            'params' => array(
                ':date' => $date
            )
        ));

        foreach ($voyage as $value) {

            /* Get ticket data */
            $ticket[$value['id']] = $slim->db->readAll('revenue_ticket', array(
                'select' => 'SUM(revenue) AS revenue',
                'where' => 'voyage = :voyage',
                'params' => array(
                    ':voyage' => $value['id']
                )
            ));

            /* Get waybill data */
            $waybill[$value['id']] = $slim->db->readAll('revenue_waybill', array(
                'where' => 'voyage = :voyage',
                'params' => array(
                    ':voyage' => $value['id']
                )
            ));

            /* Get baggage data */
            $baggage[$value['id']] = $slim->db->readAll('revenue_baggage', array(
                'where' => 'voyage = :voyage',
                'params' => array(
                    ':voyage' => $value['id']
                )
            ));

            /* Get upgrades data */
            $upgrades[$value['id']] = $slim->db->readAll('revenue_upgrades', array(
                'where' => 'voyage = :voyage',
                'params' => array(
                    ':voyage' => $value['id']
                )
            ));

            /* Init if not initialized */
            if (!isset($ticket[$value['id']][0]['revenue']))
                $ticket[$value['id']][0]['revenue'] = 0;

            if (!isset($waybill[$value['id']][0]['revenue']))
                $waybill[$value['id']][0]['revenue'] = 0;

            if (!isset($baggage[$value['id']][0]['revenue']))
                $baggage[$value['id']][0]['revenue'] = 0;

            if (!isset($upgrades[$value['id']][0]['revenue']))
                $upgrades[$value['id']][0]['revenue'] = 0;

            /* Pass values to data array */
            $data[$value['id']] = array(
                'x' => $value['number'],
                'y' => array($ticket[$value['id']][0]['revenue']*1, $waybill[$value['id']][0]['revenue']*1, 
                    $baggage[$value['id']][0]['revenue']*1, $upgrades[$value['id']][0]['revenue']*1)
            );

        }

        // Pass values to response
        if ($data)
            $resp['data'] = $data;
        else
            throw new Exception("No voyage found for date given");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Voyage request failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});  

$slim->route->get("/voyage/request/date_range/:date", function($date)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        // Get dates for the day before & after
        $date_array['yesterday'] = date('Y-m-d', strtotime($date . '-1 days'));
        $date_array['today'] = date('Y-m-d', strtotime($date));
        $date_array['tomorrow'] = date('Y-m-d', strtotime($date . '+1 days'));

        // Implode to comma separated values
        $date_csv = implode("' , '", $date_array);

        $resp['data'] = $resp['data'] = $slim->db->readAll('voyage', array(
            'where' => "departure_date IN ('{$date_csv}')"
        ));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Voyage request failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});

// Get voyage pagination
$slim->route->get("/voyage/:page/:items", function($page, $items)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Check for custom request
    if ($page == 'request') {
    	switch ($items) {
            case 'last_record':
                $resp['data'] = $slim->db->read('voyage', array(
                    'order' => 'id DESC',
                    'limit' => 1
                ));
                break;
            case 'count':  
                $resp['data'] = count($slim->db->readAll('voyage'));
                break;
            case 'count_active':
                $resp['data'] = count($slim->db->readAll('voyage', array(
                    'where' => 'active = 1'
                )));
                break;
            default: 
                $status = "404";
                $resp['status'] = 'error';
                $resp['message'] = 'Not Found';
                break;
        }
    } else {
    	// Pagination Logic
	    $index = 0;
	    for($i = 1; $i < $page; $i++) {
	        $index = $index + $items;
	    }

	    $resp['data'] = $slim->db->readAll('voyage', array(
	        'order' => "id DESC",
	        'limit' => "{$index}, {$items}",
	    ));
    }

    JSONResponse($status, $resp);
});

// Cancel Voyage
$slim->route->put("/voyage/request/cancel/:id", function($voyage)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        $blacklist = "6,7";

        // Step 1: Get ticket for the said voyage. Exclude cancelled/refunded tickets
        $ticket = $slim->db->readAll('ticket', array(
            'where' => "voyage = :voyage AND status NOT IN ({$blacklist})",
            'params' => array(
                ':voyage' => $voyage
            )
        ));

        // Step 2: Cancel All tickets within the voyage (All: No exception)
        $slim->db->update('ticket', array(
            'where' => "voyage = :voyage AND status NOT IN ({$blacklist})",
            'values' => array(
                'status' => 7
            ),
            'params' => array(
                ':voyage' => $voyage
            )
        ));

        // Step 3: Update Ticket Revenue
        foreach ($ticket as $key => $value) {
            /* Update Revenue */
            Revenue::updateTicket(array(
                'voyage' => $value['voyage'],
                'seating_class' => $value['seating_class'],
                'passenger_type' => $value['passenger_type'],
                'revenue' => $value['price_paid']
            ), 'cancel');
        }

        // Step 4: Cancel Voyage
        $slim->db->updateByPk('voyage', $voyage, array(
            'values' => array(
                'status' => 7
            )
        ));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Voyage cancellation failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});
