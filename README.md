1. Clone in your web root (Apache or Nginx)
2. Run "composer install" in /api (To generate Php dependencies)
3. Run "bower install" in /app (To generate Javascript dependencies)
4. Make sure rewrite engine is enabled by web server (sudo a2enmod rewrite using Apache)
5. Import database schema in /api/data
6. Edit database in /api/v1.0/config/settings.php
7. Access /app/app in browser to access web application

username: admin
password: admin