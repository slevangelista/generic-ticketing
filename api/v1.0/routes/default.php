<?php

// GET

// Get all record route
$slim->route->get("/:route", function($route) {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = $slim->db->readAll($route);

    JSONResponse($status, $resp);
});

// Get Single Record
$slim->route->get("/:route/:id", function($route, $id)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = $slim->db->readByPk($route, $id);

    JSONResponse($status, $resp);
});

// 4 Parameter custom REST. Avoid using this when possible
$slim->route->get("/:route/request/:request_type/:id", function($route, $request_type, $id) {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    switch($request_type)   {
        case 'voyage':
            $resp['data'] = $slim->db->readAll($route, array(
                'where' => 'voyage = :voyage',
                'params' => array(
                    ':voyage' => $id
                )
            ));
            break;
        case 'count_by_seat_class':
            $resp['data'] = count($slim->db->readAll($route, array(
                'where' => 'seating_class = :seating_class AND active = 1',
                'params' => array(
                    ':seating_class' => $id
                )
            )));
            break;
        default:
            $status = "404";
            $resp['status'] = 'error';
            $resp['message'] = 'Not Found';
            break;
    }

    JSONResponse($status, $resp);
});

// Get all record with pagination OR custom request record
$slim->route->get("/:route/:page/:items", function($route, $page, $items)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Check for custom request
    if ($page == 'request') {
        switch ($items) {
            case 'last_record':
                $resp['data'] = $slim->db->read($route, array(
                    'order' => 'id DESC',
                    'limit' => 1
                ));
                break;
            case 'count':  
                $resp['data'] = count($slim->db->readAll($route));
                break;
            case 'count_active':
                $resp['data'] = count($slim->db->readAll($route, array(
                    'where' => 'active = 1'
                )));
                break;
            default: 
                $status = "404";
                $resp['status'] = 'error';
                $resp['message'] = 'Not Found';
                break;
        }
    } else {
        // Pagination Logic
        $index = 0;
        for($i = 1; $i < $page; $i++) {
            $index = $index + $items;
        }

        $resp['data'] = $slim->db->readAll($route, array(
            'order' => 'id ASC',
            'limit' => "{$index}, {$items}",
        ));
    }

    JSONResponse($status, $resp);
});

// POST

// Insert New Record
$slim->route->post("/:route", function($route)    {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    if ($data_url = $slim->route->request->getBody())   {
        $data = json_decode($data_url, true);
 
        /* If data is null, parse string */
        if (!$data)
            parse_str($data_url, $data);

        try {
            
            if (! $data)
                throw new Exception("Data is null");

            foreach ($data as $key => $value) {
                $values[$key] = ':' . $key;
                $params[':' . $key] = $value;
            }

            $slim->db->create($route, array(
                'values' => $values,
                'params' => $params
            ));

            /* Get insert id */
            $id = $slim->db->lastInsertId();

            /* Generate response */
            $resp['data'] = $slim->db->readByPk($route, $id);

        } catch (Exception $e) {
            $status = "400";
            $resp['status'] = 'error';
            $resp['message'] = $e->getMessage();
        }

    } else {
        $status = "400";
        $resp['status'] = 'error';
        $resp['message'] = 'Invalid Request';
    }

    JSONResponse($status, $resp);
});

// PUT

// Update Record (Based on id)
$slim->route->put("/:route/:id", function($route, $id)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    if ($data_url = $slim->route->request->getBody())   {
        parse_str($data_url, $data);
        foreach ($data as $key => $value) {
            $values[$key] = ':' . $key;
            $params[':' . $key] = $value;
        }

        $slim->db->updateByPk($route, $id, array(
            'values' => $values,
            'params' => $params
        ));

        /* Generate response */
        $resp['data'] = $slim->db->readByPk($route, $id);
        
    } else {
        $status = "400";
        $resp['status'] = 'error';
        $resp['message'] = 'Invalid Request';
    }

    JSONResponse($status, $resp);
});

// DELETE

// Delete Record
$slim->route->delete("/:route/:id", function($route, $id)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    if ($id)   {
        $slim->db->updateByPk($route, $id, array(
            'values' => array(
                'active' => 0
            ),
        ));
    } 

    JSONResponse($status, $resp);
});