'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:cargoRateIndexCtrl
 * @description
 * # cargoRateIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('CargoRateIndexCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.cargoRateItems = {};

    // Pagination
    $scope.cargoRateCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Generate List

    // Cargo Class
    HelperService.getListActive('cargo_class').then(function(response)  {
        $scope.cargoClassList = response;
    });
    // Route
    HelperService.getListActive('route').then(function(response)    {
        $scope.routeList = response;
    });

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getCargoRateByPage();
        $scope.getCargoRateByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.cargoRateItems[data.id] = {
            'id': data.id,
            'route': data.route,
            'class': data.class,
            'regular_rate': data.regular_rate,
            'discount': data.discount,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // Used for Joins
    $scope.joinCargoClass = function(id)   {
        return HelperService.join(id, $scope.cargoClassList).name;
    };

    $scope.joinRoute = function(id)   {
        return HelperService.join(id, $scope.routeList).name;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getCargoRateByPage = function()    {
        RESTFactory.get('cargo_fare_rates/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.cargoRateList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getCargoRateByCount = function()   {
        RESTFactory.get('cargo_fare_rates/' + 'request/count').then(function(response) {
            $scope.cargoRateCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addCargoRate = function() {
        // Compute Discounted Rate
        var discount = ($scope.newCargo.regular_rate * ($scope.newCargo.discount / 100));
        $scope.newCargo.discounted_rate = $scope.newCargo.regular_rate - discount;

        var params = $.param($scope.newCargo);
        RESTFactory.post('cargo_fare_rates', params).then(function(response)  {
            // Response if success
            $scope.newCargo = {};
            $scope.update();
            toaster.pop("success", "Success", "Cargo rate successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Cargo rate failed to add");
        });
    };  

    $scope.editCargoRate = function()      {
        // Compute Discounted Rate
        var discount = ($scope.cargoRateItems[$scope.currentIndex].regular_rate * ($scope.cargoRateItems[$scope.currentIndex].discount / 100));
        $scope.cargoRateItems[$scope.currentIndex].discounted_rate = $scope.cargoRateItems[$scope.currentIndex].regular_rate - discount;

        var params = $.param($scope.cargoRateItems[$scope.currentIndex]);
        RESTFactory.put('cargo_fare_rates/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Cargo rate successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Cargo rate failed to edit");
        });
    };

});
