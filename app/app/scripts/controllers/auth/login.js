'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('LoginCtrl', function ($scope, $rootScope, $routeParams, $location, $http, RESTFactory, HelperService, toaster) {
    
    // Redirect to dashboard once login is already set
    // if ($rootScope.id)    
    //     $location.path('/');

    //initially set those objects to null to avoid undefined error
    $scope.login = {};
    $scope.signup = {};
    $scope.doLogin = function (customer) {
        RESTFactory.post('login', $.param(customer)).then(function (results) {
            toaster.pop(results.status, HelperService.capitalizeFirstChar(results.status), results.message);
            if (results.status == "success") {
                $location.path('/');
            }
        });
    };

    $scope.forgotPassword = function()  {
        toaster.pop('info', 'Forgot Password?', "Contact your system administrator in case you forgot your password.");
    };
});