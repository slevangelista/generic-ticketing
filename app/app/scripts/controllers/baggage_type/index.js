'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:baggageTypeIndexCtrl
 * @description
 * # baggageTypeIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('BaggageTypeIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.baggageTypeItems = {};

    // Pagination
    $scope.baggageTypeCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getBaggageTypeByPage();
        $scope.getBaggageTypeByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.baggageTypeItems[data.id] = {
            'id': data.id,
            'weight': data.weight,
            'price': data.price,
            'description': data.description,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getBaggageTypeByPage = function()    {
        RESTFactory.get('baggage_type/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.baggageTypeList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getBaggageTypeByCount = function()   {
        RESTFactory.get('baggage_type/' + 'request/count').then(function(response) {
            $scope.baggageTypeCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addBaggageType = function() {
        var params = $.param($scope.newBaggage);
        RESTFactory.post('baggage_type', params).then(function(response)  {
            // Response if success
            $scope.newBaggage = {};
            $scope.update();
            toaster.pop("success", "Success", "Baggage type successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Baggage type failed to add");
        });
    };  

    $scope.editBaggageType = function()   {
        var params = $.param($scope.baggageTypeItems[$scope.currentIndex]);
        RESTFactory.put('baggage_type/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Baggage type successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Baggage type failed to edit");
        });
    };

});
