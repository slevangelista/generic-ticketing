'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:TicketCtrl
 * @description
 * # TicketCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')

.controller('TicketIndexCtrl', function ($interval, $scope, $rootScope, RESTFactory, HelperService, toaster, socket) {

    /* Websocket Listeners */
    if (socket != null) {

        // Broadcast
        socket.on('broadcast', function (data) {
            $scope.disableAdd = data.disableAdd;
        });

        // Current user
        socket.on($rootScope.username, function (data) {

        });
    
    }
    
    // Initializations
    // ---- ---- ---- ----

    // Objects
    $scope.tickets = {};
    $scope.ticketItems = {};
    $scope.passenger = {};
    $scope.classRevenue = {};
    $scope.seatCountBySeatingClass = {};
    $scope.selectedSeats = {};
    $scope.passengerName = {};

    // Arrays
    $scope.seriesList = [];
    $scope.bool = ['No', 'Yes'];

    // Boolean
    $scope.disableAdd = false;
    $scope.isTicketPrinting = false;

    // Counters
    $scope.seriesSum = 0;
    $scope.seriesCounter = 0;
    $scope.disableCounter = 0;
    
    // Pagination
    $scope.maxSize = 10;
    $scope.currentPage = 1;    

    /* Used for seat transfer modal */
    $scope.getSeatTemplate = function() {
        getSeatList();
        getTableDimensions($scope.tickets.seating_class); 
        getSeatingTable();
    };

    var getSeatList = function() {
        HelperService.getListActive('seat').then(function(response) {
            $scope.seatList = response;
            var seatAll = response;
            $scope.seats = {};

            for (var index in seatAll)  {
                if (seatAll[index].seating_class == $scope.tickets.seating_class && seatAll[index].active == 1)    {
                    if (_.isEmpty($scope.seats[seatAll[index].x]))
                        $scope.seats[seatAll[index].x] = {};

                    $scope.seats[seatAll[index].x][seatAll[index].y] = {
                        id: seatAll[index].id,
                        name: seatAll[index].name,
                        seating_class: seatAll[index].seating_class,
                        x: seatAll[index].x,
                        y: seatAll[index].y,
                        active: seatAll[index].active,
                    }
                }
            }
        });
    };

    $scope.getSeriesIndex = function(id)    {
        $scope.seriesIndex = id;
    };

    /* REST used for seat indicator */
    $scope.joinTicket = function(seat)  {
        if ($scope.tickets.voyage != null) {
            for (var index in $scope.ticketList)  {
                if ($scope.ticketList[index].seat == seat && $scope.ticketList[index].voyage == $scope.tickets.voyage)    {
                    return $scope.ticketList[index];
                }
            }
            return 1; // Return something other than null
        } else {
            return 1; // Return something other than null
        }
    };

    /* Seat Transfer Controllers */
    $scope.getSelectedSeat = function(x,y) {
        // Check for x
        if (x in $scope.seats)  {
            // Check for y
            if (y in $scope.seats[x])   {

                var id = $scope.seats[x][y].id;
                var ticket = $scope.joinTicket(id);

                // Check if there is existing ticket for selected seat
                if (ticket != 1)    { // Seat is taken
                    toaster.pop("error", "Error", "Seat is already taken! Select another seat");
                }
                else if ($scope.selectedSeats[$scope.seats[x][y].id])   {
                    toaster.pop("error", "Error", "Seat is already selected! Select another seat");
                } else { // Seat is Available
                    $scope.seriesList[$scope.seriesIndex].seat_name = $scope.seats[x][y].name;
                    $scope.seriesList[$scope.seriesIndex].seat = $scope.seats[x][y].id;

                    // Unset old index
                    for (var index in $scope.selectedSeats) {
                        if ($scope.selectedSeats[index]-1 == $scope.seriesIndex)    {
                            $scope.selectedSeats[index] = undefined;
                        }
                    }

                    /* Get selected seats (For Display) */
                    $scope.selectedSeats[$scope.seats[x][y].id] = $scope.seriesIndex + 1; // +1 to display on getSeatColor on index `0`

                    /* Hide transfer modal */
                    $('#transferSeat').modal('hide');
                }
            }
        }
    };

    var getTableDimensions = function(id)   {
        var seatingClassList = $scope.seatingClassList;

        for (var index in seatingClassList) {
            if (seatingClassList[index].id == id)   {
                $scope.rows = seatingClassList[index].rows;
                $scope.cols = seatingClassList[index].cols;
            }
        }
    };

    var getSeatingTable = function()    {
        // Reset Value
        $scope.seatRow = [];
        $scope.seatColumn = [];

        // Row
        for (var i=0; i < $scope.rows; i++)
            $scope.seatRow.push(i);

        // Column
        for (var i=0; i < $scope.cols; i++)
            $scope.seatColumn.push(i);
    };

    $scope.getSeatColor = function(usedSeat, selectedSeat)    {
        if (usedSeat)  {
            return "background-color: #D9534F; color: WHITE;";
        } else if (selectedSeat)  {
            return "background-color: #5CB85C; color: WHITE;";
        } else        
            return "background-color: WHITE;";

    };

    // Keyboard shortcuts
    $('body').bind("keyup", function(event) {
        if ($scope.tickets.voyage != null && !$scope.isTicketPrinting)  {
            if (event.key == "+")   { // On '+'
                $scope.addToSeries();
                $("#passengerTypeInput").focus();
                $scope.$apply();
            }
            if (event.key == "-")   {
                $scope.removeLastFromSeries();
                $scope.$apply();
            }
            if (event.key == "F1")   {
                $("#passengerTypeInput").focus();
            }
            if (event.key == "F2")   {
                $("#seatingClassInput").focus();
            }
            if (event.key == "F9")   {
                $("#firstNameInput").focus();
            }
            if (event.key == "F10")   {
                $("#lastNameInput").focus();
            }
            if (event.key == "Enter")   {
                if ($scope.seriesList.length)	{
                    $('#addModal').modal('show');
		    $('#addTicketId').focus();
		}
            }
        }
    });

    // Get Lists

    // Baggage Type
    HelperService.getListActive('baggage_type').then(function(response) {
        $scope.baggageTypeList = response;
    });
    
    // Passenger Type
    HelperService.getListActive('passenger_type').then(function(response)   {
        $scope.passengerTypeList = response;
    });

    // Status
    HelperService.getListActive('status').then(function(response)   {
        $scope.statusList = response;
    });

    // Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Vessel Template
    HelperService.getListActive('vessel_template').then(function(response) {
        $scope.vesselTemplateAll = response;
    });

    // Seating Class
    HelperService.getListActive('seating_class').then(function(response)    {
        $scope.seatingClassAll = response;
    });

    // Used in baggage
    // ---- ---- ---- ----
    $scope.getBaggagePrice = function(id)   {
        for (var index in $scope.baggageTypeList)   {
            if ($scope.baggageTypeList[index].id == id) {
                $scope.newBaggage[$scope.baggageIndex].price = $scope.baggageTypeList[index].price;
                break;
            }
        }
    };

    $scope.addBaggage = function()  {
        // Add other details
        var params = $.param({
            'ticket': $scope.baggageIndex,
            'passenger': $scope.passengerId,
            'voyage': $scope.tickets.voyage,
            'price_paid': $scope.newBaggage[$scope.baggageIndex].price,
            'baggage_type': $scope.newBaggage[$scope.baggageIndex].weight,
            'created_by': $rootScope.id,
        });
        RESTFactory.post('baggage', params).then(function() {
            toaster.pop("success", "Success", "Baggage added successfully for passenger " + $scope.passengerName[$scope.baggageIndex]);

            // Get Baggage Details (For Revenue Update)
            RESTFactory.get('baggage/request/ticket/' + $scope.baggageIndex).then(function(response) {
                // Update Baggage Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.price_paid
                });
                RESTFactory.post('revenue/baggage/create', revenueParams);
            });
        }, function()   {
            toaster.pop("error", "Error", "Baggage adding failed for passenger " + $scope.passengerName[$scope.baggageIndex]);
        });
    };

    $scope.getBaggageIndex = function(id, passengerId)    {
        $scope.baggageIndex = id;
        $scope.passengerId = passengerId;
        $scope.passengerName[id] = $scope.joinPassenger(passengerId);
    };

    // Process Methods
    // ---- ---- ---- ----
    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.init = function()    {
        // $scope.ticketInterval(5000);
        $scope.updateSeries();
    }

    $scope.update = function()  {
        $scope.getTicketList();
        $scope.getTicketCount();
        $scope.getPassengerList();
        $scope.getTotalRevenue();

        /* Get seat layout */
        RESTFactory.get('seat_layout/request/voyage/' + $scope.tickets.voyage).then(function(response) {
            $scope.seatTemplateList = response.data;
        });

        /* Get seat */
        RESTFactory.get('seat/request/voyage/' + $scope.tickets.voyage).then(function(response) {
            $scope.seatList = response.data;
        });

        /* Get used seats */
    	$scope.getUsedSeats();

        /* Get infant count per voyage */
    	$scope.getInfant();
    
	$scope.getRevenueByClass();
    };

    $scope.getTicketIndex = function(data)  {
        $scope.ticketIndex = data.id;
        $scope.ticketItems[data.id] = {
            'id': data.id,
            'ticket_no': data.ticket_no,
            'price_paid': data.price_paid,
        };
    };

    // Voyage Display
    $scope.onVoyageSelect = function()  { // On voyage select do the ff
        $scope.update();
        $scope.getVesselTemplateList(); // Load this before getSeatingClassList
        $scope.getSeatingClassList();
        $scope.seriesList = [];

        $scope.tickets.seating_class = $scope.seatingClassList[0].id;
        $scope.tickets.passenger_type = $scope.passengerTypeList[0].id;
        $scope.enablePrint = 1;

        /* Get Revenue by Class */
        $scope.getRevenueByClass();

        /* Get seat template on init */
        $scope.getSeatTemplate();

        $scope.getPassengerFare();

        $scope.startTicketInterval(5000);
    };

    $scope.getActiveVoyage = function(id, vesselId)   {
        $scope.tickets.voyage = id;
        $scope.vesselId = vesselId;
    };

    $scope.deactivateVoyage = function()    {
        $scope.tickets.voyage = null;
        $scope.stopTicketInterval();
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.tickets.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.tickets.voyage = null;
    };
    // End voyage select

    $scope.resetInputFields = function()    {
        $scope.tickets.first_name = null;
        $scope.tickets.last_name = null;
    };

    $scope.resetSeriesList = function()    {
        $scope.seriesList = [];
        $scope.selectedSeats = {};
    };

    // Get List (More complex algorithm)
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            var voyageAll = response.data;
            $scope.voyageList = [];

            for (var index in voyageAll)    {
                if (voyageAll[index].status != 10)
                    $scope.voyageList.push(voyageAll[index]);
            };

        });
    };

    $scope.getSeatingClassList = function() {
        $scope.seatingClassList = [];        
        var seatingClassAll = $scope.seatingClassAll;

        // Loop by VesselTemplateList
        for (var index in $scope.vesselTemplateList)    {

            for (var i in seatingClassAll)  {
                if ($scope.vesselTemplateList[index].seating_class_id == seatingClassAll[i].id) {
                    $scope.seatingClassList.push(seatingClassAll[i]);
                }
            }
        }
    };

    $scope.getVesselTemplateList = function()   {
        var vesselTemplateAll = $scope.vesselTemplateAll;
        $scope.vesselTemplateList = [];

        for (var index in vesselTemplateAll)    {
            if ($scope.vesselId == vesselTemplateAll[index].vessel_id) {
                $scope.vesselTemplateList.push(vesselTemplateAll[index]);
            }
        }
    };

    // Joins
    $scope.joinSeatingClass = function(id)  {
        return HelperService.join(id, $scope.seatingClassList).name;
    };

    $scope.joinPassengerType = function(id) {
        return HelperService.join(id, $scope.passengerTypeList).name;
    };

    $scope.joinStatus = function(id)   {
        return HelperService.join(id, $scope.statusList).name;
    };

    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    $scope.joinPassenger = function(id) {
        var name = HelperService.join(id, $scope.passengerList).first_name + ' ' + HelperService.join(id, $scope.passengerList).last_name;
        return name;
    };

    $scope.addToSeries = function() {
        var seat_count = $scope.getSeatingCount($scope.tickets.seating_class) - $scope.seriesList.length;
        if (seat_count == 0)    {
            toaster.pop("error", "Error", "Unable to add ticket to list. No more available seats");
        } else if (!($scope.passenger_fare))  {
            toaster.pop("warning", "Warning", "Please wait for ticket booth elements to load before proceeding");
        } else {
            // Remove '+' from firstname (if detected)
            if ($scope.tickets.first_name != null)
                var first_name = $scope.tickets.first_name.split('+');
            else
                var first_name = "";

            // Remove '+' from firstname (if detected)
            if ($scope.tickets.last_name != null)
                var last_name = $scope.tickets.last_name.split('+');
            else
                var last_name = "";

            var obj = {
                'voyage': $scope.tickets.voyage,
                'seating_class': $scope.tickets.seating_class,
                'passenger_type': $scope.tickets.passenger_type,
                'passenger_fare': $scope.passenger_fare,
                'first_name': first_name[0],
                'last_name': last_name[0],
                'print': $scope.enablePrint,
                'series_no': 0
            }
            $scope.seriesList.push(obj);
            $scope.resetInputFields();

            // Get Series Sum
            getSeriesSum();
        }
    };

    var getSeriesSum = function()   {
        $scope.seriesSum = 0;
        for (var index in $scope.seriesList)    {
            $scope.seriesSum += ($scope.seriesList[index].passenger_fare*1);
        }
    }

    $scope.removeFromSeries = function(value)    {
        var index = $.inArray(value, $scope.seriesList);

        // Unset index seat selection
        for (var i in $scope.selectedSeats) {
            if ($scope.selectedSeats[i]-1 == index)    {
                $scope.selectedSeats[i] = undefined;
            }

            /* Decrement selectedSeat index (if necessary) */
            if ($scope.selectedSeats[i]-1 > index)
                $scope.selectedSeats[i] -= 1;
        }

        $scope.seriesList.splice(index, 1);

        // Get Series Sum
        getSeriesSum();
    };

    $scope.removeLastFromSeries = function()    {
        var index = $scope.seriesList.length - 1;

        // Unset index seat selection
        for (var i in $scope.selectedSeats) {
            if ($scope.selectedSeats[i]-1 == index)    {
                $scope.selectedSeats[i] = undefined;
            }

            /* Decrement selectedSeat index (if necessary) */
            if ($scope.selectedSeats[i]-1 > index)
                $scope.selectedSeats[i] -= 1;
        }

        $scope.seriesList.splice(index, 1);
        $scope.resetInputFields();

        // Get Series Sum
        getSeriesSum();
    };

    $scope.getSeatingCount = function(id) {
        var seatTempAll = $scope.seatTemplateList;
        var seatingCount = 0;

        for (var index in seatTempAll)  {
            if (seatTempAll[index].seating_class == id)    
            {
                seatingCount++;
            }    
        }
        return seatingCount;
        
        // return 0;
    };

    $scope.getSeatAvailable = function(id) {
        var seatTempAll = $scope.seatTemplateList;
        var totalSeat = [];

        for (var index in $scope.seatList)  {
            if ($scope.seatList[index].seating_class == id) {
                totalSeat.push($scope.seatList[index]);
            }
        }

        var seatingCount = totalSeat.length;

        for (var index in seatTempAll)  {
            if (seatTempAll[index].seating_class == id)    
            {
                seatingCount--;
            }    
        }
        return seatingCount;
        
        // return 0;
    };


    // REST Methods
    // ---- ---- ---- ----
    $scope.getUsedSeats = function() {
        RESTFactory.get('ticket/request/voyage/' + $scope.tickets.voyage).then(function(response)    {
            $scope.usedSeats = {};
            angular.forEach(response.data,function(ticket){
                $scope.usedSeats[ticket.seat] = ticket;
            });
        });
    };  


    $scope.getPassengerFare = function()    {
        RESTFactory.get('passenger_fare/request/voyage/' + $scope.tickets.voyage + '/seating_class/' + $scope.tickets.seating_class + '/passenger_type/' + $scope.tickets.passenger_type)
        .then(function(response)    {
            $scope.passenger_fare = response.data.price;
        }, function()   {
            toaster.pop("error", "Error", "No passenger fare set for that seating class and route! Do not use this fare");
        });
    };

    $scope.getTicketList = function()  {
        RESTFactory.get('ticket/' + $scope.currentPage + '/' + $scope.maxSize + '/' + $scope.tickets.voyage).then(function(response){
            $scope.ticketList = response.data;
        });
    };

    $scope.getPassengerList = function()    {
        RESTFactory.get('passenger/request/voyage/' + $scope.tickets.voyage).then(function(response)    {
            $scope.passengerList = response.data;
        });
    };

    $scope.getTicketCount = function()  {
        RESTFactory.get('ticket/request/count/' + $scope.tickets.voyage).then(function(response){
            $scope.ticketCount = response.data;
        });
    };

    $scope.getTotalRevenue = function() {
        RESTFactory.get('ticket/request/revenue/' + $scope.tickets.voyage).then(function(response)   {
            $scope.totalRevenue = response.data;
        });
    };

    $scope.getRevenueByClass = function()	{
    	for (var index in $scope.seatingClassList)	{
        (function(index){ 

    		RESTFactory.get("ticket/request/revenue/" + $scope.tickets.voyage + "/" + $scope.seatingClassList[index].id).then(function(response)	{
    			$scope.classRevenue[index] = response.data;
    		});

    	})(index); 
    	}
    }

    $scope.updateSeries = function()    {
        if (typeof $rootScope.id === 'undefined')   {
            /* Load session values again, then recall method again */
            RESTFactory.get('session').then(function(response)  {
                $rootScope.id = response.id;
                $rootScope.username = response.username;
                $rootScope.role = response.role;
                $rootScope.fullname = response.fullname;
                $rootScope.current_series = response.current_series;
                $scope.updateSeries();
            });
        } else {
            RESTFactory.get('users/' + $rootScope.id).then(function(response) {
                $scope.series_no = response.data.current_series;
            });
        }
    };

    $scope.addTickets = function()  {

        /* Websocket emit to disable current add */
        if (socket != null) {
            socket.emit('server', {
                username: $rootScope.username,
                disableAdd: true,
            });
        }

        /* Initialize */
        var seatIdArray = {};
        var seatRowArray = {};
        var seatRowAvailable = false;

        /* Get available seats based on voyage & seating class */
        RESTFactory.get('seat_layout/request/voyage/' + $scope.tickets.voyage).then(function(seat_temp_response)   {

            /* Filter by seating class */
            $scope.seatTempAll = [];
            $scope.seatLayout = seat_temp_response.data;
            for (var index in $scope.seatLayout)   {
                if ($scope.seatLayout[index].seating_class == $scope.tickets.seating_class)
                    $scope.seatTempAll.push($scope.seatLayout[index]);
            }

            /* Sort seatTempAll */
            $scope.seatTempAll.sort(function(a,b) { 
                return a.seat_name.localeCompare(b.seat_name); 
            });

            /* Loop seatTempAll */
            for (var i in $scope.seatTempAll)  {
    
                // Get current seat row index based on name (E.G. 10F -> index `10`)
                var seatRowIndex = HelperService.splitByLength($scope.seatTempAll[i].seat_name, $scope.seatTempAll[i].seat_name.length - 1);

                // Initialize seatRowArray of index seatRowIndex (If not yet initialized)
                if (typeof seatRowArray[seatRowIndex] === 'undefined')
                    seatRowArray[seatRowIndex] = [];

                // Push seat name to index (to group rows)
                seatRowArray[seatRowIndex].push({
                    'id': $scope.seatTempAll[i].id,
                    'seat': $scope.seatTempAll[i].seat,
                    'seat_name': $scope.seatTempAll[i].seat_name,
                });

            }

            // Find current series a seating row (if there is one available)
            for (var i in seatRowArray)  {                           
                if (seatRowArray[i].length >= $scope.seriesList.length) {
                    seatRowAvailable = i;
                    break;
                }
            }
            
            // Check if there is row available for the series
            if (seatRowAvailable)    { // Sit the series in the designated row.

                /* Loop seriesList */
                for (var index in $scope.seriesList)    {
                (function(index){

                    // Get Seat
                    if (!$scope.seriesList[index].seat)  {
                        $scope.seriesList[index].seat = seatRowArray[seatRowAvailable][0].seat;
                    }
 
                    seatIdArray[index] = seatRowArray[seatRowAvailable][0].id; // Save instance of id

                    // Remove from index
                    seatRowArray[seatRowAvailable].splice(0, 1);

                    // Initialize Static Values
                    $scope.seriesList[index].created_by = $rootScope.id;
                    $scope.seriesList[index].ticket_type = 2;
                    $scope.seriesList[index].series_no = (parseInt($scope.series_no)*1 + index*1);
                    $scope.seriesList[index].status = 2;

                    // Add Ticket
                    addTicket(index);

                })(index);
                }

            } else  { // No more row available. Sit the series in whatever seat available

                /* Loop seriesList */
                for (var index in $scope.seriesList)    {
                (function(index){
                    
                    // Get Seat
                    $scope.seriesList[index].seat = $scope.seatTempAll[0].seat;
                    seatIdArray[index] = $scope.seatTempAll[0].id; // Save instance of id

                    // Remove from index
                    $scope.seatTempAll.splice(0, 1);

                    // Initialize Static Values
                    $scope.seriesList[index].created_by = $rootScope.id;
                    $scope.seriesList[index].ticket_type = 2;
                    $scope.seriesList[index].series_no = (parseInt($scope.series_no)*1 + index*1);
                    $scope.seriesList[index].status = 2;

                    // Add Ticket
                    addTicket(index);

                })(index);
                }

            }
        });
    };

    $scope.getInfant = function()   {
        RESTFactory.get('ticket/request/infant/' + $scope.tickets.voyage).then(function(response)    {
            $scope.infantCount = response.data;
        });
    };

    var enableAddTicket = function()    {
        $scope.disableCounter++;
        if ($scope.seriesList.length == $scope.disableCounter)  {

            /* Websocket emit to enable current add */
            if (socket != null) {
                socket.emit('server', {
                    username: $rootScope.username,
                    disableAdd: false,
                });
            }

            $scope.disableCounter = 0;
        }
    };

    var addTicket = function(index)  {
        // Get Booking Counter Value
        RESTFactory.get('counter/1').then(function(counter_resp)  { // 1 stands for booking counter
            
            /* Set ticket printing indicator to true */
            $scope.isTicketPrinting = true;

            /* Get booking no */
            $scope.seriesList[index].booking_no = counter_resp.data.value;
            
            /* Do not seat infant */
            if ($scope.seriesList[index].passenger_type == 7)   {
                $scope.seriesList[index].is_seated = 0;
                $scope.seriesList[index].seat = null;
            } else 
                $scope.seriesList[index].is_seated = 1;

            /* Increment series */
            $scope.seriesCounter++;
            if ($scope.seriesList.length == $scope.seriesCounter)   {

                /* Convert array to object, into url-encoded data */
                var ticketParams = $.param(_.extend({}, $scope.seriesList));
                RESTFactory.post('ticket', ticketParams).then(function(ticket_resp)  {

                    /* Enable Add */
                    enableAddTicket();

                    toaster.pop("success", "Success", ticket_resp.data);
                    $scope.update();

                    /* Set ticket printing indicator to false */                
                    $scope.isTicketPrinting = false;

                    // Update Booking No
                    var counterParam = $.param({
                        'value': parseInt($scope.seriesList[index].booking_no) + 1
                    });
                    RESTFactory.put('counter/1', counterParam);

                    /* Update values */
                    $scope.updateSeries();
                    $scope.resetSeriesList();
                    $scope.resetInputFields();
                    $scope.seriesCounter = 0;
                    
                }, function(ticket_resp)   {
                    enableAddTicket();
                    toaster.pop("error", "Error", ticket_resp.data.message);
                });   
            }
        });
    };

    $scope.printTicket = function() {
        RESTFactory.get('renderTicket/' + $scope.ticketItems[$scope.ticketIndex].ticket_no).then(function(response) {
            // Response if success
        }, function(response)   {
            // Response if fail
        });
    };

    // Interval
    $scope.startTicketInterval = function(duration)  {
        // Do not start interval if one is already predefined
        if ( angular.isDefined($rootScope.ticketInterval) ) return

        $rootScope.ticketInterval = $interval(function()   {
            if ($scope.tickets.voyage != null)  {
                $scope.update();
            }
        }, duration);            
    };

    $scope.stopTicketInterval = function()    {
        $interval.cancel($rootScope.ticketInterval);
        $rootScope.ticketInterval = undefined;
    };

    /* Refund */
    $scope.refundTicket = function()    {
        var params = $.param({
            'status': 6
        });
        RESTFactory.put('ticket/' + $scope.ticketIndex, params).then(function() {
            $scope.update();

            // Get Ticket Details (For Revenue Update)
            RESTFactory.get('ticket/' + $scope.ticketIndex).then(function(response) {
                // Update Ticket Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    seating_class: response.data.seating_class,
                    passenger_type: response.data.passenger_type,
                    price_paid: response.data.price_paid
                });
                RESTFactory.post('revenue/ticket/refund', revenueParams);
            });
        });
    };

    /* Cancelled */
    $scope.cancelTicket = function()    {
        var params = $.param({
            'status': 7
        });
        RESTFactory.put('ticket/' + $scope.ticketIndex, params).then(function() {
            $scope.update();

            // Get Ticket Details (For Revenue Update)
            RESTFactory.get('ticket/' + $scope.ticketIndex).then(function(response) {
                // Update Ticket Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    seating_class: response.data.seating_class,
                    passenger_type: response.data.passenger_type,
                    price_paid: response.data.price_paid
                });
                RESTFactory.post('revenue/ticket/cancel', revenueParams);
            });
        });
    };

});
