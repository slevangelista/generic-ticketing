'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:ReportsManifestCtrl
 * @description
 * # ReportsManifestCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
  .controller('PassengerManifestCtrl', function ($scope, RESTFactory, HelperService) {

  	$scope.passengers = {};
  	$scope.vessel_name = '';
  	$scope.departure = '';
  	$scope.destination = '';
  	$scope.day = '';
  	$scope.year = '';
  	$scope.month = '';
   
    // Generate List

    // Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

   	// Methods
    // ---- ---- ---- ----
    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.update = function()  {
        $scope.getPassengers();
    };

    // Voyage Display
    $scope.onVoyageSelect = function()  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.passengers.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.passengers.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.passengers.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.passengers.voyage = null;
    };
    // End voyage select

    // Get List
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            $scope.voyageList = response.data;
        });
        $scope.voyageList = [];
    };

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    // REST
    $scope.getPassengers = function()    {
        RESTFactory.get('report/passenger_manifest/' + $scope.passengers.voyage).then(function(response){
            $scope.passengers_report = response.data;
            $scope.vessel_name = $scope.passengers_report[0].vessel_name;
            
            var split_route = $scope.passengers_report[0].route_name.split("-");
            $scope.departure = split_route[0];
            $scope.destination = split_route[1];

            var date = new Date($scope.selectedDate);
            var string_date = date.toDateString();
            var split_date = string_date.split(" ");
            $scope.year = split_date[3];
            $scope.month = split_date[1];
            $scope.day = split_date[2];
           
        }, function()   {
            // Response if Failed
        });
    };

    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });

        var filename = $scope.vessel_name.concat("-"+$scope.departure);
        saveAs(blob, filename+".xls");
    };


  });
