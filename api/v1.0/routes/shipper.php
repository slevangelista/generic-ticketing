<?php

$slim->route->get('/shipper', function()	use ($slim)	{

	$status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
    	$resp['data']['shipper'] = $slim->db->readAll('shipper', array(
    		'where' => 'active = 1'
    	));

    	/* Loop Shippers */
    	foreach ($resp['data']['shipper'] as $key => $value)	{
    		$resp['data']['cargo'][$value['id']] = $slim->db->readAll('cargo', array(
    			'where' => 'shipper = :shipper',
    			'params' => array(
    				':shipper' => $value['id']
    			)
    		));
    	}

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Shipper request failed. Error: " . $e->getMessage();  
    }

    JSONResponse($status, $resp);


});

$slim->route->get('/shipper/search/:name', function($name)	use ($slim) {

	$status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
    	$resp['data'] = $slim->db->readAll('shipper', array(
    		'where' => "name LIKE :name",
    		'params' => array(
    			':name' => "{$name}%"
    		)
    	));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Shipper request failed. Error: " . $e->getMessage();  
    }

    JSONResponse($status, $resp);

});

$slim->route->get('/shipper/name/:name', function($name)	use ($slim) {

	$status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
    	$resp['data'] = $slim->db->readAll('shipper', array(
    		'where' => "name = :name",
    		'params' => array(
    			':name' => $name
    		)
    	));

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Shipper request failed. Error: " . $e->getMessage();  
    }

    JSONResponse($status, $resp);

});