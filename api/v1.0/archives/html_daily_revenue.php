<?php

$slim->route->get('/', function() use ($slim)	{
	$status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {

    	/* Get data body */
    	// if ($data_json = $slim->route->request->getBody())	{	
    	// 	$data = json_decode($data_json, true);
    	// } else {
    	// 	throw new Exception('Tellers and Pursers report POST failed');
    	// }

    	/* Get all vessels */
    	$vessels = $slim->db->readAll('vessel', array(
    		'where' => 'active = 1'
    	));

    	/* Get all routes */
    	$routes = $slim->db->readAll('route', array(
    		'where' => 'active = 1'
    	));

    	/* Init */
		$url = 'localhost/archipelago/archipelago-web-api/v1.0/';
		$html = array();

    	// Loop vessels
    	foreach ($vessels as $key => $value)	{
    		
    		// Loop Routes
    		foreach ($routes as $k => $v)	{

    			/* Initialize html */
				$html[$value['id']][$v['id']] = NULL;

				$data[$value['id']][$v['id']] = json_decode( getCURL($url, "report/daily_revenue/{$date}/{$date}/{$value['id']}/{$v['id']}"), true );

				// Check if data exists
				if ($table[$value['id']][$v['id']] = $data[$value['id']][$v['id']]['data'])	{
					
					/* Header */

					$html[$value['id']][$v['id']] .= "
						<center>
				            <h3>ARCHIPELAGO PHILIPPINE FERRIES CORPORATION</h3>
				            Unit 5B, 5th Floor, UNIOIL Center Bldg., <br />
				            Commerce Ave., Madrigal Business Park, <br />
				            Ayala Alabang, Muntinlupa City <br />

				            <h3>Daily Revenue Report</h3>
				            <br />

				        </center>
				        <table class='table table-responsive table-bordered' >
				            <tr>
				            	<th colspan='2'>Revenue</th>
			        ";

			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "
			        		<th>
			                    {$vv['number']} <br />
			                    {$vv['departure_date']} @ {$vv['departure_time']}<br />
			                    {$vv['route_name']}
			                </th>
			        	";
			        }

			        $html[$value['id']][$v['id']] .= "
			        		<th>Total</th>
			        	</tr>
			        	<tr>
			                <td colspan='3'></td>
			        ";

			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }

			        $html[$value['id']][$v['id']] .= "</tr>";

			        /* PAX */
			        foreach ($table[$value['id']][$v['id']]['revenue_ticket'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "
			        		<tr>
			        			<td>{$kk}</td>
				        		<td style='margin: 0px; padding: 0px;'>
				        			<table class='table' style='margin-bottom: 0px !important;'>
			        	";

			        	foreach ($table[$value['id']][$v['id']]['passenger_type'] as $kkk => $vvv)	{
			        		$html[$value['id']][$v['id']] .= "
				        		<tr>
		                            <td>{$vvv['name']}</td>
		                        </tr>
                        	";
			        	}

			        	$html[$value['id']][$v['id']] .= "</table></td>";


		        		// Reverse Array
		        		krsort($vv);

			        	foreach ($vv as $kkk => $vvv)	{
			        
			        		$html[$value['id']][$v['id']] .= "<td><table>";
			        		foreach ($vvv as $kkkk => $vvvv)	{
				        		$html[$value['id']][$v['id']] .= "<tr><td>{$vvvv}</td></tr>";
			        		}
			        		$html[$value['id']][$v['id']] .= "</table></td>";
			        	}

			        	// $html[$value['id']][$v['id']] .= "</td></tr>";
			        	$html[$value['id']][$v['id']] .= "</tr>";

			        }

			        /* Upgrades */
			        // Reverse Array
			        krsort($table[$value['id']][$v['id']]['revenue_upgrade']);

			        $html[$value['id']][$v['id']] .= "<tr><td colspan='2'>Upgrade</td>";
			        foreach ($table[$value['id']][$v['id']]['revenue_upgrade'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td>{$vv}</td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "</tr>";

			        /* Baggage */
			        // Reverse Array
			        krsort($table[$value['id']][$v['id']]['revenue_baggage']);

			        $html[$value['id']][$v['id']] .= "<tr><td colspan='2'>Baggage</td>";
			        foreach ($table[$value['id']][$v['id']]['revenue_baggage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td>{$vv}</td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "</tr>";

			        /* Vehicle Fares */
			        // Reverse Array
			        krsort($table[$value['id']][$v['id']]['revenue_waybill']);

			        $html[$value['id']][$v['id']] .= "<tr><td colspan='2'>Vehicle Fares</td>";
			        foreach ($table[$value['id']][$v['id']]['revenue_waybill'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td>{$vv}</td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "</tr>";

			        /* Total Revenue */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='2'>TOTAL REVENUE</td>";
			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "<th>{$table[$value['id']][$v['id']]['total_revenue_no_deductions']}</th></tr>";

			        /* Space */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='3'></td>";
			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "</tr>";

			        /* PAX Discount */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='2'>LESS: PASSENGER FARE DISCOUNTS</td>";
			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "<td></td></tr>";

			        foreach ($table[$value['id']][$v['id']]['discount_passenger'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<tr><td colspan='2'>{$kk}</td>";

			        	// Reverse Array
			        	krsort($vv);
			        	foreach ($vv as $kkk => $vvv)	{
			        		$html[$value['id']][$v['id']] .= "<td>{$vvv}</td>";
			        	}
			        	$html[$value['id']][$v['id']] .= "</tr>";
			        }

			        /* Total Revenue (LESS DISCOUNT) */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='2'>TOTAL REVENUE (LESS DISCOUNT)</td>";
			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "<th>{$table[$value['id']][$v['id']]['total_revenue']}</th></tr>";

			        /* Space */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='3'></td>";
			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "</tr>";

			        /* LESS: Receivables */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='2'>LESS: RECEIVABLES</td>";
			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "<td></td></tr>";

			        foreach ($table[$value['id']][$v['id']]['on_account_waybills'] as $kk => $vv)	{
			        	// var_dump($vv);
			        	$html[$value['id']][$v['id']] .= "<tr><td colspan='2'>{$vv['plate_num']}</td>";
			        	foreach ($table[$value['id']][$v['id']]['voyage'] as $kkk => $vvv)	{
				        	$html[$value['id']][$v['id']] .= "<td></td>"; 
				        }
				        $html[$value['id']][$v['id']] .= "<td>{$vv['price_paid']}</td></tr>";
						
			        }

			        /* Net Cash Collection */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='2'>NET CASH COLLECTION</td>";

			        foreach ($table[$value['id']][$v['id']]['revenue_voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td>{$vv}</td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "<th>{$table[$value['id']][$v['id']]['total_rev_minus_waybills']}</th></tr>";

			        /* Space */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='3'></td>";
			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "</tr>";

			        /* Users */
			        // Sort Array (Remove Key)
			        sort($table[$value['id']][$v['id']]['users']);
			        foreach ($table[$value['id']][$v['id']]['users'] as $kk => $vv)	{
			        	$index = $kk + 1;
			        	$html[$value['id']][$v['id']] .= "<tr><td>{$index}</td>";
			        	$html[$value['id']][$v['id']] .= "<td>{$vv['employee_no']} - {$vv['first_name']} {$vv['last_name']} - {$vv['role']}</td>";

			        	foreach ($table[$value['id']][$v['id']]['voyage'] as $kkk => $vvv)	{
				        	$html[$value['id']][$v['id']] .= "<td></td>"; 
				        }

			        	$html[$value['id']][$v['id']] .= "<td>{$vv['revenue']}</td>";
			        	$html[$value['id']][$v['id']] .= "</tr>";
			        }
			        
			        /* User Revenue */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='2'></td>";

			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "<th>{$table[$value['id']][$v['id']]['total_users_revenue']}</th></tr>";

			        /* Space */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='3'></td>";
			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "</tr>";

			        /* Cash Beginning */
			        $html[$value['id']][$v['id']] .= "<tr><td colspan='2'>ADD: CASH BEGINNING</td>";
			        foreach ($table[$value['id']][$v['id']]['voyage'] as $kk => $vv)	{
			        	$html[$value['id']][$v['id']] .= "<td></td>"; 
			        }
			        $html[$value['id']][$v['id']] .= "<td></td></tr>";

			        

				}
    		}
    	}

    	var_dump($html);

    } catch (Exception $e)	{
    	$status = "400";
        $resp['status'] = 'error';
        $resp['message'] = $e->getMessage();
    }

    // JSONResponse($status, $resp);
});