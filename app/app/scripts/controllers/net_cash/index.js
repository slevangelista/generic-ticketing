'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:DeductionsIndexCtrl
 * @description
 * # DeductionsIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('NetCashIndexCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

    // Initialize
    // ---- ---- ---- ----
    $scope.deductions = {};
    $scope.deductionItems = {};

    $scope.bool = ['No', 'Yes'];

    // Pagination
    $scope.deductionCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    // Get Lists
    
    // Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Process Methods
    // ---- ---- ---- ----

    // Voyage Display
    $scope.getIndex = function(id)    {
        $scope.currentIndex = id;
    };
    
    $scope.getItems = function(data)    {
        $scope.deductionItems[data.id] = {
            'id': data.id,
            'name': data.name,
            'amt': data.amt,
            'voyage': data.voyage,
            'created_by': data.created_by,
            'active': data.active
        };
        // This is used to determine if active state is toggled
        $scope.deductionToggle = data.active;
    };

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.update = function()  {
        $scope.getDeductionPaginationByVoyage();
        $scope.getDeductionCountByVoyage();
    };

    $scope.onVoyageSelect = function()  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id, vesselId)   {
        $scope.deductions.voyage = id;
        $scope.vesselId = vesselId;
    };

    $scope.deactivateVoyage = function()    {
        $scope.deductions.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.deductions.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.deductions.voyage = null;
    };
    // End voyage select

    // Get List (More complex algorithm)
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            var voyageAll = response.data;
            $scope.voyageList = [];

            for (var index in voyageAll)    {
                if (voyageAll[index].status != 10)
                    $scope.voyageList.push(voyageAll[index]);
            };

        });
    };

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.getDeductionPaginationByVoyage = function()   {
        RESTFactory.get('net_cash/request/voyage/' + $scope.deductions.voyage + '/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response)    {
            $scope.deductionsList = response.data;
        });
    };

    $scope.getDeductionCountByVoyage = function()   {
        RESTFactory.get('net_cash/request/voyage/' + $scope.deductions.voyage + '/count').then(function(response) {
            $scope.deductionCount = response.data;
        });
    };

    $scope.addDeductions = function()   {
        var params = $.param({
            'voyage': $scope.deductions.voyage,
            'name': $scope.newDeductions.name,
            'amt': $scope.newDeductions.amt,
            'created_by': $rootScope.id
        });
        RESTFactory.post('net_cash', params).then(function()  {
            $scope.update();
            toaster.pop("success", "Success", "Deductions successfully added");

            // Get Deduction Details (For Revenue Update)
            RESTFactory.get('net_cash/request/last_record').then(function(response) {
                // Update Deduction Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.amt
                });
                RESTFactory.post('revenue/net_cash/create', revenueParams);
            });

        }, function(response)   {
            toaster.pop("error", "Error", "Deductions failed to add");
        });
    };

    $scope.editDeductions = function()  {

        if ($scope.deductionToggle != $scope.deductionItems[$scope.currentIndex].active)    {

            /* Determine whether `activate` or `deactivate` deduction revenue */
            if ($scope.deductionItems[$scope.currentIndex].active == 1) { // Activate
                // Get Deduction Details (For Revenue Update)
                RESTFactory.get('net_cash/' + $scope.currentIndex).then(function(response) {
                    // Update Deduction Revenue
                    var revenueParams = $.param({
                        voyage: response.data.voyage,
                        price_paid: response.data.amt
                    });
                    RESTFactory.post('revenue/net_cash/activate', revenueParams);
                });
            } else if ($scope.deductionItems[$scope.currentIndex].active == 0) { // Safety Else If. Deactivate
                // Get Deduction Details (For Revenue Update)
                RESTFactory.get('net_cash/' + $scope.currentIndex).then(function(response) {
                    // Update Deduction Revenue
                    var revenueParams = $.param({
                        voyage: response.data.voyage,
                        price_paid: response.data.amt
                    });
                    RESTFactory.post('revenue/net_cash/deactivate', revenueParams);
                });
            }
        }

        RESTFactory.get('net_cash/' + $scope.currentIndex).then(function(response) {
            
            /* Past Revenue */
            var old_revenue = response.data.amt;

            var params = $.param($scope.deductionItems[$scope.currentIndex]);
            RESTFactory.put('net_cash/' + $scope.currentIndex, params).then(function(resp)    {

                /* Edit Revenue Total */
                var new_revenue = resp.data.amt;

                // Update Revenue
                var updateParams = $.param({
                    'voyage': $scope.deductions.voyage,
                    'price_paid': old_revenue-new_revenue,
                });
                RESTFactory.post('revenue/net_cash/update', updateParams);

                $scope.update();
                toaster.pop("success", "Success", "Deductions successfully edited");
            }, function(response)   {
                toaster.pop("error", "Error", "Deductions failed to edit");
            });

        });        
    };
});
