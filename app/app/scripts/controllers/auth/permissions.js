'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:permissionIndexCtrl
 * @description
 * # permissionIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('AuthPermissionsCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.permissionItems = {};

    // Get Lists

    // Roles
    HelperService.getListActive('roles').then(function(response) {
        $scope.roleList = response;
    });

    // Pagination
    $scope.permissionCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getPermissionsByPage();
        $scope.getPermissionsByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.permissionItems[data.id] = {
            'id': data.id,
            'role': data.role,
            'url_route': data.url_route,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // Joins
    $scope.joinRole = function(id)	{
		return HelperService.join(id, $scope.roleList).name;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getPermissionsByPage = function()    {
        RESTFactory.get('permissions/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.permissionList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getPermissionsByCount = function()   {
        RESTFactory.get('permissions/' + 'request/count').then(function(response) {
            $scope.permissionCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addPermissions = function() {
        var params = $.param($scope.newPermission);
        RESTFactory.post('permissions', params).then(function(response)  {
            // Response if success
            $scope.newPermission = {};
            $scope.update();
            toaster.pop("success", "Success", "Permission successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Permission failed to add");
        });
    };  

    $scope.editPermissions = function()   {
        var params = $.param($scope.permissionItems[$scope.currentIndex]);
        RESTFactory.put('permissions/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Permission successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Permission failed to edit");
        });
    };

});
