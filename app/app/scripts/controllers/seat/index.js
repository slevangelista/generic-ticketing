'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:SeatIndexCtrl
 * @description
 * # SeatIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('SeatIndexCtrl', function ($scope, $rootScope, $interval, RESTFactory, HelperService) {

    // Initializations
    // ---- ---- ---- ----
    $scope.seat = {};

    // Get List

    // Vessel Template
    HelperService.getListActive('vessel_template').then(function(response) {
        $scope.vesselTemplateAll = response;
    });

    // Seat
    HelperService.getListActive('seat').then(function(response) {
        $scope.seatAll = response;
    });

    // Seating Class
    HelperService.getListActive('seating_class').then(function(response)    {
        $scope.seatingClassAll = response;
    });

    // Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Process Methods
    // ---- ---- ---- ----
    $scope.update = function()  {
        $scope.getVesselId();
        $scope.getVesselTemplateList();

        $scope.getTicketList();
        $scope.getPassengerList();

        // Reset Value
        $scope.seatingClass = null;
        $scope.seatRow = [];
        $scope.seatColumn = [];
    };

    // Date Methods
        $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    // Voyage Display
    $scope.onVoyageSelect = function()  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.seat.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.stopSeatDisplayInterval();

        $scope.seat.voyage = null;

        // Reset Value
        $scope.seatRow = [];
        $scope.seatColumn = [];
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.seat.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.seat.voyage = null;
    };
    // End voyage select

    // Vessel
    $scope.getVesselId = function() {
        $scope.vesselId = HelperService.join($scope.seat.voyage, $scope.voyageList).vessel;
    }

    // Get List
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            var voyageAll = response.data;
            $scope.voyageList = [];

            for (var index in voyageAll)    {
                if (voyageAll[index].status != 10)
                    $scope.voyageList.push(voyageAll[index]);
            };

        });
    };

    $scope.getVesselTemplateList = function()   {
        var vesselTemplateAll = $scope.vesselTemplateAll;
        $scope.vesselTemplateList = [];

        for (var index in vesselTemplateAll)    {
            if ($scope.vesselId == vesselTemplateAll[index].vessel_id) {
                $scope.vesselTemplateList.push(vesselTemplateAll[index]);
            }
        }
    };

    $scope.getSeatList = function() {
        var seatAll = $scope.seatAll;
        $scope.seats = {};

        for (var index in seatAll)  {
            if (seatAll[index].seating_class == $scope.seatingClass && seatAll[index].active == 1)    {
                if (_.isEmpty($scope.seats[seatAll[index].x]))
                    $scope.seats[seatAll[index].x] = {};

                $scope.seats[seatAll[index].x][seatAll[index].y] = {
                    id: seatAll[index].id,
                    name: seatAll[index].name,
                    seating_class: seatAll[index].seating_class,
                    x: seatAll[index].x,
                    y: seatAll[index].y,
                    active: seatAll[index].active,
                }
            }
        }
    };

    $scope.getSeatingTable = function()    {
        // Reset Value
        $scope.seatRow = [];
        $scope.seatColumn = [];

        // Row
        for (var i=0; i < $scope.rows; i++)
            $scope.seatRow.push(i);

        // Column
        for (var i=0; i < $scope.cols; i++)
            $scope.seatColumn.push(i);
    };

    $scope.getTableDimensions = function(id)   {
        var seatingClassAll = $scope.seatingClassAll;

        for (var index in seatingClassAll) {
            if (seatingClassAll[index].id == id)   {
                $scope.rows = seatingClassAll[index].rows;
                $scope.cols = seatingClassAll[index].cols;
            }
        }
    };

    // Joins
    $scope.joinSeatingClass = function(id)    {
        return HelperService.join(id, $scope.seatingClassAll).name;
    };

    $scope.joinPassenger = function(id) {
        var name = HelperService.join(id, $scope.passengerAll).first_name + ' ' + HelperService.join(id, $scope.passengerAll).last_name;
        return name;
    };

    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    $scope.joinTicket = function(seat)  {
        if ($scope.seat.voyage != null) {
            for (var index in $scope.ticketList)  {
                if ($scope.ticketList[index].seat == seat && $scope.ticketList[index].voyage == $scope.seat.voyage)    {
                    return $scope.ticketList[index];
                }
            }
            return 1; // Return something other than null
        } else {
            return 1; // Return something other than null
        }
    };

    // Get Seat Color
    $scope.getSeatColor = function(x,y)    {
        // Check for x
        if (x in $scope.seats)  {
            // Check for y
            if (y in $scope.seats[x])   {
                    
                var id = $scope.seats[x][y].id;
                var ticket = $scope.joinTicket(id);
                
                if (ticket != 1)   { // Seat Taken
                    return "background-color: #D9534F; color: white;";
                } else { // Seat not Taken
                    return "background-color: white;";
                }
            }
        }
        return "background-color: white;"; // If nothing is found
    };

    // Get Seat Title
    $scope.getSeatTitle = function(x,y) {
        // Check for x
        if (x in $scope.seats)  {
            // Check for y
            if (y in $scope.seats[x])   {
                    
                var id = $scope.seats[x][y].id;
                var ticket = $scope.joinTicket(id);

                if (ticket != 1)   { // Seat Taken
                    return "Ticket No: " + ticket.ticket_no + ", Passenger: " + $scope.joinPassenger(ticket.passenger);
                } else { // Seat not Taken
                    return "Seat Available";
                }
            }
        }
        return ""; // If nothing is found
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.getTicketList = function() {
        RESTFactory.get('ticket/request/voyage/' + $scope.seat.voyage).then(function(response)    {
            $scope.ticketList = response.data;
        });
    };  

    $scope.getPassengerList = function() {
        RESTFactory.get('passenger/request/voyage/' + $scope.seat.voyage).then(function(response)    {
            $scope.passengerAll = response.data;
        });
    };  

    // Interval
    // ---- ---- ---- ----
    $scope.startSeatDisplayInterval = function(duration)  {
        // Do not start interval if one is already predefined
        if ( angular.isDefined($rootScope.seatInterval) ) return

        $rootScope.seatInterval = $interval(function()   {
            if ($scope.seatingClass != null)  {

                $scope.getTicketList();
                $scope.getPassengerList();

                $scope.getSeatList(); 
                $scope.getTableDimensions($scope.seatingClass); 
                $scope.getSeatingTable();
            }
        }, duration);            
    };

    $scope.stopSeatDisplayInterval = function()    {
        $interval.cancel($rootScope.seatInterval);
        $rootScope.seatInterval = undefined;
    };

});
