'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:BoardingScanCtrl
 * @description
 * # BoardingScanCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('BoardingScanCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

	// Process Methods
    // ---- ---- ---- ----
    $scope.focus = function()   {
        $("#ticketInput").focus();
    };

    $scope.focus();

    $scope.scanTicket = function()  {
        $scope.ticket.final = $scope.ticket.init;
        $scope.ticket.init = null;

        $scope.getTicket();
        $scope.focus();
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.getTicket = function()   {
        RESTFactory.get('ticket/request/ticket_no/' + $scope.ticket.final).then(function(response)  {
            $scope.ticket.result = response.data;
   			$scope.boardTicket();

        }, function()   {
            $scope.ticket.result = null;
        });
    };

    $scope.boardTicket = function()	{
    	if ($scope.ticket.result.status_id == 4)	{
    		toaster.pop("warning", "Warning", "Ticket " + $scope.ticket.result.ticket_no + " is already boarded");
    	} else {
    		var params = $.param({
	            'status': 4
	        });

	        RESTFactory.put('ticket/' + $scope.ticket.result.id, params).then(function() {
	            $scope.focus();

	            // Update ticket revenue
	            var revenueParams = $.param({
	                'voyage': $scope.ticket.result.voyage_id,
	                'seating_class': $scope.ticket.result.seating_class_id,
	                'passenger_type': $scope.ticket.result.passenger_type_id
	            });
	            RESTFactory.post('revenue/ticket/board', revenueParams);

	            toaster.pop("success", "Success", "Ticket " + $scope.ticket.result.ticket_no + " successfully boarded");
	        }, function()   {
	            toaster.pop("error", "Error", "Ticket " + $scope.ticket.result.ticket_no + " boarding failed");
	        });
    	}
    };

});
