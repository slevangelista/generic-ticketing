'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:ReportsInclusiveTicketsCtrl
 * @description
 * # ReportsInclusiveTicketsCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('ReportsInclusiveTicketsCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

    // Initializations
    // ---- ---- ---- ----
    $scope.inclusive_tickets = {};

    // Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Route
    HelperService.getListActive('route').then(function(response) {
        $scope.routeList = response;
    });

    // Methods
    // ---- ---- ---- ----
    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.update = function()    {
        $scope.getInclusiveTickets();
    };

    // Voyage Display
    $scope.onVoyageSelect = function(trip_id)  { // On voyage select do the ff
        $scope.update();
        $scope.getTripIndex(trip_id);
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.inclusive_tickets.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.inclusive_tickets.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.inclusive_tickets.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.inclusive_tickets.voyage = null;
    };
    // End voyage select

    $scope.getTripIndex = function(id)  {
        $scope.tripIndex = id;
    }

    // Get List
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            $scope.voyageList = response.data;
        });
        $scope.voyageList = [];
    };

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    $scope.joinVoyage = function(id)    {
        if ($scope.inclusive_tickets.voyage != null)
            return HelperService.join(id, $scope.voyageList).number;
        else
            return "";
    };

    $scope.getVoyageRoute = function()    {
        if ($scope.tripIndex != null)   {
            var route_id = HelperService.join($scope.tripIndex, $scope.tripList).route_id;
            return HelperService.join(route_id, $scope.routeList).name;
        } else {
            return "";
        }
    };

    $scope.getDepartureTime = function(departure_time)  {
        $scope.departure_time = departure_time;
    };

    // REST
    $scope.getInclusiveTickets = function()    {
        RESTFactory.get('report/inclusive_ticket/' + $scope.inclusive_tickets.voyage + '/' + $rootScope.id).then(function(response){
            $scope.inclusive_ticket_report = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });

        saveAs(blob, "inclusive_tickets.xls");
    };
    
    $scope.sendToCloud = function() {
        RESTFactory.post('html/inclusive_tickets', {'voyage': $scope.inclusive_tickets.voyage}).then(function(response)    {
            toaster.pop("success", "Success", "Data successfully sent to cloud");
        }, function(response)   {
            toaster.pop("error", "Error", "Data transfer to cloud failed");
        });
    };
});
