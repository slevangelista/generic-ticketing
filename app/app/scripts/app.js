'use strict';

/**
* @ngdoc overview
* @name archipelagoWebAppApp
* @description
* # archipelagoWebAppApp
*
* Main module of the application.
*/
angular.module('archipelagoWebAppApp', [
	'ngAnimate',
	'ngCookies',
	'ngResource',
	'ngRoute',
	'ngSanitize',
	'ui.bootstrap',
	'toaster',
	'angularCharts',
	'ui.select',
	'btford.socket-io'
])
.config(function ($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl: 'views/dashboard.html',
		controller: 'DashboardCtrl'
	})
	.when('/vessel', {
		templateUrl: 'views/vessel/index.html',
		controller: 'VesselCtrl'
	})
	.when('/vessel/index', {
		templateUrl: 'views/vessel/index.html',
		controller: 'VesselCtrl'
	})
	.when('/port', {
		templateUrl: 'views/port/index.html',
		controller: 'PortCtrl'
	})
	.when('/port/index', {
		templateUrl: 'views/port/index.html',
		controller: 'PortCtrl'
	})
	.when('/cargo', {
		templateUrl: 'views/cargo/index.html',
		controller: 'CargoCtrl'
	})
	.when('/cargo/index', {
		templateUrl: 'views/cargo/index.html',
		controller: 'CargoCtrl'
	})
	.when('/cargo/waybill', {
		templateUrl: 'views/cargo/waybill.html',
		controller: 'CargoWaybillCtrl'
	})
	.when('/seat', {
		templateUrl: 'views/seat/index.html',
		controller: 'SeatIndexCtrl'
	})
	.when('/seat/index', {
		templateUrl: 'views/seat/index.html',
		controller: 'SeatIndexCtrl'
	})
	.when('/seat/create', {
		templateUrl: 'views/seat/create.html',
		controller: 'SeatCreateCtrl'
	})
	.when('/ticket', {
		templateUrl: 'views/ticket/index.html',
		controller: 'TicketIndexCtrl'
	})
	.when('/ticket/index', {
		templateUrl: 'views/ticket/index.html',
		controller: 'TicketIndexCtrl'
	})
	.when('/dashboard', {
		templateUrl: 'views/dashboard.html',
		controller: 'DashboardCtrl'
	})
	.when('/route', {
		templateUrl: 'views/route/index.html',
		controller: 'RouteIndexCtrl'
	})
	.when('/route/index', {
		templateUrl: 'views/route/index.html',
		controller: 'RouteIndexCtrl'
	})
	.when('/seating_class', {
		templateUrl: 'views/seatclass/index.html',
		controller: 'SeatclassIndexCtrl'
	})
	.when('/seating_class/index', {
		templateUrl: 'views/seatclass/index.html',
		controller: 'SeatclassIndexCtrl'
	})
	.when('/voyage', {
		templateUrl: 'views/voyage/index.html',
		controller: 'VoyageIndexCtrl'
	})
	.when('/voyage/index', {
		templateUrl: 'views/voyage/index.html',
		controller: 'VoyageIndexCtrl'
	})
	.when('/passenger_type', {
		templateUrl: 'views/passenger_type/index.html',
		controller: 'PassengerTypeIndexCtrl'
	})
	.when('/passenger_type/index', {
		templateUrl: 'views/passenger_type/index.html',
		controller: 'PassengerTypeIndexCtrl'
	})
	.when('/baggage_type', {
		templateUrl: 'views/baggage_type/index.html',
		controller: 'BaggageTypeIndexCtrl'
	})
	.when('/baggage_type/index', {
		templateUrl: 'views/baggage_type/index.html',
		controller: 'BaggageTypeIndexCtrl'
	})
	.when('/cargo_rate', {
		templateUrl: 'views/cargo_rate/index.html',
		controller: 'CargoRateIndexCtrl'
	})
	.when('/cargo_rate/index', {
		templateUrl: 'views/cargo_rate/index.html',
		controller: 'CargoRateIndexCtrl'
	})
	.when('/cargo_class', {
		templateUrl: 'views/cargo_class/index.html',
		controller: 'CargoClassIndexCtrl'
	})
	.when('/cargo_class/index', {
		templateUrl: 'views/cargo_class/index.html',
		controller: 'CargoClassIndexCtrl'
	})
	.when('/passenger_fare', {
		templateUrl: 'views/passenger_fare/index.html',
		controller: 'PassengerFareIndexCtrl'
	})
	.when('/passenger_fare/index', {
		templateUrl: 'views/passenger_fare/index.html',
		controller: 'PassengerFareIndexCtrl'
	})
	.when('/ticket/list', {
		templateUrl: 'views/ticket/list.html',
		controller: 'TicketListCtrl'
	})
	.when('/baggage', {
		templateUrl: 'views/baggage/index.html',
		controller: 'BaggageIndexCtrl'
	})
	.when('/baggage/index', {
		templateUrl: 'views/baggage/index.html',
		controller: 'BaggageIndexCtrl'
	})
	.when('/reports/inclusive_tickets', {
		templateUrl: 'views/reports/inclusive_tickets.html',
		controller: 'ReportsInclusiveTicketsCtrl'
	})
	.when('/reports/daily_revenue', {
		templateUrl: 'views/reports/daily_revenue.html',
		controller: 'ReportsDailyRevenueCtrl'
	})
	.when('/reports/manifest', {
		templateUrl: 'views/reports/passenger_manifest.html',
		controller: 'PassengerManifestCtrl'
	})
	.when('/reports/inspection_report', {
		templateUrl: 'views/reports/inspection_report.html',
		controller: 'ReportsInspectionReportCtrl'
	})
	.when('/reports/revenue_monitoring', {
		templateUrl: 'views/reports/revenue_monitoring.html',
		controller: 'ReportsRevenueMonitoringCtrl'
	})
	.when('/trips/index', {
		templateUrl: 'views/trips/index.html',
		controller: 'TripIndexCtrl'
	})
	.when('/trips', {
		templateUrl: 'views/trips/index.html',
		controller: 'TripIndexCtrl'
	})
	.when('/reports/cargo_manifest', {
		templateUrl: 'views/reports/cargo_manifest.html',
		controller: 'ReportsCargoManifestCtrl'
	})
	.when('/reports/tellers_and_pursers', {
		templateUrl: 'views/reports/tellers_and_pursers.html',
		controller: 'ReportsTellersAndPursersCtrl'
	})
	.when('/reports/complimentary_tickets', {
		templateUrl: 'views/reports/complimentary_tickets.html',
		controller: 'ReportsComplimentaryTicketsCtrl'
	})
	// Authentication Routes
	.when('/login', {
		templateUrl: 'views/auth/login.html',
		controller: 'LoginCtrl'
	})
	.when('/logout', {
		templateUrl: 'views/auth/logout.html',
		controller: 'LogoutCtrl'
	})
	.when('/ticket/scan', {
		templateUrl: 'views/ticket/scan.html',
		controller: 'TicketScanCtrl'
	})
	.when('/deductions/index', {
		templateUrl: 'views/deductions/index.html',
		controller: 'DeductionsIndexCtrl'
	})
	.when('/deductions', {
		templateUrl: 'views/deductions/index.html',
		controller: 'DeductionsIndexCtrl'
	})
	.when('/auth/accounts', {
	  templateUrl: 'views/auth/accounts.html',
	  controller: 'AuthAccountsCtrl'
	})
	.when('/auth/roles', {
	  templateUrl: 'views/auth/roles.html',
	  controller: 'AuthRolesCtrl'
	})
	.when('/auth/settings', {
	  templateUrl: 'views/auth/settings.html',
	  controller: 'AuthSettingsCtrl'
	})
	.when('/auth/permissions', {
	  templateUrl: 'views/auth/permissions.html',
	  controller: 'AuthPermissionsCtrl'
	})
	.when('/shipper/index', {
	  templateUrl: 'views/shipper/index.html',
	  controller: 'ShipperIndexCtrl'
	})
.when('/cargo/on_account', {
  templateUrl: 'views/cargo/on_account.html',
  controller: 'CargoOnAccountCtrl'
})
.when('/printer/index', {
  templateUrl: 'views/printer/index.html',
  controller: 'PrinterIndexCtrl'
})
.when('/net_cash/index', {
  templateUrl: 'views/net_cash/index.html',
  controller: 'NetCashIndexCtrl'
})
.when('/upgrades/index', {
  templateUrl: 'views/upgrades/index.html',
  controller: 'UpgradesIndexCtrl'
})
.when('/baggage/new_baggage', {
  templateUrl: 'views/baggage/new_baggage.html',
  controller: 'BaggageNewBaggageCtrl'
})
.when('/upgrades/list', {
  templateUrl: 'views/upgrades/list.html',
  controller: 'UpgradesListCtrl'
})
.when('/sync', {
  templateUrl: 'views/sync.html',
  controller: 'SyncCtrl'
})
.when('/reports/traffic_report', {
  templateUrl: 'views/reports/traffic_report.html',
  controller: 'ReportsTrafficReportCtrl'
})
.when('/cargo/create', {
  templateUrl: 'views/cargo/create.html',
  controller: 'CargoCreateCtrl'
})
.when('/boarding/scan', {
  templateUrl: 'views/boarding/scan.html',
  controller: 'BoardingScanCtrl'
})
.when('/boarding/index', {
  templateUrl: 'views/boarding/index.html',
  controller: 'BoardingIndexCtrl'
})
	.otherwise({
		redirectTo: '/'
	});
}).run(function ($rootScope, $location, RESTFactory, toaster, $interval) {
	
	/* On route change */
	$rootScope.$on("$routeChangeStart", function (event, next, current) {
		
		var url = $location.url();
		if (url.length != 0)	{ // Allow session only if route url is defined
			/* Check session to see if user is logged in */
			$rootScope.authenticated = false;
			RESTFactory.get('session').then(function (results) {

				/* Check if authenticated*/
				if (results.id) {
				
					// Initialize root values
					$rootScope.authenticated = true;
					$rootScope.id = results.id;
					$rootScope.username = results.username;
					$rootScope.role = results.role;
					$rootScope.fullname = results.fullname;
					$rootScope.current_series = results.current_series;

					/* Stop ticket interval */
					$interval.cancel($rootScope.ticketInterval);
					$rootScope.ticketInterval = undefined;

					/* Stop seat interval */
					$interval.cancel($rootScope.seatInterval);
					$rootScope.seatInterval = undefined;

					// Get Permission List
					RESTFactory.get('permissions/request/role/' + $rootScope.role).then(function(response)	{
						var permissionList = response.data;

						// Allow this routes by default regardless of role
						var allowedRoute = ['/', '/dashboard', '/login', '/logout', '/auth/settings'];

						for (var index in allowedRoute)	{
							permissionList.push({
								id: $rootScope.id,
								role: $rootScope.role,
								url_route: allowedRoute[index]
							});
						}

						/* Check if user is allowed for selected route */
						var authorized = false;
						for (var index in permissionList)	{
							if (permissionList[index].url_route == url)	{ // Checks if authenticated
								authorized = true;
								break; // Stops unecessary loop
							}
						}

						// Redirect to error page if not authorized
						if (!authorized)	{
							$location.path("/");
							toaster.pop('error', "Error", "You are not authorized to view that page");
						}
					});

				} else {
				    $location.path("/login");
				}
			});
		}
	});
});
