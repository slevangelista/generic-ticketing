<?php

$slim->route->get('/barcode/:text', function($text)  {
    global $slim;

    // Including all required classes
    require_once('../barcode/class/BCGFontFile.php');
    require_once('../barcode/class/BCGColor.php');
    require_once('../barcode/class/BCGDrawing.php');

    // Including the barcode technology
    require_once('../barcode/class/BCGcode39.barcode.php');

    // Loading Font
    $font = new BCGFontFile('../barcode/font/Arial.ttf', 18);

    // Don't forget to sanitize user inputs
    $text = isset($text) ? $text : 'HELLO';

    // The arguments are R, G, B for color.
    $color_black = new BCGColor(0, 0, 0);
    $color_white = new BCGColor(255, 255, 255);

    $drawException = null;
    try {
        $code = new BCGcode39();
        $code->setScale(2); // Resolution
        $code->setThickness(30); // Thickness
        $code->setForegroundColor($color_black); // Color of bars
        $code->setBackgroundColor($color_white); // Color of spaces
        $code->setFont($font); // Font (or 0)
        $code->parse($text); // Text
    } catch(Exception $exception) {
        $drawException = $exception;
    }

    /* Here is the list of the arguments
    1 - Filename (empty : display on screen)
    2 - Background color */
    $drawing = new BCGDrawing('', $color_white);
    if($drawException) {
        $drawing->drawException($drawException);
    } else {
        $drawing->setBarcode($code);
        $drawing->draw();
    }

    generateBarcode(200,$drawing);
});

