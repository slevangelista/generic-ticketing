'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:ReportsComplimentaryTicketsCtrl
 * @description
 * # ReportsComplimentaryTicketsCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('ReportsComplimentaryTicketsCtrl', function ($scope, $rootScope, RESTFactory, HelperService) {

	// Initializations
	// ---- ---- ---- ----
	$scope.complimentary_tickets = {};

    // Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Vessel
    HelperService.getListActive('vessel').then(function(response) {
        $scope.vesselList = response;
    });

    // Port
    HelperService.getListActive('port').then(function(response) {
        $scope.portList = response;
    });

    // Route
    HelperService.getListActive('route').then(function(response) {
        $scope.routeList = response;
    });

    // Shipper
    HelperService.getListActive('shipper').then(function(response)    {
        $scope.shipperList = response;
    });

	// Process Methods
	// ---- ---- ---- ----
	$scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

	$scope.update = function()	{
		$scope.getWaybills();
	};

	// Voyage Display
    $scope.onVoyageSelect = function(voyage_number, vessel_id, trip_id)  { // On voyage select do the ff
        $scope.update();
        $scope.voyage_number = voyage_number;
        $scope.vessel_id = vessel_id;
        $scope.trip_id = trip_id;
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.complimentary_tickets.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.complimentary_tickets.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.complimentary_tickets.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.complimentary_tickets.voyage = null;
    };
    // End voyage select

    // Get Lists
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            $scope.voyageList = response.data;
        });
        $scope.voyageList = [];
    };

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    $scope.joinShipper = function(id)   {
        return HelperService.join(id, $scope.shipperList).name;
    };

    $scope.joinVessel = function(id)	{
    	if ($scope.complimentary_tickets.voyage != null)	{
    		return HelperService.join(id, $scope.vesselList).name;	
    	}
    	return "";
    };

    $scope.joinPortByTrip = function(id)	{
    	if ($scope.complimentary_tickets.voyage != null)	{
    		var route = HelperService.join(id, $scope.tripList).route_id; // Get Route via Trip
    		var source_port = HelperService.join(route, $scope.routeList).source_port; // Get source port (id) via Route
    		return HelperService.join(source_port, $scope.portList).name;
    	}
    	return "";
    };

	// REST Methods
	// ---- ---- ---- ----
	$scope.getWaybills = function()	{
		RESTFactory.get('report/complimentary_tickets/' + $scope.complimentary_tickets.voyage).then(function(response){
			$scope.waybills = response.data;
		});
	};

    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });

        saveAs(blob, "complimentary_tickets.xls");
    };

});
