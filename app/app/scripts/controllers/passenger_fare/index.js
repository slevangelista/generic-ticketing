'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:passengerFareIndexCtrl
 * @description
 * # passengerFareIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('PassengerFareIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.passengerFareItems = {};

    // Pagination
    $scope.passengerFareCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Generate Lists

    // Passenger Type
    RESTFactory.get('passenger_type').then(function(response)   {
        $scope.passengerTypeList = response.data;
    });
    // Route
    HelperService.getListActive('route').then(function(response)    {
        $scope.routeList = response;
    });
    // Seating Class
    HelperService.getListActive('seating_class').then(function(response)    {
        $scope.seatingClassList = response;
    });

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getPassengerFareByPage();
        $scope.getPassengerFareByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.passengerFareItems[data.id] = {
            'id': data.id,
            'type': data.type,
            'route': data.route,
            'class': data.class,
            'price': data.price,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // Used for Joins
    $scope.joinPassengerType = function(id)   {
        return HelperService.join(id, $scope.passengerTypeList).name;
    };

    $scope.joinRoute = function(id)   {
        return HelperService.join(id, $scope.routeList).name;
    };

    $scope.joinSeatingClass = function(id)    {
        return HelperService.join(id, $scope.seatingClassList).name;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getPassengerFareByPage = function()    {
        RESTFactory.get('passenger_fare/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.passengerFareList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getPassengerFareByCount = function()   {
        RESTFactory.get('passenger_fare/' + 'request/count').then(function(response) {
            $scope.passengerFareCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addPassengerFare = function() {
        var params = $.param($scope.newPassengerFare);
        RESTFactory.post('passenger_fare', params).then(function(response)  {
            // Response if success
            $scope.newPassengerFare = {};
            $scope.update();
            toaster.pop("success", "Success", "Passenger fare successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Passenger fare failed to add");
        });
    };  

    $scope.editPassengerFare = function()   {
        var params = $.param($scope.passengerFareItems[$scope.currentIndex]);
        RESTFactory.put('passenger_fare/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Passenger fare successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Passenger fare failed to edit");
        });
    };

});
