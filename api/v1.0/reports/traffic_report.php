<?php

$slim->route->get('/report/traffic_report/:voyage', function($voyage_id) use ($slim)   {

    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());
    $status = 200;

    try {

        // Get voyage based on date
        $sql = "
            SELECT
                voyage.id AS id,
                voyage.trip,
                voyage.number,
                voyage.vessel,
                voyage.capacity,
                voyage.departure_date,
                trip.departure_time as departure_time,
                route.name as route_name
            FROM voyage
            LEFT JOIN trip ON voyage.trip = trip.id
            LEFT JOIN route ON trip.route_id = route.id
            WHERE voyage.id = :voyage
        ";

        $voyage = $slim->db->SQL($sql, array(
            ':voyage' => $voyage_id
        ));

        // Call method getTrafficReport to process report
        $resp['data'] = getTrafficReport($voyage);

    } catch (Exception $e)  {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = $e->getMessage();
    }

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);

});

$slim->route->get('/report/traffic_report/:date_start/:date_end/:vessel/:route', function($date_start, $date_end, $vessel, $route) use ($slim)	{

	$resp = array('status'=>'success','message'=>'Query Success','data'=>array());
    $status = 200;

    try {
    	
        /* Check if correct start/end */
        if ( strtotime($date_start) > strtotime($date_end) )
            throw new Exception("Start date must not exceed end date");

        /* Step 1: SQL of voyage based on date range */
        $sql = "
            SELECT
                voyage.id AS id,
                voyage.trip,
                voyage.number,
                voyage.vessel,
                voyage.capacity,
                voyage.departure_date,
                trip.departure_time as departure_time,
                route.name as route_name
            FROM voyage
            LEFT JOIN trip ON voyage.trip = trip.id
            LEFT JOIN route ON trip.route_id = route.id
            WHERE departure_date >= :date_start AND departure_date <= :date_end
        ";
        $params[':date_start'] = $date_start;
        $params[':date_end'] = $date_end;

        // Step 1.A: Add vessel param (if selected)
        if ($vessel)   {
            $sql .= " AND vessel = :vessel";
            $params[':vessel'] = $vessel;
        }

        // Step 1.B: Add route param (if selected)
        if ($route)    {
            $sql .= " AND trip.route_id = :route";
            $params[':route'] = $route;
        }

        $voyage = $slim->db->SQL($sql, $params);
        
        // Call method getTrafficReport to process report
        $resp['data'] = getTrafficReport($voyage);

    } catch (Exception $e) {
    	$status = "400";
        $resp['status'] = "error";
        $resp['message'] = $e->getMessage();
    }

    JSONResponse($status, $resp);

});

function getTrafficReport($voyage)	{
	global $slim;

    /* Throw exception if voyage is empty */
    if (!$voyage)
        throw new Exception("No voyage found");

    /* Init */
    $response = array(
        'revenue_total' => 0,
        'revenue_waybill_total' => 0,
        'pax_count_total' => 0,
        'waybill_count_total' => 0
    );

    $revenue = array(
        'ticket' => 0
    );

    $blacklist = "6,7";
    $capacity_total = 0;

    /* Query Init */

    // Get Seating Class
    $seating_class = $slim->db->readAll('seating_class', array(
        'where' => 'active = 1'
    ));

    // Get Passenger Type
    $passenger_type = $slim->db->readAll('passenger_type', array(
        'where' => 'active = 1'
    ));

    // Get Cargo Class
    $cargo_class = $slim->db->readAll('cargo_class', array(
        'where' => 'active = 1'
    ));

    /* Loop Voyage */
	foreach ($voyage as $key => $value)	{

        /* Get voyage per date */
        $response['date'][$value['departure_date']]['voyage'][$value['id']] = $value;

        // Get VesselTemplate
        $vesselTemplate = $slim->db->readAll('vessel_template', array(
            'where' => 'vessel_id = :vessel_id',
            'params' => array(
                ':vessel_id' => $value['vessel']
            )
        ));

        // Get capacity total
        $capacity_total += $value['capacity'];

        $response['date'][$value['departure_date']]['capacity_total_average'] = $capacity_total / count($response['date'][$value['departure_date']]['voyage']);

        foreach ($vesselTemplate as $k => $v) {

            // Get seating class
            $seat_class_per_vessel = $slim->db->readByPk('seating_class', $v['seating_class_id']);

            $response['date'][$value['departure_date']]['capacity'][$seat_class_per_vessel['code']][$value['id']] = count($slim->db->readAll('seat', array(
                'where' => 'seating_class = :seating_class AND active = 1',
                'params' => array(
                    ':seating_class' => $v['seating_class_id']
                )
            ))); 

            /* Get capacity average */
            $response['date'][$value['departure_date']]['capacity_average'][$seat_class_per_vessel['code']] = 
                array_sum($response['date'][$value['departure_date']]['capacity'][$seat_class_per_vessel['code']]) / 
                count($response['date'][$value['departure_date']]['capacity'][$seat_class_per_vessel['code']]);
        }

		// Tickets
        // Tickets depends on not just voyage as its pk, so readAll is necessary
		$tickets = $slim->db->readAll('revenue_ticket', array(
			'where' => 'voyage = :voyage',
			'params' => array(
				':voyage' => $value['id']
			)
		));

        // Summarize all ticket
        $revenue['ticket'] = 0;
        foreach ($tickets as $k => $v)  {
            $revenue['ticket'] += $v['revenue'];
        }

        // Waybill
        $waybill = $slim->db->read('revenue_waybill', array(
            'where' => 'voyage = :voyage',
            'params' => array(
                ':voyage' => $value['id']
            )
        ));
        if ($waybill)
            $revenue['waybill'] = $waybill['revenue'];
        else
            $revenue['waybill'] = 0;

        // Baggage
        $baggage = $slim->db->read('revenue_baggage', array(
            'where' => 'voyage = :voyage',
            'params' => array(
                ':voyage' => $value['id']
            )
        ));
        if ($baggage)
            $revenue['baggage'] = $baggage['revenue'];
        else
            $revenue['baggage'] = 0;

        // Upgrades
        $upgrades = $slim->db->read('revenue_upgrades', array(
            'where' => 'voyage = :voyage',
            'params' => array(
                ':voyage' => $value['id']
            )
        ));

        if ($upgrades)
            $revenue['upgrades'] = $upgrades['revenue'];
        else
            $revenue['upgrades'] = 0;
        
        /*
        // Cash-on-Hand
        $cash_on_hand = $slim->db->read('revenue_net_cash', array(
            'where' => 'voyage = :voyage',
            'params' => array(
                ':voyage' => $value['id']
            )
        ));

        if ($cash_on_hand)
            $revenue['cash_on_hand'] = $cash_on_hand['revenue'];
        else
            $revenue['cash_on_hand'] = 0;

        // Disbursements
        $disbursements = $slim->db->read('revenue_deductions', array(
            'where' => 'voyage = :voyage',
            'params' => array(
                ':voyage' => $value['id']
            )
        ));

        if ($disbursements)
            $revenue['disbursements'] = $disbursements['revenue'];
        else
            $revenue['disbursements'] = 0;
        */

		// Summarize all revenues
		$response['date'][$value['departure_date']]['revenue_voyage'][$value['id']] = 0;
        // Init 
        if (!isset($response['date'][$value['departure_date']]['revenue_date']))
            $response['date'][$value['departure_date']]['revenue_date'] = 0;

		foreach ($revenue as $k => $v)	{
            // Init
            if (!isset($response['date'][$value['departure_date']]['revenue_cat'][$value['id']][$k]))
                $response['date'][$value['departure_date']]['revenue_cat'][$value['id']][$k] = 0;

            if (!isset($response['date'][$value['departure_date']]['revenue_cat_total'][$k]))
                $response['date'][$value['departure_date']]['revenue_cat_total'][$k] = 0;

            if (!isset($response['revenue_cat_total'][$k]))
                $response['revenue_cat_total'][$k] = 0;

			$response['date'][$value['departure_date']]['revenue_voyage'][$value['id']] += $v;
            $response['date'][$value['departure_date']]['revenue_cat'][$value['id']][$k] += $v;
            $response['date'][$value['departure_date']]['revenue_cat_total'][$k] += $v;
            $response['date'][$value['departure_date']]['revenue_date'] += $v;
            $response['revenue_cat_total'][$k] += $v;
            $response['revenue_total'] += $v;
		}

        /* Get PAX Count */
        foreach ($seating_class as $k => $v) {
            foreach ($passenger_type as $kk => $vv) {
                // Get PAX Count
                $pax = count($slim->db->readAll('ticket', array(
                    'where' => "voyage = :voyage AND seating_class = :seating_class AND passenger_type = :passenger_type AND status NOT IN ({$blacklist})",
                    'params' => array(
                        ':voyage' => $value['id'],
                        ':seating_class' => $v['id'],
                        ':passenger_type' => $vv['id']
                    )
                )));

                // Init
                if (!isset($response['date'][$value['departure_date']]['pax_count'][$value['id']][$v['code']][$vv['name']]))
                    $response['date'][$value['departure_date']]['pax_count'][$value['id']][$v['code']][$vv['name']] = 0;

                if (!isset($response['date'][$value['departure_date']]['pax_count']['total'][$v['code']][$vv['name']]))
                    $response['date'][$value['departure_date']]['pax_count']['total'][$v['code']][$vv['name']] = 0;

                if (!isset($response['date'][$value['departure_date']]['pax_count_voyage'][$value['id']]))
                    $response['date'][$value['departure_date']]['pax_count_voyage'][$value['id']] = 0;

                if (!isset($response['date'][$value['departure_date']]['pax_count_date']))
                    $response['date'][$value['departure_date']]['pax_count_date'] = 0;

                if (!isset($response['date'][$value['departure_date']]['pax_count_code'][$v['code']][$value['id']]))
                    $response['date'][$value['departure_date']]['pax_count_code'][$v['code']][$value['id']] = 0;

                if (!isset($response['date'][$value['departure_date']]['pax_count_code_total'][$v['code']]))
                     $response['date'][$value['departure_date']]['pax_count_code_total'][$v['code']] = 0;

                // Assign to values
                $response['date'][$value['departure_date']]['pax_count'][$value['id']][$v['code']][$vv['name']] += $pax;
                $response['date'][$value['departure_date']]['pax_count']['total'][$v['code']][$vv['name']] += $pax;
                $response['date'][$value['departure_date']]['pax_count_voyage'][$value['id']] += $pax;
                $response['date'][$value['departure_date']]['pax_count_code'][$v['code']][$value['id']] += $pax;
                $response['date'][$value['departure_date']]['pax_count_code_total'][$v['code']] += $pax;
                $response['date'][$value['departure_date']]['pax_count_date'] += $pax;
                $response['pax_count_total'] += $pax;

                // Get Code Average
                $response['date'][$value['departure_date']]['pax_count_code_average'][$v['code']] = $response['date'][$value['departure_date']]['pax_count_code_total'][$v['code']] 
                / count($response['date'][$value['departure_date']]['voyage']);
            }
        }

        foreach ($seating_class as $k => $v)    {
             /* Get Load Factor of PAX per voyage*/
            $response['date'][$value['departure_date']]['load_factor'][$v['code']][$value['id']] = ( $response['date'][$value['departure_date']]['pax_count_code'][$v['code']][$value['id']] 
            / $response['date'][$value['departure_date']]['capacity'][$v['code']][$value['id']] ) * 100;
        

            /* Total load factor total */
            $response['date'][$value['departure_date']]['load_factor_total'][$v['code']] = ( $response['date'][$value['departure_date']]['pax_count_code_average'][$v['code']]
            / $response['date'][$value['departure_date']]['capacity_average'][$v['code']] ) * 100;

        }

        /* Get waybill count */
        foreach ($cargo_class as $k => $v)  {
            // Get waybill count
            $waybill = count($slim->db->readAll('waybill', array(
                'where' => "voyage = :voyage AND cargo_class = :cargo_class AND status NOT IN ({$blacklist})",
                'params' => array(
                    ':voyage' => $value['id'],
                    ':cargo_class' => $v['id']
                )
            )));

            // Init
            if (!isset($response['date'][$value['departure_date']]['waybill_count']['total'][$v['name']]))
                $response['date'][$value['departure_date']]['waybill_count']['total'][$v['name']] = 0;

            if (!isset($response['date'][$value['departure_date']]['waybill_count_voyage'][$value['id']]))
                $response['date'][$value['departure_date']]['waybill_count_voyage'][$value['id']] = 0;

            if (!isset($response['date'][$value['departure_date']]['waybill_count_date']))
                $response['date'][$value['departure_date']]['waybill_count_date'] = 0;

            // Assign to values
            $response['date'][$value['departure_date']]['waybill_count'][$value['id']][$v['name']] = $waybill;
            $response['date'][$value['departure_date']]['waybill_count']['total'][$v['name']] += $waybill;
            $response['date'][$value['departure_date']]['waybill_count_voyage'][$value['id']] += $waybill;
            $response['date'][$value['departure_date']]['waybill_count_date'] += $waybill;

            $response['waybill_count_total'] += $waybill;
        }

	}

    return $response;

}