'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:waybillIndexCtrl
 * @description
 * # waybillIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('CargoWaybillCtrl', function ($scope, RESTFactory, HelperService) {

    // Initialize
    // --- ---- ---- ----
    $scope.waybillItems = {};

    // Pagination
    $scope.waybillCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Generate List
    HelperService.getListActive('cargo_class').then(function(response)  {
        $scope.cargoClassList = response;
    });

    HelperService.getListActive('status').then(function(response)   {
        $scope.statusList = response;
    });

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getWaybillByPage();
        $scope.getWaybillByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.waybillItems[data.id] = {
            'id': data.id,
            'lading_no': data.lading_no,
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // Used for Joins
    $scope.joinCargoClass = function(id)   {
        var cargoClassAll = $scope.cargoClassList;
        for (var index in cargoClassAll)    {
            if (cargoClassAll[index].id == id)  {
                return cargoClassAll[index].name;
            }
        }
    };

    $scope.joinStatus = function(id)   {
        var statusAll = $scope.statusList;
        for (var index in statusAll)    {
            if (statusAll[index].id == id)  {
                return statusAll[index].name;
            }
        }
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getWaybillByPage = function()    {
        RESTFactory.get('waybill/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.waybillList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getWaybillByCount = function()   {
        RESTFactory.get('waybill/' + 'request/count').then(function(response) {
            $scope.waybillCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.printWaybill = function() {
        RESTFactory.get('renderWaybill/' + $scope.waybillItems[$scope.currentIndex].lading_no).then(function(response) {
            // Response if success
        }, function(response)   {
            // Response if fail
        });
    };

    $scope.refundWaybill = function()   {
        var params = $.param({
            'status': 6
        });
        RESTFactory.put('waybill/' + $scope.currentIndex, params).then(function() {
            $scope.update();

            // Get Waybill Details (For Revenue Update)
            RESTFactory.get('waybill/' + $scope.currentIndex).then(function(response) {
                // Update Ticket Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.price_paid,
                });
                RESTFactory.post('revenue/waybill/refund', revenueParams);
            });
        });
    };

    $scope.cancelWaybill = function()   {
        var params = $.param({
            'status': 7
        });
        RESTFactory.put('waybill/' + $scope.currentIndex, params).then(function() {
            $scope.update();

            // Get Waybill Details (For Revenue Update)
            RESTFactory.get('waybill/' + $scope.currentIndex).then(function(response) {
                // Update Ticket Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.price_paid,
                });
                RESTFactory.post('revenue/waybill/cancel', revenueParams);
            });
        });
    };

});
