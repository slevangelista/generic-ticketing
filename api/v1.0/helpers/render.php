<?php

class Render {

    /* Constants */
    const COMPANY_NAME = "ARCHIPELAGO PHILIPPINE FERRIES CORPORATION";
    const COMPANY_ADDRESS_1 = "5D Unioil Center Bldg., Acacia Ave";
    const COMPANY_ADDRESS_2 = "Madrigal Business Park, Ayala Alabang, Muntinlupa City";
    const COMPANY_CONTACT_NOS = "Tel. No: (02) 842-9341 Fax No: 775-0442";

    /**
     * Public Methods
     */
    public static function renderTicket($ticket_no)   {
        $ticket = self::ticketQueryAPI($ticket_no);
        self::renderTicketAPI($ticket_no, $ticket[0]);
    }

    public static function renderWaybill($lading_no)    {
        $waybill = self::waybillQueryAPI($lading_no);
        self::renderWaybillAPI($lading_no, $waybill[0]);
    }

    /**
     * Internal API Methods
     */
    function ticketQueryAPI($ticket_no)   {
        global $slim;

        // Query join values to generate data
        $sql = "
            SELECT
                ticket.id AS id,
                ticket.ticket_no,
                ticket.booking_no,
                ticket.series_no,
                voyage.number as voyage,
                CONCAT(passenger.first_name, ' ', passenger.last_name) as passenger,
                ticket.price_paid,
                seat.name as seat,
                seating_class.name as seating_class,
                CONCAT(users.first_name, ' ', users.last_name) as teller,
                ticket.created_by as created_by,
                vessel.name as vessel_name,
                trip.departure_time,
                route.name as route_name,
                create_time as date
            FROM ticket
            LEFT JOIN passenger ON ticket.passenger = passenger.id
            LEFT JOIN voyage ON ticket.voyage = voyage.id
            LEFT JOIN vessel ON voyage.vessel = vessel.id
            LEFT JOIN trip ON voyage.trip = trip.id
            LEFT JOIN route ON trip.route_id = route.id
            LEFT JOIN seating_class ON ticket.seating_class = seating_class.id
            LEFT JOIN seat ON ticket.seat = seat.id
            LEFT JOIN users ON ticket.created_by = users.id
            WHERE ticket.ticket_no = :ticket_no
        ";

        $ticket = $slim->db->SQL($sql, array(
            ':ticket_no' => $ticket_no
        ));

        // Get summation of upgrades for a ticket (if there are any)
        $upgrades = $slim->db->read('upgrades', array(
            'select' => 'SUM(amt) AS amt',
            'where' => 'ticket_id = :ticket_id',
            'params' => array(
                ':ticket_id' => $ticket[0]['id']
            )
        ));
        
        // If upgrade query returns result
        if ($upgrades)
            $ticket[0]['price_paid'] += $upgrades['amt'];

        return $ticket;
    }

    function waybillQueryAPI($lading_no)    {
        global $slim;

        // Query join values to generate data
        $sql = "
            SELECT
                waybill.lading_no,
                waybill.booking_no,
                waybill.series_no,
                voyage.number as voyage,
                waybill.price_paid,
                cargo.plate_num as plate_num,
                cargo.shipper as shipper,
                cargo.address as address,
                cargo.article_desc as article_desc,
                cargo_class.name as cargo_name,
                shipper.name as shipper_name,
                vessel.name as vessel_name,
                trip.departure_time,
                route.name as route_name,
                CONCAT(users.first_name, ' ', users.last_name) as teller,
                waybill.created_by as created_by,
                create_time as date
            FROM waybill
            LEFT JOIN voyage ON waybill.voyage = voyage.id
            LEFT JOIN vessel ON voyage.vessel = vessel.id
            LEFT JOIN trip ON voyage.trip = trip.id
            LEFT JOIN route ON trip.route_id = route.id
            LEFT JOIN users ON waybill.created_by = users.id
            LEFT JOIN cargo ON waybill.cargo = cargo.id
            LEFT JOIN cargo_class ON waybill.cargo_class = cargo_class.id
            LEFT JOIN shipper ON cargo.shipper = shipper.id
            WHERE waybill.lading_no = :lading_no
        ";

        $waybill = $slim->db->SQL($sql, array(
            ':lading_no' => $lading_no
        ));

        return $waybill;
    }

    /**
     * Renders ticket and save to pdf
     */
    function renderTicketAPI($barcode, $ticket = array(
            'ticket_no' => NULL,
            'booking_no' => NULL,
            'series_no' => NULL,
            'passenger' => NULL,
            'voyage' => NULL,
            'price_paid' => NULL,
            'passenger_type' => NULL,
            'seat' => NULL,
            'seating_class' => NULL,
            'teller' => NULL,
            'date' => NULL
        ))    {
        global $slimConfig;
        global $slim;

        // Save CUPS Settings to local variable
        // $cups = $slimConfig['cups'];
        $user = $slim->db->readByPk('users', $ticket['created_by']);
        $cups = $slim->db->readByPk('printer', $user['assigned_printer']);

        // Get barcode image
        $html = "
        <html>
            <head>
                <style>
                    @page { margin: 0px; }
                    body { margin: 0px; }
                    p { font-size: 9px; margin: 0px; }
                    td { font-size: 9px; margin: 0px; }
                    th { font-size: 10px; margin: 0px; }
                    .address { font-size: 7px; margin: 0px; }
                    .seat {
                        border: 1px solid black;
                        border-radius: 25px;
                        box-shadow: 10px 10px 5px #888888;
                        margin: 10px;
                    }
                </style>
            </head>
            <body>
                <center>
                    <div>
                        <br />
                        <p class='header'>" . self::COMPANY_NAME . "</p>
                        <br />
                        <p class='header'>Inspector Copy</p>
                        <table style='margin-left: auto; margin-right: auto;'>
                            <tr>
                                <td>Ticket No: {$ticket['ticket_no']}</td>
                                <td>Booking No: {$ticket['booking_no']}</td>
                            </tr>
                            <tr>
                                <td>Series No: {$ticket['series_no']}</td>
                                <td>Voyage: {$ticket['voyage']}</td>
                            </tr>
                            <tr>
                                <td>Vessel: {$ticket['vessel_name']}</td>
                                <td>Route: {$ticket['route_name']}</td>
                            </tr>
                            <tr>
                                <td>Departure Time: </td>
                                <td>{$ticket['departure_time']}</td>
                            </tr>
                            <tr>
                                <td>Passenger Name: {$ticket['passenger']}</td>
                                <td>Price Paid: Php {$ticket['price_paid']}</td>
                            </tr>
                            <tr>
                                <th colspan='2'>Seat Assinged: {$ticket['seat']}</th>
                            </tr>
                            <tr>
                                <th colspan='2'>Seating Class: {$ticket['seating_class']}</th>
                            </tr>
                        </table>
                        <p>Ticket Generated By: {$ticket['teller']}</p>
                        <p>Time Created: {$ticket['date']}</p>
                        <p>FerrySafe, FerryFast, FerryConvenient</p>
                        <br />
                        <img style='width: 140px; height: auto;' src='http://{$slimConfig['url']}/archipelago-web-api/v1.0/barcode/{$barcode}' />
                    </div>
                    <center>---------------------------------------------------</center>
                    <div>
                        <p class='header'>" . self::COMPANY_NAME . "</p>
                        <p class='address'>" . self::COMPANY_ADDRESS_1 . "</p>
                        <p class='address'>" . self::COMPANY_ADDRESS_2 . "</p>
                        <p class='address'>" . self::COMPANY_CONTACT_NOS . "</p>
                        <br />
                        <table style='margin-left: auto; margin-right: auto;'>
                            <tr>
                                <td>Ticket No: {$ticket['ticket_no']}</td>
                                <td>Booking No: {$ticket['booking_no']}</td>
                            </tr>
                            <tr>
                                <td>Series No: {$ticket['series_no']}</td>
                                <td>Voyage: {$ticket['voyage']}</td>
                            </tr>
                            <tr>
                                <td>Vessel: {$ticket['vessel_name']}</td>
                                <td>Route: {$ticket['route_name']}</td>
                            </tr>
                            <tr>
                                <td>Departure Time: </td>
                                <td>{$ticket['departure_time']}</td>
                            </tr>
                            <tr>
                                <td>Passenger Name: {$ticket['passenger']}</td>
                                <td>Price Paid: Php {$ticket['price_paid']}</td>
                            </tr>
                        </table>

                        <div class='seat'>
                            <h4>Seat Assigned:</h4>
                            <h1 style='margin-bottom: 0;'>{$ticket['seat']}</h1>
                            <h2>{$ticket['seating_class']}</h2>
                        </div>

                        <p>Ticket Generated By: {$ticket['teller']}</p>
                        <p>Time Created: {$ticket['date']}</p>
                        <p>FerrySafe, FerryFast, FerryConvenient</p>
                        <br />
                        <img style='width: 140px; height: auto;' src='http://{$slimConfig['url']}/archipelago-web-api/v1.0/barcode/{$barcode}' />
                    </div>
                </center>
            </body>
        </html>
        ";

        $paper_size = array(0,0,160, 650);
        $dompdf = new DOMPDF();
        $dompdf->set_paper($paper_size);
        $dompdf->load_html($html);
        $dompdf->render();

        // $dompdf->stream("{$barcode}.pdf");
        // die();

        $output = $dompdf->output();
        $file = "../assets/{$barcode}.pdf";
        file_put_contents($file, $output);

        // Execute Script
        exec("lpr -H {$cups['host_address']}:{$cups['port']} -U {$cups['host_name']} -P {$cups['printer']} -r {$file}");
    }

    function renderWaybillAPI($barcode, $waybill = array()) {
        global $slimConfig;
        global $slim;

        // Save CUPS Settings to local variable
        // $cups = $slimConfig['cups'];
        $user = $slim->db->readByPk('users', $waybill['created_by']);
        $cups = $slim->db->readByPk('printer', $user['assigned_printer']);

        // Get barcode image
        $html = "
        <html>
            <head>
                <style>
                    @page { margin: 0px; }
                    body { margin: 0px; }
                    p { font-size: 9px; margin: 0px; }
                    td { font-size: 9px; margin: 0px; }
                    th { font-size: 10px; margin: 0px; }
                    .address { font-size: 7px; margin: 0px; }
                    .seat {
                        border: 1px solid black;
                        border-radius: 25px;
                        box-shadow: 10px 10px 5px #888888;
                        margin: 10px;
                    }
                </style>
            </head>
            <body>
                <center>
                    <div>
                        <br />
                        <p class='header'>" . self::COMPANY_NAME . "</p>
                        <br />
                        <p class='header'>Inspector Copy</p>
                        <table style='margin-left: auto; margin-right: auto;'>
                            <tr>
                                <td>Lading No: {$waybill['lading_no']}</td>
                                <td>Booking No: {$waybill['booking_no']}</td>
                            </tr>
                            <tr>
                                <td>Series No: {$waybill['series_no']}</td>
                                <td>Voyage: {$waybill['voyage']}</td>
                            </tr>
                            <tr>
                                <td>Vessel: {$waybill['vessel_name']}</td>
                                <td>Route: {$waybill['route_name']}</td>
                            </tr>
                            <tr>
                                <td>Departure Time: </td>
                                <td>{$waybill['departure_time']}</td>
                            </tr>
                            <tr>
                                <td>Plate Number: {$waybill['plate_num']}</td>
                                <td>Shipper: {$waybill['shipper_name']}</td>
                            </tr>
                            <tr>
                                <td>Article Desc: {$waybill['article_desc']}</td>
                                <td>Price Paid: Php {$waybill['price_paid']}</td>
                            </tr>
                        </table>

                        <p class='header'>{$waybill['cargo_name']}</p>

                        <br />
                        <p>Ticket Generated By: {$waybill['teller']}</p>
                        <p>Time Created: {$waybill['date']}</p>
                        <p>FerrySafe, FerryFast, FerryConvenient</p>
                        <br />
                        <img style='width: 140px; height: auto;' src='http://{$slimConfig['url']}/archipelago-web-api/v1.0/barcode/{$barcode}' />
                    </div>
                    <center>---------------------------------------------------</center>
                    <div>
                        <br />
                        <p class='header'>" . self::COMPANY_NAME . "</p>
                        <p class='address'>" . self::COMPANY_ADDRESS_1 . "</p>
                        <p class='address'>" . self::COMPANY_ADDRESS_2 . "</p>
                        <p class='address'>" . self::COMPANY_CONTACT_NOS . "</p>
                        <br />
                        <table style='margin-left: auto; margin-right: auto;'>
                            <tr>
                                <td>Lading No: {$waybill['lading_no']}</td>
                                <td>Booking No: {$waybill['booking_no']}</td>
                            </tr>
                            <tr>
                                <td>Series No: {$waybill['series_no']}</td>
                                <td>Shipper: {$waybill['shipper_name']}</td>
                            </tr>
                            <tr>
                                <td>Vessel: {$waybill['vessel_name']}</td>
                                <td>Route: {$waybill['route_name']}</td>
                            </tr>
                            <tr>
                                <td>Departure Time: </td>
                                <td>{$waybill['departure_time']}</td>
                            </tr>
                            <tr>
                                <td>Plate Number: {$waybill['plate_num']}</td>
                                <td>Article Desc: {$waybill['article_desc']}</td>
                            </tr>
                            <tr>
                                <td colspan='2'>Price Paid: {$waybill['price_paid']}</td>
                            </tr>
                        </table>

                        <div class='seat'>
                            <h5>{$waybill['cargo_name']}</h5>
                            <h5 style='margin-bottom: 0;'>{$waybill['plate_num']}</h5>
                            <h6>Voyage: {$waybill['voyage']}</h6>
                        </div>

                        <br />
                        <p>Ticket Generated By: {$waybill['teller']}</p>
                        <p>Time Created: {$waybill['date']}</p>
                        <p>FerrySafe, FerryFast, FerryConvenient</p>
                        <br />
                        <img style='width: 140px; height: auto;' src='http://{$slimConfig['url']}/archipelago-web-api/v1.0/barcode/{$barcode}' />
                    </div>
                </center>
            </body>
        </html>
        ";

        $paper_size = array(0,0,160, 650);
        $dompdf = new DOMPDF();
        $dompdf->set_paper($paper_size);
        $dompdf->load_html($html);
        $dompdf->render();

        // $dompdf->stream("{$barcode}.pdf");
        // die();

        $output = $dompdf->output();
        $file = "../assets/{$barcode}.pdf";
        file_put_contents($file, $output);

        // Execute Script
        exec("lpr -H {$cups['host_address']}:{$cups['port']} -U {$cups['host_name']} -P {$cups['printer']} -r {$file}");
    }
}
