<?php

$slim->route->get("/report/inspection_report/:voyage_id", function($voyage)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $sql = "SELECT cargo.shipper, cargo.on_account, cargo.plate_num, cargo.article_desc, voyage.number AS voyage_no,"
    	 . " voyage.departure_date, voyage.id as voyage_id,cargo_class.name AS class, waybill.lading_no,"
    	 . " waybill.series_no, waybill.booking_no, waybill.price_paid, route.name AS route_name, route.dest_port,"
    	 . " route.source_port"
		 . " FROM waybill"
		 . " INNER JOIN cargo ON cargo.id = waybill.cargo"
		 . " INNER JOIN voyage ON voyage.id = waybill.voyage"
		 . " INNER JOIN cargo_class ON cargo_class.id = waybill.cargo_class"
		 //. " INNER JOIN route ON route.id = 1"
		 . " INNER JOIN trip ON trip.id = voyage.trip"
         . " INNER JOIN route on route.id = trip.route_id"
		 . " AND waybill.voyage = :voyage_id";

    $params = array(":voyage_id" => $voyage);
	$waybills = $slim->db->SQL($sql,$params);
	
	$resp['data'] = $slim->db->SQL($sql,$params);
    JSONResponse($status, $resp);
});	