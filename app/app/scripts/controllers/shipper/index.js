'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:shipperIndexCtrl
 * @description
 * # shipperIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('ShipperIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.shipperItems = {};

    // Pagination
    $scope.shipperCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getShipperByPage();
        $scope.getShipperByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.shipperItems[data.id] = {
            'id': data.id,
            'name': data.name,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getShipperByPage = function()    {
        RESTFactory.get('shipper/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.shipperList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getShipperByCount = function()   {
        RESTFactory.get('shipper/' + 'request/count').then(function(response) {
            $scope.shipperCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addShipper = function() {
        var params = $.param($scope.newShipper);
        RESTFactory.post('shipper', params).then(function(response)  {
            // Response if success
            $scope.newShipper = {};
            $scope.update();
            toaster.pop("success", "Success", "Shipper successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Shipper failed to add");
        });
    };  

    $scope.editShipper = function()   {
        var params = $.param($scope.shipperItems[$scope.currentIndex]);
        RESTFactory.put('shipper/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Shipper successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Shipper failed to edit");
        });
    };

});

