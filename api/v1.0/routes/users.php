<?php

// GET

$slim->route->get("/users", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = $slim->db->readAll('users', array(
        'select' => 'id, username, employee_no, first_name, last_name, email, active, role'
    ));

    JSONResponse($status, $resp);
});

// Get Single Record
$slim->route->get("/users/:id", function($id)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = $slim->db->read('users', array(
        'select' => 'id, username, employee_no, first_name, last_name, email, active, role, current_series, assigned_printer',
        'where' => 'id = :id',
        'params' => array(
            ':id' => $id
        )
    ));

    JSONResponse($status, $resp);
});

// 4 Parameter custom REST. Avoid using this when possible
$slim->route->get("/users/request/:request_type/:id", function($request_type, $id) {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    switch($request_type)   {
        case 'count_by_seat_class':
            $resp['data'] = count($slim->db->readAll('users', array(
                'select' => 'id, username, employee_no, first_name, last_name, email, active, role, current_series, assigned_printer',
                'where' => 'seating_class = :seating_class AND active = 1',
                'params' => array(
                    ':seating_class' => $id
                )
            )));
            break;
        default:
            $status = "404";
            $resp['status'] = 'error';
            $resp['message'] = 'Not Found';
            break;
    }

    JSONResponse($status, $resp);
});

// Get all record with pagination OR custom request record
$slim->route->get("/users/:page/:items", function($page, $items)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Check for custom request
    if ($page == 'request') {
        switch ($items) {
            case 'last_record':
                $resp['data'] = $slim->db->read('users', array(
                    'select' => 'id, username, employee_no, first_name, last_name, email, active, role, current_series, assigned_printer',
                    'order' => 'id DESC',
                    'limit' => 1
                ));
                break;
            case 'count':  
                $resp['data'] = count($slim->db->readAll('users'));
                break;
            default: 
                $status = "404";
                $resp['status'] = 'error';
                $resp['message'] = 'Not Found';
                break;
        }
    } else {
        // Pagination Logic
        $index = 0;
        for($i = 1; $i < $page; $i++) {
            $index = $index + $items;
        }

        $resp['data'] = $slim->db->readAll('users', array(
            'select' => 'id, username, employee_no, first_name, last_name, email, active, role, current_series, assigned_printer',
            'order' => 'id ASC',
            'limit' => "{$index}, {$items}",
        ));
    }

    JSONResponse($status, $resp);
});

$slim->route->post('/users', function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            if ($data['password'] == $data['passwordMatch'])    {
                unset($data['passwordMatch']);
                $data['password'] = passwordHash::hash($data['password']);

                foreach ($data as $key => $value) {
                    $values[$key] = ':' . $key;
                    $params[':' . $key] = $value;
                }

                $slim->db->create('users', array(
                    'values' => $values,
                    'params' => $params
                ));
            } else {
                throw new Exception('Password do not match');
            }
        }
    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Add user failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});

$slim->route->put('/users/request/passwordAdmin/:id', function($id) {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            if ($data['password'] == $data['passwordMatch'])    {
                unset($data['passwordMatch']);
                $data['password'] = passwordHash::hash($data['password']);

                foreach ($data as $key => $value) {
                    $values[$key] = ':' . $key;
                    $params[':' . $key] = $value;
                }

                $slim->db->updateByPk('users', $id, array(
                    'values' => $values,
                    'params' => $params
                ));
            } else {
                throw new Exception('Password do not match');
            }
        }
    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Add user failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});

$slim->route->put('/users/request/passwordUser/:id', function($id) {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        if ($data_url = $slim->route->request->getBody())   {
            parse_str($data_url, $data);

            /* Check if old password matches */
            $user = $slim->db->readByPk('users', $id);

            if (!passwordHash::check_password($user['password'], $data['old_password']))
                throw new Exception("Old password did not match");

            /* Check if password match */
            if ($data['password'] == $data['passwordMatch'])    {
                unset($data['passwordMatch']);
                unset($data['old_password']);
                $data['password'] = passwordHash::hash($data['password']);

                foreach ($data as $key => $value) {
                    $values[$key] = ':' . $key;
                    $params[':' . $key] = $value;
                }

                $slim->db->updateByPk('users', $id, array(
                    'values' => $values,
                    'params' => $params
                ));
            } else {
                throw new Exception('Password do not match');
            }
        }
    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Add user failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});