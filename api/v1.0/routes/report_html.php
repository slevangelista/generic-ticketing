<?php

$slim->route->post("/html/inclusive_tickets/", function() use ($slim)	{

	$status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {

    	/* Get data body */
    	if ($data_json = $slim->route->request->getBody())	{	
    		$data = json_decode($data_json, true);
    	} else {
    		throw new Exception('Inclusive Ticket report POST failed');
    	}

    	/* If voyage exists */
    	if ( isset($data['voyage']) )	{
    		$voyage = $data['voyage'];
    	} else {
    		throw new Exception('Field `voyage` does not exist');
    	}

    	/* Get all tellers */
		$tellers = $slim->db->readAll('users', array(
			'where' => 'active = 1 AND role IN (1,3)'
		));

		/* Init */
		$url = 'localhost/archipelago/archipelago-web-api/v1.0/';
		$html = array();

		// Loop tellers
		foreach ($tellers as $key => $value)	{
			/* Initialize html */
			$html[$value['id']] = NULL;

			$data[$value['id']] = json_decode( getCURL($url, "report/inclusive_ticket/{$voyage}/{$value['id']}"), true );
		
			// Output value
			if (isset ($data[$value['id']]['data']['table']) )	{
				$table[$value['id']] = $data[$value['id']]['data']['table'];

				$html[$value['id']] .= "
					<table class='table table-striped table-responsive'>
		                <tr>
		                    <th colspan='2'>Kind of Receipt</th>
		                    <th colspan='3'><center>INCLUSIVE TICKETS AND WAYBILL NO</center></th>
		                    <th>Number Used</th>
		                    <th>Fare</th>
		                    <th>Total Amount</th>
		                </tr>
		        ";

		        foreach ($table[$value['id']] as $k => $v)	{
		        	$html[$value['id']] .= "
		        		<tr>
		                    <td>{$v['seating_class']}</td>
		                    <td>{$v['passenger_type']}</td>
		                    <td>{$v['ticket_start']}</td>
		                    <td>-0</td>
		                    <td>{$v['ticket_end']}</td>
		                    <td>{$v['no_of_items']}</td>
		                    <td>{$v['fare']}</td>
		                    <td>{$v['total_fare']}</td>
		                </tr>
		        	";
		        }

		        $html[$value['id']] .= "
		        	<tr>
	                    <th colspan='7'>Total Per Voyage: (Without Discount)</th>
	                	<th>{$data[$value['id']]['data']['total_fare']}</th>
	                </tr>
	                <tr>
	                    <th colspan='7'>LESS: Passenger Fare Discount</th>
	                    <th></th>
	                </tr>
		        ";

		        foreach ($table[$value['id']] as $k => $v)	{
		        	if ($v['differential'])	{
		        		$html[$value['id']] .= "
			        		<tr ng-show='value.differential'>
			        			 <td>{$v['seating_class']}</td>
			                    <td>{$v['passenger_type']}</td>
			                    <td>{$v['ticket_start']}</td>
			                    <td>-0</td>
			                    <td>{$v['ticket_end']}</td>
			                    <td>{$v['no_of_items']}</td>
			                    <td>{$v['differential']}</td>
			                    <td>{$v['total_differential']}</td>
			                </tr>
			        	";
		        	}
		        }

		        $html[$value['id']] .= "
		          	<tr>
	                    <th colspan='7'>Total Per Voyage: (Minus Discount)</th>
	                    <th>{$data[$value['id']]['data']['total_fare_w_diff']}</th>
	                </tr>
	                </table>
		        ";

		        // echo $html[$value['id']];
			}
		}

		// Get additional body params
		$voyage_array = $slim->db->readByPk('voyage', $voyage);
		$trip_array = $slim->db->readByPk('trip', $voyage_array['trip']);

		$params['voyage'] = $voyage;
		$params['voyage_number'] = $voyage_array['number'];
		$params['departure_date'] = $voyage_array['departure_date'];
		$params['trip'] = $voyage_array['trip'];
		$params['vessel'] = $voyage_array['vessel'];
		$params['route'] = $trip_array['route_id'];

		// Set up POST params
		$post_array = array(
			'body' => $html,
			'params' => $params
		);

		// POST Request
		$post_url = '128.199.232.112/api/v1/';
		$post_route = 'portTellersReports';
		$post = postCURL($post_url, $post_route, $post_array);

		$resp['data'] = $post;

    } catch (Exception $e)	{
    	$status = "400";
        $resp['status'] = 'error';
        $resp['message'] = $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post('/test', function() use($slim)	{

	echo json_encode($_POST);

});

$slim->route->post("/html/tellers_and_pursers", function() use ($slim)	{

	$status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {

    	/* Get data body */
    	if ($data_json = $slim->route->request->getBody())	{	
    		$data = json_decode($data_json, true);
    	} else {
    		throw new Exception('Tellers and Pursers report POST failed');
    	}

    	/* If voyage exists */
    	if ( isset($data['voyage']) )	{
    		$voyage = $data['voyage'];
    	} else {
    		throw new Exception('Field `voyage` does not exist');
    	}

		/* Get all pursers */
		$pursers = $slim->db->readAll('users', array(
			'where' => 'active = 1 AND role IN (1,4)'
		));

		/* Init */
		$url = 'localhost/archipelago/archipelago-web-api/v1.0/';
		$html = array();

		// Load shippers
		$shipperList = $slim->db->readAll('shipper', array(
			'where' => 'active = 1'
		));

		// Loop tellers
		foreach ($pursers as $key => $value)	{
			/* Initialize html */
			$html[$value['id']] = NULL;

			$data[$value['id']] = json_decode( getCURL($url, "report/tellers_and_pursers/{$voyage}/{$value['id']}"), true );

			// Output Value
			if ( $data[$value['id']]['data']['table'] )	{
				$table[$value['id']] = $data[$value['id']]['data']['table'];

				$html[$value['id']] .= "
					<table class='table table-responsive table-striped'>
			    		<tr>
			    			<th>Kind of Receipt</th>
		                    <th>Series No</th>
			    			<th colspan='3'>Inclusive Tickets & Waybill Number</th>
						    <th>Number Used</th>
			    			<th>Total Amount</th>
				    	</tr>
				";

				foreach($table[$value['id']] as $k=>$v)	{
					if ($v['waybill_id'])	{
						$shipper = joinList($v['waybill_id'], $shipperList);
					}
					else
						$shipper = array('name' => NULL);

					$html[$value['id']] .= "
						<tr>
				    		<td>{$v['lading_no']}</td>
		                    <td>{$v['series_no']}</td>
				    		<td>{$shipper['name']}</td>
						    <td>{$v['plate_num']}</td>
		                    <td>{$v['cargo_name']}</td>
				    		<td>1</td>
				    		<td>{$v['price_paid']}</td>
				    	</tr>
					";
				}

				$count = count($table[$value['id']]);
				$html[$value['id']] .= "
					<tr>
			    		<th colspan='5'>Total Accountability</th>
			    		<td>{$count}</td>
			    		<th>{$data[$value['id']]['data']['total_fare']}</th>
			    	</tr>
			    	<tr>
	                    <td colspan='8'></td>
	                </tr>
	                <tr>
	                    <th colspan='7'>LESS: Waybill Fare Discount</th>
	                </tr>
				";

				foreach($table[$value['id']] as $k=>$v)	{
					if ($v['differential'])	{
						if ($v['waybill_id'])	{
							$shipper = joinList($v['waybill_id'], $shipperList);
						}
						else
							$shipper = array('name' => NULL);

						$html[$value['id']] .= "
							<tr>
					    		<td>{$v['lading_no']}</td>
			                    <td>{$v['series_no']}</td>
					    		<td>{$shipper['name']}</td>
							    <td>{$v['plate_num']}</td>
			                    <td>{$v['cargo_name']}</td>
					    		<td>1</td>
					    		<td>{$v['differential']}</td>
					    	</tr>
						";
					}
				}		

				$html[$value['id']] .= "
					<tr>
	                    <th colspan='6'>Total Accountability (Less Differential)</th>
	                    <th>{$data[$value['id']]['data']['total_fare_less_differential']}</th>
	                </tr>
	                <tr>
	                    <td colspan='8'></td>
	                </tr>
	                <tr>
	                    <th colspan='7'>LESS: Collectible Waybills</th>
	                </tr>
				";

				$on_account_total[$value['id']] = 0;
				foreach($table[$value['id']] as $k=>$v)	{
					if ($v['on_account'])	{
						$on_account_total[$value['id']] += $v['price_paid'];
						$html[$value['id']] .= "
							<tr>
			                    <td colspan='6'>{$v['plate_num']}</td>
			                    <td>{$v['discounted_price']}</td>
			                </tr>
						";
					}
				}

				$total_less_accountables[$value['id']] = $data[$value['id']]['data']['total_fare_less_differential'] - $on_account_total[$value['id']];

				$html[$value['id']] .= "
					<tr>
	                    <th colspan='6'>Total Accountability (Less Collectibles)</th>
	                    <th>{$total_less_accountables[$value['id']]}</th>
	                </tr>
	                <tr>
	                    <td colspan='8'></td>
	                </tr>
	                <tr>
	                    <th colspan='6'>Net Cash to be Remitted</th>
	                    <th>{$total_less_accountables[$value['id']]}</th>
	                </tr>
	                </table>
				";

				// echo $html[$value['id']];

			}
		}

		// Get additional body params
		$voyage_array = $slim->db->readByPk('voyage', $voyage);
		$trip_array = $slim->db->readByPk('trip', $voyage_array['trip']);

		$params['voyage'] = $voyage;
		$params['voyage_number'] = $voyage_array['number'];
		$params['departure_date'] = $voyage_array['departure_date'];
		$params['trip'] = $voyage_array['trip'];
		$params['vessel'] = $voyage_array['vessel'];
		$params['route'] = $trip_array['route_id'];

		// Set up POST params
		$post_array = array(
			'body' => $html,
			'params' => $params
		);

		// POST Request
		$post_url = '128.199.232.112/api/v1/';
		$post_route = 'portPursersReports';
		$post = postCURL($post_url, $post_route, $post_array);

		$resp['data'] = $post;

	    } catch (Exception $e)	{
	    	$status = "400";
	        $resp['status'] = 'error';
	        $resp['message'] = $e->getMessage();
	    }

    JSONResponse($status, $resp);

});

$slim->route->post('/html/daily_revenue', function()	use ($slim)	{

	$status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {

    	/* Get data body */
    	if ($data_json = $slim->route->request->getBody())	{	
    		$data = json_decode($data_json, true);
    	} else {
    		throw new Exception('Daily Revenue report POST failed');
    	}

    	/* If voyage exists */
    	if ( isset($data['voyage']) )	{
    		$voyage = $data['voyage'];
    	} else {
    		throw new Exception('Field `voyage` does not exist');
    	}

    	/* Init */
		$url = 'localhost/archipelago/archipelago-web-api/v1.0/';

		/* Get cURL of daily revenue report */
		$data = json_decode( getCURL($url, "report/daily_revenue/{$voyage}"), true );
		$data = $data['data'];

		/* POPULATE POST BODY */
		
			// Title
			$post['data']['title_revenue'] = $data['voyage'][0]['number'] . "<br />" .  $data['voyage'][0]['departure_date'] . '@' .  $data['voyage'][0]['departure_time']
			. "<br />" . $data['voyage'][0]['route_name'];

			// PAX Revenue
			foreach ($data['revenue_ticket'] as $key => $value)	{
				foreach ($value as $k => $v)	{
					foreach ($v as $kk => $vv)	{
						if ($k != 'total')	{
							$post['data'][$key . "_" . $kk] = $vv;
						}	
					}
				}
			}

			// Upgrade
			$post['data']['upgrade'] = $data['revenue_upgrade']['total'];

			// Baggage
			$post['data']['baggage'] = $data['revenue_baggage']['total'];

			// Vehicle
			$post['data']['vehicle'] = $data['revenue_waybill']['total'];

			// Vehicle Discount
			$post['data']['vehicle_discount'] = $data['discount_waybill']['total'];

			// PAX Discount
			foreach ($data['discount_passenger'] as $key => $value)	{
				$post['data'][$key . "_discount"] = $value['total'];
			}

			// Receivables
			$receivables = array();
			foreach ($data['on_account_waybills'] as $key => $value)	{
				$receivables[$value['plate_num']] = $value['price_paid'];
			}
			$post['data']['receivables'] = json_encode($receivables);

			// Revenue
			$post['data']['total_revenue'] = $data['total_revenue_no_deductions'];
			$post['data']['total_revenue_less_discount'] = $data['total_revenue'];
			$post['data']['total_revenue_less_receivables'] = $data['total_rev_minus_waybills'];
			$post['data']['total_users_revenue'] = $data['total_users_revenue'];

			// Users
			$post['data']['users'] = json_encode($data['users']);

			/* Cash Beginning */
			$cash_on_hand = array();
			foreach ($data['cash_on_hand'] as $key => $value)	{
				$cash_on_hand[$value['name']] = $value['amt'];
			}
			$post['data']['cash_beginning'] = json_encode($cash_on_hand);
			$post['data']['cash_beginning_total'] = $data['cash_on_hand_total'];

			/* Disbursment */
			$disbursment = array();
			foreach ($data['deductions'] as $key => $value)	{
				$disbursment[$value['name']] = $value['amt'];
			}
			$post['data']['disbursment'] = json_encode($disbursment);
			$post['data']['disbursment_total'] = $data['deductions_total'];

			/* End Cash Balance */
			$post['data']['end_cash_balance'] = $data['deductions_total'];

		/* END POPULATE */

    	// Get additional body params
		$voyage_array = $slim->db->readByPk('voyage', $voyage);
		$trip_array = $slim->db->readByPk('trip', $voyage_array['trip']);

		$post['params']['voyage'] = $voyage;
		$post['params']['voyage_number'] = $voyage_array['number'];
		$post['params']['departure_date'] = $voyage_array['departure_date'];
		$post['params']['trip'] = $voyage_array['trip'];
		$post['params']['vessel'] = $voyage_array['vessel'];
		$post['params']['route'] = $trip_array['route_id'];

		// POST Request
		$post_url = '128.199.232.112/api/v1/';
		$post_route = 'portDailyRevenueReports';
		$post_response = postCURL($post_url, $post_route, $post);

		$resp['data'] = $post_response;

    } catch (Exception $e)	{
    	$status = "400";
        $resp['status'] = 'error';
        $resp['message'] = $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->post('/html/daily_monitoring', function() use ($slim)	{

	$status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {

    	/* Get data body */
    	if ($data_json = $slim->route->request->getBody())	{	
    		$data = json_decode($data_json, true);
    	} else {
    		throw new Exception('Daily Revenue report POST failed');
    	}

    	/* If voyage exists */
    	if ( isset($data['voyage']) )	{
    		$voyage = $data['voyage'];
    	} else {
    		throw new Exception('Field `voyage` does not exist');
    	}

    	/* Init */
		$url = 'localhost/archipelago/archipelago-web-api/v1.0/';
		$exceed_first_instance_loop = false;

		/* Get cURL of daily revenue report */
		$data = json_decode( getCURL($url, "report/traffic_report/{$voyage}"), true );
		$data = $data['data']; 

		/* POPULATE PARAMS */
		$voyage_array = $slim->db->readByPk('voyage', $voyage);
		$trip_array = $slim->db->readByPk('trip', $voyage_array['trip']);

		$post['params']['voyage'] = $voyage;
		$post['params']['voyage_number'] = $voyage_array['number'];
		$post['params']['departure_date'] = $voyage_array['departure_date'];
		$post['params']['trip'] = $voyage_array['trip'];
		$post['params']['vessel'] = $voyage_array['vessel'];
		$post['params']['route'] = $trip_array['route_id'];

		/* POPULATE BODY */

		// foreach date to get values (You should only get 1 loop instance theoretically)
		foreach($data['date'] as $date => $value)	{

			/* Throw exception if this loop is runned more than 1 instance */
			if ($exceed_first_instance_loop)
				throw new Exception("More than one voyage instance detected. This route cannot (and should not) process multiple voyages");

			$post['data']['trip_no'] = $post['params']['voyage_number'];
			$post['data']['trip_schedule'] = $trip_array['departure_time'];
			$post['data']['no_of_trips'] = 1;
			$post['data']['total_revenue'] = $data['revenue_total'];
			$post['data']['average_revenue_per_trip'] = $data['revenue_total'];
			$post['data']['pax_count'] = json_encode($value['pax_count'][$voyage]);
			$post['data']['pax_total'] = $value['pax_count_voyage'][$voyage]; 
			$post['data']['revenue_pax'] = $value['revenue_cat'][$voyage]['ticket'];

			/* To avoid division by 0 */
			if ($value['pax_count_voyage'][$voyage])
				$post['data']['average_pax_fare'] = $value['revenue_cat'][$voyage]['ticket'] / $value['pax_count_voyage'][$voyage];
			else
				$post['data']['average_pax_fare'] = 0;

			$post['data']['vehicle_count'] = json_encode($value['waybill_count'][$voyage]);
			$post['data']['vehicle_total'] = $value['waybill_count_voyage'][$voyage];
			$post['data']['revenue_vehicle'] = $value['revenue_cat'][$voyage]['waybill'];

			/* To avoid division by 0 */
			if ($value['waybill_count_voyage'][$voyage])
				$post['data']['average_vehicle_fare'] = $value['revenue_cat'][$voyage]['waybill'] / $value['waybill_count_voyage'][$voyage];
			else
				$post['data']['average_vehicle_fare'] = 0;

			$post['data']['revenue_baggage'] = $value['revenue_cat'][$voyage]['baggage'];
			$post['data']['revenue_upgrade'] = $value['revenue_cat'][$voyage]['upgrades'];
			$post['data']['capacity'] = json_encode($value['capacity']);
			$post['data']['capacity_total'] = $value['capacity_total_average'];
			$post['data']['actual_pax_count'] = json_encode($value['pax_count_code_total']);
			$post['data']['load_factor'] = json_encode($value['load_factor']);
			
			// Set exceed_first_instance_loop boolean
			$exceed_first_instance_loop = true;
		}

		// $post['data']['date'] = $data;

		// POST Request
		$post_url = '128.199.232.112/api/v1/';
		$post_route = 'portDailyMonitoringReports';
		
		// $post_url = 'localhost/archipelago-cloud/api/v1/';
		// $post_route = 'test';
		
		$post_response = postCURL($post_url, $post_route, $post);

		$resp['data'] = json_decode($post_response, true);

    } catch (Exception $e)	{
    	$status = "400";
        $resp['status'] = 'error';
        $resp['message'] = $e->getMessage();
    }

    JSONResponse($status, $resp);

});
