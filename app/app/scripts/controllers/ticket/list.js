'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:TicketListCtrl
 * @description
 * # TicketListCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('TicketListCtrl', function ($scope, RESTFactory, HelperService) {

    // Initialization
    // ---- ---- ---- ----
    $scope.ticketItems = {};

    HelperService.getListActive('passenger_type').then(function(response)    {
        $scope.passengerTypeList = response;
    });

    HelperService.getListActive('status').then(function(response)   {
        $scope.statusList = response;
    });

    // Pagination
    $scope.ticketCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    // Process Methods
    // ---- ---- ---- ----
    $scope.update = function()  {
        $scope.getTicketList();
        $scope.getTicketCount();
    };

    $scope.getTicketIndex = function(data)  {
        $scope.ticketIndex = data.id;
        $scope.ticketItems[data.id] = {
            'id': data.id,
            'ticket_no': data.ticket_no,
        };
    };

    // Joins
    $scope.joinPassengerType = function(id)   {
        return HelperService.join(id,$scope.passengerTypeList).name;
    };

    $scope.joinStatus = function(id)   {
        return HelperService.join(id,$scope.statusList).name;
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.getTicketList = function()  {
        RESTFactory.get('ticket/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.ticketList = response.data;
        });
    };

    $scope.getTicketCount = function()  {
        RESTFactory.get('ticket/request/count').then(function(response){
            $scope.ticketCount = response.data;
        });
    };

    // Print Ticket
    $scope.printTicket = function() {
        RESTFactory.get('renderTicket/' + $scope.ticketItems[$scope.ticketIndex].ticket_no).then(function(response) {
            // Response if success
        }, function(response)   {
            // Response if fail
        });
    };

    /* Refund */
    $scope.refundTicket = function()    {
        var params = $.param({
            'status': 6
        });
        RESTFactory.put('ticket/' + $scope.ticketIndex, params).then(function() {
            $scope.update();

            // Get Ticket Details (For Revenue Update)
            RESTFactory.get('ticket/' + $scope.ticketIndex).then(function(response) {
                // Update Ticket Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    seating_class: response.data.seating_class,
                    passenger_type: response.data.passenger_type,
                    price_paid: response.data.price_paid
                });
                RESTFactory.post('revenue/ticket/refund', revenueParams);
            });
        });
    };

    /* Cancelled */
    $scope.cancelTicket = function()    {
        var params = $.param({
            'status': 7
        });
        RESTFactory.put('ticket/' + $scope.ticketIndex, params).then(function() {
            $scope.update();

            // Get Ticket Details (For Revenue Update)
            RESTFactory.get('ticket/' + $scope.ticketIndex).then(function(response) {
                // Update Ticket Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    seating_class: response.data.seating_class,
                    passenger_type: response.data.passenger_type,
                    price_paid: response.data.price_paid
                });
                RESTFactory.post('revenue/ticket/cancel', revenueParams);
            });
        });
    };

});
