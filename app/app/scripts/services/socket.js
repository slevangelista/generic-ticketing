'use strict';

/**
 * @ngdoc service
 * @name archipelagoWebAppApp.socket
 * @description
 * # socket
 * Factory in the archipelagoWebAppApp.
 */
angular.module('archipelagoWebAppApp')
.factory('socket', function (socketFactory) {
   
    /* Check if io exists */ 
    if (typeof io !== 'undefined')  {
        var myIoSocket = io.connect('http://localhost:3000');
        var socket = socketFactory({
            ioSocket: myIoSocket
        });

        return socket;
    } else
        return null;

});
