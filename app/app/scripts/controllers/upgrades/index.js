'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:UpgradeNewUpgradeCtrl
 * @description
 * # UpgradeNewUpgradeCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('UpgradesIndexCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

    // Initializations
    // ---- ---- ---- ----
    $scope.upgrade = {};

    // Get List

    // Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Process Methods
    // ---- ---- ---- ----
    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.update = function()  {

    };

    // Voyage Display
    $scope.onVoyageSelect = function(trip_id)  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.upgrade.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.upgrade.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.upgrade.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.upgrade.voyage = null;
    };
    // End voyage select

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    // Get List
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            $scope.voyageList = response.data;
        });
        $scope.voyageList = [];
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.addUpgrade = function()  {
        $scope.newUpgrade.voyage = $scope.upgrade.voyage;
        $scope.newUpgrade.created_by = $rootScope.id;

        var params = $.param($scope.newUpgrade)
        RESTFactory.post('upgrades', params).then(function(response) {
            toaster.pop('success', "Success", "Upgrade successfully added");

            $scope.newUpgrade = {};

            // Get Upgrade Details (For Revenue Update)
            RESTFactory.get('upgrades/request/last_record').then(function(response) {
                // Update Upgrade Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.amt
                });
                RESTFactory.post('revenue/upgrades/create', revenueParams);
            });
        }, function()   {
            toaster.pop('error', "Error", "Add Upgrade failed");
        });
    };

});
