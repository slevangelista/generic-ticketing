'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:routeIndexCtrl
 * @description
 * # routeIndexCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
    .controller('RouteIndexCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialize
    // --- ---- ---- ----
    $scope.routeItems = {};

    // Pagination
    $scope.routeCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  

    $scope.bool = ['No', 'Yes'];

    // Get Lists
    HelperService.getListActive('port').then(function(response) { 
        $scope.portList = response; 
    });

    // Process Methods
    // --- ---- ---- ----
    $scope.update = function()  {
        $scope.getRouteByPage();
        $scope.getRouteByCount();
    };
    
    $scope.getItems = function(data)    {
        $scope.routeItems[data.id] = {
            'id': data.id,
            'name': data.name,
            'source_port': data.source_port,
            'dest_port': data.dest_port,
            'active': data.active
        };
    };

    $scope.getIndex = function(id)  {
        $scope.currentIndex = id;
    };

    // Used for Join
    $scope.joinPort = function(id)   {
        return HelperService.join(id, $scope.portList).name;
    };

    // REST Methods
    // --- ---- ---- ----
    $scope.getRouteByPage = function()    {
        RESTFactory.get('route/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response){
            $scope.routeList = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.getRouteByCount = function()   {
        RESTFactory.get('route/' + 'request/count').then(function(response) {
            $scope.routeCount = response.data;
        }, function()   {
            // Response if Failed
        });
    };

    $scope.addRoute = function() {
        var params = $.param($scope.newRoute);
        RESTFactory.post('route', params).then(function(response)  {
            // Response if success
            $scope.newRoute = {};
            $scope.update();
            toaster.pop("success", "Success", "Route successfully added");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Route failed to add");
        });
    };  

    $scope.editRoute = function()   {
        var params = $.param($scope.routeItems[$scope.currentIndex]);
        RESTFactory.put('route/' + $scope.currentIndex, params).then(function(response)  {
            // Response if success
            $scope.update();
            toaster.pop("success", "Success", "Route successfully edited");
        }, function()  {
            // Response if fail
            toaster.pop("error", "Error", "Route failed to edit");
        });
    };

});
