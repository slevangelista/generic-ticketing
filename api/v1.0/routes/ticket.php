<?php

// Request ticket based on trip
$slim->route->get('/ticket/request/trip/:trip_id', function($trip)  {

    global $slim;
    $status = 200;
    $resp = array('status'=>'success', 'message'=>'Query Success', 'data'=>array());

    try {

        // Get Voyage
        $voyage = $slim->db->readAll('voyage', array(
            'where' => 'trip = :trip',
            'params' => array(
                ':trip' => $trip
            )
        ));
        
        // Store voyage id here
        $voyage_array = array();
        foreach ($voyage as $value)  {
            $voyage_array[$value['id']] = $value['id'];
        }

        // Implode
        $voyage_list = implode(',', $voyage_array);
        
        // Get Ticket
        $blacklist = "6,7";
        $resp['data'] = $slim->db->readAll('ticket', array(
            'where' => "voyage IN ({$voyage_list}) AND status NOT IN ({$blacklist})",
        ));

    } catch (Exception $e)  {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Ticket request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

$slim->route->get("/ticket/request/infant/:voyage", function($voyage)  {

    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = count($slim->db->readAll('ticket', array(
        'where' => 'is_seated = 0 AND voyage = :voyage',
        'params' => array(
            ':voyage' => $voyage
        )
    )));

    JSONResponse($status, $resp);
});

// All Tickets for given day
$slim->route->get("/ticket/request/date/:date", function($date) {

    global $slim;
    $status = "200";
    $resp = array('status'=>'success', 'message'=>'Query Success', 'data'=>array());
   
    try {
        
        // Set Blacklist
        $blacklist = "6,7";

        $resp['data'] = $slim->db->readAll('ticket', array(
            'where' => "create_time LIKE :date AND status NOT IN ({$blacklist})",
            'params' => array(
                ':date' => $date . "%"
            )   
        ));

    } catch (Exception $e)  {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Ticket revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);

});

// Ticket Pagination w/ Join
$slim->route->get("/ticket/:page/:items", function($page, $items)   {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Check for custom request
    if ($page == 'request') {
        switch ($items) {
            case 'last_record':
                $resp['data'] = $slim->db->read('ticket', array(
                    'order' => 'id DESC',
                    'limit' => 1
                ));
                break;
            case 'count':  
                $resp['data'] = count($slim->db->readAll('ticket', array(
                    'where' => 'status NOT IN (5,6,7)'
                )));
                break;
            default: 
                $status = "404";
                $resp['status'] = 'error';
                $resp['message'] = 'Not Found';
                break;
        }
    } else {
        // Pagination Logic
        $index = 0;
        for($i = 1; $i < $page; $i++) {
            $index = $index + $items;
        }

        $sql = "
            SELECT
                ticket.id as id,
                CONCAT(passenger.first_name, ' ', passenger.last_name) as passenger,
                ticket.series_no,
                ticket.passenger_type,
                ticket.price_paid,
                ticket.status,
                ticket.ticket_no
            FROM ticket
            LEFT JOIN passenger ON ticket.passenger = passenger.id
	    WHERE ticket.status NOT IN (6,7)
 	    ORDER BY id ASC
            LIMIT {$index}, {$items}
        ";

        $resp['data'] = $slim->db->SQL($sql, array());
    }

    JSONResponse($status, $resp);
});

// GET
// Get ticket count
$slim->route->get("/ticket/request/count/:voyage", function($voyage)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $resp['data'] = count($slim->db->readAll('ticket', array(
        'where' => "voyage = :voyage AND status NOT IN (5,6,7)",
        'params' => array(
            ':voyage' => $voyage
        )
    )));

    JSONResponse($status, $resp);
});

// Get total revenue for voyage
$slim->route->get("/ticket/request/revenue/:voyage", function($voyage)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {     
        $ticket = $slim->db->readAll('ticket', array(
            'where' => "voyage = :voyage AND status NOT IN (6,7)",
            'params' => array(
                ':voyage' => $voyage
            )
        ));

        /* Get total revenue */
        $revenue = 0;
        foreach ($ticket as $value) {
            $revenue += (float) $value['price_paid'];
        }

        $resp['data'] = $revenue;

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Ticket revenue request failed. Error: " . $e->getMessage();
    }

    JSONResponse($status, $resp);
});

$slim->route->get("/ticket/request/revenue/:voyage/:seating_class", function($voyage, $seating_class)   {

        global $slim;
        $status = "200";
        $resp = array('status' => 'success', 'message' => 'Query Success', 'data'=> array());

        try     {

                $ticket = $slim->db->readAll('ticket', array(
                        'where' => "voyage = :voyage AND status NOT IN (6,7) AND seating_class = :seating_class",
                        'params' => array(
                                ':voyage' => $voyage,
                                ':seating_class' => $seating_class
                        )
                ));
        
                /* Get total revenue */
                $revenue = 0;
                foreach ($ticket as $value) {
                    $revenue += (float) $value['price_paid'];
                }

                $resp['data'] = $revenue;

        } catch (Exception $e)  {
                $status = "400";
                $resp['status'] = "error";
                $resp['message'] = "Ticket revenue request failed. Error: " . $e->getMessage();
        }

        JSONResponse($status, $resp);
});

// Get ticket by voyage
$slim->route->get("/ticket/request/voyage/:id", function($id)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    $blacklist = "5,6,7";
    $resp['data'] = $slim->db->readAll('ticket', array(
        'where' => "status NOT IN ({$blacklist}) AND voyage = :voyage",
        'params' => array(
            ':voyage' => $id
        )
    ));

    JSONResponse($status, $resp);
});

// Get ticket by status
$slim->route->get('/ticket/request/voyage/:voyage/status/:status', function($voyage_id, $status_key)   {

    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        $ticket = $slim->db->readAll('ticket', array(
            'where' => "status IN ({$status_key}) AND voyage = :voyage",
            'params' => array(
                ':voyage' => $voyage_id
            )
        ));

        if ($ticket)    {
            $resp['data'] = $ticket;
        } else
            throw new Exception("No ticket found");

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Ticket request failed. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);

});

// Get ticket by ticket no
$slim->route->get("/ticket/request/ticket_no/:ticket_no", function($ticket_no)  {
    global $slim;
    $status = 200;

    $sql = "
        SELECT 
            ticket_no,
            booking_no,
            series_no,
            ticket.id as id,
            CONCAT(passenger.first_name, ' ', passenger.last_name) as passenger,
            passenger.id as passenger_id,
            voyage.number as voyage,
            voyage.id as voyage_id,
            seat.name as seat,
            seat.id as seat_id,
            seating_class.name as seating_class,
            seating_class.id as seating_class_id,
            passenger_type.name as passenger_type,
            passenger_type.id as passenger_type_id,
            price_paid,
            status.name as status,
            status.id as status_id,
            create_time
        FROM ticket
        LEFT JOIN passenger ON ticket.passenger = passenger.id
        LEFT JOIN voyage ON ticket.voyage = voyage.id
        LEFT JOIN seat ON ticket.seat = seat.id
        LEFT JOIN seating_class ON ticket.seating_class = seating_class.id
        LEFT JOIN passenger_type ON ticket.passenger_type = passenger_type.id
        LEFT JOIN status ON ticket.status = status.id
        WHERE ticket_no = :ticket_no
        ORDER BY id DESC
    ";

    $ticket = $slim->db->SQL($sql, array(
        ':ticket_no' => $ticket_no
    ));

    if ($ticket)    {
        $resp['data'] = $ticket[0];
    } else {
        $status = 400;
        $resp['message'] = "Ticket {$ticket_no} not found";
    }

    JSONResponse($status, $resp);
});

// Get ticket pagination
$slim->route->get("/ticket/:page/:items/:voyage", function($page, $items, $voyage)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    // Pagination Logic
    $index = 0;
    for($i = 1; $i < $page; $i++) {
        $index = $index + $items;
    }

    $blacklist = "3,4,5,6,7";
    $resp['data'] = $slim->db->readAll('ticket', array(
        'where' => "voyage = :voyage AND status NOT IN ({$blacklist})",
        'order' => "id DESC",
        'limit' => "{$index}, {$items}",
        'params' => array(
            ':voyage' => $voyage
        )
    ));

    JSONResponse($status, $resp);
});

// POST
$slim->route->post("/ticket", function()    {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    if ($data_url = $slim->route->request->getBody())   {
        parse_str($data_url, $data);

        /* Check if data[0] is set, if it is not use JSON Decode instead */
        if (!isset($data[0])) 
            $data = json_decode($data_url, true);

        try {

            if (! $data)
                throw new Exception("Data is null");

            /* Loop data */
            foreach ($data as $value)   {

                // Step 1: Get voyage
                $voyage = $slim->db->readByPk('voyage', $value['voyage']);

                // Step 2: Get trip from voyage value
                $trip = $slim->db->readByPk('trip', $voyage['trip']);

                // Step 3: Get price range
                $passenger_fare = $slim->db->read('passenger_fare', array(
                    'where' => 'route = :route AND class = :class AND type = :type',
                    'params' => array(
                        ':route' => $trip['route_id'],
                        ':class' => $value['seating_class'],
                        ':type' => $value['passenger_type']
                    )
                ));

                // Step 4: If passenger fare exists for inputted route, class and passenger type:
                if ($passenger_fare)    {
                    
                    // Step 4.1: Create passenger
                    $slim->db->create('passenger', array(
                        'values' => array(
                            'first_name' => ':first_name',
                            'last_name' => ':last_name',
                        ),
                        'params' => array(
                            ':first_name' => $value['first_name'],
                            ':last_name' => $value['last_name'],
                        )
                    ));

                    // Step 4.2: Get Id inserter from passenger
                    $passenger_id = $slim->db->lastInsertId();

                    // Step 4.3: Do manual input of ticket params:
                    $ticket_no = getNumberPadding('ticket'); // Ticket No
                    $booking_no = getNumberPaddingPerDigit('booking', $value['booking_no']); // Booking No

                    // Step 4.4: Create Ticket
                    $slim->db->create('ticket', array(
                        'values' => array(
                            'passenger' => ':passenger',
                            'passenger_type' => ':passenger_type',
                            'voyage' => ':voyage',
                            'seat' => ':seat',
                            'ticket_type' => ':ticket_type',
                            'seating_class' => ':seating_class',
                            'ticket_no' => ':ticket_no',
                            'booking_no' => ':booking_no',
                            'series_no' => ':series_no',
                            'price_paid' => ':price_paid',
                            'created_by' => ':created_by',
                            'status' => ':status',
                            'is_seated' => ':is_seated',
                        ),
                        'params' => array(
                            ':passenger' => $passenger_id, 
                            ':passenger_type' => $value['passenger_type'],
                            ':voyage' => $value['voyage'],
                            ':seat' => $value['seat'],
                            ':ticket_type' => $value['ticket_type'],
                            ':seating_class' => $value['seating_class'],
                            ':ticket_no' => $ticket_no,
                            ':booking_no' => $booking_no,
                            ':status' => $value['status'],
                            ':series_no' => $value['series_no'],   
                            ':price_paid' => $passenger_fare['price'],
                            ':created_by' => $value['created_by'],
                            ':is_seated' => $value['is_seated']
                        )
                    ));

                    // Step 4.5: Decrement Voyage Capacity
                    $voyage_seat = $voyage['available_seats'] - 1;
                    $slim->db->updateByPk('voyage', $voyage['id'], array(
                        'values' => array(
                            'available_seats' => ':available_seats',
                        ),
                        'params' => array(
                            ':available_seats' => $voyage_seat,
                        ),
                    ));

                    // Step 4.6: Update series
                    $slim->db->updateByPk('users', $value['created_by'], array(
                        'values' => array(
                            'current_series' => ':current_series',
                        ),
                        'params' => array(
                            ':current_series' => $value['series_no']+1
                        )
                    ));

                    // Step 4.7: Update Revenue
                    if ($value['status'] == 2)  {
                        Revenue::updateTicket(array(
                            'voyage' => $value['voyage'],
                            'seating_class' => $value['seating_class'],
                            'passenger_type' => $value['passenger_type'],
                            'revenue' => $passenger_fare['price']
                        ), 'create');
                    } else if ($value['status'] == 7)   {
                        Revenue::updateTicket(array(
                            'voyage' => $value['voyage'],
                            'seating_class' => $value['seating_class'],
                            'passenger_type' => $value['passenger_type'],
                            'revenue' => 0
                        ), 'cancel');
                    }
            
                    // Step 4.7: Print Ticket (If enabled)
                    if ($value['print'])
                        Render::renderTicket($ticket_no);

                } else {
                    throw new Exception("No ticket fare found for selected passenger type and seating class");
                }
            }

        } catch (Exception $e)  {
            /* Throw bad request response */
            $status = "400";
            $resp['status'] = 'error';
            $resp['message'] = $e->getMessage();
        }

    } else {
        /* Throw bad request response */
        $status = "400";
        $resp['status'] = 'error';
        $resp['message'] = 'Invalid Request';
    }

    // Step 5: Set response
    $resp['data'] = "Tickets successfully created";

    // Step 6: Print response
    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);
    
});

$slim->route->post("/ticket/request/voyageTransfer", function() {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    if ($data_url = $slim->route->request->getBody())   {
        parse_str($data_url, $data);

        try {
            
            // Step 1: Get voyage
            $voyage = $slim->db->readByPk('voyage', $data['voyage']);

            // Step 2: Get trip from voyage value
            $trip = $slim->db->readByPk('trip', $voyage['trip']);

            // Step 3: Get price range
            $passenger_fare = $slim->db->read('passenger_fare', array(
                'where' => 'route = :route AND class = :class AND type = :type',
                'params' => array(
                    ':route' => $trip['route_id'],
                    ':class' => $data['seating_class'],
                    ':type' => $data['passenger_type']
                )
            ));

            // Step 4: If passenger fare exists for inputted route, class and passenger type:
            if ($passenger_fare)    {
                
                // Step 4.1: Do manual input of ticket params:
                $ticket_no = getNumberPadding('ticket'); // Ticket No
                $booking_no = getNumberPadding('booking'); // Booking No

                // Step 4.2: Create Ticket
                $slim->db->create('ticket', array(
                    'values' => array(
                        'passenger' => ':passenger',
                        'passenger_type' => ':passenger_type',
                        'voyage' => ':voyage',
                        'seat' => ':seat',
                        'ticket_type' => ':ticket_type',
                        'seating_class' => ':seating_class',
                        'ticket_no' => ':ticket_no',
                        'booking_no' => ':booking_no',
                        'series_no' => ':series_no',
                        'price_paid' => ':price_paid',
                        'created_by' => ':created_by',
                    ),
                    'params' => array(
                        ':passenger' => $data['passenger'], 
                        ':passenger_type' => $data['passenger_type'],
                        ':voyage' => $data['voyage'],
                        ':seat' => $data['seat'],
                        ':ticket_type' => $data['ticket_type'],
                        ':seating_class' => $data['seating_class'],
                        ':ticket_no' => $ticket_no,
                        ':booking_no' => $booking_no,
                        ':series_no' => $data['series_no'],   
                        ':price_paid' => $passenger_fare['price'],
                        ':created_by' => $data['created_by'],
                    )
                ));

                // Step 4.3: Decrement Voyage Capacity
                $voyage_seat = $voyage['available_seats'] - 1;
                $slim->db->updateByPk('voyage', $voyage['id'], array(
                    'values' => array(
                        'available_seats' => ':available_seats',
                    ),
                    'params' => array(
                        ':available_seats' => $voyage_seat,
                    ),
                ));

                // Step 4.4: Update series
                $slim->db->updateByPk('users', $data['created_by'], array(
                    'values' => array(
                        'current_series' => ':current_series',
                    ),
                    'params' => array(
                        ':current_series' => $data['series_no']+1
                    )
                ));

                // Step 4.5: Response ticket no (for printing)
                $resp['data'] = $ticket_no;

            } else {
                throw new Exception("No ticket fare found for selected passenger type and seating class");
            }

        } catch (Exception $e) {
            $status = "400";
            $resp['status'] = 'error';
            $resp['message'] = $e->getMessage();
        }

    } else {
        /* Throw bad request response */
        $status = "400";
        $resp['status'] = 'error';
        $resp['message'] = 'Invalid Request';
    }

    JSONResponse($status, $resp);

});

// PUT
$slim->route->put("/ticket/voyage/:voyage_id", function($voyage_id) {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {
        
        // Step 1: Get all tickets with status < 2 for this voyage
        $tickets = $slim->db->readAll('ticket', array(
            'where' => "status <= 2 AND voyage = :voyage",
            'params' => array(
                ':voyage' => $voyage_id
            )
        ));

        // Step 2: Update those tickets to "No Show"
        $slim->db->update('ticket', array(
            'where' => "status <= 2 AND voyage = :voyage",
            'values' => array(
                'status' => 5
            ),
            'params' => array(
                ':voyage' => $voyage_id
            )
        ));

        // Step 3: Update seat temp for the changed tickets
        foreach ($tickets as $value)    {
            $slim->db->update('seat_temp', array(
                'where' => 'seat = :seat AND voyage = :voyage AND active = 1 AND status = 15',
                'values' => array(
                    'status' => 14,
                ),
                'params' => array(
                    ':seat' => $value['seat'],
                    ':voyage' => $value['voyage']
                )
            ));

            /* Update Revenue */
            Revenue::updateTicket(array(
                'voyage' => $value['voyage'],
                'seating_class' => $value['seating_class'],
                'passenger_type' => $value['passenger_type']
            ), 'no_show');
        }

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Ticket update fail. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);

});

$slim->route->put("/ticket/boardAll/voyage/:voyage_id", function($voyage_id)    {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    try {

        $blacklist = "5,6,7";

        // Count how many ticket to board
        $ticket = $slim->db->readAll('ticket', array(
            'where'=> "voyage = :voyage AND status NOT IN ({$blacklist})",
            'values' => array(
                'status' => 4
            ),
            'params' => array(
                ':voyage' => $voyage_id
            )
        ));

        /* Set tickets as boarded */
        $slim->db->update('ticket', array(
            'where'=> "voyage = :voyage AND status NOT IN ({$blacklist})",
            'values' => array(
                'status' => 4
            ),
            'params' => array(
                ':voyage' => $voyage_id
            )
        ));

        // Add boarded tickets to revenue_ticket
        foreach ($ticket as $key => $value) {
            /* Update Revenue */
            Revenue::updateTicket(array(
                'voyage' => $value['voyage'],
                'seating_class' => $value['seating_class'],
                'passenger_type' => $value['passenger_type']
            ), 'board');
        }

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Ticket update fail. Error: " . $e->getMessage(); 
    }

    JSONResponse($status, $resp);
});
