'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:ReportsTellersAndPursersCtrl
 * @description
 * # ReportsTellersAndPursersCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('ReportsTellersAndPursersCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {
	
	// Initializations
	// ---- ---- ----
	$scope.tellers = {};
    $scope.onAmountTotal = 0;

    // Trips
    HelperService.getListActive('trip').then(function(response)    {
        $scope.tripList = response;
    });

    // Shipper
    HelperService.getListActive('shipper').then(function(response)    {
        $scope.shipperList = response;
    });

	// Process Methods
	// ---- ---- ---- ----
	$scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

	$scope.update = function()	{
		$scope.getWaybills();
	};

	// Voyage Display
    $scope.onVoyageSelect = function()  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.tellers.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.tellers.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.tellers.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.tellers.voyage = null;
    };
    // End voyage select

    // Get Lists
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            $scope.voyageList = response.data;
        });
        $scope.voyageList = [];
    };

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    $scope.joinShipper = function(id)   {
        if (id == 0)
            return "Baggage";
        else if (id == null)
            return "";
        else
            return HelperService.join(id, $scope.shipperList).name;
    };

    $scope.getOnAccountWaybills = function()    {
        $scope.onAccountWaybills = [];
        for (var index in $scope.waybills.table)  {
            if ($scope.waybills.table[index].on_account == 1) {
                $scope.onAccountWaybills.push($scope.waybills.table[index]);
            }
        }
    };

    $scope.getOnAccountTotal = function(amt)   {
        $scope.onAmountTotal = ($scope.onAmountTotal*1) + (amt*1);
    };

    $scope.netCashAmount = function()   {
        if (typeof $scope.waybills !== 'undefined')  {
            return ($scope.waybills.total_fare - $scope.onAmountTotal).toFixed(2);
        } else
            return null;
    };

	// REST Methods
	// ---- ---- ---- ----
	$scope.getWaybills = function()	{
		RESTFactory.get('report/tellers_and_pursers/' + $scope.tellers.voyage + '/' + $rootScope.id).then(function(response)    {
    		$scope.waybills = response.data;

            /* Get on account waybills */
            $scope.getOnAccountWaybills();
		});
	};

    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });

        saveAs(blob, "tellers_and_pursers.xls");
    };

    $scope.sendToCloud = function() {
        RESTFactory.post('html/tellers_and_pursers', {'voyage': $scope.tellers.voyage}).then(function(response)    {
            toaster.pop("success", "Success", "Data successfully sent to cloud");
        }, function(response)   {
            toaster.pop("error", "Error", "Data transfer to cloud failed");
        });
    };

});
