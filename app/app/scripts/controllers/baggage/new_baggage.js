'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:BaggageNewBaggageCtrl
 * @description
 * # BaggageNewBaggageCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('BaggageNewBaggageCtrl', function ($scope, $rootScope, RESTFactory, HelperService, toaster) {

    // Initializations
    // ---- ---- ---- ----
    $scope.baggage = {};

    // Get List

    // Trip
    HelperService.getListActive('trip').then(function(response) {
        $scope.tripList = response;
    });

    // Process Methods
    // ---- ---- ---- ----
    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.update = function()  {

    };

    // Voyage Display
    $scope.onVoyageSelect = function(trip_id)  { // On voyage select do the ff
        $scope.update();
    };

    $scope.getActiveVoyage = function(id)   {
        $scope.baggage.voyage = id;
    };

    $scope.deactivateVoyage = function()    {
        $scope.baggage.voyage = null;
    };

    $scope.isActiveVoyage = function(id)    {
        if ($scope.baggage.voyage == id)    {
            return true;
        } else {
            return false;
        }
    };

    $scope.getToday = function()    {
        $scope.selectedDate = new Date();
    };

    $scope.changeDate = function()  {
        $scope.selectedDate = HelperService.getDate($scope.selectedDate);
        $scope.getVoyageList();
        $scope.baggage.voyage = null;
    };
    // End voyage select

    // Joins
    $scope.joinTrip = function(id)  {
        return HelperService.join(id, $scope.tripList).departure_time;
    };

    // Get List
    $scope.getVoyageList = function()    {
        RESTFactory.get('voyage/request/date/' + $scope.selectedDate).then(function(response)   {
            $scope.voyageList = response.data;
        });
        $scope.voyageList = [];
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.addBaggage = function()  {
        $scope.newBaggage.voyage = $scope.baggage.voyage;
        $scope.newBaggage.created_by = $rootScope.id;

        var params = $.param($scope.newBaggage)
        RESTFactory.post('baggage', params).then(function(response) {
            toaster.pop('success', "Success", "Baggage successfully added");

            $scope.newBaggage = {};

            // Get Baggage Details (For Revenue Update)
            RESTFactory.get('baggage/request/last_record').then(function(response) {
                // Update Baggage Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.price_paid
                });
                RESTFactory.post('revenue/baggage/create', revenueParams);
            });
        }, function()   {
            toaster.pop('error', "Error", "Add Baggage failed");
        });
    };

});
