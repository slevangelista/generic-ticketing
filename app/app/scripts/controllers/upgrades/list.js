'use strict';

/**
 * @ngdoc function
 * @name archipelagoWebAppApp.controller:UpgradeListCtrl
 * @description
 * # UpgradeListCtrl
 * Controller of the archipelagoWebAppApp
 */
angular.module('archipelagoWebAppApp')
.controller('UpgradesListCtrl', function ($scope, RESTFactory, HelperService, toaster) {

    // Initialization
    // ---- ---- ---- ----

    // Pagination
    $scope.upgradeCount = 0;
    $scope.maxSize = 10;
    $scope.currentPage = 1;  


    // Process Methods
    // ---- ---- ---- ----
    $scope.update = function()  {
        $scope.getUpgradeList();
        $scope.getUpgradeCount();
    };

    $scope.getUpgradeIndex = function(id) {
        $scope.currentIndex = id;
    };

    // REST Methods
    // ---- ---- ---- ----
    $scope.getUpgradeList = function()  {
        RESTFactory.get('upgrades/' + $scope.currentPage + '/' + $scope.maxSize).then(function(response) {
            $scope.upgradeList = response.data;
        });
    };

    $scope.getUpgradeCount = function()  {
        RESTFactory.get('upgrades/request/count').then(function(response){
            $scope.upgradeCount = response.data;
        });
    };

    // REST Methods
    // ---- ---- ---- ----

    /* Refund */
    $scope.refundUpgrade = function()    {
        var params = $.param({
            'status': 6
        });
        RESTFactory.put('upgrades/' + $scope.currentIndex, params).then(function() {
            $scope.update();

            // Get Upgrade Details (For Revenue Update)
            RESTFactory.get('upgrades/' + $scope.currentIndex).then(function(response) {
                // Update Upgrade Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.amt
                });
                RESTFactory.post('revenue/upgrades/refund', revenueParams);
            });
        });
    };

    /* Cancelled */
    $scope.cancelUpgrade = function()    {
        var params = $.param({
            'status': 7
        });
        RESTFactory.put('upgrades/' + $scope.currentIndex, params).then(function() {
            $scope.update();

            // Get Upgrade Details (For Revenue Update)
            RESTFactory.get('upgrades/' + $scope.currentIndex).then(function(response) {
                // Update Upgrade Revenue
                var revenueParams = $.param({
                    voyage: response.data.voyage,
                    price_paid: response.data.amt
                });
                RESTFactory.post('revenue/upgrades/cancel', revenueParams);
            });
        });
    };

});
