<?php

$slim->route->get('/seat/request/voyage/:voyage', function($voyage_id)  {
    global $slim;
    $status = "200";
    $resp = array('status'=>'success','message'=>'Query Success','data'=>array());

    /* Get seats available for the said voyage */
    try {
        // Step 1: Get vessel from voyage
        $voyage = $slim->db->readByPk('voyage', $voyage_id);
        $vessel_id = $voyage['vessel'];

        // Step 2: Get vessel template from voyage
        $vessel_template = $slim->db->readAll('vessel_template', array(
            'where' => 'vessel_id = :vessel',
            'params' => array(
                ':vessel' => $vessel_id
            )
        ));

        // Step 3: Get seating_class from vessel_template
        $comma = null;
        $seating_class = null;
        foreach ($vessel_template as $value)    {
            $seating_class .= "{$comma}{$value['seating_class_id']}";
            $comma = ",";
        }

        // Step 4: Get seats from seating_class
        $seat = $slim->db->readAll('seat', array(
            'where' => "seating_class IN ({$seating_class})",
        ));
        
        /* Pass seat query data to response */
        $resp['data'] = $seat;

    } catch (Exception $e) {
        $status = "400";
        $resp['status'] = "error";
        $resp['message'] = "Seat request failed. Error: " . $e->getMessage();
    }

    // echo "<pre>" . print_r($resp['data'], 1) . "</pre>";
    JSONResponse($status, $resp);

});